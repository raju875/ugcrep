<header>

    <!-- Sidebar -->
    <div class="sidebar-custom position-custom mt-5 pt-lg-5">

        <a class="logo-wrapper waves-effect">
            <img src="{{ asset('admin/asset/') }}/img/logo.png" class="img-fluid" alt="">
        </a>

        <div class="list-group list-group-flush">
            <a href="{{ url('/database/dashboard') }}" class="list-group-item active waves-effect">
                Dashboard
            </a>
            <?php $role = Session::get('adminRole')?>
            @if($role == 'super-admin')
                {{--<button  type="button">--}}
                    {{--<a style="color: white" class="top-nav-link text-right" href="{{ url('/database/control/user-control') }}"--}}
                       {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('control-form').submit();">--}}
                        {{--User Control--}}
                    {{--</a>--}}

                    {{--<form id="control-form" action="{{ url('/database/control/user-control-form') }}" method="POST" style="display: none;">--}}
                        {{--{{ csrf_field() }}--}}
                    {{--</form>--}}
                {{--</button>--}}
                <a href="{{ url('/database/control/user-control-table') }}" class="list-group-item list-group-item-action waves-effect">
                    User Control
                </a>

                <a href="{{ url('/database/activity-log/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Activity Log
                </a>

                <a href="{{ url('/database/university/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Universities
                </a>

                <a href="{{ url('/database/district/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Districts
                </a>

                <a href="{{ url('/database/centre/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Centre
                </a>
                <a href="{{ url('/database/centre-info/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Centre-Info
                </a>

                <a href="{{ url('/database/department/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Departments
                </a>

                <a href="{{ url('/database/faculty/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Faculty
                </a>

                <a href="{{ url('/database/faculty-info/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Faculty Info
                </a>

                <a href="{{ url('/database/financial-year/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Financial Year
                </a>

                <a href="{{ url('/database/institute/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Institutes
                </a>

                <a href="{{ url('/database/institute-info/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Institute Info
                </a>

                <a href="{{ url('/database/manpower-info/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Manpower Info
                </a>

                <a href="{{ url('/database/office/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Offices
                </a>

                <a href="{{ url('/database/original-post/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Original Post
                </a>

                <a href="{{ url('/database/program/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Programs
                </a>

                <a href="{{ url('/database/session/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Session
                </a>

                <a href="{{ url('/database/vehicle/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Vehicles
                </a>

                <a href="{{ url('/database/vehicle-info/listing') }}" class="list-group-item list-group-item-action waves-effect">
                    Vehicle Info
                </a>

            @else
                <?php $userId = Session::get('adminId')?>
                <?php $permissionModules = \App\KosPermissionModule::where('role_id',$userId)->get()?>

            @foreach($permissionModules as $module )
            @if($module->table_name == 'kos_universities' )
             <a href="{{ url('/database/university/listing') }}" class="list-group-item list-group-item-action waves-effect">
                   Universities
             </a>

            @elseif($module->table_name == 'kos_centres' )
            <a href="{{ url('/database/centre/listing') }}" class="list-group-item list-group-item-action waves-effect">
                Centre
            </a>

             @elseif($module->table_name == 'kos_centre_infos' )
            <a href="{{ url('/database/centre-info/listing') }}" class="list-group-item list-group-item-action waves-effect">
                Centre-Info
            </a>

             @elseif($module->table_name == 'kos_departments' )
            <a href="{{ url('/database/department/listing') }}" class="list-group-item list-group-item-action waves-effect">
                Departments
            </a>
            @elseif($module->table_name == 'kos_faculties' )
            <a href="{{ url('/database/faculty/listing') }}" class="list-group-item list-group-item-action waves-effect">
                Faculty
            </a>

            @elseif($module->table_name == 'kos_faculty_infos' )
            <a href="{{ url('/database/faculty-info/listing') }}" class="list-group-item list-group-item-action waves-effect">
                Faculty Info
            </a>

             @elseif($module->table_name == 'kos_financial_years' )
             <a href="{{ url('/database/financial-year/listing') }}" class="list-group-item list-group-item-action waves-effect">
                  Financial Year
              </a>

             @elseif($module->table_name == 'kos_institutes' )
            <a href="{{ url('/database/institute/listing') }}" class="list-group-item list-group-item-action waves-effect">
                Institutes
            </a>

            @elseif($module->table_name == 'kos_institute_infos' )
             <a href="{{ url('/database/institute-info/listing') }}" class="list-group-item list-group-item-action waves-effect">
                Institute Info
              </a>

            @elseif($module->table_name == 'kos_staff_infos' )
            <a href="{{ url('/database/manpower-info/listing') }}" class="list-group-item list-group-item-action waves-effect">
                 Manpower Info
            </a>

             @elseif($module->table_name == 'kos_offices' )
             <a href="{{ url('/database/office/listing') }}" class="list-group-item list-group-item-action waves-effect">
                  Offices
              </a>

             @elseif($module->table_name == 'kos_original_posts' )
             <a href="{{ url('/database/original-post/listing') }}" class="list-group-item list-group-item-action waves-effect">
                 Original Post
              </a>

            @elseif($module->table_name == 'kos_programs' )
            <a href="{{ url('/database/program/listing') }}" class="list-group-item list-group-item-action waves-effect">
                Programs
            </a>

            @elseif($module->table_name == 'kos_sessions' )
            <a href="{{ url('/database/session/listing') }}" class="list-group-item list-group-item-action waves-effect">
                Session
            </a>

             @elseif($module->table_name == 'kos_vehicles' )
            <a href="{{ url('/database/vehicle/listing') }}" class="list-group-item list-group-item-action waves-effect">
                Vehicles
            </a>

            @elseif($module->table_name == 'kos_vehicle_infos' )
             <a href="{{ url('/database/vehicle-info/listing') }}" class="list-group-item list-group-item-action waves-effect">
                   Vehicle Info
             </a>



                        @else
                    @continue
                            <a href="form-edit.html" class="list-group-item list-group-item-action waves-effect">
                                Edit Form
                            </a>
                        @endif
                    @endforeach
                @endif
        </div>

    </div>
    <!-- Sidebar -->

</header>
