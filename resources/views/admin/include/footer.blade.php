<footer class="page-footer text-center font-small primary-color-dark darken-2 mt-4 wow fadeIn">
    <hr class="my-4">

    <!--Copyright-->
    <div class="clr"></div>
    <div class="footer-copyright py-3">
       <p style="display: block">© 2018 Copyright: <a href="http://ugcerp.com" target="_blank"> ugcerp.com </a></p>
    </div>
    <!--/.Copyright-->

</footer>
