<nav class="navbar navbar-light bg-light">
    <div class="btn-group" style="margin-left: auto;">
        <button style="width: 120px" type="button" class="custom-bg custom-font btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-user mr-3"></i>
        </button>
        <utt class="custom-bg dropdown-menu dropdown-menu-right">
            <a href="{{ url('/database/dashboard') }}">
                <button class="dropdown-item custom-font" type="button"><i class="mr-3 fas fa-home"></i>Home</button>
            </a>
            <a href="{{ url('/database/view-profile') }}">
            <button class="dropdown-item custom-font" type="button"><i class="mr-3 fas fa-key"></i>Change Password

            </button>
            </a>

            <button class="dropdown-item custom-font" type="button"><i class="mr-3 fas fa-sign-out-alt"></i>
                <a style="color: white" class="top-nav-link text-right" href="{{ url('/database/admin-logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{ url('/database/admin-logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </button>


    </div>
    </div>
</nav>
