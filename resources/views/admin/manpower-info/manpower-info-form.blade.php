@extends('admin.master')

@section('title')
    <title>Manpower Info</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Add Manpower Info</span>
                    </h4>
                    <a class="btn btn-outline-info btn-rounded btn-secondary btn-lg white-text"
                       href="{{ url('/database/manpower-info/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>


                </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <form class="col-md-12 mb-4" action="{{ url('/database/manpower-info/add') }}" method="POST">

                @csrf
                <!-- Form with panel -->
                    <div class="card card-cascade narrower">

                        <div class="px-4">

                            <div class="table-wrapper">

                                <label class="form-label">University :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    @if(Session::get('adminRole') == 'super-admin')
                                        <select class="custom-select mdb-select" required name="university"
                                                searchable="Search here..">
                                            <?php $universities = \App\KosUniversity::all(); ?>
                                            <option value="">Select University</option>
                                            @foreach($universities as $university )
                                                <option value="{{ $university->id }}">{{ $university->university }}</option>
                                            @endforeach
                                            <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                        </select>
                                    @else
                                        <select class="custom-select mdb-select" required name="university"
                                                searchable="Search here..">
                                            <?php $userId = Session::get('adminId'); ?>
                                            <?php $universityAccess = DB::table('kos_university_permissions')->where('role_id', $userId)->get(); ?>
                                            <option value="">Select University</option>
                                            @foreach($universityAccess as $university )
                                                <?php  $universityById = \App\KosUniversity::find($university->university_id); ?>
                                                <option value="{{ $universityById->id }}">{{ $universityById->university }}</option>
                                            @endforeach
                                            <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                        </select>
                                    @endif
                                </div>

                                <label class="form-label">Office :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="office"
                                            searchable="Search here..">
                                        <?php $offices = \App\KosOffice::all(); ?>
                                        <option value="">Select Office</option>
                                        @foreach($offices as $office )
                                            <option value="{{ $office->id }}">{{ $office->office }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <label class="form-label">Type of post :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="type_of_post"
                                            searchable="Search here..">
                                        <option value="teacher">Teacher</option>
                                        <option value="staff">staff</option>
                                    </select>
                                </div>

                                <label class="form-label">Type of employment :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="type_od_employment"
                                            searchable="Search here..">
                                        <option value="regular">Regular</option>
                                        <option value="honorary">Honorary</option>
                                    </select>
                                </div>

                                <label class="form-label">Original Post :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="original_post"
                                            searchable="Search here..">
                                        <?php $originalPosts = \App\KosOriginalPost::all(); ?>
                                        @if(!empty($originalPosts))
                                            <option value="">Select Original Post</option>
                                            @foreach($originalPosts as $post )
                                                <option value="{{ $post->id }}">{{ $post->original_post }}</option>
                                            @endforeach
                                        @else
                                            <option value="">Select Original Post</option>
                                        @endif
                                    </select>
                                </div>

                                <label class="form-label">Grade :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="grade"
                                            searchable="Search here..">
                                        <option value="">Select Grade</option>
                                        <option value="g1">G1</option>
                                        <option value="g2">G2</option>
                                        <option value="g3">G3</option>
                                        <option value="g4">G4</option>
                                        <option value="g5">G5</option>
                                        <option value="g6">G6</option>
                                        <option value="g7">G7</option>
                                        <option value="g8">G8</option>
                                        <option value="g9">G9</option>
                                        <option value="g10">G10</option>
                                        <option value="g11">G11</option>
                                        <option value="g12">G12</option>
                                        <option value="g13">G13</option>
                                        <option value="g14">G14</option>
                                        <option value="g15">G15</option>
                                        <option value="g16">G16</option>
                                        <option value="g17">G17</option>
                                        <option value="g18">G18</option>
                                        <option value="g19">G19</option>
                                        <option value="g20">G20</option>
                                    </select>
                                </div>

                                <label class="form-label">Financial Year :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="financial_year"
                                            searchable="Search here..">
                                        <?php $financialYears = \App\KosFinancialYear::all(); ?>
                                        @if(!empty($financialYears))
                                            <option value="">Select Financial Year</option>
                                            @foreach($financialYears as $year )
                                                <option value="{{ $year->id }}">{{ $year->financial_year }}</option>
                                            @endforeach
                                        @else
                                            <option value="">Select Financial Year</option>
                                        @endif
                                    </select>
                                </div>

                                <label class="form-label">No of post in organogram :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of post in organogram" name="no_of_post_in_organogram"
                                           type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">No of post approved by UGC :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of post approved by UGC" name="no_of_post_approved_by_UGC"
                                           type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Date of approval by UGC :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Date of approval by UGC" name="date_of_approval_by_UGC"
                                           type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">No of post regular appointed :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of post regular appointed"
                                           name="no_of_post_regular_appointed" type="text" id="form1"
                                           class="form-control custom-input">
                                </div>

                                <label class="form-label">No of post upgraded :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of post upgraded" name="no_of_post_upgraded" type="text"
                                           id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Name of post upgraded 1 :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Name of post upgraded 1" name="name_of_post_upgraded_1"
                                           type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Name of post upgraded 2 :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Name of post upgraded 2" name="name_of_post_upgraded_2"
                                           type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Name of post upgraded 3 :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Name of post upgraded 3" name="name_of_post_upgraded_3"
                                           type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Remarks :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <textarea class="custom-textarea" name="remarks"></textarea>
                                    <br>
                                    <button style="color: white"
                                            class="mt-lg-5 mb-lg-5 btn btn-outline-info btn-rounded btn-secondary btn-lg"
                                            type="submit">Save
                                    </button>


                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- Table with panel -->
            </div>

        </div>
        <!--Grid row-->

        </div>
    </main>
@endsection
