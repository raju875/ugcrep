<div style="text-align: center; padding-top: 50px;font-family: arial, sans-serif;border-collapse: collapse;">
    <h2>Public University Management Information System (PUMIS)</h2>
    {{--if selected university names are same start--}}
    @if($uni==1 )
        @foreach ($staffInfos as $staffInfo )
            <?php $staffInfoById = \App\KosStaffInfo::find($staffInfo); ?>
            <?php $university = \App\KosUniversity::find($staffInfoById->university); ?>
            <h3>{{ $university->university }}</h3>
            @break
        @endforeach
    @endif
    {{--if selected university names are same start--}}
</div>
<style>
    table,th,td{
        border: 1px solid #ddd;
        border-collapse: collapse;
        font-size: 11px;

    }
    table{
        margin: 0 auto;
    }
    th{
        padding: 12px 0;
        text-align: center;
    }
</style>



<table width="100%">
    <thead>
    <tr>
        @if(Session::get('staffInfoUniversity'))
            @if($uni==0)
                <th     >{{ 'University' }}</th
            @endif
        @endif

        @if(Session::get('staffInfoOffice'))
            <th     >{{ 'Office' }}</th>
        @endif

        @if(Session::get('staffInfoTypeOfPost'))
            <th     >{{ 'Type of pos' }}</th>
        @endif

        @if(Session::get('staffInfoTypeOfEmployment'))
            <th     >{{ 'Type of employement' }}</th>
        @endif

        @if(Session::get('staffInfoOriginalPost'))
            <th     >{{ 'Original post' }}</th>
        @endif

        @if(Session::get('staffInfoGrade'))
            <th     >{{ 'Grade' }}</th>
        @endif

        @if(Session::get('staffInfoFinancialYear'))
            <th     >{{ 'Financial Year' }}</th>
        @endif

        @if(Session::get('staffInfoNoOfPostInOrganogram'))
            <th     >{{ 'No of post in organogram' }}</th>
        @endif

        @if(Session::get('staffInfoNoOfPostApprovedByUGC'))
            <th     >{{ 'No of post Approved by UGC' }}</th>
        @endif

        @if(Session::get('staffInfoDateOfApprovalByUGC'))
            <th     >{{ 'Date of approval by UGC' }}</th>
        @endif

        @if(Session::get('staffInfoNoOfPostRegularAppointed'))
            <th     >{{ 'No of post regular appointed' }}</th>
        @endif

        @if(Session::get('staffInfoNameOfPostUpgraded1'))
            <th     >{{ 'Name of post upgraded 1' }}</th>
        @endif

        @if(Session::get('staffInfoNameOfPostUpgraded2'))
            <th     >{{ 'Name of post upgraded 2' }}</th>
        @endif

        @if(Session::get('staffInfoNameOfPostUpgraded3'))
            <th     >{{ 'Name of post upgraded 3' }}</th>
        @endif

        @if(Session::get('staffInfoRemarks'))
            <th     >{{ 'Remarks' }}</th>
        @endif

    </tr>
    </thead>
    <tbody>

    @foreach($staffInfos as $staffInfo )
        <tr     >
            <?php $staffInfoById = \App\KosStaffInfo::find($staffInfo); ?>
            @if(Session::get('staffInfoUniversity'))
                @if($uni==0 )
                    {{--//if selected university name is not same--}}
                    <?php $university = \App\KosUniversity::find($staffInfoById->university); ?>
                    @if(!empty($university))
                        <td     >{{ $university->university }}</td>
                    @else
                        <td     ></td>
                    @endif
                @endif
            @endif


            @if(Session::get('staffInfoOffice'))
                <?php $office = \App\KosOffice::find($staffInfoById->office); ?>
                @if (empty($office))
                    <td     >{{ 'Null' }}</td>
                @else
                    <td     >{{ $office->office }}</td>
                @endif
            @endif


            @if(Session::get('staffInfoTypeOfPost'))
                <td     >
                    @if($staffInfoById->type_of_post=='teacher'){{'Teacher'}}
                    @endif
                    @if($staffInfoById->type_of_post=='staff'){{'Staff'}}
                    @endif
                </td>
            @endif

            @if(Session::get('staffInfoTypeOfEmployment'))
                <td     >
                    @if($staffInfoById->type_of_employment=='honorary'){{'Honorary'}}
                    @endif
                    @if($staffInfoById->type_of_employment=='regular'){{'Regular'}}
                    @endif
                </td>
            @endif


            @if(Session::get('staffInfoOriginalPost'))
                <?php $originalPost = \App\KosOriginalPost::find($staffInfoById->original_post); ?>
                @if (empty($originalPost))
                    <td     ></td>
                @else
                    <td     >{{ $office->office }}</td>
                @endif
            @endif

            @if(Session::get('staffInfoGrade'))
                @if($staffInfoById->grade != '')
                            @if($staffInfoById->grade == 'g1')
                                <td     >{{ 'G1' }}</td>
                            @elseif($staffInfoById->grade == 'g2')
                                <td     >{{ 'G2' }}</td>
                            @elseif($staffInfoById->grade == 'g3')
                                <td     >{{ 'G3' }}</td>
                            @elseif($staffInfoById->grade == 'g4')
                                <td     >{{ 'G4' }}</td>
                            @elseif($staffInfoById->grade == 'g5')
                                <td     >{{ 'G5' }}</td>
                            @elseif($staffInfoById->grade == 'g6')
                                <td     >{{ 'G6' }}</td>
                            @elseif($staffInfoById->grade == 'g7')
                                <td     >{{ 'G7' }}</td>
                            @elseif($staffInfoById->grade == 'g8')
                                <td     >{{ 'G8' }}</td>
                            @elseif($staffInfoById->grade == 'g9')
                                <td     >{{ 'G9' }}</td>
                            @elseif($staffInfoById->grade == 'g10')
                                <td     >{{ 'G10' }}</td>
                            @elseif($staffInfoById->grade == 'g11')
                                <td     >{{ 'G11' }}</td>
                            @elseif($staffInfoById->grade == 'g12')
                                <td     >{{ 'G12' }}</td>
                            @elseif($staffInfoById->grade == 'g13')
                                <td     >{{ 'G13' }}</td>
                            @elseif($staffInfoById->grade == 'g14')
                                <td     >{{ 'G14' }}</td>
                            @elseif($staffInfoById->grade == 'g15')
                                <td     >{{ 'G15' }}</td>
                            @elseif($staffInfoById->grade == 'g16')
                                <td     >{{ 'G16' }}</td>
                            @elseif($staffInfoById->grade == 'g1')
                                <td     >{{ 'G1' }}</td>
                            @elseif($staffInfoById->grade == 'g17')
                                <td     >{{ 'G17' }}</td>
                            @elseif($staffInfoById->grade == 'g18')
                                <td     >{{ 'G18' }}</td>
                            @elseif($staffInfoById->grade == 'g19')
                                <td     >{{ 'G19' }}</td>
                            @elseif($staffInfoById->grade == 'g20')
                                <td     >{{ 'G20' }}</td>
                            @else
                            @endif


                @else
                    <td     ></td>
                @endif
            @endif

            @if(Session::get('staffInfoFinancialYear'))
                <?php $financialYear = \App\KosFinancialYear::find($staffInfoById->financial_year); ?>
                @if (empty($financialYear))
                    <td     ></td>
                @else
                    <td     >{{ $financialYear->financial_year }}</td>
                @endif
            @endif

            @if(Session::get('staffInfoNoOfPostInOrganogram'))
                @if($staffInfoById->no_of_post_in_organogram != '')
                    <td     >{{ $staffInfoById->no_of_post_in_organogram }}</td>
                @else
                    <td     ></td>
                @endif
            @endif

            @if(Session::get('staffInfoNoOfPostApprovedByUGC'))
                @if($staffInfoById->date_of_approval_by_UGC != '')
                    <td     >{{ $staffInfoById->date_of_approval_by_UGC }}</td>
                @else
                    <td     ></td>
                @endif
            @endif

            @if(Session::get('staffInfoDateOfApprovalByUGC'))
                @if($staffInfoById->date_of_approval_by_UGC != '')
                    <td     >{{ $staffInfoById->date_of_approval_by_UGC }}</td>
                @else
                    <td     ></td>

                @endif
            @endif

            @if(Session::get('staffInfoNoOfPostRegularAppointed'))
                @if($staffInfoById->no_of_post_regular_appointed!= '')
                    <td     >{{ $staffInfoById->no_of_post_regular_appointed }}</td>
                @else
                    <td     ></td>
                @endif
            @endif

            @if(Session::get('staffInfoNameOfPostUpgraded1'))
                @if($staffInfoById->name_of_post_upgraded_1 != '')
                    <td     >{{ $staffInfoById->name_of_post_upgraded_1 }}</td>
                @else
                    <td     ></td>
                @endif
            @endif

            @if(Session::get('staffInfoNameOfPostUpgraded2'))
                @if($staffInfoById->name_of_post_upgraded_2 != '')
                    <td     >{{ $staffInfoById->name_of_post_upgraded_2 }}</td>
                @else
                    <td     ></td>
                @endif
            @endif

            @if(Session::get('staffInfoNameOfPostUpgraded3'))
                @if($staffInfoById->name_of_post_upgraded_3 != '')
                    <td     >{{ $staffInfoById->name_of_post_upgraded_3 }}</td>
                @else
                    <td     ></td>
                @endif
            @endif

            @if(Session::get('staffInfoRemarks'))
                @if($staffInfoById->remarks != '')
                    <td     >{{ $staffInfoById->remarks }}</td>
                @else
                    <td     ></td>
                @endif
            @endif
        </tr>

    @endforeach
    </tbody>
</table>
