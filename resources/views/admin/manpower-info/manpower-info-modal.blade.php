@extends('admin.master')

@section('title')
    <title>Manpower Info</title>
@endsection

@section('body')
    <div class="content-div">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->


                <div class="card-body d-sm-flex justify-content-between">


                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Manpower Info</span>
                    </h4>

                    <div class="right-align">
                        <input placeholder="Search One" type="text" id="search-default">
                        <input placeholder="Search Two" type="text" id="search-filter">
                    </div>

                </div>
            </div>
            <!-- Heading -->
            <!-- Button trigger modal -->
            <button type="button" class="mb-lg-3 btn btn-primary btn-md show-field-btn" data-toggle="modal"
                    data-target="#exampleModalCenter">
                Show Field
            </button>
            <div class="clr"></div>

            <!-- Modal -->
            <div class="centre modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content m-lg-5">
                        <form action="{{ url('/database/manpower-info/modal-listing') }}" method="POST">
                            @csrf
                            <ul style="list-style-type: none">
                                <li>
                                    <input type="checkbox" class="allModal">Select All
                                </li>
                                <li>
                                    @if(Session::has('staffInfoUniversity'))
                                        <input class="singelModal" type="checkbox" id="university" name="column[]"
                                               value="university"
                                               checked="checked"> University
                                    @else
                                        <input class="singelModal" type="checkbox" id="university" name="column[]"
                                               value="university"
                                        > University
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoOffice'))
                                        <input class="singelModal" type="checkbox" id="office" name="column[]"
                                               value="office"
                                               checked="checked"> Office
                                    @else
                                        <input class="singelModal" type="checkbox" id="office" name="column[]"
                                               value="office"
                                        > Office
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoTypeOfPost'))
                                        <input class="singelModal" type="checkbox" id="type_of_post" name="column[]"
                                               value="type_of_post" checked="checked"> Type of post
                                    @else
                                        <input class="singelModal" type="checkbox" id="type_of_post" name="column[]"
                                               value="type_of_post"> Type of post
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoTypeOfEmployment'))
                                        <input class="singelModal" type="checkbox" id="type_of_employment"
                                               name="column[]"
                                               value="type_of_employment" checked="checked"> Type of employment
                                    @else
                                        <input class="singelModal" type="checkbox" id="type_of_employment"
                                               name="column[]"
                                               value="type_of_employment"> Type of employment
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoOriginalPost'))
                                        <input class="singelModal" type="checkbox" id="original_post" name="column[]"
                                               value="original_post" checked="checked"> Original post
                                    @else
                                        <input class="singelModal" type="checkbox" id="original_post" name="column[]"
                                               value="original_post"> Original post
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoGrade'))
                                        <input class="singelModal" type="checkbox" id="grade" name="column[]"
                                               value="grade"
                                               checked="checked"> Grade
                                    @else
                                        <input class="singelModal" type="checkbox" id="grade" name="column[]"
                                               value="grade"> Grade
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoFinancialYear'))
                                        <input class="singelModal" type="checkbox" id="financial_year" name="column[]"
                                               value="financial_year" checked="checked"> Financial year
                                    @else
                                        <input class="singelModal" type="checkbox" id="financial_year" name="column[]"
                                               value="financial_year"> Financial year
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoNoOfPostInOrganogram'))
                                        <input class="singelModal" type="checkbox" id="no_of_post_in_organogram"
                                               name="column[]"
                                               value="no_of_post_in_organogram" checked="checked"> No of post in
                                        organogram
                                    @else
                                        <input class="singelModal" type="checkbox" id="no_of_post_in_organogram"
                                               name="column[]"
                                               value="no_of_post_in_organogram"> No of post in organogram
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoNoOfPostApprovedByUGC'))
                                        <input class="singelModal" type="checkbox" id="no_of_post_approved_by_UGC"
                                               name="column[]"
                                               value="no_of_post_approved_by_UGC" checked="checked"> No of post
                                        approved by UGC
                                    @else
                                        <input class="singelModal" type="checkbox" id="no_of_post_approved_by_UGC"
                                               name="column[]"
                                               value="no_of_post_approved_by_UGC"> No of post approved by UGC
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoDateOfApprovalByUGC'))
                                        <input class="singelModal" type="checkbox" id="date_of_approval_by_UGC"
                                               name="column[]"
                                               value="date_of_approval_by_UGC" checked="checked"> Date of
                                        approval
                                        by UGC
                                    @else
                                        <input class="singelModal" type="checkbox" id="date_of_approval_by_UGC"
                                               name="column[]"
                                               value="date_of_approval_by_UGC"> Date of approval
                                        by UGC
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoNoOfPostRegularAppointed'))
                                        <input class="singelModal" type="checkbox" id="no_of_post_regular_appointed"
                                               name="column[]"
                                               value="no_of_post_regular_appointed" checked="checked"> No of
                                        post
                                        regular appointed
                                    @else
                                        <input class="singelModal" type="checkbox" id="no_of_post_regular_appointed"
                                               name="column[]"
                                               value="no_of_post_regular_appointed"> No of post
                                        regular appointed
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoNameOfPostUpgraded1'))
                                        <input class="singelModal" type="checkbox" id="name_of_post_upgraded_1"
                                               name="column[]"
                                               value="name_of_post_upgraded_1" checked="checked"> Name of post
                                        upgraded 1
                                    @else
                                        <input class="singelModal" type="checkbox" id="name_of_post_upgraded_1"
                                               name="column[]"
                                               value="name_of_post_upgraded_1"> Name of post
                                        upgraded 1
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoNameOfPostUpgraded2'))
                                        <input class="singelModal" type="checkbox" id="name_of_post_upgraded_2"
                                               name="column[]"
                                               value="name_of_post_upgraded_2" checked="checked"> Name of post
                                        upgraded 1
                                    @else
                                        <input class="singelModal" type="checkbox" id="name_of_post_upgraded_2"
                                               name="column[]"
                                               value="name_of_post_upgraded_2"> Name of post
                                        upgraded 2
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoNameOfPostUpgraded3'))
                                        <input class="singelModal" type="checkbox" id="name_of_post_upgraded_3"
                                               name="column[]"
                                               value="name_of_post_upgraded_3" checked="checked"> Name of post
                                        upgraded 1
                                    @else
                                        <input class="singelModal" type="checkbox" id="name_of_post_upgraded_3"
                                               name="column[]"
                                               value="name_of_post_upgraded_3"> Name of post
                                        upgraded 3
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('staffInfoRemarks'))
                                        <input class="singelModal" type="checkbox" id="remarks" name="column[]"
                                               value="remarks"
                                               checked="checked"> Remarks
                                    @else
                                        <input class="singelModal" type="checkbox" id="remarks" name="column[]"
                                               value="remarks"
                                        > Remarks
                                    @endif
                                </li>

                            </ul>

                            <button style="width: 150px;margin-left: 35px;" type="submit"
                                    class="btn btn-secondary">
                                Save Changes
                            </button>
                        </form>
                    </div>

                </div>
            </div>
            @if(Session::has('message'))
                <h4 class="text text-center text-success">{{ Session::get('message') }}</h4>
            @endif
            @if(Session::has('alert'))
                <h4 class="text text-center text-danger">{{ Session::get('alert') }}</h4>
        @endif
        <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Table with panel -->
                    <div class="card card-cascade narrower">
                        <!--Form Starts-->
                        <form action="{{ url('/database/manpower-info/edit') }}" method="POST">
                        @csrf
                        <!--Card image-->
                            <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">

                                {{--<div>--}}
                                @if( Session::get('adminRole') == 'super-admin')
                                    <?php $count = DB::table('kos_staff_infos')->count(); ?>
                                    <label>Total Items {{ $count }}</label>
                                    <div>
                                        <a href="{{ url('/database/manpower-info/form') }}" title="Add"
                                           class="btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i class="custom-fontawesome fas fa-plus-circle"></i>
                                        </a>
                                        <button type="submit" name="submit" value="report" title="Report"
                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Report" class="custom-fontawesome fas fa-pen-square"></i>
                                        </button>
                                        <button type="submit" value="copy" name="submit"
                                                onclick="return confirm('Are you sure to Copy it !!!')" title="Copy"
                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                        </button>
                                        <button type="submit" value="delete" name="submit"
                                                onclick="return confirm('Are you sure to Delete it !!!')" title="Delete"
                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                        </button>

                                    </div>
                                @else
                                    <div class="col-lg-12">
                                        <?php $count = DB::table('kos_staff_infos')
                                            ->join('kos_university_permissions', 'kos_staff_infos.university', '=', 'kos_university_permissions.university_id')
                                            ->join('kos_universities', 'kos_staff_infos.university', '=', 'kos_universities.id')
                                            ->where('kos_university_permissions.role_id', Session::get('adminId'))
                                            ->count()?>
                                        <div class="row">
                                            <div class="col-lg-9">
                                                <label>Total Items {{ $count }}</label>
                                            </div>

                                            <div class="col-lg-3 text-right">
                                                <?php $permissions = \App\KosPermission::where('role_id', Session::get('adminId'))->get();?>
                                                @foreach($permissions as $permission )
                                                    @if($permission->permission == 'add')
                                                        <a href="{{ url('/database/manpower-info/form') }}" title="Add"
                                                           class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                            <i class="custom-fontawesome fas fa-plus-circle"></i>
                                                        </a>
                                                    @endif
                                                    @if($permission->permission == 'report')
                                                        <button type="submit" name="submit" value="report"
                                                                title="Report"
                                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                            <i title="Report"
                                                               class="custom-fontawesome fas fa-pen-square"></i>
                                                        </button>
                                                    @endif
                                                    @if($permission->permission == 'copy')
                                                        <button type="submit" value="copy" name="submit"
                                                                onclick="return confirm('Are you sure to Copy it !!!')"
                                                                title="Copy"
                                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                            <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                                        </button>
                                                    @endif
                                                    @if($permission->permission == 'delete')
                                                        <button type="submit" value="delete" name="submit"
                                                                onclick="return confirm('Are you sure to Delete it !!!')"
                                                                title="Delete"
                                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                            <i title="Delete"
                                                               class="custom-fontawesome far fa-trash-alt"></i>
                                                        </button>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <!--/Card image-->


                            <div class="px-4">

                                <div class="table-wrapper table-responsive">
                                    <!--Table-->

                                    {{--**************For Super Admin start here**************--}}

                                    @if(Session::get('adminRole') == 'super-admin')

                                        <table id="datatable" class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                {{--this is hidden column by default checked all--}}
                                                <th style="display: none;"><input type="checkbox" checked
                                                                                  class="selectall"></th>
                                                {{--this is hidden column by default checked all--}}

                                                <th><input type="checkbox" class="all"></th>
                                                <th class="university">University</th>
                                                <th class="office">Office</th>
                                                <th class="type_of_post">Type of post</th>
                                                <th class="type_of_employment">Type of employment</th>
                                                <th class="original_post">Original post</th>
                                                <th class="grade">Grade</th>
                                                <th class="financial_year">Financial year</th>
                                                <th class="no_of_post_in_organogram">No of post in organogram</th>
                                                <th class="no_of_post_approved_by_UGC">No of post approved by UGC</th>
                                                <th class="date_of_approval_by_UGC">Date of approval by UGC</th>
                                                <th class="no_of_post_regular_appointed">No of post regular appointed
                                                </th>
                                                <th class="name_of_post_upgraded_1">Name of post upgraded 1</th>
                                                <th class="name_of_post_upgraded_2">Name of post upgraded 2</th>
                                                <th class="name_of_post_upgraded_3">Name of post upgraded 3</th>
                                                <th class="remarks">Remarks</th>
                                            </tr>
                                            </thead>
                                            <tbody>


                                            <?php $manpowerInfos = DB::table('kos_staff_infos')->orderBy('id', 'desc')->paginate(100);?>
                                            @foreach($manpowerInfos as $info )
                                                <tr>
                                                    {{--this is hidden column by default checked all--}}
                                                    <td style="display: none;"><input checked type="checkbox"
                                                                                      class="individual"
                                                                                      name="manpower_info_hide[]"
                                                                                      value="{{ $info->id }}"></td>

                                                    <td><input type="checkbox" class="single" name="manpower_info[]"
                                                               value="{{ $info->id }}"></td>

                                                    <td class="university">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            <?php $uni = \App\KosUniversity::find($info->university)?>
                                                            @if(!empty($uni))
                                                                {{ $uni->university }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="office">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            <?php $off = \App\KosOffice::find($info->office)?>
                                                            @if(!empty($off))
                                                                {{ $off->office }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="type_of_post">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            {{ $info->type_of_post }}
                                                        </a>
                                                    </td>

                                                    <td class="type_of_employment">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            {{ $info->type_of_employment }}
                                                        </a>
                                                    </td>

                                                    <td class="original_post">
                                                        <?php $post = \App\KosOriginalPost::find($info->original_post)?>
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            @if(!empty($post))
                                                                {{ $post->original_post }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="grade">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            @if($info->grade == 'g1')
                                                                {{ 'G1' }}
                                                            @elseif($info->grade == 'g2')
                                                                {{ 'G2' }}
                                                            @elseif($info->grade == 'g3')
                                                                {{ 'G3' }}
                                                            @elseif($info->grade == 'g4')
                                                                {{ 'G4' }}
                                                            @elseif($info->grade == 'g5')
                                                                {{ 'G5' }}
                                                            @elseif($info->grade == 'g6')
                                                                {{ 'G6' }}
                                                            @elseif($info->grade == 'g7')
                                                                {{ 'G7' }}
                                                            @elseif($info->grade == 'g8')
                                                                {{ 'G8' }}
                                                            @elseif($info->grade == 'g9')
                                                                {{ 'G9' }}
                                                            @elseif($info->grade == 'g10')
                                                                {{ 'G10' }}
                                                            @elseif($info->grade == 'g11')
                                                                {{ 'G11' }}
                                                            @elseif($info->grade == 'g12')
                                                                {{ 'G12' }}
                                                            @elseif($info->grade == 'g13')
                                                                {{ 'G13' }}
                                                            @elseif($info->grade == 'g14')
                                                                {{ 'G14' }}
                                                            @elseif($info->grade == 'g15')
                                                                {{ 'G15' }}
                                                            @elseif($info->grade == 'g16')
                                                                {{ 'G16' }}
                                                            @elseif($info->grade == 'g1')
                                                                {{ 'G1' }}
                                                            @elseif($info->grade == 'g17')
                                                                {{ 'G17' }}
                                                            @elseif($info->grade == 'g18')
                                                                {{ 'G18' }}
                                                            @elseif($info->grade == 'g19')
                                                                {{ 'G19' }}
                                                            @elseif($info->grade == 'g20')
                                                                {{ 'G20' }}
                                                            @else
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="financial_year">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            <?php $year = \App\KosFinancialYear::find($info->financial_year)?>
                                                            @if(!empty($year))
                                                                {{ $year->financial_year }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="no_of_post_in_organogram">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            {{ $info->no_of_post_in_organogram }}
                                                        </a>
                                                    </td>

                                                    <td class="no_of_post_approved_by_UGC">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            {{ $info->no_of_post_approved_by_UGC }}
                                                        </a>
                                                    </td>

                                                    <td class="date_of_approval_by_UGC">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            {{ $info->date_of_approval_by_UGC }}
                                                        </a>
                                                    </td>

                                                    <td class="no_of_post_regular_appointed">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            {{ $info->no_of_post_regular_appointed }}
                                                        </a>
                                                    </td>

                                                    <td class="name_of_post_upgraded_1">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            {{ $info->name_of_post_upgraded_1 }}
                                                        </a>
                                                    </td>

                                                    <td class="name_of_post_upgraded_2">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            {{ $info->name_of_post_upgraded_2 }}
                                                        </a>
                                                    </td>
                                                    <td class="name_of_post_upgraded_3">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            {{ $info->name_of_post_upgraded_3 }}
                                                        </a>
                                                    </td>

                                                    <td class="remarks">
                                                        <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                            {{ $info->remarks }}
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                        {{ $manpowerInfos ->links() }}
                                        {{--**************For Super Admin end here**************--}}


                                        {{--************For Normal User or Admin start here**************--}}
                                    @else

                                        <table id="datatable" class="table display" border="1">
                                            <thead>
                                            <tr>
                                                {{--this is hidden column by default checked all--}}
                                                <th style="display: none;"><input type="checkbox" checked
                                                                                  class="selectall"></th>

                                                <th><input type="checkbox" class="all"></th>
                                                <th class="university">University</th>
                                                <th class="office">Office</th>
                                                <th class="type_of_post">Type of post</th>
                                                <th class="type_of_employment">Type of employment</th>
                                                <th class="original_post">Original post</th>
                                                <th class="grade">Grade</th>
                                                <th class="financial_year">Financial year</th>
                                                <th class="no_of_post_in_organogram">No of post in organogram</th>
                                                <th class="no_of_post_approved_by_UGC">No of post approved by UGC</th>
                                                <th class="date_of_approval_by_UGC">Date of approval by UGC</th>
                                                <th class="no_of_post_regular_appointed">No of post regular appointed
                                                </th>
                                                <th class="name_of_post_upgraded_1">Name of post upgraded 1</th>
                                                <th class="name_of_post_upgraded_2">Name of post upgraded 2</th>
                                                <th class="name_of_post_upgraded_3">Name of post upgraded 3</th>
                                                <th class="remarks">Remarks</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php $manpowerInfos = DB::table('kos_staff_infos')
                                                ->join('kos_university_permissions', 'kos_staff_infos.university', '=', 'kos_university_permissions.university_id')
                                                ->join('kos_universities', 'kos_staff_infos.university', '=', 'kos_universities.id')
                                                ->where('kos_university_permissions.role_id', Session::get('adminId'))
                                                ->select('kos_staff_infos.*', 'kos_universities.university')->orderBy('id', 'desc')
                                                ->paginate(100)?>
                                            @foreach($manpowerInfos as $info )
                                                {{--this is hidden column by default checked all--}}
                                                <td style="display: none;"><input checked type="checkbox"
                                                                                  class="individual"
                                                                                  name="manpower_info_hide[]"
                                                                                  value="{{ $info->id }}"></td>

                                                <td><input type="checkbox" class="single" name="manpower_info[]"
                                                           value="{{ $info->id }}"></td>

                                                <td class="university">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->university }}
                                                    </a>
                                                </td>

                                                <td class="office">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->office }}
                                                    </a>
                                                </td>

                                                <td class="type_of_post">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->type_of_post }}
                                                    </a>
                                                </td>

                                                <td class="type_of_employment">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->type_of_employment }}
                                                    </a>
                                                </td>

                                                <td class="original_post">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->original_post }}
                                                    </a>
                                                </td>

                                                <td class="grade">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        @if($info->grade == 'g1')
                                                            {{ 'G1' }}
                                                        @elseif($info->grade == 'g2')
                                                            {{ 'G2' }}
                                                        @elseif($info->grade == 'g3')
                                                            {{ 'G3' }}
                                                        @elseif($info->grade == 'g4')
                                                            {{ 'G4' }}
                                                        @elseif($info->grade == 'g5')
                                                            {{ 'G5' }}
                                                        @elseif($info->grade == 'g6')
                                                            {{ 'G6' }}
                                                        @elseif($info->grade == 'g7')
                                                            {{ 'G7' }}
                                                        @elseif($info->grade == 'g8')
                                                            {{ 'G8' }}
                                                        @elseif($info->grade == 'g9')
                                                            {{ 'G9' }}
                                                        @elseif($info->grade == 'g10')
                                                            {{ 'G10' }}
                                                        @elseif($info->grade == 'g11')
                                                            {{ 'G11' }}
                                                        @elseif($info->grade == 'g12')
                                                            {{ 'G12' }}
                                                        @elseif($info->grade == 'g13')
                                                            {{ 'G13' }}
                                                        @elseif($info->grade == 'g14')
                                                            {{ 'G14' }}
                                                        @elseif($info->grade == 'g15')
                                                            {{ 'G15' }}
                                                        @elseif($info->grade == 'g16')
                                                            {{ 'G16' }}
                                                        @elseif($info->grade == 'g1')
                                                            {{ 'G1' }}
                                                        @elseif($info->grade == 'g17')
                                                            {{ 'G17' }}
                                                        @elseif($info->grade == 'g18')
                                                            {{ 'G18' }}
                                                        @elseif($info->grade == 'g19')
                                                            {{ 'G19' }}
                                                        @elseif($info->grade == 'g20')
                                                            {{ 'G20' }}
                                                        @else
                                                        @endif
                                                    </a>
                                                </td>

                                                <td class="financial_year">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        <?php $year = \App\KosFinancialYear::find($info->financial_year)?>
                                                        @if(!empty($year))
                                                            {{ $year->financial_year }}
                                                        @endif
                                                    </a>
                                                </td>

                                                <td class="no_of_post_in_organogram">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->no_of_post_in_organogram }}
                                                    </a>
                                                </td>

                                                <td class="no_of_post_approved_by_UGC">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->no_of_post_approved_by_UGC }}
                                                    </a>
                                                </td>

                                                <td class="date_of_approval_by_UGC">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->date_of_approval_by_UGC }}
                                                    </a>
                                                </td>

                                                <td class="no_of_post_regular_appointed">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->no_of_post_regular_appointed }}
                                                    </a>
                                                </td>

                                                <td class="name_of_post_upgraded_1">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->name_of_post_upgraded_1 }}
                                                    </a>
                                                </td>

                                                <td class="name_of_post_upgraded_2">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->name_of_post_upgraded_2 }}
                                                    </a>
                                                </td>
                                                <td class="name_of_post_upgraded_3">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->name_of_post_upgraded_3 }}
                                                    </a>
                                                </td>

                                                <td class="remarks">
                                                    <a href="{{ url('/database/manpower-info/edit/'.$info->id) }}">
                                                        {{ $info->remarks }}
                                                    </a>
                                                </td>

                                                </tr>

                                            @endforeach

                                            </tbody>
                                        </table>
                                        {{ $manpowerInfos ->links() }}
                                        {{--************For Normal User or Admin end here**************--}}
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




