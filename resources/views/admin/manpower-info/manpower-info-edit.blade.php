@extends('admin.master')

@section('title')
    <title>Manpower Info</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Edit Manpower Info</span>
                    </h4>
                    <a href="{{ url('/database/manpower-info/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>

                </div>

                @if( Session::get('adminRole') == 'super-admin')
                    <div class="button-section">

                        <div>
                            <a href="{{ url('/database/manpower-info/form') }}" title="Add" class="btn-secondary btn btn-outline-white btn-rounded btn-sm px-2">
                                <i class="custom-fontawesome fas fa-plus-circle"></i>
                            </a>

                            <a href="{{ url('/database/manpower-info/copy/'.$staffInfoById->id) }}" title="Add" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="btn-secondary btn btn-outline-white btn-rounded btn-sm px-2">
                                <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                            </a>
                            <a href="{{ url('/database/manpower-info/delete/'.$staffInfoById->id) }}" value="delete" onclick="return confirm('Are you sure to delete it !!!')" title="Delete" class="btn-secondary btn btn-outline-white btn-rounded btn-sm px-2">
                                <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                            </a>
                        </div>
                    </div>
                @else
                    <div class="button-section">
                        <?php $userId       = Session::get('adminId');?>
                        <?php $permissions  = \App\KosPermission::where('role_id',$userId)->get();?>
                        <div>
                            @foreach($permissions as $permission )
                                @if($permission->permission == 'add' )
                                    <a href="{{ url('/database/manpower-info/form') }}" title="Add" class="btn-secondary btn btn-outline-white btn-rounded btn-sm px-2">
                                        <i class="custom-fontawesome fas fa-plus-circle"></i>
                                    </a>
                                @endif
                                @if($permission->permission == 'copy' )
                                    <a href="{{ url('/database/manpower-info/copy/'.$staffInfoById->id) }}" title="Add" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="btn-secondary btn btn-outline-white btn-rounded btn-sm px-2">
                                        <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                    </a>
                                @endif
                                @if($permission->permission == 'delete' )
                                    <a href="{{ url('/database/manpower-info/delete/'.$staffInfoById->id) }}" value="delete" onclick="return confirm('Are you sure to delete it !!!')" title="Delete" class="btn-secondary btn btn-outline-white btn-rounded btn-sm px-2">
                                        <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <form class="col-md-12 mb-4" action="{{ url('/database/manpower-info/update') }}" method="POST">

                @csrf
                <!-- Form with panel -->
                    <div class="card card-cascade narrower">

                        <div class="px-4">

                            <div class="table-wrapper">

                             <input  name="id" required type="hidden" id="form1" value="{{ $staffInfoById->id }}" class="form-control custom-input">
                                <label class="form-label">University :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    {{--if user is Spuer Admin start here--}}
                                    @if(Session::get('adminRole') == 'super-admin')
                                        <select class="custom-select mdb-select" required name="university" searchable="Search here..">
                                            <?php $universities = \App\KosUniversity::all(); ?>
                                            <?php $uni= \App\KosUniversity::where('id',$staffInfoById->university)->first();?>
                                                @if (!empty($uni))
                                                    <option value="{{ $uni->id }}" >{{ $uni->university }}</option>
                                                    <option value="" >Select University</option>
                                                    @foreach($universities as $university )
                                                        @if($uni->id != $university->id )
                                                            <option value="{{ $university->id }}">{{ $university->university }}</option>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <select class="custom-select mdb-select" required name="university" searchable="Search here..">
                                                        <?php $universities = \App\KosUniversity::all(); ?>
                                                        <option value="" >Select University</option>
                                                        @foreach($universities as $university )
                                                            <option value="{{ $university->id }}">{{ $university->university }}</option>
                                                        @endforeach
                                                        <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                                    </select>
                                                @endif
                                            <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                        </select>
                                        {{--if user is Spuer Admin end here--}}

                                        {{--if user is not Spuer Admin start here--}}
                                    @else
                                        <select class="mdb-select" required name="university" searchable="Search here..">
                                            <?php $universities = \App\KosUniversityPermission::where('role_id',Session::get('adminId'))->get(); ?>
                                            <?php $uni = \App\KosUniversity::where('id',$staffInfoById->university)->first();?>
                                            @if (!empty($uni))
                                                <option value="{{ $uni->id }}" >{{ $uni->university }}</option>
                                                <option value="" >Select University</option>
                                                @foreach($universities as $university )
                                                    @if($uni->id != $university->university_id )
                                                        <?php $versity =\App\KosUniversity::find($university->university_id);?>
                                                        <option value="{{ $versity->id }}">{{ $versity->university }}</option>
                                                    @endif
                                                @endforeach
                                            @else
                                                <select class="custom-select mdb-select" required name="university" searchable="Search here..">
                                                    <option value="" >Select University</option>
                                                    <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                                </select>
                                            @endif
                                            <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                        </select>
                                    @endif
                                    {{--if user is not Spuer Admin end here--}}
                                </div>

                                    <label class="form-label">Office :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="office" searchable="Search here..">
                                        <?php $offices = \App\KosOffice::all(); ?>
                                        <?php $officeById = \App\KosOffice::where('id',$staffInfoById->office)->first()?>
                                            @if(!empty($officeById))
                                                <option value="{{ $officeById->id }}" >{{ $officeById->office }}</option>
                                        <option value="" >Select Office</option>
                                                @foreach($offices as $office )
                                                    @if($staffInfoById->id != $office->id )
                                                        <option value="{{ $office->id }}">{{ $office->office }}</option>
                                                    @endif
                                                @endforeach
                                    </select>
                                    @else
                                        <?php $offices = \App\KosOffice::all(); ?>
                                        <option value="" >Select Office</option>
                                        @foreach($offices as $office )
                                            <option value="{{ $office->id }}">{{ $office->office }}</option>
                                        @endforeach
                                    @endif
                                </div>

                                <label class="form-label">Type of post :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="type_of_post" searchable="Search here..">
                                        @if($staffInfoById->type_of_post == 'teacher')
                                            <option value="teacher" >Teacher</option>
                                            <option value="staff" >Staff</option>
                                        @else
                                            <option value="staff" >Staff</option>
                                            <option value="teacher" >Teacher</option>
                                        @endif
                                    </select>
                                </div>

                                <label class="form-label">Type of employment :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="type_od_employment" searchable="Search here..">
                                        @if($staffInfoById->type_od_employment == 'regular')
                                            <option value="regular" >Regular</option>
                                            <option value="honorary" >Honorary</option>
                                        @else
                                            <option value="honorary" >Honorary</option>
                                            <option value="regular" >Regular</option>
                                        @endif
                                    </select>
                                </div>

                                <label class="form-label">Original Post :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="original_post" searchable="Search here..">
                                            <?php $originalPosts = \App\KosOriginalPost::all(); ?>
                                            <?php $postById = \App\KosOriginalPost::where('id',$staffInfoById->original_post)->first()?>
                                            @if(!empty($postById))
                                                <option value="{{ $postById->id }}" >{{ $postById->original_post }}</option>
                                                <option value="" >Select Original Post</option>
                                                @foreach($originalPosts as $post )
                                                    @if($postById->id != $post->id )
                                                        <option value="{{ $post->id }}">{{ $post->original_post }}</option>
                                                    @endif
                                                @endforeach
                                        @else
                                            <option value="" >Select Original Post</option>
                                            @foreach($originalPosts as $post )
                                                    <option value="{{ $post->id }}">{{ $post->original_post }}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                </div>

                                <label class="form-label">Grade :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="grade" searchable="Search here..">
                                        @if( $staffInfoById->grade == 'g1')
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g2')
                                            <option value="g2" >G2</option>
                                            <option value="g1" >G1</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g3')
                                            <option value="g3" >G3</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g4')
                                            <option value="g4" >G4</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g5')
                                            <option value="g5" >G5</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g6')
                                            <option value="g6" >G6</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g7')
                                            <option value="g7" >G7</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g8')
                                            <option value="g8" >G8</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g9')
                                            <option value="g9" >G9</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g10')
                                            <option value="g10" >G10</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g11')
                                            <option value="g11" >G11</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g12')
                                            <option value="g12" >G12</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g13')
                                            <option value="g13" >G13</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g14')
                                            <option value="g14" >G14</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g15')
                                            <option value="g15" >G15</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g16')
                                            <option value="g16" >G16</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g17')
                                            <option value="g17" >G17</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g18')
                                            <option value="g18" >G18</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g19" >G19</option>
                                            <option value="g20" >G20</option>
                                        @elseif( $staffInfoById->grade == 'g19')
                                            <option value="g19" >G19</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g20" >G20</option>
                                            @else
                                            <option value="g20" >G20</option>
                                            <option value="g1" >G1</option>
                                            <option value="g2" >G2</option>
                                            <option value="g3" >G3</option>
                                            <option value="g4" >G4</option>
                                            <option value="g5" >G5</option>
                                            <option value="g6" >G6</option>
                                            <option value="g7" >G7</option>
                                            <option value="g8" >G8</option>
                                            <option value="g9" >G9</option>
                                            <option value="g10" >G10</option>
                                            <option value="g11" >G11</option>
                                            <option value="g12" >G12</option>
                                            <option value="g13" >G13</option>
                                            <option value="g14" >G14</option>
                                            <option value="g15" >G15</option>
                                            <option value="g16" >G16</option>
                                            <option value="g17" >G17</option>
                                            <option value="g18" >G18</option>
                                            <option value="g19" >G19</option>
                                                @endif
                                    </select>
                                </div>

                                <label class="form-label">Financial Year :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="financial_year" searchable="Search here..">
                                        <?php $financialYears = \App\KosFinancialYear::all(); ?>
                                        <?php $financial = \App\KosFinancialYear::where('id',$staffInfoById->financial_year)->first()?>
                                        @if(!empty($financial))
                                            <option value="{{ $financial->id }}" >{{ $financial->financial_year }}</option>
                                            <option value="" >Select Financial Year</option>
                                                        @foreach($financialYears as $year )
                                                                @if($financial->financial_year != $year->id )
                                                                        <option value="{{ $year->id }}">{{ $year->financial_year }}</option>
                                                                @endif
                                                        @endforeach
                                        @else
                                            <option value="" >Select Financial Year</option>
                                        @endif
                                    </select>
                                </div>

                                <label class="form-label">No of post in organogram :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of post in organogram" name="no_of_post_in_organogram" value="{{ $staffInfoById->no_of_post_in_organogram }}" type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">No of post approved by UGC :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of post approved by UGC" name="no_of_post_approved_by_UGC" value="{{ $staffInfoById->no_of_post_approved_by_UGC }}" type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Date of approval by UGC :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Date of approval by UGC" name="date_of_approval_by_UGC" value="{{ $staffInfoById->date_of_approval_by_UGC }}" type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">No of post regular appointed :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of post regular appointed" name="no_of_post_regular_appointed" value="{{ $staffInfoById->no_of_post_regular_appointed }}" type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">No of post upgraded :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of post upgraded" name="no_of_post_upgraded" value="{{ $staffInfoById->no_of_post_upgraded }}" type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Name of post upgraded 1 :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Name of post upgraded 1" name="name_of_post_upgraded_1" value="{{ $staffInfoById->name_of_post_upgraded_1 }}" type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Name of post upgraded 2 :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Name of post upgraded 2" name="name_of_post_upgraded_2" value="{{ $staffInfoById->name_of_post_upgraded_2 }}" type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Name of post upgraded 3 :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Name of post upgraded 3"  name="name_of_post_upgraded_3" value="{{ $staffInfoById->name_of_post_upgraded_3 }}" type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Remarks :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <textarea class="custom-textarea" name="remarks">{{ $staffInfoById->remarks }}</textarea>
                                        <br>
                                        @if( Session::get('adminRole') == 'super-admin')
                                                <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg waves-effect z-depth-0" type="submit">Update It</button>
                                        @else
                                                @foreach($permissions as $permission )
                                                        @if($permission->permission == 'edit')
                                                                <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg waves-effect z-depth-0" type="submit">Update</button>
                                                        @endif
                                                @endforeach
                                        @endif
                                </div>

                            </div>
                </form>
                <!-- Table with panel -->
            </div>

        </div>
        <!--Grid row-->

        </div>
    </main>
@endsection
