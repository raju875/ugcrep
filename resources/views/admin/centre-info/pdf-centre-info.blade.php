
<div style="text-align: center; padding-top: 50px;font-family: arial, sans-serif;border-collapse: collapse;">
    <h2>Public University Management Information System (PUMIS)</h2>
    {{--if selected university names are same start--}}
    @if($uni==1 )
        @foreach ($centreInfos as $centreInfo )
            <?php $centreInfoById = \App\KosCentreInfo::find($centreInfo); ?>
            <?php $university = \App\KosUniversity::find($centreInfoById->university); ?>
            <h3>{{ $university->university }}</h3>
            @break
        @endforeach
    @endif
    {{--if selected university names are same start--}}
</div>
<style>
    table,th,td{
        border: 1px solid #ddd;
        border-collapse: collapse;
        font-size: 13px;

    }
    th{
        padding: 12px 0;
        text-align: center;
    }
</style>


<table width="100%">
    <thead>
    <tr>

        @if(Session::get('centreInfoUniversity'))
                @if($uni==0)
                    <th style=" ">{{ 'University' }}</th>
                @endif
                @endif


            @if(Session::get('centreInfoCentre'))
                <th style=" ">{{ 'Centre' }}</th>
            @endif

            @if(Session::get('centreInfoCentreDateOfApproval'))
                <th style=" ">{{ 'Centre date of approval' }}</th>
            @endif

            @if(Session::get('centreInfoDepartment'))
                <th style=" ">{{ 'Department' }}</th>
            @endif

            @if(Session::get('centreInfoPrograms'))
                <th style=" ">{{ 'Programs' }}</th>
            @endif

            @if(Session::get('centreInfoProgramDateOfApproval'))
                <th style=" ">{{ 'Program date of approval' }}</th>
            @endif

            @if(Session::get('centreInfoSession'))
                <th style=" ">{{ 'Session' }}</th>
            @endif

            @if(Session::get('centreInfoNoOfSeat'))
                <th style=" ">{{ 'No of seat' }}</th>
            @endif

            @if(Session::get('centreInfoEnrolledStudents'))
                <th style=" ">{{ 'Enrolled students' }}</th>
            @endif

            @if(Session::get('centreInfoMaleStudents'))
                <th style=" ">{{ 'Male students' }}</th>
            @endif

            @if(Session::get('centreInfoFemaleStudents'))
                <th style=" ">{{ 'Female students' }}</th>
            @endif

            @if(Session::get('centreInfoCurrentStudents'))
                <th style=" ">{{ 'Current students' }}</th>
            @endif

            @if(Session::get('centreInfoYear'))
                <th style=" ">{{ 'Year' }}</th>
            @endif

            @if(Session::get('centreInfoDateOfApproval'))
                <th style=" ">{{ 'Date of approval' }}</th>
            @endif

            @if(Session::get('centreInfoDateOfApproval'))
                <th style=" ">{{ 'Remarks' }}</th>
            @endif

    </tr>
    </thead>
    <tbody>

    @foreach($centreInfos as $centreInfo )
        <tr style=" ">

                <?php $centreInfoById = \App\KosCentreInfo::find($centreInfo); ?>
                @if(Session::get('centreInfoUniversity'))
                    @if($uni==0 )
                        {{--//if selected university name is not same--}}
                        <?php $university = \App\KosUniversity::find($centreInfoById->university); ?>
                        @if(!empty($university))
                            <td style=" ">{{ $university->university }}</td>
                            @else
                                <td style=" ">{{ 'null' }}</td>
                        @endif
                    @endif
                    @endif


                    @if(Session::get('centreInfoCentre'))
                        <?php $centre = \App\KosCentre::find($centreInfoById->centre); ?>
                        @if (!empty($centre))
                            <td style=" ">{{ $centre->centre }}</td>
                            @else
                                <td style=" ">{{ 'null' }}</td>
                        @endif
                    @endif


                    @if(Session::get('centreInfoCentreDateOfApproval'))
                        <td style=" ">{{ $centreInfoById->centre_date_of_approval }}</td>
                    @endif

                    @if(Session::get('centreInfoDepartment'))
                        <?php $department = \App\KosDepartment::find($centreInfoById->department); ?>
                        @if (!empty($department))
                            <td style=" ">{{ $department->department }}</td>
                            @else
                                <td style=" ">{{ 'null' }}</td>
                        @endif
                    @endif

                    @if(Session::get('centreInfoPrograms'))
                        <?php $program = \App\KosProgram::find($centreInfoById->programs); ?>
                        @if (!empty($program))
                            <td style=" ">{{ $program->program }}</td>
                            @else
                                <td style=" ">{{ 'null' }}</td>
                        @endif
                    @endif

                    @if(Session::get('centreInfoProgramDateOfApproval'))
                        @if($centreInfoById->program_date_of_approval == '')
                            <td style=" ">{{ '' }}</td>
                            @else
                        <td style=" ">{{ $centreInfoById->program_date_of_approval }}</td>
                            @endif
                    @endif

                    @if(Session::get('centreInfoSession'))
                            <?php $session = \App\KosSession::find($centreInfoById->session); ?>
                            @if (!empty($session))
                                <td style=" ">{{ $session->session }}</td>
                                @else
                                    <td style=" ">{{ 'Null' }}</td>
                            @endif
                        @endif

                    @if(Session::get('centreInfoNoOfSeat'))
                        @if( $centreInfoById->no_of_seat == '')
                            <td style=" ">{{ $centreInfoById->no_of_seat }}</td>
                        @else
                        <td style=" ">{{ $centreInfoById->no_of_seat }}</td>
                            @endif
                    @endif

                    @if(Session::get('centreInfoEnrolledStudents'))
                        @if($centreInfoById->enrolled_students=='')
                            <td style=" ">{{'' }}</td>
                        @else
                        <td style=" ">{{ $centreInfoById->enrolled_students }}</td>
                            @endif
                    @endif

                    @if(Session::get('centreInfoMaleStudents'))
                        @if($centreInfoById->male_students=='')
                            <td style=" ">{{'' }}</td>
                        @else
                            <td style=" ">{{ $centreInfoById->male_students }}</td>
                        @endif
                    @endif

                    @if(Session::get('centreInfoFemaleStudents'))
                        @if($centreInfoById->female_students=='')
                            <td style=" ">{{'' }}</td>
                        @else
                            <td style=" ">{{ $centreInfoById->female_students }}</td>
                        @endif
                    @endif

                    @if(Session::get('centreInfoCurrentStudents'))
                        @if($centreInfoById->current_students=='')
                            <td style=" ">{{'' }}</td>
                        @else
                            <td style=" ">{{ $centreInfoById->current_students }}</td>
                        @endif
                    @endif

                    @if(Session::get('centreInfoYear'))
                        @if($centreInfoById->year=='')
                            <td style=" ">{{'' }}</td>
                        @else
                            <td style=" ">{{ $centreInfoById->year }}</td>
                        @endif
                    @endif


                    @if(Session::get('centreInfoDateOfApproval'))
                        @if($centreInfoById->date_of_approval=='')
                            <td style=" ">{{'' }}</td>
                        @else
                            <td style=" ">{{ $centreInfoById->date_of_approval }}</td>
                        @endif
                    @endif

                    @if(Session::get('centreInfoRemarks'))
                        @if($centreInfoById->remarks=='')
                            <td style=" ">{{'' }}</td>
                        @else
                            <td style=" ">{{ $centreInfoById->remarks }}</td>
                        @endif
                        @endif
        </tr>

    @endforeach
    </tbody>
</table>
