@extends('admin.master')

@section('title')
    <title>Centre Info</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Add Centre Info</span>
                    </h4>
                    <a class="btn btn-outline-info btn-rounded btn-secondary btn-lg white-text" href="{{ url('/database/centre-info/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>

                </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <form class="col-md-12 mb-4" action="{{ url('/database/centre-info/add') }}" method="POST">

                @csrf
                <!-- Form with panel -->
                    <div class="card card-cascade narrower">

                        <div class="px-4">

                            <div class="table-wrapper">

                                <label class="form-label">University :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    @if(Session::get('adminRole') == 'super-admin')
                                    <select class="custom-select mdb-select" required name="university" searchable="Search here..">
                                        <?php $universities = \App\KosUniversity::all(); ?>
                                        <option value="" >Select University</option>
                                            @foreach($universities as $university )
                                                <option value="{{ $university->id }}">{{ $university->university }}</option>
                                                @endforeach
                                        <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                    </select>
                                        @else
                                        <select class="custom-select mdb-select" required name="university" searchable="Search here..">
                                            <?php $userId = Session::get('adminId'); ?>
                                            <?php $universityAccess = DB::table('kos_university_permissions')->where('role_id',$userId)->get(); ?>
                                            <option value="" >Select University</option>
                                            @foreach($universityAccess as $university )
                                                <?php  $universityById = \App\KosUniversity::find($university->university_id); ?>
                                                <option value="{{ $universityById->id }}">{{ $universityById->university }}</option>
                                            @endforeach
                                            <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                        </select>
                                    @endif
                                </div>

                                <label class="form-label">Centre :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="centre" searchable="Search here..">
                                        <?php $centres = \App\KosCentre::all(); ?>
                                        <option value="" >Select Centre</option>
                                            @foreach($centres as $centre )
                                                <option value="{{ $centre->id }}">{{ $centre->centre }}</option>
                                                @endforeach
                                    </select>
                                </div>


                                <label class="form-label">Centre date of approval :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="address"name="centre_date_of_approval" type="text" id="form1" class="form-control custom-input">
                                    <span style="color: red">{{ $errors->has('address') ? $errors->first('address') : ' ' }}</span>
                                </div>

                                <label class="form-label">Programs :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <?php $programs = DB::table('kos_programs')->get();?>
                                    <select class="custom-select mdb-select"  name="programs" searchable="Search here..">
                                        <option value="" >Select Progrem</option>
                                        @foreach($programs as $program )
                                            <option value="{{ $program->id }}">{{ $program->program }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <label class="form-label">Department :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <?php $departments = DB::table('kos_departments')->get();?>
                                    <select class="custom-select mdb-select"  name="department" searchable="Search here..">
                                        <option value="" >Select Department</option>
                                        @foreach($departments as $department )
                                            <option value="{{ $department->id }}">{{ $department->department }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <label class="form-label">Program date of approval :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Program date of approval" type="text" name="program_date_of_approval" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Session :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <?php $sessions = DB::table('kos_sessions')->get();?>
                                    <select class="custom-select mdb-select"  name="sessions" searchable="Search here..">
                                        <option value="" >Select Session</option>
                                        @foreach($sessions as $session )
                                            <option value="{{ $session->id }}">{{ $session->session }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <label class="form-label">No of seat :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of seat" type="number" name="no_of_seat" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Enrolled students :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Enrolled students" type="number" name="enrolled_students" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Male students :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Male students" type="number" name="male_students" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Female students :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Female students" type="number" name="female_students" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Current students :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Current students" type="number" name="current_students" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Year :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <select class="custom-select mdb-select"  name="year" searchable="Search here..">
                                        <option value="1st_year" >1st year</option>
                                        <option value="2nd_year" >2nd year</option>
                                        <option value="3rd_year" >3rd year</option>
                                        <option value="4th_year" >4th year</option>
                                        <option value="Internee" >Internee</option>
                                    </select>
                                </div>

                                <label class="form-label">Date of approval :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input  placeholder="Date of approval" type="text" name="date_of_approval" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Remarks :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <textarea class="custom-textarea" name="remarks"></textarea>
                                </div>

                                    <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg waves-effect z-depth-0" type="submit">Save</button>

                        </div>

                    </div>
                    </div>
                </form>
                <!-- Table with panel -->
            </div>

        </div>
        <!--Grid row-->

        </div>
    </main>
@endsection
