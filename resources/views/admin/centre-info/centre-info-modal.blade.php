@extends('admin.master')

@section('title')
    <title>Centre Info</title>
@endsection

@section('body')
    <div class="content-div">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->


                <div class="card-body d-sm-flex justify-content-between">


                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Centre Info</span>
                    </h4>

                    <div class="right-align">
                        <input placeholder="Search One" type="text" id="search-default">
                        <input placeholder="Search Two" type="text" id="search-filter">
                    </div>


                </div>

            </div>
            <!-- Button trigger modal -->
            <button type="button" class="mb-lg-3 btn btn-primary btn-md show-field-btn" data-toggle="modal" data-target="#exampleModalCenter">
                Show Field
            </button>
            <div class="clr"></div>

            <!-- Modal -->
            <div class="centre modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content m-lg-5">
                        <form action="{{ url('/database/centre-info/modal-listing') }}" method="POST">
                            @csrf
                            <ul style="list-style-type: none">
                                <li>
                                    <input type="checkbox" class="allModal" > Select All
                                </li>
                                <li>
                                    @if(Session::has('centreInfoUniversity'))
                                        <input type="checkbox" id="university" class="singelModal" value="university" name="column[]" checked="checked"> University
                                    @else
                                        <input type="checkbox" class="singelModal"  id="university" value="university" name="column[]"> University
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoCentre'))
                                        <input type="checkbox" class="singelModal"  id="centre" value="centre" name="column[]" checked="checked"> Centre
                                    @else
                                        <input type="checkbox" class="singelModal"  id="centre" value="centre" name="column[]"> Centre
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoCentreDateOfApproval'))
                                        <input type="checkbox" class="singelModal"  id="centre_date_of_approval" value="centre_date_of_approval" name="column[]" checked="checked"> Centre date of approval
                                    @else
                                        <input class="singelModal"  type="checkbox" id="centre_date_of_approval" value="centre_date_of_approval" name="column[]"> Centre date of approval
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoDepartment'))
                                        <input class="singelModal"  type="checkbox" id="department" value="department" name="column[]" checked="checked"> Department
                                    @else
                                        <input class="singelModal"  type="checkbox" id="department" value="department" name="column[]"> Department
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoPrograms'))
                                        <input class="singelModal"  type="checkbox" id="programs" value="programs" name="column[]" checked="checked"> Programs
                                    @else
                                        <input class="singelModal"  type="checkbox" id="programs" value="programs" name="column[]"> Programs
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoProgramDateOfApproval'))
                                        <input  class="singelModal" type="checkbox" id="program_date_of_approval" value="program_date_of_approval" name="column[]" checked="checked"> Program date of approval
                                    @else
                                        <input class="singelModal"  type="checkbox" id="program_date_of_approval" value="program_date_of_approval" name="column[]"> Program date of approval
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoSession'))
                                        <input class="singelModal"  type="checkbox" id="session" value="session" name="column[]" checked="checked"> Session
                                    @else
                                        <input class="singelModal"  type="checkbox" id="session" value="session" name="column[]"> Session
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('facultyInfoNoOfSeat'))
                                        <input class="singelModal"  type="checkbox" id="no_of_seat" value="no_of_seat" name="column[]" checked="checked"> No of Seat
                                    @else
                                        <input class="singelModal"  type="checkbox" id="no_of_seat" value="no_of_seat" name="column[]"> No of Seat
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoEnrolledStudents'))
                                        <input  class="singelModal" type="checkbox" id="enrolled_students" value="enrolled_students" name="column[]" checked="checked"> Enrolled students
                                    @else
                                        <input class="singelModal"  type="checkbox" id="enrolled_students" value="enrolled_students" name="column[]"> Enrolled students
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoMaleStudents'))
                                        <input class="singelModal"  type="checkbox" id="male_students" value="male_students" name="column[]" checked="checked"> Male students
                                    @else
                                        <input  class="singelModal" type="checkbox" id="male_students" value="male_students" name="column[]"> Male students
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoFemaleStudents'))
                                        <input  class="singelModal" type="checkbox" id="female_students" value="female_students" name="column[]" checked="checked"> Female students
                                    @else
                                        <input class="singelModal"  type="checkbox" id="female_students" value="female_students" name="column[]"> Female students
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoCurrentStudents'))
                                        <input class="singelModal"  type="checkbox" id="current_students" value="current_students" name="column[]" checked="checked"> Current students
                                    @else
                                        <input class="singelModal"  type="checkbox" id="current_students" value="current_students" name="column[]"> Current students
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoYear'))
                                        <input class="singelModal"  type="checkbox" id="year" value="year" name="column[]" checked="checked"> Year
                                    @else
                                        <input class="singelModal"  type="checkbox" id="year" value="year" name="column[]"> Year
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoDateOfApproval'))
                                        <input class="singelModal"  type="checkbox" id="date_of_approval" value="date_of_approval" name="column[]" checked="checked"> Date of approval
                                    @else
                                        <input class="singelModal"  type="checkbox" id="date_of_approval" value="date_of_approval" name="column[]"> Date of approval
                                    @endif
                                </li>

                                <li>
                                    @if(Session::has('centreInfoRemarks'))
                                        <input class="singelModal"  type="checkbox" id="remarks" value="remarks" name="column[]" checked="checked"> Remarks
                                    @else
                                        <input class="singelModal"  type="checkbox" id="remarks" value="remarks" name="column[]"> Remarks
                                    @endif
                                </li>
                            </ul>

                            <button style="width: 150px;margin-left: 35px;" type="submit"
                                    class="btn btn-secondary">
                                Save Changes
                            </button>
                        </form>
                    </div>

                </div>
            </div>
            <!-- Heading -->
            @if(Session::has('message'))
                <h4 class="text text-center text-success">{{ Session::get('message') }}</h4>
            @endif
            @if(Session::has('alert'))
                <h4 class="text text-center text-danger">{{ Session::get('alert') }}</h4>
        @endif
        <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Table with panel -->
                    <div class="card card-cascade narrower">
                        <!--Form Starts-->
                        <form action="{{ url('/database/centre-info/edit') }}" method="POST">
                        @csrf

                        <!--Card image-->
                            <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">
                                @if( Session::get('adminRole') == 'super-admin')
                                    <?php $count = DB::table('kos_centre_infos')->count(); ?>
                                    <label>Total Items {{ $count }}</label>
                                    <div>
                                        <a href="{{ url('/database/centre-info/form') }}" title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i class="custom-fontawesome fas fa-plus-circle"></i>
                                        </a>
                                        <button type="submit" name="submit" value="report" title="Report" class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Report" class="custom-fontawesome fas fa-pen-square"></i>
                                        </button>
                                        <button type="submit" value="copy" name="submit" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                        </button>
                                        <button type="submit" value="delete" name="submit" onclick="return confirm('Are you sure to Delete it !!!')" title="Delete" class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                        </button>

                                    </div>
                                @else
                                    <?php $count = DB::table('kos_centre_infos')
                                        ->join('kos_university_permissions', 'kos_centre_infos.university', '=', 'kos_university_permissions.university_id')
                                        ->join('kos_universities', 'kos_centre_infos.university', '=', 'kos_universities.id')
                                        ->where('kos_university_permissions.role_id', Session::get('adminId'))
                                        ->count()?>

                                    <label for="">Total Items {{ $count }}</label>
                                    <div>
                                        <?php $userId       = Session::get('adminId');?>
                                        <?php $permissions  = \App\KosPermission::where('role_id',$userId)->get();?>
                                        @foreach($permissions as $permission )
                                            @if($permission->permission == 'add')
                                                <a href="{{ url('/database/centre-info/form') }}" title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i class="custom-fontawesome fas fa-plus-circle"></i>
                                                </a>
                                            @endif
                                            @if($permission->permission == 'report')
                                                <button target="_blank" type="submit" name="submit" value="report" title="Report" class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i title="Report" class="custom-fontawesome fas fa-pen-square"></i>
                                                </button>
                                            @endif
                                            @if($permission->permission == 'copy')
                                                <button type="submit" value="copy" name="submit" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                                </button>
                                            @endif
                                            @if($permission->permission == 'delete')
                                                <button type="submit" value="delete" name="submit" onclick="return confirm('Are you sure to Delete it !!!')" title="Delete" class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                                </button>
                                            @endif
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                            <!--/Card image-->


                            <div class="px-4">

                                <div class="table-wrapper table-responsive">
                                    <!--Table-->

                                    {{--**************For Super Admin start here**************--}}

                                    @if(Session::get('adminRole') == 'super-admin')

                                        <table id="datatable" class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                {{--this is hidden column by default checked all--}}
                                                <th style="display: none;"><input type="checkbox" checked class="selectall"></th>
                                                {{--this is hidden column by default checked --}}

                                                <th><input type="checkbox"  class="all"></th>
                                                <th class="university">University</th>
                                                <th class="centre">Centre</th>
                                                <th class="centre_date_of_approval">Centre date of approval</th>
                                                <th class="department">Department</th>
                                                <th class="programs">Programs</th>
                                                <th class="program_date_of_approval">Program date of approval</th>
                                                <th class="session">Session</th>
                                                <th class="no_of_seat">No of seat</th>
                                                <th class="enrolled_students">Enrolled students</th>
                                                <th class="male_students">Male students</th>
                                                <th class="female_students">Female students</th>
                                                <th class="current_students">Current students</th>
                                                <th class="year">Year</th>
                                                <th class="date_of_approval">Date of approval</th>
                                                <th class="remarks">Remarks</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $centreInfos = DB::table('kos_centre_infos')->orderBy('id', 'desc')->paginate(100);?>
                                            @foreach($centreInfos as $info )
                                                <tr>
                                                    {{--this is hidden column by default checked all--}}
                                                    <td style="display: none;"><input checked type="checkbox" class="individual" name="centre_info_hide[]" value="{{ $info->id }}"></td>

                                                    <td><input type="checkbox" class="single" name="centre_info[]" value="{{ $info->id }}"></td>

                                                    <td class="university">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            <?php $uni = \App\KosUniversity::find($info->university)?>
                                                            @if(!empty($uni))
                                                                {{ $uni->university }}
                                                            @else
                                                                {{ 'Null' }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="centre">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            <?php $cen = \App\KosCentre::find($info->centre); ?>
                                                            @if(!empty($cen))
                                                                {{ $cen ->centre }}
                                                            @else
                                                                {{ 'null' }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="centre_date_of_approval">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            {{ $info->centre_date_of_approval }}

                                                        </a>
                                                    </td>

                                                    <td class="department">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            <?php $dept = \App\KosDepartment::find($info->department); ?>
                                                            @if(!empty($dept))
                                                                {{ $dept ->department }}
                                                            @else
                                                                {{ 'null' }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="programs">

                                                        <?php $program = \App\KosProgram::find($info->programs)?>
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            @if(!empty($program))
                                                                {{ $program->program }}
                                                            @else
                                                                {{ 'Null' }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="program_date_of_approval">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            @if($info->program_date_of_approval == 'null')
                                                            @else
                                                                {{ $info->program_date_of_approval }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="session">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            <?php $session = \App\KosSession::find($info->session)?>
                                                            @if(!empty($session))
                                                                {{ $session->session }}
                                                            @else
                                                                {{ 'Null' }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="no_of_seat">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            {{ $info->no_of_seat }}
                                                        </a>
                                                    </td>

                                                    <td class="enrolled_students">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            {{ $info->enrolled_students }}
                                                        </a>
                                                    </td>

                                                    <td class="male_students">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            {{ $info->male_students }}
                                                        </a>
                                                    </td>

                                                    <td class="female_students">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            {{ $info->female_students }}
                                                        </a>
                                                    </td>

                                                    <td class="current_students">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            {{ $info->current_students }}
                                                        </a>
                                                    </td>

                                                    <td class="year">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            {{ $info->year }}
                                                        </a>
                                                    </td>
                                                    <td class="date_of_approval">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            {{ $info->date_of_approval }}
                                                        </a>
                                                    </td>

                                                    <td class="remarks">
                                                        <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                            {{ $info->remarks }}
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                        {{ $centreInfos ->links() }}
                                        {{--**************For Super Admin end here**************--}}


                                        {{--************For Normal User or Admin start here**************--}}
                                    @else

                                        <table id="datatable" class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                {{--this is hidden column by default checked all--}}
                                                <th style="display: none;"><input type="checkbox" checked class="selectall"></th>
                                                {{--this is hidden column by default checked all--}}

                                                <th><input type="checkbox"  class="all"></th>
                                                <th class="university">University</th>
                                                <th class="centre">Centre</th>
                                                <th class="centre_date_of_approval">Centre date of approval</th>
                                                <th class="department">Department</th>
                                                <th class="programs">Programs</th>
                                                <th class="program_date_of_approval">Program date of approval</th>
                                                <th class="session">Session</th>
                                                <th class="no_of_seat">No of seat</th>
                                                <th class="enrolled_students">Enrolled students</th>
                                                <th class="male_students">Male students</th>
                                                <th class="female_students">Female students</th>
                                                <th class="current_students">Current students</th>
                                                <th class="year">Year</th>
                                                <th class="date_of_approval">Date of approval</th>
                                                <th class="remarks">Remarks</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php $centreInfos = DB::table('kos_centre_infos')
                                                ->join('kos_university_permissions', 'kos_centre_infos.university', '=', 'kos_university_permissions.university_id')
                                                ->join('kos_universities', 'kos_centre_infos.university', '=', 'kos_universities.id')
                                                ->where('kos_university_permissions.role_id', Session::get('adminId'))
                                                ->select('kos_centre_infos.*', 'kos_universities.university')->orderBy('id', 'desc')
                                                ->paginate(100)?>

                                            @foreach($centreInfos as $info )
                                                {{--this is hidden column by default checked all--}}
                                                {{--this is hidden column by default checked all--}}
                                                <td style="display: none;"><input checked type="checkbox" class="individual" name="centre_info_hide[]" value="{{ $info->id }}"></td>

                                                <td><input type="checkbox" class="single" name="centre_info[]" value="{{ $info->id }}"></td>

                                                <td class="university">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->university }}
                                                    </a>
                                                </td>

                                                <td class="centre">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info ->centre }}
                                                    </a>
                                                </td>

                                                <td class="centre_date_of_approval">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->centre_date_of_approval }}

                                                    </a>
                                                </td>

                                                <td class="department">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info ->department }}
                                                    </a>
                                                </td>

                                                <td class="programs">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->programs }}
                                                    </a>
                                                </td>

                                                <td class="program_date_of_approval">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->program_date_of_approval }}
                                                    </a>
                                                </td>

                                                <td class="session">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->session }}
                                                    </a>
                                                </td>

                                                <td class="no_of_seat">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->no_of_seat }}
                                                    </a>
                                                </td>

                                                <td class="enrolled_students">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->enrolled_students }}
                                                    </a>
                                                </td>

                                                <td class="male_students">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->male_students }}
                                                    </a>
                                                </td>

                                                <td class="female_students">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->female_students }}
                                                    </a>
                                                </td>

                                                <td class="current_students">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->current_students }}
                                                    </a>
                                                </td>

                                                <td class="year">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->year }}
                                                    </a>
                                                </td>
                                                <td class="date_of_approval">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->date_of_approval }}
                                                    </a>
                                                </td>

                                                <td class="remarks">
                                                    <a href="{{ url('/database/centre-info/edit/'.$info->id) }}">
                                                        {{ $info->remarks }}
                                                    </a>
                                                </td>

                                                </tr>

                                            @endforeach

                                            </tbody>
                                        </table>
                                        {{ $centreInfos ->links() }}
                                        {{--************For Normal User or Admin end here**************--}}
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




