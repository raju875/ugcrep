@extends('admin.master')

@section('title')
    <title>Centre Info</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Edit Centre Info</span>
                    </h4>

                    <a class="btn btn-outline-info btn-rounded btn-secondary btn-lg white-text" href="{{ url('/database/centre-info/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>

                </div>

            </div>
            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <form class="col-md-12 mb-4" action="{{ url('/database/centre-info/update') }}" method="POST">

                @csrf
                <!-- Form with panel -->
                    <div class="card card-cascade narrower">
                        <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">
                            @if( Session::get('adminRole') == 'super-admin')
                                <div class="button-section">

                                    <div>
                                        <a href="{{ url('/database/centre-info/form') }}" title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i class="custom-fontawesome fas fa-plus-circle"></i>
                                        </a>
                                        <a href="{{ url('/database/centre-info/copy/'.$centreInfoById->id) }}" title="Add" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                        </a>
                                        <a href="{{ url('/database/centre-info/delete/'.$centreInfoById->id) }}" value="delete" onclick="return confirm('Are you sure to delete it !!!')" title="Delete" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                        </a>
                                    </div>
                                </div>
                            @else
                                <div class="button-section">
                                    <?php $userId       = Session::get('adminId');?>
                                    <?php $permissions  = \App\KosPermission::where('role_id',$userId)->get();?>
                                    <div>
                                        @foreach($permissions as $permission )
                                            @if($permission->permission == 'add' )
                                                <a href="{{ url('/database/centre-info/form') }}" title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i class="custom-fontawesome fas fa-plus-circle"></i>
                                                </a>
                                            @endif
                                            @if($permission->permission == 'copy' )
                                                <a href="{{ url('/database/centre-info/copy/'.$centreInfoById->id) }}" title="Add" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                                </a>
                                            @endif
                                            @if($permission->permission == 'delete' )
                                                <a href="{{ url('/database/centre-info/delete/'.$centreInfoById->id) }}" value="delete" onclick="return confirm('Are you sure to delete it !!!')" title="Delete" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                                </a>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>


                        <div class="px-4">

                            <div class="table-wrapper">

                                <input style="display: none" name="id" value="{{ $centreInfoById->id }}"  class="form-control">

                                <label class="form-label">University :</label>
                                <!-- Material input -->
                                <div class="md-form">

                                    {{--if user is Spuer Admin start here--}}
                                  @if( Session::get('adminRole') == 'super-admin')
                                        <select class="mdb-select" required name="university" searchable="Search here..">
                                            <?php $universities = \App\KosUniversity::all(); ?>
                                            <?php $uni = \App\KosUniversity::where('id',$centreInfoById->university)->first();?>
                                            @if (!empty($uni))
                                                    <option value="{{ $uni->id }}" >{{ $uni->university }}</option>
                                                    <option value="" >Select University</option>
                                                    @foreach($universities as $university )
                                                        @if($uni->id != $university->id )
                                                            <option value="{{ $university->id }}">{{ $university->university }}</option>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <select class="custom-select mdb-select" required name="university" searchable="Search here..">
                                                        <?php $universities = \App\KosUniversity::all(); ?>
                                                        <option value="" >Select University</option>
                                                            @foreach($universities as $university )
                                                                <option value="{{ $university->id }}">{{ $university->university }}</option>
                                                            @endforeach
                                                            <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                                    </select>
                                                @endif
                                                <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                        </select>
                                        {{--if user is Spuer Admin end here--}}


                                        {{--if user is not Spuer Admin start here--}}
                                    @else
                                        <select class="mdb-select" required name="university" searchable="Search here..">
                                            <?php $universities = \App\KosUniversityPermission::where('role_id',Session::get('adminId'))->get(); ?>
                                            <?php $uni = \App\KosUniversity::where('id',$centreInfoById->university)->first();?>
                                            @if (!empty($uni))
                                                <option value="{{ $uni->id }}" >{{ $uni->university }}</option>
                                                <option value="" >Select University</option>
                                                @foreach($universities as $university )
                                                    @if($uni->id != $university->university_id )
                                                        <?php $versity =\App\KosUniversity::find($university->university_id);?>
                                                        <option value="{{ $versity->id }}">{{ $versity->university }}</option>
                                                    @endif
                                                @endforeach
                                            @else
                                                <select class="custom-select mdb-select" required name="university" searchable="Search here..">
                                                    <option value="" >Select University</option>
                                                    <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                                </select>
                                            @endif
                                            <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                        </select>
                                        {{--if user is not Spuer Admin end here--}}

                                    @endif
                                </div>

                                <label class="form-label">Centre :</label>
                                <div class="md-form">
                                    <select class="mdb-select" required name="centre" searchable="Search here..">
                                        <?php $centres = \App\KosCentre::all(); ?>
                                        <?php $cntr = \App\KosCentre::where('id',$centreInfoById->centre)->first()?>
                                            @if(!empty($cntr))
                                            <option value="{{ $cntr->id }}" >{{ $cntr->centre }}</option>
                                        <option value="" >Select Centre</option>
                                        @foreach($centres as $centre )
                                            @if($centreInfoById->centre != $centre->id )
                                                <option value="{{ $centre->id }}">{{ $centre->centre }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @else
                                            <?php $centres = \App\KosCentre::all(); ?>
                                            <option value="" >Select Centre</option>
                                            @foreach($centres as $centre )
                                                <option value="{{ $centre->id }}">{{ $centre->centre }}</option>
                                            @endforeach
                                        @endif
                                </div>


                                <label class="form-label">Centre date of approval :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="address"name="centre_date_of_approval" value="{{ $centreInfoById->centre_date_of_approval }}" type="text" id="form1" class="form-control">
                                    <span style="color: red">{{ $errors->has('address') ? $errors->first('address') : ' ' }}</span>
                                </div>

                                <label class="form-label">Programs :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <?php $programs = DB::table('kos_programs')->get();?>
                                    <?php $prgm = \App\KosProgram::where('id',$centreInfoById->programs )->first()?>
                                        @if(!empty($prgm))
                                    <select class="mdb-select"  name="programs" searchable="Search here..">
                                            <option value="{{ $prgm->id }}" >{{ $prgm->program }}</option>
                                            <option value="" >Select Program</option>
                                        @foreach($programs as $program )
                                            @if($prgm->id != $program->id )
                                                <option value="{{ $program->id }}">{{ $program->program }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                            @else
                                            <select class="custom-select mdb-select"  name="programs" searchable="Search here..">
                                                <option value="" >Select Progrem</option>
                                                @foreach($programs as $program )
                                                    <option value="{{ $program->id }}">{{ $program->program }}</option>
                                                @endforeach
                                            </select>
                                            @endif
                                </div>

                                <label class="form-label">Department :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <?php $departments = DB::table('kos_departments')->get();?>
                                    <?php $dpt = \App\KosDepartment::where('id',$centreInfoById->department )->first()?>
                                        @if(!empty($dpt))
                                    <select class="mdb-select"  name="department" searchable="Search here..">
                                            <option value="{{ $dpt->id }}" >{{ $dpt->department }}</option>
                                            <option value="" >Select Department</option>
                                        @foreach($departments as $department )
                                            @if($dpt->id != $department->id )
                                                <option value="{{ $department->id }}">{{ $department->department }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                            @else
                                            <?php $departments = DB::table('kos_departments')->get();?>
                                            <select class="custom-select mdb-select"  name="department" searchable="Search here..">
                                                <option value="" >Select Department</option>
                                                @foreach($departments as $department )
                                                    <option value="{{ $department->id }}">{{ $department->department }}</option>
                                                @endforeach
                                            </select>
                                    @endif
                                </div>

                                <label class="form-label">Program date of approval :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Program date of approval" type="text" name="program_date_of_approval" value="{{ $centreInfoById->program_date_of_approval }}" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Session :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <?php $sessions = DB::table('kos_sessions')->get();?>
                                    <?php $ssn = \App\KosSession::where('id',$centreInfoById->session )->first()?>
                                    @if(!empty($ssn))
                                        <select class="mdb-select"  name="department" searchable="Search here..">
                                                <option value="{{ $ssn->id }}" >{{ $ssn->session }}</option>
                                            <option value="" >Select Session</option>
                                            @foreach($sessions as $session )
                                                @if($ssn->id != $session->id )
                                                    <option value="{{ $session->id }}">{{ $session->session }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @else
                                            <?php $sessions = DB::table('kos_sessions')->get();?>
                                    <select class="custom-select mdb-select"  name="sessions" searchable="Search here..">
                                        <option value="" >Select Session</option>
                                        @foreach($sessions as $session )
                                            <option value="{{ $session->id }}">{{ $session->session }}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                </div>


                                <label class="form-label">No of seat :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of seat" type="number" name="no_of_seat" value="{{ $centreInfoById->no_of_seat }}" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Enrolled students :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Enrolled students" type="number" name="enrolled_students"  value="{{ $centreInfoById->enrolled_students }}" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Male students :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Male students" type="number" name="male_students"  value="{{ $centreInfoById->male_students }}" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Female students :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Female students" type="number" name="female_students"  value="{{ $centreInfoById->female_students }}" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Current students :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Current students" type="number" name="current_students"  value="{{ $centreInfoById->current_students }}" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Year :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <select class="mdb-select"  name="year" searchable="Search here..">
                                        <option value="1st_year" >1st year</option>
                                        <option value="2nd_year" >2nd year</option>
                                        <option value="3rd_year" >3rd year</option>
                                        <option value="4th_year" >4th year</option>
                                        <option value="Internee" >Internee</option>
                                    </select>
                                </div>

                                <label class="form-label">Date of approval :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Date of approval" type="text" name="date_of_approval"  value="{{ $centreInfoById->date_of_approval }}" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Remarks :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <textarea class="custom-textarea"  name="remarks">{{$centreInfoById->remarks}}</textarea>

                                </div>
                                @if( Session::get('adminRole') == 'super-admin')
                                    <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg waves-effect z-depth-0" type="submit">Update It</button>
                                @else
                                    @foreach($permissions as $permission )
                                        @if($permission->permission == 'edit')
                                            <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg waves-effect z-depth-0" type="submit">Update</button>
                                        @endif
                                    @endforeach
                                @endif

                            </div>
                        </div>

                    </div>
                </form>
                <!-- Table with panel -->
            </div>

        </div>
        <!--Grid row-->

        </div>
    </main>
@endsection
