@extends('admin.master')

@section('title')
    <title>Center Info</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Centre Info</span>
                    </h4>

                    <a href="{{ url('/database/centre-info/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>

                </div>

            </div>
            @if(Session::has('message'))
                <h5 class="text text-center text-success">{{ Session::get('message') }}</h5>
            @endif
            @if(Session::has('alert'))
                <h5 class="text text-center text-danger">{{ Session::get('alert') }}</h5>
        @endif
        <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Table with panel -->
                    <div class="card card-cascade narrower">
                        <!--Form Starts-->
                        <form action="{{ url('/database/centre-info/pdf') }}" method="POST">
                        @csrf
                        <!--Card image-->
                            <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">

                                {{--<div>--}}
                                <?php $count = 0 ?>
                                @foreach($InfoId as $info )
                                    <?php $count++ ?>
                                @endforeach
                                <label>Items Select {{ $count }}</label>


                                <div>
                                    <button type="submit" value="pdf" name="submit" onclick="return confirm('Are you sure to generate pdf!!!')" title="pdf" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                        <i  title="pdf" class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                    </button>
                                </div>

                            </div>
                            <!--/Card image-->


                            <div class="px-4">

                                <div class="table-wrapper table-responsive">
                                    <!--Table-->

                                    <table id="datatable" class="table display" border="1">
                                        <thead>
                                        <tr>
                                            <th><input type="checkbox" class="selectall"></th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="university" checked>University</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="centre" checked>Centre</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="centre_date_of_approval" checked>Centre date of approval</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="department" checked>Department</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="programs" checked>Programs</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="program_date_of_approval" checked>Program date of approval</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="session" checked>Session</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="no_of_seat" checked>No of seat</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="enrolled_students" checked>Enrolled students</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="male_students" checked>Male students</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="female_students" checked>Female students</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="current_students" checked>Current students</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="year" checked>Year</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="date_of_approval" checked>Date of approval</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="remarks" checked>Remarks</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($InfoId as $infoId )
                                            <?php $infoById = DB::table('kos_centre_infos')->where('id',$infoId)->get()?>

                                            @foreach($infoById as $info )
                                            <tr>
                                                <td><input checked type="checkbox" class="individual" name="checkbox[]" value="{{ $info->id }}"></td>

                                                <td>
                                                        <?php $university = DB::table('kos_universities')->where('id',$info->university)->get()?>
                                                        @foreach($university as $uni)
                                                            {{ $uni->university }}
                                                        @endforeach
                                                </td>


                                                <td>
                                                        <?php $centre = DB::table('kos_centres')->where('id',$info->centre)->get()?>
                                                    @if(!$centre->isEmpty())
                                                        @foreach($centre as $centre)
                                                            {{ $centre->centre }}
                                                        @endforeach
                                                        @else
                                                    {{ 'Null' }}
                                                        @endif
                                                </td>


                                                <td>
                                                        {{ $info->centre_date_of_approval }}
                                                </td>


                                                <td>
                                                    <?php $department = DB::table('kos_departments')->where('id',$info->department)->get()?>
                                                    @if(!$department->isEmpty())
                                                        @foreach($department as $department)
                                                            {{ $department->department }}
                                                        @endforeach
                                                            @else
                                                                {{ 'Null' }}
                                                            @endif
                                                </td>

                                                <td>
                                                    <?php $program = DB::table('kos_programs')->where('id',$info->programs)->get()?>
                                                    @if(!$program->isEmpty())
                                                        @foreach($program as $program)
                                                            {{ $program->program }}
                                                        @endforeach
                                                        @else
                                                            {{ 'Null' }}
                                                        @endif
                                                </td>

                                                <td>
                                                        {{ $info->	program_date_of_approval }}
                                                </td>

                                                <td>
                                                    <?php $session = DB::table('kos_sessions')->where('id',$info->session)->get()?>
                                                    @if(!$session->isEmpty())
                                                        @foreach($session as $session)
                                                            {{ $session->session }}
                                                        @endforeach
                                                        @else
                                                            {{ 'Null' }}
                                                        @endif
                                                </td>

                                                <td>
                                                        {{ $info->no_of_seat }}
                                                </td>

                                                <td>
                                                        {{ $info->enrolled_students }}
                                                </td>


                                                <td>
                                                        {{ $info->male_students }}
                                                </td>

                                                <td>
                                                        {{ $info->female_students }}
                                                </td>

                                                <td>
                                                        {{ $info->current_students }}
                                                </td>

                                                <td>
                                                        {{ $info->year }}
                                                </td>

                                                <td>
                                                        {{ $info->date_of_approval }}
                                                </td>

                                                <td>
                                                        {{ $info->remarks }}
                                                </td>

                                            </tr>
                                                @endforeach
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection


