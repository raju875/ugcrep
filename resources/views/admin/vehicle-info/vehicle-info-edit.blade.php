@extends('admin.master')

@section('title')
    <title>Vehicle Info</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Edit Vehicle Info</span>
                    </h4>

                    <a style="margin: 0 auto" href="{{ url('/database/vehicle-info/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>
                    @if( Session::get('adminRole') == 'super-admin')
                        <div class="button-section">

                            <div>
                                <a href="{{ url('/database/vehicle-info/form') }}" title="Add" class="btn-secondary btn btn-outline-white btn-rounded btn-sm px-2">
                                    <i class="custom-fontawesome fas fa-plus-circle"></i>
                                </a>

                                <a href="{{ url('/database/vehicle-info/copy/'.$vehicleInfoById->id) }}" title="Add" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="btn-secondary btn btn-outline-white btn-rounded btn-sm px-2">
                                    <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                </a>
                                <a href="{{ url('/database/vehicle-info/delete/'.$vehicleInfoById->id) }}" value="delete" onclick="return confirm('Are you sure to delete it !!!')" title="Delete" class="btn-secondary btn btn-outline-white btn-rounded btn-sm px-2">
                                    <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                </a>
                            </div>
                        </div>
                    @else
                        <div class="button-section">
                            <?php $userId       = Session::get('adminId');?>
                            <?php $permissions  = \App\KosPermission::where('role_id',$userId)->get();?>
                            <div>
                                @foreach($permissions as $permission )
                                    @if($permission->permission == 'add' )
                                        <a href="{{ url('/database/vehicle-info/form') }}" title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i class="fas fa-plus-circle"></i>
                                        </a>
                                    @endif
                                    @if($permission->permission == 'copy' )
                                        <a href="{{ url('/database/vehicle-info/copy/'.$vehicleInfoById->id) }}" title="Add" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Copy" class="far fa-clone"></i>
                                        </a>
                                    @endif
                                    @if($permission->permission == 'delete' )
                                        <a href="{{ url('/database/vehicle-info/delete/'.$vehicleInfoById->id) }}" value="delete" onclick="return confirm('Are you sure to delete it !!!')" title="Delete" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Delete" class="far fa-trash-alt"></i>
                                        </a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>

            </div>
            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <form class="col-md-12 mb-4" action="{{ url('/database/vehicle-info/update') }}" method="POST">

                @csrf
                <!-- Form with panel -->
                    <div class="card card-cascade narrower">

                        <div class="px-4">

                            <div class="table-wrapper">

                                <input style="display: none" name="id" value="{{ $vehicleInfoById->id }}"  class="form-control">

                                <label class="form-label">University :</label>
                                <!-- Material input -->
                                <div class="md-form">

                                    {{--if user is Spuer Admin start here--}}
                                    @if( Session::get('adminRole') == 'super-admin')
                                        <select class="mdb-select" required name="university" searchable="Search here..">
                                            <?php $universities = \App\KosUniversity::all(); ?>
                                            <?php $uni = \App\KosUniversity::where('id',$vehicleInfoById->university)->first();?>
                                            @if (!empty($uni))
                                                <option value="{{ $uni->id }}" >{{ $uni->university }}</option>
                                                <option value="" >Select University</option>
                                                @foreach($universities as $university )
                                                    @if($uni->id != $university->id )
                                                        <option value="{{ $university->id }}">{{ $university->university }}</option>
                                                    @endif
                                                @endforeach
                                            @else
                                                <select class="custom-select mdb-select" required name="university" searchable="Search here..">
                                                    <?php $universities = \App\KosUniversity::all(); ?>
                                                    <option value="" >Select University</option>
                                                    @foreach($universities as $university )
                                                        <option value="{{ $university->id }}">{{ $university->university }}</option>
                                                    @endforeach
                                                    <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                                </select>
                                            @endif
                                            <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                        </select>
                                        {{--if user is Spuer Admin end here--}}


                                        {{--if user is not Spuer Admin start here--}}
                                    @else
                                        <select class="mdb-select" required name="university" searchable="Search here..">
                                            <?php $universities = \App\KosUniversityPermission::where('role_id',Session::get('adminId'))->get(); ?>
                                            <?php $uni = \App\KosUniversity::where('id',$vehicleInfoById->university)->first();?>
                                            @if (!empty($uni))
                                                <option value="{{ $uni->id }}" >{{ $uni->university }}</option>
                                                <option value="" >Select University</option>
                                                @foreach($universities as $university )
                                                    @if($uni->id != $university->university_id )
                                                        <?php $versity =\App\KosUniversity::find($university->university_id);?>
                                                        <option value="{{ $versity->id }}">{{ $versity->university }}</option>
                                                    @endif
                                                @endforeach
                                            @else
                                                <select class="custom-select mdb-select" required name="university" searchable="Search here..">
                                                    <option value="" >Select University</option>
                                                    <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                                </select>
                                            @endif
                                            <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                        </select>
                                        {{--if user is not Spuer Admin end here--}}

                                    @endif
                                </div>
                                <label class="form-label">Office :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="office" searchable="Search here..">
                                        <?php $offices = \App\KosOffice::all(); ?>
                                        <?php $off = \App\KosOffice::where('id',$vehicleInfoById->office)->first()?>
                                        @if(!empty($off))
                                                <option value="{{ $off->id }}" >{{ $off->office }}</option>
                                                <option value="" >Select Office</option>
                                                @foreach($offices as $office )
                                                    @if($office->id != $off->id)
                                                        <option value="{{ $office->id }}">{{ $office->office }}</option>
                                                    @endif
                                                @endforeach
                                            @else
                                                <option value="" >Select Office</option>
                                                @foreach($offices as $office )
                                                    <option value="{{ $office->id }}">{{ $office->office }}</option>
                                                @endforeach
                                            @endif
                                    </select>
                                </div>

                                <label class="form-label">Vehicle :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="vehicle" searchable="Search here..">

                                     <?php $vehicles = \App\KosVehicle::all(); ?>
                                            <?php $veh = \App\KosVehicle::where('id',$vehicleInfoById->vehicle)->first()?>
                                            @if(!empty($veh))
                                                <option value="{{ $veh->id }}" >{{ $veh->vehicle }}</option>
                                                <option value="" >Select Vehicle</option>
                                                @foreach($vehicles as $vehicle )
                                                    @if($vehicle->id != $veh->id)
                                                        <option value="{{ $vehicle->id }}">{{ $vehicle->vehicle }}</option>
                                                    @endif
                                                @endforeach
                                            @else
                                                <option value="" >Select Vehicle</option>
                                            @foreach($vehicles as $vehicle )
                                                    <option value="{{ $vehicle->id }}">{{ $vehicle->vehicle }}</option>
                                            @endforeach
                                            @endif
                                    </select>
                                </div>


                                <label class="form-label">No of vehicles in organogram :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of vehicles in organogram" name="no_of_vehicles_in_organogram" value="{{ $vehicleInfoById->no_of_vehicles_in_organogram }}"  type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Year of organogram approval :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of vehicles in organogram" name="year_of_organogram_approval" value="{{ $vehicleInfoById->year_of_organogram_approval }}" type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Financial Year :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="financial_year" searchable="Search here..">
                                        <?php $financialYears = \App\KosFinancialYear::all(); ?>
                                        <?php $financial = \App\KosFinancialYear::where('id',$vehicleInfoById->financial_year)->first(); ?>
                                        @if(!empty($financial))
                                            <option value="{{ $financial->id }}" >{{ $financial->financial_year }}</option>
                                            <option value="" >Select Financial Year</option>
                                            @foreach($financialYears as $year )
                                                @if($year->id != $financial->id )
                                                        <option value="{{ $year->id }}">{{ $year->financial_year }}</option>
                                                    @endif
                                            @endforeach
                                        @else
                                            <option value="" >Select Financial Year</option>
                                        @endif
                                    </select>
                                </div>

                                <label class="form-label">Approved number of vehicles by ugc :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Approved number of vehicles by ugc" type="text" value="{{ $vehicleInfoById->approved_number_of_vehicles_by_ugc }}" name="approved_number_of_vehicles_by_ugc" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">No of vehicles purchased by university :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of vehicles purchased by university" type="text"  value="{{ $vehicleInfoById->no_of_vehicles_purchased_by_university }}" name="no_of_vehicles_purchased_by_university" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Year of purchase vehicles by university :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Year of purchase vehicles by university" type="text"  value="{{ $vehicleInfoById->year_of_purchase_vehicles_by_university }}" name="year_of_purchase_vehicles_by_university" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Proposed budget :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Proposed budget" type="text" name="proposed_budget" id="form1"  value="{{ $vehicleInfoById->proposed_budget }}" class="form-control custom-input">
                                </div>

                                <label class="form-label">Approved budget :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Approved budget" type="text" name="approved_budget" id="form1" value="{{ $vehicleInfoById->approved_budget }}" class="form-control custom-input">
                                </div>

                                <label class="form-label">Remarks :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <textarea class="custom-textarea" name="remarks">{{ $vehicleInfoById->remarks }}</textarea>
                                    <br>
                                    @if( Session::get('adminRole') == 'super-admin')
                                        <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg waves-effect z-depth-0" type="submit">Update It</button>
                                    @else
                                        @foreach($permissions as $permission )
                                            @if($permission->permission == 'edit')
                                                <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg waves-effect z-depth-0" type="submit">Update</button>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>

                            </div>
                </form>
                <!-- Table with panel -->
            </div>

        </div>
        <!--Grid row-->

        </div>
    </main>
@endsection
