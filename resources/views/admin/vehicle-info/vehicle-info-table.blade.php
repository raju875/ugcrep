@extends('admin.master')

@section('title')
    <title>Vehicle Info</title>
@endsection

@section('body')
    <div class="content-div">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->


                <div class="card-body d-sm-flex justify-content-between">


                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Vehicle Info</span>
                    </h4>

                    <div class="right-align">
                        <input placeholder="Search One" type="text" id="search-default">
                        <input placeholder="Search Two" type="text" id="search-filter">
                    </div>


                </div>
            </div>
            <!-- Heading -->
            <!-- Button trigger modal -->
            <button type="button" class="mb-lg-3 btn btn-primary btn-md show-field-btn" data-toggle="modal" data-target="#exampleModalCenter">
                Show Field
            </button>
            <div class="clr"></div>

            <!-- Modal -->
            <div class="centre modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content m-lg-5">
                        <form action="{{ url('/database/vehicle-info/modal-listing') }}" method="POST">
                            @csrf
                            <ul style="list-style-type: none">
                                <li>
                                    <input type="checkbox" class="allModal">Select All
                                </li>
                                <li>
                                    @if(Session::get('vehicleInfoUniversity'))
                                        <input class="singelModal" type="checkbox" id="university" name="column[]"
                                               value="university"
                                               checked="checked"> University

                                    @else
                                        <input class="singelModal" type="checkbox" id="university" name="column[]"
                                               value="university"
                                        > University
                                    @endif
                                </li>

                                <li>
                                    @if(Session::get('vehicleInfoOffice'))
                                        <input class="singelModal" type="checkbox" id="office" name="column[]" value="office"
                                               checked="checked"> Office
                                    @else
                                        <input class="singelModal" type="checkbox" id="office" name="column[]" value="office"
                                        > Office
                                    @endif
                                </li>

                                <li> @if(Session::get('vehicleInfoVehicle'))
                                        <input class="singelModal" type="checkbox" id="vehicle" name="column[]"
                                               value="vehicle" checked="checked"> Vehicle
                                    @else
                                        <input class="singelModal" type="checkbox" id="vehicle" name="column[]"
                                               value="vehicle"> Vehicle
                                    @endif
                                </li>

                                <li>
                                    @if(Session::get('vehicleInfoNoOfVehiclesInOrganogram'))
                                        <input class="singelModal" type="checkbox" id="no_of_vehicles_in_organogram" name="column[]"
                                               value="no_of_vehicles_in_organogram" checked="checked"> No of
                                        vehicles in organogram
                                    @else
                                        <input class="singelModal" type="checkbox" id="no_of_vehicles_in_organogram" name="column[]"
                                               value="no_of_vehicles_in_organogram"> No of vehicles in
                                        organogram
                                    @endif
                                </li>

                                <li>
                                    @if(Session::get('vehicleInfoYearOfOrganogramApproval'))
                                        <input class="singelModal" type="checkbox" id="year_of_organogram_approval" name="column[]"
                                               value="year_of_organogram_approval" checked="checked"> Year of
                                        organogram approval
                                    @else
                                        <input class="singelModal" type="checkbox" id="year_of_organogram_approval" name="column[]"
                                               value="year_of_organogram_approval"> Year of organogram approval
                                    @endif
                                </li>

                                <li>
                                    @if(Session::get('vehicleInfoFinancialYear'))
                                        <input class="singelModal" type="checkbox" id="financial_year" name="column[]"
                                               value="financial_year"
                                               checked="checked"> Financial year
                                    @else
                                        <input class="singelModal" type="checkbox" id="financial_year" name="column[]"
                                               value="financial_year"
                                        > Financial year
                                    @endif
                                </li>

                                <li>
                                    @if(Session::get('vehicleInfoApprovedNumberOfVehiclesByUgc'))
                                        <input class="singelModal" type="checkbox" id="approved_number_of_vehicles_by_ugc"
                                               name="column[]"
                                               value="approved_number_of_vehicles_by_ugc" checked="checked">
                                        Approved number of vehicles by ugc
                                    @else
                                        <input class="singelModal" type="checkbox" id="approved_number_of_vehicles_by_ugc"
                                               name="column[]"
                                               value="approved_number_of_vehicles_by_ugc"> Approved number of
                                        vehicles by ugc
                                    @endif
                                </li>

                                <li>
                                    @if(Session::get('vehicleInfoNoOfVehiclesPurchasedByUniversity'))
                                        <input class="singelModal" type="checkbox" id="no_of_vehicles_purchased_by_university"
                                               name="column[]"
                                               value="no_of_vehicles_purchased_by_university" checked="checked">
                                        No of vehicles purchased by university
                                        organogram
                                    @else
                                        <input class="singelModal" type="checkbox" id="no_of_vehicles_purchased_by_university"
                                               name="column[]"
                                               value="no_of_vehicles_purchased_by_university"> No of vehicles
                                        purchased by university
                                        organogram
                                    @endif
                                </li>

                                <li>
                                    @if(Session::get('vehicleInfoYearOfPurchaseVehiclesByUniversity'))
                                        <input class="singelModal" type="checkbox" id="year_of_purchase_vehicles_by_university"
                                               name="column[]"
                                               value="year_of_purchase_vehicles_by_university"
                                               checked="checked"> Year of purchase vehicles by university
                                        approved by UGC
                                    @else
                                        <input class="singelModal" type="checkbox" id="year_of_purchase_vehicles_by_university"
                                               name="column[]"
                                               value="year_of_purchase_vehicles_by_university"> Year of purchase
                                        vehicles by university
                                        approved by UGC
                                    @endif
                                </li>

                                <li>
                                    @if(Session::get('vehicleInfoProposedBudget'))
                                        <input class="singelModal" type="checkbox" id="proposed_budget" name="column[]"
                                               value="proposed_budget" checked="checked"> Proposed budget
                                        by UGC
                                    @else
                                        <input class="singelModal" type="checkbox" id="proposed_budget" name="column[]"
                                               value="proposed_budget"> Proposed budget
                                        by UGC
                                    @endif
                                </li>

                                <li>
                                    @if(Session::get('vehicleInfoApprovedBudget'))
                                        <input class="singelModal" type="checkbox" id="approved_budget" name="column[]"
                                               value="approved_budget" checked="checked"> Approved budget
                                        regular appointed
                                    @else
                                        <input class="singelModal" type="checkbox" id="approved_budget" name="column[]"
                                               value="approved_budget"> Approved budget
                                        regular appointed
                                    @endif
                                </li>

                                <li>
                                    @if(Session::get('vehicleInfoRemarks'))
                                        <input class="singelModal" type="checkbox" id="remarks" name="column[]" value="remarks"
                                               checked="checked"> Remarks
                                    @else
                                        <input class="singelModal" type="checkbox" id="remarks" name="column[]" value="remarks"
                                        > Remarks
                                    @endif
                                </li>


                            </ul>

                            <button style="width: 150px;margin-left: 35px;" type="submit"
                                    class="btn btn-secondary">
                                Save Changes
                            </button>
                        </form>
                    </div>

                </div>
            </div>
            @if(Session::has('message'))
                <h4 class="text text-center text-success">{{ Session::get('message') }}</h4>
            @endif
            @if(Session::has('alert'))
                <h4 class="text text-center text-danger">{{ Session::get('alert') }}</h4>
        @endif
        <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Table with panel -->
                    <div class="card card-cascade narrower">
                        <!--Form Starts-->
                        <form action="{{ url('/database/vehicle-info/edit') }}" method="POST">
                        @csrf
                        <!--Card image-->
                            <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">

                                {{--<div>--}}
                                @if( Session::get('adminRole') == 'super-admin')
                                    <?php $count = DB::table('kos_vehicle_infos')->count(); ?>
                                    <label>Total Items {{ $count }}</label>
                                    <div>
                                        <a href="{{ url('/database/vehicle-info/form') }}" title="Add"
                                           class="btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i class="custom-fontawesome fas fa-plus-circle"></i>
                                        </a>
                                        <button type="submit" name="submit" value="report" title="Report"
                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Report" class="custom-fontawesome fas fa-pen-square"></i>
                                        </button>
                                        <button type="submit" value="copy" name="submit"
                                                onclick="return confirm('Are you sure to Copy it !!!')" title="Copy"
                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                        </button>
                                        <button type="submit" value="delete" name="submit"
                                                onclick="return confirm('Are you sure to Delete it !!!')" title="Delete"
                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                        </button>

                                    </div>
                                @else
                                    <div class="col-lg-12">
                                        <?php $count = DB::table('kos_vehicle_infos')
                                            ->join('kos_university_permissions', 'kos_vehicle_infos.university', '=', 'kos_university_permissions.university_id')
                                            ->join('kos_universities', 'kos_vehicle_infos.university', '=', 'kos_universities.id')
                                            ->where('kos_university_permissions.role_id', Session::get('adminId'))
                                            ->count()?>
                                        <div class="row">
                                            <div class="col-lg-9">
                                                <label>Total Items {{ $count }}</label>
                                            </div>

                                            <?php $permissions = \App\KosPermission::where('role_id', Session::get('adminId'))->get();?>
                                            <div class="col-lg-3 text-right">
                                                @foreach($permissions as $permission )
                                                    @if($permission->permission == 'add')
                                                        <a href="{{ url('/database/vehicle-info/form') }}" title="Add"
                                                           class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                            <i class="custom-fontawesome fas fa-plus-circle"></i>
                                                        </a>
                                                    @endif
                                                    @if($permission->permission == 'report')
                                                        <button type="submit" name="submit" value="report" title="Report"
                                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                            <i title="Report" class="custom-fontawesome fas fa-pen-square"></i>
                                                        </button>
                                                    @endif
                                                    @if($permission->permission == 'copy')
                                                        <button type="submit" value="copy" name="submit"
                                                                onclick="return confirm('Are you sure to Copy it !!!')"
                                                                title="Copy"
                                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                            <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                                        </button>
                                                    @endif
                                                    @if($permission->permission == 'delete')
                                                        <button type="submit" value="delete" name="submit"
                                                                onclick="return confirm('Are you sure to Delete it !!!')"
                                                                title="Delete"
                                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                            <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                                        </button>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <!--/Card image-->


                            <div class="px-4">

                                <div class="table-wrapper table-responsive">
                                    <!--Table-->

                                    {{--**************For Super Admin start here**************--}}

                                    @if(Session::get('adminRole') == 'super-admin')
                                        <table id="datatable" class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                {{--this is hidden column by default checked all--}}
                                                <th style="display: none;"><input type="checkbox" checked
                                                                                  class="selectall"></th>
                                                {{--this is hidden column by default checked all--}}

                                                <th><input type="checkbox" class="all"></th>
                                                <th class="university">University</th>
                                                <th class="office">Office</th>
                                                <th class="vehicle">Vehicle</th>
                                                <th class="no_of_vehicles_in_organogram">No of vehicles in organogram
                                                </th>
                                                <th class="year_of_organogram_approval">Year of organogram approval</th>
                                                <th class="financial_year">Financial year</th>
                                                <th class="approved_number_of_vehicles_by_ugc">Approved number of
                                                    vehicles by ugc
                                                </th>
                                                <th class="no_of_vehicles_purchased_by_university">No of vehicles
                                                    purchased by university
                                                </th>
                                                <th class="year_of_purchase_vehicles_by_university">Year of purchase
                                                    vehicles by university
                                                </th>
                                                <th class="proposed_budget">Proposed budget</th>
                                                <th class="approved_budget">Approved budget</th>
                                                <th class="remarks">Remarks</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php $vehicleInfos = DB::table('kos_vehicle_infos')->orderBy('id', 'desc')->paginate(100);?>
                                            @foreach($vehicleInfos as $info )
                                                <tr>
                                                    {{--this is hidden column by default checked all--}}
                                                    <td style="display: none;"><input checked type="checkbox"
                                                                                      class="individual"
                                                                                      name="vehicle_info_hide[]"
                                                                                      value="{{ $info->id }}"></td>

                                                    <td><input type="checkbox" class="single" name="vehicle_info[]"
                                                               value="{{ $info->id }}"></td>
                                                    <td class="university">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            <?php $university = \App\KosUniversity::find($info->university)?>
                                                            @if(!empty($university))
                                                                {{ $university->university }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="office">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            <?php $office = \App\KosOffice::find($info->office)?>
                                                            @if(!empty($office))
                                                                {{ $office->office }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="vehicle">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            <?php $vehicle = \App\KosVehicle::find($info->vehicle)?>
                                                            @if(!empty($vehicle))
                                                                {{ $vehicle->vehicle }}
                                                            @endif
                                                        </a>
                                                    </td>

                                                    <td class="no_of_vehicles_in_organogram">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->no_of_vehicles_in_organogram }}
                                                        </a>
                                                    </td>

                                                    <td class="year_of_organogram_approval">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->year_of_organogram_approval }}
                                                        </a>
                                                    </td>

                                                    <td class="financial_year">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            <?php $year = \App\KosFinancialYear::find($info->financial_year)?>
                                                            {{ $year->financial_year }}
                                                        </a>
                                                    </td>

                                                    <td class="approved_number_of_vehicles_by_ugc">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->approved_number_of_vehicles_by_ugc }}
                                                        </a>
                                                    </td>

                                                    <td class="no_of_vehicles_purchased_by_university">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->no_of_vehicles_purchased_by_university }}
                                                        </a>
                                                    </td>

                                                    <td class="year_of_purchase_vehicles_by_university">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->year_of_purchase_vehicles_by_university }}
                                                        </a>
                                                    </td>

                                                    <td class="proposed_budget">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->proposed_budget }}
                                                        </a>
                                                    </td>

                                                    <td class="approved_budget">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->approved_budget }}
                                                        </a>
                                                    </td>

                                                    <td class="remarks">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->remarks }}
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        {{ $vehicleInfos ->links() }}

                                        {{--**************For Super Admin end here**************--}}


                                        {{--************For Normal User or Admin start here**************--}}
                                    @else

                                        <table id="datatable" class="table display" border="1">
                                            <thead>
                                            <tr>
                                                {{--this is hidden column by default checked all--}}
                                                <th style="display: none;"><input type="checkbox" checked
                                                                                  class="selectall"></th>

                                                <th><input type="checkbox" class="all"></th>
                                                <th class="university">University</th>
                                                <th class="office">Office</th>
                                                <th class="vehicle">Vehicle</th>
                                                <th class="no_of_vehicles_in_organogram">No of vehicles in organogram
                                                </th>
                                                <th class="year_of_organogram_approval">Year of organogram approval</th>
                                                <th class="financial_year">Financial year</th>
                                                <th class="approved_number_of_vehicles_by_ugc">Approved number of
                                                    vehicles by ugc
                                                </th>
                                                <th class="no_of_vehicles_purchased_by_university">No of vehicles
                                                    purchased by university
                                                </th>
                                                <th class="year_of_purchase_vehicles_by_university">Year of purchase
                                                    vehicles by university
                                                </th>
                                                <th class="proposed_budget">Proposed budget</th>
                                                <th class="approved_budget">Approved budget</th>
                                                <th class="remarks">Remarks</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php $vehicleInfos = DB::table('kos_vehicle_infos')
                                                ->join('kos_university_permissions', 'kos_vehicle_infos.university', '=', 'kos_university_permissions.university_id')
                                                ->join('kos_universities', 'kos_vehicle_infos.university', '=', 'kos_universities.id')
                                                ->where('kos_university_permissions.role_id', Session::get('adminId'))
                                                ->select('kos_vehicle_infos.*', 'kos_universities.university')->orderBy('id', 'desc')
                                                ->paginate(100)?>
                                            @foreach($vehicleInfos as $info )
                                                <tr>
                                                    {{--this is hidden column by default checked all--}}
                                                    <td style="display: none;"><input checked type="checkbox"
                                                                                      class="individual"
                                                                                      name="vehicle_info_hide[]"
                                                                                      value="{{ $info->id }}">
                                                    </td>

                                                    <td><input type="checkbox" class="single"
                                                               name="vehicle_info[]" value="{{ $info->id }}">
                                                    </td>
                                                    <td class="university">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->university }}
                                                        </a>
                                                    </td>

                                                    <td class="office">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->office }}
                                                        </a>
                                                    </td>

                                                    <td class="vehicle">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->vehicle }}
                                                        </a>
                                                    </td>

                                                    <td class="no_of_vehicles_in_organogram">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->no_of_vehicles_in_organogram }}
                                                        </a>
                                                    </td>

                                                    <td class="year_of_organogram_approval">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->year_of_organogram_approval }}
                                                        </a>
                                                    </td>

                                                    <td class="financial_year">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->financial_year }}
                                                        </a>
                                                    </td>

                                                    <td class="approved_number_of_vehicles_by_ugc">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->approved_number_of_vehicles_by_ugc }}
                                                        </a>
                                                    </td>

                                                    <td class="no_of_vehicles_purchased_by_university">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->no_of_vehicles_purchased_by_university }}
                                                        </a>
                                                    </td>

                                                    <td class="year_of_purchase_vehicles_by_university">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->year_of_purchase_vehicles_by_university }}
                                                        </a>
                                                    </td>

                                                    <td class="proposed_budget">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->proposed_budget }}
                                                        </a>
                                                    </td>

                                                    <td class="approved_budget">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->approved_budget }}
                                                        </a>
                                                    </td>

                                                    <td class="remarks">
                                                        <a href="{{ url('/database/vehicle-info/edit/'.$info->id) }}">
                                                            {{ $info->remarks }}
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                        {{ $vehicleInfos ->links() }}
                                        {{--************For Normal User or Admin end here**************--}}
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




