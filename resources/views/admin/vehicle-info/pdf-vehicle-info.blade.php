<div style="text-align: center; padding-top: 50px;font-family: arial, sans-serif;border-collapse: collapse;">
    <h2>Public University Management Information System (PUMIS)</h2>
    {{--if selected university names are same start--}}
    @if($uni==1 )
        @foreach ($vehicleInfos as $vehicleInfo )
            <?php $vehicleInfoById = \App\KosVehicleInfo::find($vehicleInfo); ?>
            <?php $university = \App\KosUniversity::find($vehicleInfoById->university); ?>
        @if(!empty($university))
            <h3>{{ $university->university }}</h3>
                @endif
            @break
        @endforeach
    @endif
    {{--if selected university names are same start--}}
</div>
<style>
    table,th,td{
        border: 1px solid #ddd;
        border-collapse: collapse;
        font-size: 13px;

    }
    th{
        padding: 12px 0;
        text-align: center;
    }
</style>

<table >
    <thead>
    <tr>
        @if(Session::get('vehicleInfoUniversity'))
                @if($uni==0)
                    <th style=" ">{{ 'University' }}</th>
                @endif
            @endif

            @if(Session::get('vehicleInfoOffice'))
                <th style=" ">{{ 'Office' }}</th>
            @endif

            @if(Session::get('vehicleInfoVehicle'))
                <th style=" ">{{ 'vehicle' }}</th>
            @endif

            @if(Session::get('vehicleInfoNoOfVehiclesInOrganogram'))
                <th style=" ">{{ 'No of vehicles in organogram' }}</th>
            @endif

            @if(Session::get('vehicleInfoYearOfOrganogramApproval'))
                <th style=" ">{{ 'Year of organogram approval' }}</th>
            @endif

            @if(Session::get('vehicleInfoFinancialYear'))
                <th style=" ">{{ 'Financial year' }}</th>
            @endif

            @if(Session::get('vehicleInfoApprovedNumberOfVehiclesByUgc'))
                <th style=" ">{{ 'Approved number of vehicles by ugc' }}</th>
            @endif

            @if(Session::get('vehicleInfoNoOfVehiclesPurchasedByUniversity'))
                <th style=" ">{{ 'No of vehicles purchased by university' }}</th>
            @endif

            @if(Session::get('vehicleInfoYearOfPurchaseVehiclesByUniversity'))
                <th style=" ">{{ 'Year of purchase vehicles by university' }}</th>
            @endif

            @if(Session::get('vehicleInfoProposedBudget'))
                <th style=" ">{{ 'Proposed budget' }}</th>
            @endif

            @if(Session::get('vehicleInfoApprovedBudget'))
                <th style=" ">{{ 'Approved budget' }}</th>
            @endif

            @if(Session::get('vehicleInfoRemarks'))
                <th style=" ">{{ 'Remarks' }}</th>
            @endif
    </tr>
    </thead>
    <tbody>

    @foreach($vehicleInfos as $vehicleInfo )
        <tr style=" ">
                <?php $vehicleInfoById = \App\KosVehicleInfo::find($vehicleInfo); ?>
                    @if(Session::get('vehicleInfoUniversity'))
                    @if($uni==0 )
                        {{--//if selected university name is not same--}}
                        <?php $university = \App\KosUniversity::find($vehicleInfoById->university); ?>
                        @if(!empty($university))
                        <td style=" ">{{ $university->university }}</td>
                            @else
                                <td style=" "></td>
                            @endif
                    @endif
                @endif


                    @if(Session::get('vehicleInfoOffice'))
                    <?php $office = \App\KosOffice::find($vehicleInfoById->office); ?>
                    @if (empty($office))
                        <td style=" "></td>
                    @else
                        <td style=" ">{{ $office->office }}</td>
                    @endif
                @endif

                    @if(Session::get('vehicleInfoVehicle'))
                    <?php $vehicle = \App\KosVehicle::find($vehicleInfoById->vehicle); ?>
                    @if (empty($vehicle))
                        <td style=" "></td>
                    @else
                        <td style=" ">{{ $vehicle->vehicle }}</td>
                    @endif
                @endif

                    @if(Session::get('vehicleInfoNoOfVehiclesInOrganogram'))
                    <td style=" ">{{ $vehicleInfoById->no_of_vehicles_in_organogram }}</td>
                @endif

                    @if(Session::get('vehicleInfoYearOfOrganogramApproval'))
                    <td style=" ">{{ $vehicleInfoById->year_of_organogram_approval }}</td>
                @endif

                    @if(Session::get('vehicleInfoFinancialYear'))
                    <?php $year = \App\KosFinancialYear::find($vehicleInfoById->financial_year); ?>
                    @if (empty($year))
                        <td style=" "></td>
                    @else
                        <td style=" ">{{ $year->financial_year }}</td>
                    @endif
                @endif

                    @if(Session::get('vehicleInfoApprovedNumberOfVehiclesByUgc'))
                    <td style=" ">{{ $vehicleInfoById->approved_number_of_vehicles_by_ugc }}</td>
                @endif

                    @if(Session::get('vehicleInfoNoOfVehiclesPurchasedByUniversity'))
                    <td style=" ">{{ $vehicleInfoById->no_of_vehicles_purchased_by_university }}</td>
                @endif

                    @if(Session::get('vehicleInfoYearOfPurchaseVehiclesByUniversity'))
                    <td style=" ">{{ $vehicleInfoById->year_of_purchase_vehicles_by_university }}</td>
                @endif

                    @if(Session::get('vehicleInfoProposedBudget'))
                    <td style=" ">{{ $vehicleInfoById->proposed_budget }}</td>
                @endif

                    @if(Session::get('vehicleInfoApprovedBudget'))
                    <td style=" ">{{ $vehicleInfoById->approved_budget }}</td>
                @endif


                    @if(Session::get('vehicleInfoRemarks'))
                    <td style=" ">{{ $vehicleInfoById->remarks }}</td>
                @endif
        </tr>

    @endforeach
    </tbody>
</table>
