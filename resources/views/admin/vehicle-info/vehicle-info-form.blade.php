@extends('admin.master')

@section('title')
    <title>Vehicle Info</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Add Vehicle Info</span>
                    </h4>
                    <a  class="btn btn-outline-info btn-rounded btn-secondary btn-lg white-text" href="{{ url('/database/vehicle-info/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>

                </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <form class="col-md-12 mb-4" action="{{ url('/database/vehicle-info/add') }}" method="POST">

                @csrf
                <!-- Form with panel -->
                    <div class="card card-cascade narrower">

                        <div class="px-4">

                            <div class="table-wrapper">

                                <label class="form-label">University :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    @if(Session::get('adminRole') == 'super-admin')
                                        <select class="custom-select mdb-select" required name="university" searchable="Search here..">
                                            <?php $universities = \App\KosUniversity::all(); ?>
                                            <option value="" >Select University</option>
                                            @foreach($universities as $university )
                                                <option value="{{ $university->id }}">{{ $university->university }}</option>
                                            @endforeach
                                            <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                        </select>
                                    @else
                                        <select class="custom-select mdb-select" required name="university" searchable="Search here..">
                                            <?php $userId = Session::get('adminId'); ?>
                                            <?php $universityAccess = DB::table('kos_university_permissions')->where('role_id',$userId)->get(); ?>
                                            <option value="" >Select University</option>
                                            @foreach($universityAccess as $university )
                                                <?php  $universityById = \App\KosUniversity::find($university->university_id); ?>
                                                <option value="{{ $universityById->id }}">{{ $universityById->university }}</option>
                                            @endforeach
                                            <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                        </select>
                                    @endif
                                </div>

                                <label class="form-label">Office :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="office" searchable="Search here..">
                                        <?php $offices = \App\KosOffice::all(); ?>
                                        <option value="" >Select Office</option>
                                        @foreach($offices as $office )
                                            <option value="{{ $office->id }}">{{ $office->office }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <label class="form-label">Vehicle :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="vehicle" searchable="Search here..">
                                        <?php $vehicles = \App\KosVehicle::all(); ?>
                                        <option value="" >Select Vehicle</option>
                                        @foreach($vehicles as $vehicle )
                                            <option value="{{ $vehicle->id }}">{{ $vehicle->vehicle }}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <label class="form-label">No of vehicles in organogram :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of vehicles in organogram" name="no_of_vehicles_in_organogram" type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Year of organogram approval :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of vehicles in organogram" name="year_of_organogram_approval" type="text" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Financial Year :</label>
                                <div class="md-form">
                                    <select class="custom-select mdb-select" required name="financial_year" searchable="Search here..">
                                        <?php $financialYears = \App\KosFinancialYear::all(); ?>
                                        @if(!empty($financialYears))
                                            <option value="" >Select Financial Year</option>
                                            @foreach($financialYears as $year )
                                                <option value="{{ $year->id }}">{{ $year->financial_year }}</option>
                                            @endforeach
                                        @else
                                            <option value="" >Select Financial Year</option>
                                        @endif
                                    </select>
                                </div>

                                <label class="form-label">Approved number of vehicles by ugc :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Approved number of vehicles by ugc" type="text" name="approved_number_of_vehicles_by_ugc" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">No of vehicles purchased by university :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="No of vehicles purchased by university" type="text" name="no_of_vehicles_purchased_by_university" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Year of purchase vehicles by university :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Year of purchase vehicles by university" type="text" name="year_of_purchase_vehicles_by_university" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Proposed budget :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Proposed budget" type="text" name="proposed_budget" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Approved budget :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Approved budget" type="text" name="approved_budget" id="form1" class="form-control custom-input">
                                </div>

                                <label class="form-label">Remarks :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <textarea class="custom-textarea" name="remarks"></textarea>
                                </div>
                                <button name="btn"
                                        class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg  waves-effect z-depth-0"
                                        type="submit">Save
                                </button>

                            </div>
                </form>
                <!-- Table with panel -->
            </div>

        </div>
        <!--Grid row-->

        </div>
    </main>
@endsection
