@extends('admin.master')

@section('title')
    <title>Faculty</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Add New Faculty</span>
                    </h4>

                </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Form with panel -->
                    <form action="{{ url('/database/faculty/add') }}" method="POST">
                        @csrf
                        <div class="card card-cascade narrower">
                            <div class="px-4">

                                <div class="table-wrapper">

                                    <label class="form-label mt-5">Faculty</label>
                                    <!-- Material input -->
                                    <div class="md-form">
                                        <input  name="faculty" required type="text" id="form1" class="form-control">
                                    </div>
                                    <button style="color: white" class="mt-4 mb-lg-5 btn btn-outline-info btn-rounded btn-secondary btn-lg" type="submit">Save</button>

                                </div>

                            </div>

                        </div>
                    </form>
                    <!-- Table with panel -->
                </div>

            </div>
            <!--Grid row-->

        </div>
    </main>
@endsection
