@extends('admin.master')

@section('title')
    <title>Financial Year</title>
@endsection

@section('body')

    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">
                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Financial Year</span>
                    </h4>
                    <input placeholder="Search" type="text" id="search-default">


                </div>
            </div>

            <!-- Heading -->
            @if(Session::has('message'))
                <h5 class="text text-center text-success">{{ Session::get('message') }}</h5>
            @endif
            @if(Session::has('alert'))
                <h5 class="text text-center text-danger">{{ Session::get('alert') }}</h5>
        @endif
        <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Table with panel -->
                    <div class="card card-cascade narrower">
                        <!--Form Starts-->
                        <form action="{{ url('/database/financial-year/edit') }}" method="POST">
                        @csrf
                        <!--Card image-->
                            <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">


                                <?php $count = DB::table('Kos_financial_years')->count(); ?>
                                <label>Total Items {{ $count }}</label>


                                @if( Session::get('adminRole') == 'super-admin')
                                    <div>
                                        <a href="{{ url('/database/financial-year/form') }}" title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i class="custom-fontawesome fas fa-plus-circle"></i>
                                        </a>

                                        <button type="submit" value="copy" name="submit" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                        </button>

                                        <button type="submit" value="delete" name="submit" onclick="return confirm('Are you sure to Delete it !!!')" title="Delete" class="transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                        </button>

                                    </div>
                                @else
                                    <div>
                                        <?php $userId = Session::get('adminId');?>
                                        <?php $permissions = \App\KosPermission::where('role_id',$userId)->get();?>
                                        @foreach($permissions as $permission )
                                            @if($permission->permission == 'add')
                                                <a href="{{ url('/database/financial-year/form') }}" title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i class="custom-fontawesome fas fa-plus-circle"></i>
                                                </a>
                                            @endif

                                            @if($permission->permission == 'copy')
                                                <button type="submit" value="copy" name="submit" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                                </button>
                                            @endif
                                            @if($permission->permission == 'delete')
                                                <button type="submit" value="delete" name="submit" onclick="return confirm('Are you sure to Delete it !!!')" title="Delete" class="transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                                </button>

                                            @endif
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                            <!--/Card image-->


                            <div class="px-4">

                                <div class="table-wrapper table-responsive">
                                    <!--Table-->

                                    <table id="datatable" class="table  table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th ><input type="checkbox" class="selectall"></th>
                                            <th class="hide_or_show">Financial Year</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($financialYears as $financialYear )
                                            <tr>
                                                <td ><input type="checkbox" class="individual" name="financial_year[]"  value="{{ $financialYear->id }}"></td>
                                                <td  class="hide_or_show"><a href="{{ url('/database/financial-year/edit/'.$financialYear->id) }}">{{ $financialYear->financial_year }}</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $financialYears->links() }}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

    {{--<script>--}}
    {{--$(document).ready(function(){--}}

    {{--fetch_customer_data();--}}

    {{--function fetch_customer_data(query = '')--}}
    {{--{--}}
    {{--$.ajax({--}}
    {{--url:"{{ route('live_search.action') }}",--}}
    {{--method:'GET',--}}
    {{--data:{query:query},--}}
    {{--dataType:'json',--}}
    {{--success:function(data)--}}
    {{--{--}}
    {{--$('tbody').html(data.table_data);--}}
    {{--$('#total_records').text(data.total_data);--}}
    {{--}--}}
    {{--})--}}
    {{--}--}}

    {{--$(document).on('keyup', '#search', function(){--}}
    {{--var query = $(this).val();--}}
    {{--fetch_customer_data(query);--}}
    {{--});--}}
    {{--});--}}
    {{--</script>--}}

@endsection


