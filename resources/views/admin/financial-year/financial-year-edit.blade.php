@extends('admin.master')

@section('title')
    <title>Financial Year</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Edit Centre</span>
                    </h4>

                    <a class="btn btn-outline-info btn-rounded btn-secondary btn-lg white-text" href="{{ url('/database/financial-year/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>


                </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Form with panel -->
                    <form action="{{ url('/database/financial-year/update') }}" method="POST">
                        @csrf
                        <div class="card card-cascade narrower">
                            <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">

                                <div class="button-section">
                                    {{--if user if Super Admin Start here--}}
                                    @if( Session::get('adminRole') == 'super-admin')
                                        <div style="margin-left: auto!important;">
                                            <a href="{{ url('/database/financial-year/form') }}" title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                <i class="custom-fontawesome fas fa-plus-circle"></i>
                                            </a>
                                            <a href="{{ url('/database/financial-year/copy/'.$financialYearById->id) }}" title="Copy" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                            </a>
                                            <a href="{{ url('/database/financial-year/delete/'.$financialYearById->id) }}" value="delete" onclick="return confirm('Are you sure to delete it !!!')" title="Delete" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                            </a>
                                        </div>
                                        {{--if user is Super Admin end here--}}

                                        {{--if user us not Super Admin start here--}}
                                    @else
                                        <div style="margin-left: auto!important;">
                                            <?php $userId       = Session::get('adminId');?>
                                            <?php $permissions  = \App\KosPermission::where('role_id',$userId)->get();?>
                                            @foreach($permissions as $permission )
                                                @if($permission->permission == 'add')
                                                    <a href="{{ url('/database/financial-year/form') }}" title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                        <i class="custom-fontawesome fas fa-plus-circle"></i>
                                                    </a>
                                                @endif
                                                @if($permission->permission == 'copy')
                                                    <a href="{{ url('/database/financial-year/copy/'.$financialYearById->id) }}" title="Copy" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                        <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                                    </a>
                                                @endif
                                                @if($permission->permission == 'delete')
                                                    <a href="{{ url('/database/financial-year/delete/'.$financialYearById->id) }}" value="delete" onclick="return confirm('Are you sure to delete it !!!')" title="Delete" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                        <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                                    </a>
                                                @endif
                                            @endforeach
                                        </div>
                                        {{--if user us not Super Admin here here--}}
                                    @endif
                                </div>
                            </div>
                            <div class="px-4">
                                <div class="table-wrapper">

                                    <label class="form-label mt-lg-5">Financial Year</label>
                                    <!-- Material input -->
                                    <div class="md-form">
                                        <input  name="id" required type="hidden" id="form1" value="{{ $financialYearById->id }}" class="form-control custom-input">
                                        <input  name="financial_year" required type="text" id="form1" value="{{ $financialYearById->financial_year }}" class="form-control custom-input">
                                    </div>
                                    @if( Session::get('adminRole') == 'super-admin')
                                        <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg waves-effect z-depth-0" type="submit">Update It</button>
                                    @else
                                        @foreach($permissions as $permission )
                                            @if($permission->permission == 'edit')
                                                <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg waves-effect z-depth-0" type="submit">Update</button>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>

                            </div>

                        </div>
                    </form>
                    <!-- Table with panel -->
                </div>

            </div>
            <!--Grid row-->

        </div>
    </main>
@endsection
