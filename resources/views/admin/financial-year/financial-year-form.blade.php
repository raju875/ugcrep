@extends('admin.master')

@section('title')
    <title>Financial Year</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Add New Financial Year</span>
                    </h4>
                    <a class="btn btn-outline-info btn-rounded btn-secondary btn-lg white-text" href="{{ url('/database/financial-year/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>


                </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Form with panel -->
                    <form action="{{ url('/database/financial-year/add') }}" method="POST">
                        @csrf
                        <div class="card card-cascade narrower">
                            <div class="px-4">

                                <div class="table-wrapper">

                                    <label class="form-label mt-lg-5">Financial Year</label>
                                    <!-- Material input -->
                                    <div class="md-form">
                                        <input  name="financial_year" required type="text" id="form1" class="form-control">
                                    </div>
                                    <button class="mb-lg-5 mt-lg-5 btn btn-secondary btn-lg" type="submit">Save</button>

                                </div>

                            </div>

                        </div>
                    </form>
                    <!-- Table with panel -->
                </div>

            </div>
            <!--Grid row-->

        </div>
    </main>
@endsection
