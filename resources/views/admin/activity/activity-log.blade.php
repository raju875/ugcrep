@extends('admin.master')

@section('title')
    <title>Activity Log</title>
@endsection


@section('body')
    <div class="content-div">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            @if(Session::has('message'))
                <h5 class="text text-center text-success">{{ Session::get('message') }}</h5>
            @endif
            @if(Session::has('alert'))
                <h5 class="text text-center text-danger">{{ Session::get('alert') }}</h5>
            @endif
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Activity Log</span>
                    </h4>

                    <input placeholder="Search" type="text" id="search-user">

                </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Table with panel -->
                    <div class="card card-cascade narrower">
                        <!--Form Starts-->
                        <form action="{{ url('/database/activity-log/delete') }}" method="POST">
                            @csrf
                            <!--Card image-->
                            <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">

                                <div>
                                    <?php $count = DB::table('kos_activity_logs')->count() ?>
                                    <label class="ml-2" for="">Total items {{ $count }}</label>
                                </div>


                                <div>
                                    <button type="button" class="transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                        <i title="Report" class="custom-fontawesome  far fa-eye"></i>
                                    </button>
                                    <button type="submit" value="delete" name="submit" onclick="return confirm('Are you sure to Permanently Delete it !!!')" title="Delete" class="transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                        <i title="Delete" class="custom-fontawesome  far fa-trash-alt"></i>
                                    </button>
                                </div>

                            </div>
                            <!--/Card image-->


                            <div class="px-4">

                                <div class="table-wrapper table-responsive">
                                    <!--Table-->
                                    <table id="datatable" class="table  table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th><input type="checkbox" class="selectall"></th>
                                            <th>User</th>
                                            <th>Description</th>
                                            <th>Time</th>
                                        </tr>
                                        </thead>
                                        <tbody id="live_pagination">
                                        @include('admin.activity.live_pagination')
                                        </tbody>
                                    </table>
                                    {{ $activities->links() }}
                                    <!--Table-->
                                    <!--Pagination -->
                                </div>

                            </div>
                        </form>
                        <!--Form Ends-->

                    </div>
                    <!-- Table with panel -->
                </div></div>
            <!--Grid row-->

        </div>
    </div>
@endsection

@section('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

    <script type="text/javascript">

        $(window).on('hashchange', function() {

            if (window.location.hash) {

                var page = window.location.hash.replace('#', '');

                if (page == Number.NaN || page <= 0) {

                    return false;

                }else{

                    getData(page);

                }

            }

        });



        $(document).ready(function()

        {

            $(document).on('click', '.pagination a',function(event)

            {

                event.preventDefault();

                $('li').removeClass('active');

                $(this).parent('li').addClass('active');

                var myurl = $(this).attr('href');

                var page=$(this).attr('href').split('page=')[1];

                getData(page);

            });

        });



        function getData(page){

            $.ajax(

                {

                    url: '?page=' + page,

                    type: "get",

                    datatype: "html"

                })

                .done(function(data)

                {

                    $("#tag_container").empty().html(data);

                    location.hash = page;

                })

                .fail(function(jqXHR, ajaxOptions, thrownError)

                {

                    alert('No response from server');

                });

        }

    </script>
@stop
