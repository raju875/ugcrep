@extends('admin.master')

@section('title')
    <title>Center</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Add New Centre</span>
                    </h4>

                    <a href="{{ url('/database/centre/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>

                    <form class="d-flex justify-content-center">
                        <!-- Default input -->
                        <input type="search" placeholder="Type your query" aria-label="Search" class="form-control">
                        <button class="btn btn-primary btn-sm my-0 p" type="submit">
                            <i class="fa fa-search"></i>
                        </button>

                    </form>

                </div>

            </div>
            @if(Session::has('message'))
                <h5 class="text text-center text-success">{{ Session::get('message') }}</h5>
            @endif
            @if(Session::has('alert'))
                <h5 class="text text-center text-danger">{{ Session::get('alert') }}</h5>
        @endif
        <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Table with panel -->
                    <div class="card card-cascade narrower">
                        <!--Form Starts-->
                        <form action="{{ url('/database/centre/pdf') }}" method="POST">
                        @csrf
                        <!--Card image-->
                            <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">

                                {{--<div>--}}
                                <?php $count = 0 ?>
                                @foreach($centreById as $id )
                                    <?php $count++ ?>
                                    @endforeach
                                <label>Items Select {{ $count }}</label>


                                <div>
                                        <button type="submit" value="pdf" name="submit" onclick="return confirm('Are you sure to generate pdf!!!')" title="pdf" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i  title="pdf" class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                        </button>
                                </div>

                            </div>
                            <!--/Card image-->


                            <div class="px-4">

                                <div class="table-wrapper table-responsive">
                                    <!--Table-->

                                    <table id="datatable" class="table display" border="1">
                                        <thead>
                                        <tr>
                                            <th><input type="checkbox" class="selectall"></th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="centre" checked>Centre</th>
                                            <th><input type="checkbox" class="individual" name="column[]" value="created_at" checked>Time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($centreById as $id )
                                            <?php $centre = \App\KosCentre::find($id);?>
                                            <tr>
                                                <td><input type="checkbox" class="individual" name="checkbox[]"  value="{{ $centre->id }}" checked></td>
                                                <td><a href="{{ url('/database/centre/edit/'.$centre->id) }}">{{ $centre->centre }}</a></td>
                                                <td>{{ $centre->created_at }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection


