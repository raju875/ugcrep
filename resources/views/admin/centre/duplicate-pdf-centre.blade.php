<div style="text-align: center; padding-top: 50px;font-family: arial, sans-serif;border-collapse: collapse;">
    <h1>Public University Management Information System (PUMIS)</h1>
    <h2>Dhaka University</h2>
    <h3>Computer Science</h3>
    <h3>Dhaka University</h3>
    {{--@foreach($columns as $column )--}}
        {{--<h3>{{ $column }}</h3>--}}
        {{--@break--}}
    {{--@endforeach--}}
    @foreach($columns as $column )
            @foreach($checkboxes as $checkbox )
                <?php $columnName = \App\KosCentre::find($checkbox)->select($column)->first(); ?>
            @endforeach
            <h3>{{ $columnName->centre }}</h3>
    @endforeach
</div>

<table class="table" border="1" width="80%" style="margin: 0 auto">
    <thead>
    <tr>
        @foreach($columns as $column )
            @if($column!='centre')
            <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{ $column }}</th>
            @endif
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($checkboxes as $checkbox )
        <tr style="border: 1px solid #dddddd;text-align: left;padding: 8px;">
            @foreach($columns as $column )
                @if($column!='centre')
                <?php $result = \App\KosCentre::find($checkbox)->select($column)->first(); ?>
                    <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{  $result->created_at }}</td>
                @endif
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
