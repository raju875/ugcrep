<div style="text-align: center; padding-top: 50px;font-family: arial, sans-serif;border-collapse: collapse;">
    <h1>Public University Management Information System (PUMIS)</h1>
    <h2>Dhaka University</h2>
    <h3>Computer Science</h3>
    <h3>Dhaka University</h3>
</div>

<table class="table" border="1" width="80%" style="margin: 0 auto">
    <thead>
    <tr>
        @foreach($columns as $column )
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{ $column }}</th>
            @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($checkboxes as $checkbox )
        <tr style="border: 1px solid #dddddd;text-align: left;padding: 8px;">
        @foreach($columns as $column )
                <?php $result = DB::table('kos_centres')->where('id',$checkbox)->select($column)->first(); ?>
                @if($column=='centre')
                    <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{  $result->centre }}</td>
                @elseif($column=='created_at')
                    <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{  $result->created_at }}</td>
                    @else
                    <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{{  'Something worng!!!' }}</td>
                @endif
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>
