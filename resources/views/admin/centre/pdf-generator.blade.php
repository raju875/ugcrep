<div style="text-align: center; padding-top: 50px;font-family: arial, sans-serif;border-collapse: collapse;">
    <h1>Public University Management Information System (PUMIS)</h1>
    <h2>Dhaka University</h2>
    <h3>Computer Science</h3>
    <h3>Dhaka University</h3>
</div>

<table class="table" border="1" width="80%" style="margin: 0 auto">
    <thead>
    <tr>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Firstname</th>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Lastname</th>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Email</th>
    </tr>
    </thead>
    <tbody>
    <tr style="border: 1px solid #dddddd;text-align: left;padding: 8px;">
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">John</td>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Doe</td>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">john@example.com</td>
    </tr>
    <tr style="border: 1px solid #dddddd;text-align: left;padding: 8px;">
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Mary</td>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Moe</td>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">mary@example.com</td>
    </tr>
    <tr style="border: 1px solid #dddddd;text-align: left;padding: 8px;">
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">July</td>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Dooley</td>
        <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">july@example.com</td>
    </tr>
    </tbody>
</table>
