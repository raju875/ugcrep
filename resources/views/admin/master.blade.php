<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    @yield('title')
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('admin/asset/') }}/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    {{--<link href="{{ asset('admin/asset/') }}/css/mdb.min.css" rel="stylesheet">--}}
    <!-- Your custom styles (optional) -->
    <link href="{{ asset('admin/asset/') }}/css/style.min.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    {{--Datatabel CSS Start--}}

    <link type="text/css" href="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
    <link type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/sl-1.2.5/datatables.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://editor.datatables.net/extensions/Editor/css/editor.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" />

    <link href="{{ asset('admin/asset/') }}/css/style.css" rel="stylesheet">



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    {{--user control multi-select js--}}
    <script type="text/javascript" src="{{ asset('admin/asset/') }}/js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('admin/asset/') }}/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('admin/asset/') }}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
    <!-- MDB core JavaScript -->
    {{--<script type="text/javascript" src="{{ asset('admin/asset/') }}/js/mdb.min.js"></script>--}}
    {{--user control multi-select js--}}



    {{--Datatabel CSS End--}}
</head>

<body class="grey lighten-3">

<!--Main Navigation-->
@include('admin.include.sidebar')
@include('admin.include.navbar')
<!--Main Navigation-->

<!--Main layout-->
@yield('body')
<!--Main layout-->

<!--Footer-->
@include('admin.include.footer')
<!--/.Footer-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="{{ asset('admin/asset/') }}/js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{ asset('admin/asset/') }}/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{ asset('admin/asset/') }}/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
{{--<script type="text/javascript" src="{{ asset('admin/asset/') }}/js/mdb.min.js"></script>--}}
<!-- DataTables -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/sl-1.2.5/datatables.min.js"></script>
<script type="text/javascript" src="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>


{{--Datable JS link start here--}}
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
{{--<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
{{--<script type="text/javascript" src="https://editor.datatables.net/extensions/Editor/js/dataTables.editor.min.js"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<script src="https://cdn.datatables.net/scroller/1.5.1/js/dataTables.scroller.min.js"></script>

{{--Datable Custom search starts here--}}
<script>
    $(document).ready(function(){
        var table= $('#datatable').DataTable();

        $('#search-default').keyup(function(){
            table.search($(this).val()).draw() ;
        })
        $('#search-filter').keyup(function(){
            table.search($(this).val()).draw() ;
        })
        $('#search-user').keyup(function(){
            table.search($(this).val()).draw() ;
        })

    });
</script>
{{--<script>--}}
    {{--$(document).ready(function(){--}}
        {{--var table= $('#datatable').DataTable();--}}

        {{----}}

    {{--});--}}
{{--</script>--}}
{{--<script>--}}
    {{--$(document).ready(function(){--}}
        {{--var table= $('#datatable').DataTable();--}}

        {{----}}

    {{--});--}}
{{--</script>--}}
{{--Datable Custom search ends here--}}
{{--Datable JS link end here--}}

<script type="text/javascript">
    $(".selectall").click(function(){
        $(".individual").prop("checked",$(this).prop("checked"));
    });
    $(".all").click(function(){
        $(".single").prop("checked",$(this).prop("checked"));
    });
    $(".allModal").click(function(){
        $(".singelModal").prop("checked",$(this).prop("checked"));
    });
</script>

{{--Hide/show column codepen--}}
<script type="text/javascript">
    $("input:checkbox:not(:checked)").each(function() {
        var column = "table ." + $(this).attr("id");
        $(column).hide();
    });

    $("input:checkbox").click(function(){
        var column = "table ." + $(this).attr("id");
        $(column).toggle();
    });

</script>
{{--Hide/show column codepen--}}

{{--/*********************Multi-select control user starts here***********/--}}
<script>
    $(function() {
        $('#varsity').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            filterPlaceholder: 'Search for something...'
        });
    });
</script>

<script>
    $(function() {
        $('#module').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            filterPlaceholder: 'Search for something...'
        });
    });
</script>
<script>
    $(function() {
        $('#role').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            filterPlaceholder: 'Search for something...'
        });
    });
</script>

{{--/***********This is for hidden checkbox by default selectall and individual start**********/--}}
{{--<script type="text/javascript">--}}
    {{--$(".selectall").click(function(){--}}
        {{--$(".individual").prop("checked",$(this).prop("checked")});--}}

{{--</script>--}}
<script type="text/javascript">
    $('.selectall').click(function () {
        $('.individual').prop('checked',$(this).prop('checked'))
    })
</script>
{{--/***********This is for hidden checkbox by default selectall and individual end**********/--}}


{{--/***********This is for shown checkbox manual select checkbox start**********/--}}
<script type="text/javascript">
    $('.all').click(function () {
        $('.single').prop('checked',$(this).prop('checked'))
    })
</script>

{{--<script type="text/javascript">--}}
    {{--$(".all").click(function(){--}}
        {{--$(".single").prop("checked",$(this).prop("checked"));--}}

{{--</script>--}}
{{--/***********This is for shown checkbox manual select checkbox end**********/--}}


{{--/*********************Multi-select control user ends here***********/--}}

{{--multiselect dropdown select--}}
<!-- Initialize the plugin: -->
<script type="text/javascript">
    $('#example-multiple-selected').multiselect();
</script>
{{--multiselect dropdown select--}}


{{--Datable JS  end here--}}
{{--<script type="text/javascript">--}}
    {{--$(document).ready(function() {--}}
        {{--$('#datatable').DataTable( {--}}

             {{--dom: 'Bfrtip',--}}

            {{--buttons: [--}}
                {{--'copy', 'csv',--}}

                {{--{--}}
                    {{--extend: 'pdf',--}}
                    {{--download: 'open'--}}
                {{--}--}}
            {{--],--}}
            {{--select: true--}}
        {{--} );--}}
    {{--} );--}}

    {{--$(".selectall").click(function(){--}}
        {{--$(".individual").prop("checked",$(this).prop("checked"));--}}
    {{--});--}}
{{--</script>--}}

{{--/*************pdf**************/--}}
{{--<script type="text/javascript">--}}
    {{--$(document).ready(function() {--}}
        {{--$('#datatable').DataTable( {--}}
            {{--/******select checkbox*********/--}}
            {{--dom: 'Bfrtip',--}}
            {{----}}

        {{--} );--}}
    {{--} );--}}

{{--</script>--}}
{{--/*************pdf**************/--}}


{{--/*************datatable call**************/--}}

{{--<script type="text/javascript">--}}
    {{--$('#datatable').DataTable({--}}
        {{--"searching" : false--}}

    {{--});--}}
{{--</script>--}}
{{--/*************datatable call**************/--}}


{{--//table column data search start here--}}
{{--<script type="text/javascript">--}}
    {{--$(document).ready(function() {--}}
        {{--// Setup - add a text input to each footer cell--}}
        {{--$('#datatable thead tr').clone(true).appendTo( '#datatable thead' );--}}
        {{--$('#datatable thead tr:eq(1) th').each( function (i) {--}}
            {{--var title = $(this).text();--}}
            {{--$(this).html( '<input type="text" />' );--}}

            {{--$( 'input', this ).on( 'keyup change', function () {--}}
                {{--if ( table.column(i).search() !== this.value ) {--}}
                    {{--table--}}
                        {{--.column(i)--}}
                        {{--.search( this.value )--}}
                        {{--.draw();--}}
                {{--}--}}
            {{--} );--}}
        {{--} );--}}


    {{--} );--}}
{{--</script>--}}
{{--//table column data search end here--}}

{{--<script type="text/javascript">--}}
    {{--$('#datatable tbody').on( 'click', 'td', function () {--}}
        {{--var columnData = table--}}
            {{--.column( $(this).index()+':visIdx' )--}}
            {{--.data();--}}
    {{--} );--}}
    {{--var table = $('#datatable').DataTable();--}}

    {{--alert( 'Column index 0 is '+--}}
        {{--(table.column( i ).visible() === true ? 'visible' : 'not visible')--}}
    {{--);--}}
{{--</script>--}}

{{--<script type="text/javascript">--}}

{{--</script>--}}

{{--//table column data search end here--}}

{{--<script type="text/javascript" >--}}
    {{--$(document).on('click','.pagination a', function (e) {--}}
        {{--e.preventDefault();--}}
        {{--var page = $(this).attr('href').split('page=')[1];--}}
        {{--getFaculty(page);--}}
    {{--})--}}

    {{--function getFaculty(page) {--}}
        {{--$.ajax({--}}
            {{--url : '/ajax/faculty-info/?page='+page--}}
        {{--}).done(function (data) {--}}
            {{--console.log(data);--}}
        {{--});--}}
    {{--}--}}
{{--</script>--}}

<script type="text/javascript">
    // Animations initialization
   // new WOW().init();
</script>
<script type="text/javascript">
    $('.dropdown-toggle').dropdown()

</script>

{{--customize search box--}}
{{--<script type="text/javascript">--}}
    {{--var table = $('#datatable').DataTable();--}}

    {{--$('#firstInput').on( 'keyup', function () {--}}
        {{--table.search( this.value ).draw();--}}
    {{--} )--}}
{{--</script>--}}

{{--<script type="text/javascript">--}}
    {{--var table = $('#datatable').DataTable();--}}

    {{--$('#secondInput').on( 'keyup', function () {--}}
        {{--table.search( this.value ).draw();--}}
    {{--} )--}}
{{--</script>--}}
{{--<script type="text/javascript">--}}
    {{--$(document).ready(function() {--}}
        {{--$('#datatable').DataTable( {--}}
            {{--"paging":   false,--}}
            {{--"ordering": false,--}}
            {{--"info":     false--}}
        {{--} );--}}
    {{--} );--}}

{{--</script>--}}
{{--customize search box--}}


</body>

</html>
