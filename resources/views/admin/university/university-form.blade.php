@extends('admin.master')

@section('title')
   <title>University</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Add New University</span>
                    </h4>
                    <a  class="btn btn-outline-info btn-rounded btn-secondary btn-lg white-text" href="{{ url('/database/university/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>

                </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <form class="col-md-12 mb-4" action="{{ url('/database/university/add') }}" method="POST">

                        @csrf
                    <!-- Form with panel -->
                    <div class="card card-cascade narrower">

                        <div class="px-4">

                            <div class="table-wrapper">

                                <label class="form-label">University :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="university name" name="university" type="text" id="form1" required class="form-control">
                                    <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                </div>


                                <label class="form-label">Address :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="address"name="address" type="text" id="form1" class="form-control">
                                    <span style="color: red">{{ $errors->has('address') ? $errors->first('address') : ' ' }}</span>
                                </div>

                                <label class="form-label">District :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                <?php $districts = DB::table('kos_districts')->get();?>
                                    <select class="mdb-select"  name="district" searchable="Search here..">
                                        <option value="" ></option>
                                        @foreach($districts as $district )
                                        <option value="{{ $district->id }}">{{ $district->name }}</option>
                                            @endforeach
                                    </select>
                                </div>

                                <label class="form-label">Zipcode :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="zip-code" name="zipcode" type="number" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Establish Date :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Establish Date" type="date" name="establish_date" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Phone :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Phone" type="number" name="phone" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Fax :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Fax" type="text" name="fax" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Email :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input placeholder="Email" type="email" name="email" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Status :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <select class="mdb-select" name="status" searchable="Search here..">
                                        <option value="1"></option>
                                        <option value="0">Published</option>
                                        <option value="1">Unpublished</option>
                                    </select>
                                </div>

                                <label  class="form-label">Yes/No :</label>
                                <select class="mdb-select" name="yesno" searchable="Search here..">
                                    <option value="1" ></option>
                                    <option value="0" >No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                            <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg waves-effect z-depth-0" type="submit">Save</button>

                        </div>

                    </div>
                </form>
                    <!-- Table with panel -->
                </div>

            </div>
            <!--Grid row-->

        </div>
    </main>
@endsection
