@extends('admin.master')

@section('title')
    <title>University</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Edit University</span>
                    </h4>
                    <a  class="btn btn-outline-info btn-rounded btn-secondary btn-lg white-text" href="{{ url('/database/university/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>

                </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <form class="col-md-12 mb-4" action="{{ url('/database/university/update') }}" method="POST">

                @csrf
                <!-- Form with panel -->
                    <div class="card card-cascade narrower">

                        <!--Card image-->
                        <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-end align-items-center">
                            @if( Session::get('adminRole') == 'super-admin')
                            <div>
                                <a href="{{ url('/database/university/form') }}"  title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                    <i title="Add" class="custom-fontawesome fas fa-plus-circle"></i>
                                </a>
                                <a href="{{ url('/database/university/copy/'.$universityById->id) }}" title="Add" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                    <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                </a>
                                <a href="{{ url('/database/university/delete/'.$universityById->id) }}" title="Add" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                    <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                </a>
                            </div>
                                @else
                                <div>
                                    <?php $userId       = Session::get('adminId');?>
                                    <?php $permissions  = \App\KosPermission::where('role_id',$userId)->get();?>
                                    @foreach($permissions as $permission )
                                        @if($permission->permission == 'edit')
                                            <button type="submit" title="Save" class="transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                <i class="custom-fontawesome fas fa-save"></i>
                                            </button>
                                        @endif
                                        @if($permission->permission == 'add')
                                            <a href="{{ url('/database/university/form') }}" title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                <i class="custom-fontawesome fas fa-plus-circle"></i>
                                            </a>
                                        @endif
                                        @if($permission->permission == 'copy')
                                            <a href="{{ url('/database/university/copy/'.$universityById->id) }}" title="Copy" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                            </a>
                                        @endif
                                        @if($permission->permission == 'delete')
                                            <a href="{{ url('/database/university/delete/'.$universityById->id) }}" value="delete" onclick="return confirm('Are you sure to delete it !!!')" title="Delete" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                            </a>
                                        @endif
                                    @endforeach
                                </div>
                                @endif

                        </div>
                        <!--/Card image-->

                        <div class="px-4">

                            <div class="table-wrapper">

                                <label class="form-label">University :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input value="{{$universityById->id}}"  name="id" type="hidden" id="form1" required class="form-control">
                                    <input value="{{$universityById->university}}"  name="university" type="text" id="form1" required class="form-control">
                                    <span style="color: red">{{ $errors->has('university') ? $errors->first('university') : ' ' }}</span>
                                </div>


                                <label class="form-label">Address :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input value="{{$universityById->address}}" name="address" type="text" id="form1" class="form-control">
                                    <span style="color: red">{{ $errors->has('address') ? $errors->first('address') : ' ' }}</span>
                                </div>

                                <label class="form-label">District :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <?php $districts = DB::table('kos_districts')->get();?>
                                    <select class="mdb-select"  name="district" searchable="Search here..">
                                        <option value="" >Select District</option>
                                        @foreach($districts as $district )
                                            <option value="{{ $district->id }}">{{ $district->name }}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <label class="form-label">Zipcode :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input value="{{$universityById->zipcode}}" name="zipcode" type="number" id="form1" class="form-control">
                                </div>



                                <label class="form-label">Establish Date :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input value="{{$universityById->establish_date}}" type="date" name="establish_date" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Phone :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input value="{{$universityById->phone}}" type="number" name="phone" id="form1" class="form-control">
                                    <span style="color: red">{{ $errors->has('phone') ? $errors->first('phone') : ' ' }}</span>
                                </div>

                                <label class="form-label">Fax :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input value="{{$universityById->fax}}" type="text" name="fax" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Email :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <input value="{{$universityById->email}}" type="email" name="email" id="form1" class="form-control">
                                </div>

                                <label class="form-label">Status :</label>
                                <!-- Material input -->
                                <div class="md-form">
                                    <select class="mdb-select" name="status" searchable="Search here..">
                                        @if($universityById->status==1)
                                        <option value="1">Published</option>
                                        <option value="0">Unpublished</option>
                                            @else
                                            <option value="0">Unpublished</option>
                                            <option value="1">Unpublished</option>
                                        @endif
                                    </select>
                                </div>

                                <label  class="form-label">Yes/No :</label>
                                <select class="mdb-select" name="yesno" searchable="Search here..">
                                 @if($universityById->yesno==1)
                                    <option value="1" >Yes</option>
                                    <option value="0">No</option>
                                     @else
                                        <option value="0" >No</option>
                                        <option value="1">Yes</option>
                                     @endif
                                </select>
                            </div>
                            <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-lg btn-rounded  waves-effect z-depth-0" type="submit">Update It</button>
                        </div>

                    </div>
                </form>
                <!-- Table with panel -->
            </div>

        </div>
        <!--Grid row-->

        </div>
    </main>
@endsection
