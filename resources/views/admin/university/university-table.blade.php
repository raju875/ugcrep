@extends('admin.master')

@section('title')
    <title>University</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>University</span>
                    </h4>
                    <input placeholder="Search" type="text" id="search-default">

                </div>
            </div>
            <!-- Heading -->
            @if(Session::has('message'))
                <h5 class="text text-center text-success">{{ Session::get('message') }}</h5>
            @endif
            @if(Session::has('alert'))
                <h5 class="text text-center text-danger">{{ Session::get('alert') }}</h5>
        @endif
        <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Table with panel -->
                    <div class="card card-cascade narrower">
                        <!--Form Starts-->
                        <form action="{{ url('/database/university/edit') }}" method="POST">
                        @csrf
                        <!--Card image-->
                            <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">

                                <?php $count = DB::table('kos_universities')->count(); ?>
                                <label>Total Items {{ $count }}</label>

                                    @if( Session::get('adminRole') == 'super-admin')
                                        <div>
                                            <a href="{{ url('/database/university/form') }}" title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                <i class="custom-fontawesome fas fa-plus-circle"></i>
                                            </a>
                                            <button type="submit" value="copy" name="submit" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                            </button>

                                            <button type="submit" value="delete" name="submit" onclick="return confirm('Are you sure to Delete it !!!')" title="Delete" class="transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                            </button>

                                        </div>
                                    @else
                                        <div>
                                            <?php $userId = Session::get('adminId');?>
                                            <?php $permissions = \App\KosPermission::where('role_id',$userId)->get();?>
                                            @foreach($permissions as $permission )
                                                @if($permission->permission == 'add')
                                                    <a href="{{ url('/database/university/form') }}" title="Add" class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                        <i class="custom-fontawesome fas fa-plus-circle"></i>
                                                    </a>
                                                @endif
                                                @if($permission->permission == 'copy')
                                                    <button type="submit" value="copy" name="submit" onclick="return confirm('Are you sure to Copy it !!!')" title="Copy" class="transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                        <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                                    </button>
                                                @endif
                                                @if($permission->permission == 'delete')
                                                    <button type="submit" value="delete" name="submit" onclick="return confirm('Are you sure to Delete it !!!')" title="Delete" class="transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                        <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                                    </button>

                                                @endif
                                            @endforeach
                                        </div>
                                    @endif

                            </div>
                            <!--/Card image-->


                            <div class="px-4">

                                <div class="table-wrapper table-responsive">
                                    <!--Table-->

                                    <table id="datatable" class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th><input type="checkbox" class="selectall"></th>
                                            <th>University</th>
                                            <th>Address</th>
                                            <th>District</th>
                                            <th>Zip code</th>
                                            <th>Establish Date</th>
                                            <th>Phone</th>
                                            <th>Fax</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Yes/No</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(Session::get('adminRole') == 'super-admin')
                                            @foreach($universities as $university )
                                                    <tr>
                                                        <td><input type="checkbox" class="individual" name="university[]"  value="{{ $university->id }}"></td>
                                                        <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->university }}</a></td>
                                                        <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->address }}</a></td>
                                                        <td>
                                                            <a href="{{ url('/database/university/edit/'.$university->id) }}">
                                                                <?php $district = \App\KosDistrict::find($university->id) ?>
                                                            </a>
                                                        </td>
                                                        <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->zipcode }}</a></td>
                                                        <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->establish_date }}</a></td>
                                                        <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->phone }}</a></td>
                                                        <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->fax }}</a></td>
                                                        <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->email }}</a></td>
                                                        <td><a href="{{ url('/database/university/edit/'.$university->id) }}">@if($university->status == 1 ){{'Published'}}@else{{ 'Unpublished' }}@endif</a></td>
                                                        <td><a href="{{ url('/database/university/edit/'.$university->id) }}">@if($university->yesno == 1 ){{'Yes'}}@else{{ 'No' }}@endif</a></td>
                                                    </tr>
                                            @endforeach
                                            @else
                                        <?php $universityAccess = \App\KosUniversityPermission::where('role_id',Session::get('adminId'))->get()?>
                                        @foreach($universityAccess as $access )
                                            @foreach($universities as $university )
                                                @if($access->university_id == $university->id )
                                            <tr>
                                                <td><input type="checkbox" class="individual" name="university[]"  value="{{ $university->id }}"></td>
                                                <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->university }}</a></td>
                                                <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->address }}</a></td>
                                                <td>
                                                    <a href="{{ url('/database/university/edit/'.$university->id) }}">
                                                        <?php $district = \App\KosDistrict::find($university->id) ?>
                                                        </a>
                                                </td>
                                                <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->zipcode }}</a></td>
                                                <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->establish_date }}</a></td>
                                                <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->phone }}</a></td>
                                                <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->fax }}</a></td>
                                                <td><a href="{{ url('/database/university/edit/'.$university->id) }}">{{ $university->email }}</a></td>
                                                <td><a href="{{ url('/database/university/edit/'.$university->id) }}">@if($university->status == 1 ){{'Published'}}@else{{ 'Unpublished' }}@endif</a></td>
                                                <td><a href="{{ url('/database/university/edit/'.$university->id) }}">@if($university->yesno == 1 ){{'Yes'}}@else{{ 'No' }}@endif</a></td>
                                            </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection


