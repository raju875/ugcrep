@extends('admin.master')

@section('title')
    <title>Department</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Add New Department</span>
                    </h4>

                    <a class="btn btn-outline-info btn-rounded btn-secondary btn-lg white-text" href="{{ url('/database/department/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>

                </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Form with panel -->
                    <form action="{{ url('/database/department/add') }}" method="POST">
                        @csrf
                        <div class="card card-cascade narrower">
                            <div class="px-4">

                                <div class="table-wrapper">

                                    <label class="form-label pt-5 ">Department</label>
                                    <!-- Material input -->
                                    <div class="md-form pb-3">
                                        <input  name="department" required type="text" id="form1" placeholder="Department Name" class="form-control">
                                        <span style="color: red">{{ $errors->has('department') ? $errors->first('department') : ' ' }}</span>
                                    </div>

                                    <button style="color: white" class="mt-2 mb-lg-5 btn btn-outline-info btn-rounded btn-secondary btn-lg" type="submit">Save</button>


                                </div>

                            </div>

                        </div>
                    </form>
                    <!-- Table with panel -->
                </div>

            </div>
            <!--Grid row-->

        </div>
    </main>
@endsection
