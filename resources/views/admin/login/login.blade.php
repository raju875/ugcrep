<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Center Info</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('admin/asset/') }}/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('admin/asset/') }}/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{ asset('admin/asset/') }}/css/style.min.css" rel="stylesheet">
    <link href="{{ asset('admin/asset/') }}/css/style.css" rel="stylesheet">
</head>

<body class="grey lighten-3">
<div class="row mt-5">
    <div class="col-md-6  mx-auto">


        <!-- Material form login -->
        <div class="card">

            <h5 class="card-header info-color white-text text-center py-4">
                <strong>Sign in</strong>
            </h5>
            @if(Session::has('message'))
                <h5 class="text text-center text-success">{{ Session::get('message') }}</h5>
        @endif
            <!--Card content-->
            <div class="card-body px-lg-5 pt-0">

                <!-- Form -->
                <form action="{{ url('/login_check') }}" method="POST" class="text-center" style="color: #757575;">
                    @csrf

                    <!-- Email -->
                    <div class="md-form">
                        <input type="email" name="email" id="materialLoginFormEmail" class="form-control">
                        <label for="materialLoginFormEmail">E-mail</label>
                    </div>

                    <!-- Password -->
                    <div class="md-form">
                        <input type="password" name="password" id="materialLoginFormPassword" class="form-control">
                        <label for="materialLoginFormPassword">Password</label>
                    </div>
                        {{--<div class="md-form">--}}
                        {{--<input type="password" name="password" id="materialLoginFormPassword" class="form-control">--}}
                        {{--<label for="materialLoginFormPassword">Password</label>--}}
                    {{--</div>--}}

                    <div class="d-flex justify-content-around">
                    </div>
                    <!-- Captcha -->
                    <div class='m-5'>
                        <?php $first  = rand(1,3) ?>
                        <?php $second = rand(4,6) ?>
                        <?php $result = $first*$second ?>
                        <?php  echo $first.'*'.$second ?>
                        <input type="hidden" name="res1" value="{{ $result}}">
                        <input class="" type='text' name="res2" required id=''></input>  @if(Session::has('alert'))<h5 class="text text-center text-danger">{{ Session::get('alert') }}</h5>@endif
                    </div>
                    <!-- Sign in button -->
                    <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">Sign in</button>
                </form>
                <!-- Form -->

            </div>

        </div>
        <!-- Material form login -->
    </div>
</div>
<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="{{ asset('admin/asset/') }}/js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{ asset('admin/asset/') }}/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{ asset('admin/asset/') }}/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{ asset('admin/asset/') }}/js/mdb.min.js"></script>
<!-- Initializations -->
<script type="text/javascript">
    // Animations initialization
    new WOW().init();
</script>
</body>

</html>
