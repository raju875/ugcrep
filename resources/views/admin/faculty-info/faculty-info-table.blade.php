@extends('admin.master')

@section('title')
    <title>Faculty Info</title>
@endsection

@section('body')
    <div class="content-div">
        <div class="custom-container">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->

                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Faculty Info</span>
                    </h4>

                    <input placeholder="Search One" type="text" id="search-default">
                    <input placeholder="Search Two" type="text" id="search-filter">


                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                        Show Field
                    </button>

                    <!-- Modal -->
                    <div class="centre modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content m-lg-5">
                                <form action="{{ url('/database/faculty-info/modal-listing') }}" method="POST">
                                    @csrf
                                    <ul style="list-style-type: none">
                                        <li>
                                            <input type="checkbox" class="allModal"> Select All
                                        </li>
                                        <li>
                                            @if(Session::has('facultyInfoUniversity'))
                                                <input class="singelModal" type="checkbox" id="university"
                                                       name="column[]" value="university" checked="checked"> University
                                            @else
                                                <input class="singelModal" type="checkbox" id="university"
                                                       name="column[]" value="university"> University
                                            @endif
                                        </li>
                                        <li>
                                            @if(Session::has('facultyInfoFaculty'))
                                                <input class="singelModal" type="checkbox" id="faculty" name="column[]"
                                                       value="faculty" checked="checked"> Faculty
                                            @else
                                                <input class="singelModal" type="checkbox" id="faculty" name="column[]"
                                                       value="faculty"> Faculty
                                            @endif
                                        </li>

                                        <li>
                                            @if(Session::has('facultyInfoFacultyDateOfApproval'))
                                                <input class="singelModal" type="checkbox" id="faculty_date_of_approval"
                                                       name="column[]" value="faculty_date_of_approval"
                                                       checked="checked"> Faculty date of approval
                                            @else
                                                <input class="singelModal" type="checkbox" id="faculty_date_of_approval"
                                                       name="column[]" value="faculty_date_of_approval"> Faculty date of
                                                approval
                                            @endif
                                        </li>
                                        <li>
                                            @if(Session::has('facultyInfoDepartment'))
                                                <input class="singelModal" type="checkbox" id="department"
                                                       name="column[]" value="department" checked="checked"> Department
                                            @else
                                                <input class="singelModal" type="checkbox" id="department"
                                                       name="column[]" value="department"> Department
                                            @endif
                                        </li>
                                        <li>
                                            @if(Session::has('facultyInfoDepartmentDateOfApproval'))
                                                <input class="singelModal" type="checkbox"
                                                       id="department_date_of_approval" name="column[]"
                                                       value="department_date_of_approval" checked="checked"> Department
                                                date of approval
                                            @else
                                                <input class="singelModal" type="checkbox"
                                                       id="department_date_of_approval" name="column[]"
                                                       value="department_date_of_approval"> Department date of approval
                                            @endif
                                        </li>
                                        <li>
                                            @if(Session::has('facultyInfoPrograms'))
                                                <input class="singelModal" type="checkbox" id="programs" name="column[]"
                                                       value="programs" checked="checked"> Programs
                                            @else
                                                <input class="singelModal" type="checkbox" id="programs" name="column[]"
                                                       value="programs"> Programs
                                            @endif
                                        </li>
                                        <li>
                                            @if(Session::has('facultyInfoProgramDateOfApproval'))
                                                <input class="singelModal" type="checkbox" id="program_date_of_approval"
                                                       name="column[]" value="program_date_of_approval"
                                                       checked="checked"> Program date of approval
                                            @else
                                                <input class="singelModal" type="checkbox" id="program_date_of_approval"
                                                       name="column[]" value="program_date_of_approval"> Program date of
                                                approval
                                            @endif
                                        </li>
                                        <li>
                                            @if(Session::has('facultyInfoSession'))
                                                <input class="singelModal" type="checkbox" id="session" name="column[]"
                                                       value="session" checked="checked"> Session
                                            @else
                                                <input class="singelModal" type="checkbox" id="session" name="column[]"
                                                       value="session"> Session
                                            @endif
                                        </li>

                                        <li>
                                            @if(Session::has('facultyInfoNoOfSeat'))
                                                <input class="singelModal" type="checkbox" id="no_of_seat"
                                                       name="column[]" value="no_of_seat" checked="checked"> No of Seat
                                            @else
                                                <input class="singelModal" type="checkbox" id="no_of_seat"
                                                       name="column[]" value="no_of_seat"> No of Seat
                                            @endif
                                        </li>
                                        <li>
                                            @if(Session::has('facultyInfoEnrolledStudents'))
                                                <input class="singelModal" type="checkbox" id="enrolled_students"
                                                       name="column[]" value="enrolled_students" checked="checked">
                                                Enrolled students
                                            @else
                                                <input class="singelModal" type="checkbox" id="enrolled_students"
                                                       name="column[]" value="enrolled_students"> Enrolled students
                                            @endif
                                        </li>

                                        <li>
                                            @if(Session::has('facultyInfoMaleStudents'))
                                                <input class="singelModal" type="checkbox" id="male_students"
                                                       name="column[]" value="male_students" checked="checked"> Male
                                                students
                                            @else
                                                <input class="singelModal" type="checkbox" id="male_students"
                                                       name="column[]" value="male_students"> Male students
                                            @endif
                                        </li>

                                        <li>
                                            @if(Session::has('facultyInfoFemaleStudents'))
                                                <input class="singelModal" type="checkbox" id="female_students"
                                                       name="column[]" value="female_students" checked="checked"> Female
                                                students
                                            @else
                                                <input class="singelModal" type="checkbox" id="female_students"
                                                       name="column[]" value="female_students"> Female students
                                            @endif
                                        </li>
                                        <li>
                                            @if(Session::has('facultyInfoCurrentStudents'))
                                                <input class="singelModal" type="checkbox" id="current_students"
                                                       name="column[]" value="current_students" checked="checked">
                                                Current students
                                            @else
                                                <input class="singelModal" type="checkbox" id="current_students"
                                                       name="column[]" value="current_students"> Current students
                                            @endif
                                        </li>
                                        <li>
                                            @if(Session::has('facultyInfoYear'))
                                                <input class="singelModal" type="checkbox" id="year" name="column[]"
                                                       value="year" checked="checked"> Year
                                            @else
                                                <input class="singelModal" type="checkbox" id="year" name="column[]"
                                                       value="year"> Year
                                            @endif
                                        </li>
                                        <li>
                                            @if(Session::has('facultyInfoRemarks'))
                                                <input class="singelModal" type="checkbox" id="remarks" name="column[]"
                                                       value="remarks" checked="checked"> Remarks
                                            @else
                                                <input class="singelModal" type="checkbox" id="remarks" name="column[]"
                                                       value="remarks"> Remarks
                                            @endif
                                        </li>

                                    </ul>

                                    <button style="width: 150px;margin-left: 35px;" type="submit"
                                            class="btn btn-secondary">
                                        Save Changes
                                    </button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Heading -->
            @if(Session::has('message'))
                <h4 class="text text-center text-success">{{ Session::get('message') }}</h4>
            @endif
            @if(Session::has('alert'))
                <h4 class="text text-center text-danger">{{ Session::get('alert') }}</h4>
        @endif
        <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Table with panel -->
                    <div class="card card-cascade narrower">
                        <!--Form Starts-->
                        <form action="{{ url('/database/faculty-info/edit') }}" method="POST">
                        @csrf
                        <!--Card image-->
                            <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">

                                {{--<div>--}}
                                @if( Session::get('adminRole') == 'super-admin')
                                    <?php $count = DB::table('kos_faculty_infos')->count(); ?>
                                    <label>Total Items {{ $count }}</label>
                                    <div>
                                        <a href="{{ url('/database/faculty-info/form') }}" title="Add"
                                           class="btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i class="custom-fontawesome fas fa-plus-circle"></i>
                                        </a>
                                        <button type="submit" name="submit" value="report" title="Report"
                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Report" class="custom-fontawesome fas fa-pen-square"></i>
                                        </button>
                                        <button type="submit" value="copy" name="submit"
                                                onclick="return confirm('Are you sure to Copy it !!!')" title="Copy"
                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                        </button>
                                        <button type="submit" value="delete" name="submit"
                                                onclick="return confirm('Are you sure to Delete it !!!')" title="Delete"
                                                class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                            <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                        </button>

                                    </div>
                                @else
                                    <div>
                                        <?php $userId = Session::get('adminId');?>
                                        <?php $permissions = \App\KosPermission::where('role_id', $userId)->get();?>
                                        @foreach($permissions as $permission )
                                            @if($permission->permission == 'add')
                                                <a href="{{ url('/database/faculty-info/form') }}" title="Add"
                                                   class="btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i class="custom-fontawesome fas fa-plus-circle"></i>
                                                </a>
                                            @endif
                                            @if($permission->permission == 'report')
                                                <button type="submit" name="submit" value="report" title="Report"
                                                        class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i title="Report" class="custom-fontawesome fas fa-pen-square"></i>
                                                </button>
                                            @endif
                                            @if($permission->permission == 'copy')
                                                <button type="submit" value="copy" name="submit"
                                                        onclick="return confirm('Are you sure to Copy it !!!')"
                                                        title="Copy"
                                                        class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i title="Copy" class="custom-fontawesome far fa-clone"></i>
                                                </button>
                                            @endif
                                            @if($permission->permission == 'delete')
                                                <button type="submit" value="delete" name="submit"
                                                        onclick="return confirm('Are you sure to Delete it !!!')"
                                                        title="Delete"
                                                        class=" transparent btn btn-outline-white btn-rounded btn-sm px-2">
                                                    <i title="Delete" class="custom-fontawesome far fa-trash-alt"></i>
                                                </button>
                                            @endif
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                            <!--/Card image-->


                            <div class="px-4">

                                <div class="table-wrapper table-responsive">
                                    <!--Table-->
                                    {{--**************For Super Admin start here**************--}}
                                    @if(Session::get('adminRole') == 'super-admin')
                                        <table id="datatable" class="table  table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                {{--this is hidden column by default checked all--}}
                                                <th style="display: none;"><input type="checkbox" checked
                                                                                  class="selectall"></th>

                                                <th><input type="checkbox" class="all"></th>
                                                <th class="university">University</th>
                                                <th class="faculty">Faculty</th>
                                                <th class="faculty_date_of_approval">Faculty date of approval</th>
                                                <th class="department">Department</th>
                                                <th class="department_date_of_approval">Department date of approval</th>
                                                <th class="programs">Programs</th>
                                                <th class="program_date_of_approval">Program date of approval</th>
                                                <th class="session">Session</th>
                                                <th class="no_of_seat">No of seat</th>
                                                <th class="enrolled_students">Enrolled students</th>
                                                <th class="male_students">Male students</th>
                                                <th class="female_students">Female students</th>
                                                <th class="current_students">Current students</th>
                                                <th class="year">Year</th>
                                                <th class="remarks">Remarks</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php $facutyInfos = DB::table('kos_faculty_infos')->orderBy('id', 'desc')->paginate(100);?>
                                            @foreach($facutyInfos as $info )
                                                <tr>
                                                    {{--this is hidden column by default checked all--}}
                                                    <td style="display: none;"><input checked type="checkbox"
                                                                                      class="individual"
                                                                                      name="faculty_info_hide[]"
                                                                                      value="{{ $info->id }}"></td>

                                                    <td><input type="checkbox" class="single" name="faculty_info[]"
                                                               value="{{ $info->id }}"></td>
                                                    <td class="university">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            <?php $university = DB::table('kos_universities')->where('id', $info->university)->get()?>
                                                            @foreach($university as $uni)
                                                                {{ $uni->university }}
                                                            @endforeach
                                                        </a>
                                                    </td>

                                                    <td class="faculty">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            <?php $faculty = \App\KosFacultyInfo::where('faculty', $info->faculty)->first()?>
                                                            <?php $fcl = \App\KosFaculty::find($faculty->faculty)?>
                                                            {{ $fcl->faculty }}
                                                        </a>
                                                    </td>

                                                    <td class="faculty_date_of_approval">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->faculty_date_of_approval }}
                                                        </a>
                                                    </td>

                                                    <td class="department">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            <?php $department = DB::table('kos_departments')->where('id', $info->department)->get()?>
                                                            @foreach($department as $department)
                                                                {{ $department->department }}
                                                            @endforeach
                                                        </a>
                                                    </td>

                                                    <td class="department_date_of_approval">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->department_date_of_approval }}
                                                        </a>
                                                    </td>

                                                    <td class="programs">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            <?php $program = DB::table('kos_programs')->where('id', $info->programs)->get()?>
                                                            @foreach($program as $program)
                                                                {{ $program->program }}
                                                            @endforeach
                                                        </a>
                                                    </td>

                                                    <td class="program_date_of_approval">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->	program_date_of_approval }}
                                                        </a>
                                                    </td>

                                                    <td class="session">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            <?php $session = DB::table('kos_sessions')->where('id', $info->session)->get()?>
                                                            @foreach($session as $session)
                                                                {{ $session->session }}
                                                            @endforeach
                                                        </a>
                                                    </td>

                                                    <td class="no_of_seat">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->no_of_seat }}
                                                        </a>
                                                    </td>

                                                    <td class="enrolled_students">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->enrolled_students }}
                                                        </a>
                                                    </td>

                                                    <td class="male_students">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->male_students }}
                                                        </a>
                                                    </td>
                                                    <td class="female_students">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->female_students }}
                                                        </a>
                                                    </td>
                                                    <td class="current_students">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->current_students }}
                                                        </a>
                                                    </td>
                                                    <td class="year">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->year }}
                                                        </a>
                                                    </td>
                                                    <td class="remarks">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->remarks }}
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        {{ $facutyInfos->links() }}
                                        {{--**************For Super Admin End here**************--}}


                                        {{--**************For User or Admin start here**************--}}

                                    @else
                                        <table id="datatable" class="table  table-hover table-bordered">
                                            <tr>
                                                {{--this is hidden column by default checked all--}}
                                                <th style="display: none;"><input type="checkbox" checked
                                                                                  class="selectall"></th>

                                                <th><input type="checkbox" class="all"></th>
                                                <th class="university">University</th>
                                                <th class="faculty">Faculty</th>
                                                <th class="faculty_date_of_approval">Faculty date of approval</th>
                                                <th class="department">Department</th>
                                                <th class="department_date_of_approval">Department date of approval</th>
                                                <th class="programs">Programs</th>
                                                <th class="program_date_of_approval">Program date of approval</th>
                                                <th class="session">Session</th>
                                                <th class="no_of_seat">No of seat</th>
                                                <th class="enrolled_students">Enrolled students</th>
                                                <th class="male_students">Male students</th>
                                                <th class="female_students">Female students</th>
                                                <th class="current_students">Current students</th>
                                                <th class="year">Year</th>
                                                <th class="remarks">Remarks</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php $count = DB::table('kos_faculty_infos')
                                                ->join('kos_university_permissions', 'kos_faculty_infos.university', '=', 'kos_university_permissions.university_id')
                                                ->join('kos_universities', 'kos_faculty_infos.university', '=', 'kos_universities.id')
                                                ->where('kos_university_permissions.role_id', Session::get('adminId'))
                                                ->count()?>
                                            <label>Total Items {{ $count }}</label>

                                            <?php $facultyInfos = DB::table('kos_faculty_infos')
                                                ->join('kos_university_permissions', 'kos_faculty_infos.university', '=', 'kos_university_permissions.university_id')
                                                ->join('kos_universities', 'kos_faculty_infos.university', '=', 'kos_universities.id')
                                                ->where('kos_university_permissions.role_id', Session::get('adminId'))
                                                ->select('kos_faculty_infos.*', 'kos_universities.university')->orderBy('id', 'desc')
                                                ->paginate(100)?>

                                            @foreach($facultyInfos as $info )
                                                <tr>
                                                    {{--this is hidden column by default checked all--}}
                                                    <td style="display: none;"><input checked type="checkbox"
                                                                                      class="individual"
                                                                                      name="faculty_info_hide[]"
                                                                                      value="{{ $info->id }}"></td>

                                                    <td><input type="checkbox" class="single" name="faculty_info[]"
                                                               value="{{ $info->id }}"></td>

                                                    <td class="university">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->university }}
                                                        </a>
                                                    </td>

                                                    <td class="faculty">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->faculty }}
                                                        </a>
                                                    </td>

                                                    <td class="faculty_date_of_approval">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->faculty_date_of_approval }}
                                                        </a>
                                                    </td>

                                                    <td class="department">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->department }}
                                                        </a>
                                                    </td>

                                                    <td class="department_date_of_approval">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->department_date_of_approval }}
                                                        </a>
                                                    </td>

                                                    <td class="programs">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            <?php $program = DB::table('kos_programs')->where('id', $info->programs)->get()?>
                                                            {{ $info->programs }}
                                                        </a>
                                                    </td>

                                                    <td class="program_date_of_approval">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->	program_date_of_approval }}
                                                        </a>
                                                    </td>

                                                    <td class="session">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->session }}
                                                        </a>
                                                    </td>

                                                    <td class="no_of_seat">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->no_of_seat }}
                                                        </a>
                                                    </td>

                                                    <td class="enrolled_students">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->enrolled_students }}
                                                        </a>
                                                    </td>

                                                    <td class="male_students">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->male_students }}
                                                        </a>
                                                    </td>
                                                    <td class="female_students">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->female_students }}
                                                        </a>
                                                    </td>
                                                    <td class="current_students">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->current_students }}
                                                        </a>
                                                    </td>
                                                    <td class="year">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->year }}
                                                        </a>
                                                    </td>
                                                    <td class="remarks">
                                                        <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                                                            {{ $info->remarks }}
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        {{ $facultyInfos->links() }}

                                    @endif
                                    {{--**************For User or Admin ends here**************--}}

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection



