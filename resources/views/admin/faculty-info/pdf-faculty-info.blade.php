<div style="text-align: center; padding-top: 50px;font-family: arial, sans-serif;border-collapse: collapse;">
    <h2>Public University Management Information System (PUMIS)</h2>
    {{--if selected university names are same start--}}
    @if(Session::get('facultyInfoUniversity'))
        @if($uni==1 )
            @foreach ($facultyInfos as $facultyInfo )
                <?php $facultyInfoById = \App\KosFacultyInfo::find($facultyInfo); ?>
                <?php $university = \App\KosUniversity::find($facultyInfoById->university); ?>
                <h3>{{ $university->university }}</h3>
                @break
            @endforeach
        @endif
    @endif
    {{--if selected university names are same start--}}
</div>
<style>
    table,th,td{
        border: 1px solid #ddd;
        border-collapse: collapse;
        font-size: 11px;

    }
    th{
        padding: 12px 0;
        text-align: center;
    }
</style>


<table width="100%">
    <thead>
    <tr>
        @if(Session::get('facultyInfoUniversity'))
            @if($uni==0)
                <th   >{{ 'University' }}</th>
            @endif
        @endif

        @if(Session::get('facultyInfoFaculty'))
            <th   >{{ 'Faculty' }}</th>
        @endif

        @if(Session::get('facultyInfoFacultyDateOfApproval'))
            <th   >{{ 'Faculty date of approval' }}</th>
        @endif

        @if(Session::get('facultyInfoDepartment'))
            <th   >{{ 'Department' }}</th>
        @endif

        @if(Session::get('facultyInfoDepartmentDateOfApproval'))
            <th   >{{ 'Department date of approval' }}</th>
        @endif

        @if(Session::get('facultyInfoPrograms'))
            <th   >{{ 'Programs' }}</th>
        @endif

        @if(Session::get('facultyInfoProgramDateOfApproval'))
            <th   >{{ 'Program date of approval' }}</th>
        @endif

        @if(Session::get('facultyInfoSession'))
            <th   >{{ 'Session' }}</th>
        @endif

        @if(Session::get('facultyInfoNoOfSeat'))
            <th   >{{ 'No of seat' }}</th>
        @endif

        @if(Session::get('facultyInfoEnrolledStudents'))
            <th   >{{ 'Enrolled students' }}</th>
        @endif

        @if(Session::get('facultyInfoMaleStudents'))
            <th   >{{ 'Male students' }}</th>
        @endif

        @if(Session::get('facultyInfoFemaleStudents'))
            <th   >{{ 'Female students' }}</th>
        @endif

        @if(Session::get('facultyInfoCurrentStudents'))
            <th   >{{ 'Current students' }}</th>
        @endif

        @if(Session::get('facultyInfoYear'))
            <th   >{{ 'Year' }}</th>
        @endif

        @if(Session::get('facultyInfoRemarks'))
            <th   >{{ 'Remarks' }}</th>
        @endif

    </tr>
    </thead>
    <tbody>

    @foreach($facultyInfos as $facultyInfo )
        <tr   >

            <?php $facultyInfoById = \App\KosFacultyInfo::find($facultyInfo); ?>

            @if(Session::get('facultyInfoUniversity'))
                @if($uni==0 )
                    {{--//if selected university name is not same--}}
                    <?php $university = \App\KosUniversity::find($facultyInfoById->university); ?>
                    @if(!empty($university))
                        <td   >{{ $university->university }}</td>
                    @else
                        <td   ></td>
                    @endif
                @endif
            @endif

            @if(Session::get('facultyInfoFaculty'))
                <?php $faculty = \App\KosFaculty::find($facultyInfoById->faculty); ?>
                @if (!empty($faculty))
                    <td   >{{ $faculty->faculty }}</td>
                @else
                    <td   ></td>
                @endif
            @endif


            @if(Session::get('facultyInfoFacultyDateOfApproval'))
                @if($facultyInfoById->faculty_date_of_approval!='')
                    <td   >{{ $facultyInfoById->faculty_date_of_approval }}</td>
                @else
                    <td   ></td>
                @endif
            @endif

            @if(Session::get('facultyInfoDepartment'))
                <?php $department = \App\KosDepartment::where('id', $facultyInfoById->department)->first(); ?>
                @if (!empty($department))
                    <td   >{{ $department->department }}</td>
                @else
                    <td   ></td>
                @endif
            @endif

            @if(Session::get('facultyInfoDepartmentDateOfApproval'))
                @if($facultyInfoById->department_date_of_approval!='')
                    <td   >{{ $facultyInfoById->department_date_of_approval }}</td>
                @else
                    <td   ></td>
                @endif
            @endif

            @if(Session::get('facultyInfoPrograms'))
                <?php $program = \App\KosProgram::find($facultyInfoById->programs); ?>
                @if (!empty($program))
                    <td   >{{ $program->program }}</td>
                @else
                    <td   ></td>
                @endif
            @endif

            @if(Session::get('facultyInfoProgramDateOfApproval'))
                @if($facultyInfoById->program_date_of_approval!='')
                    <td   >{{ $facultyInfoById->program_date_of_approval }}</td>
                @else
                    <td   ></td>
                @endif
            @endif

            @if(Session::get('facultyInfoSession'))
                <?php $session = \App\KosSession::find($facultyInfoById->session); ?>
                @if (!empty($session))
                    <td   >{{ $session->session }}</td>
                    @else
                        <td   ></td>
                @endif
            @endif

            @if(Session::get('facultyInfoNoOfSeat'))
                @if($facultyInfoById->no_of_seat!='')
                    <td   >{{ $facultyInfoById->no_of_seat }}</td>
                @else
                    <td   ></td>
                @endif
            @endif

            @if(Session::get('facultyInfoEnrolledStudents'))
                @if($facultyInfoById->enrolled_students!='')
                <td   >{{ $facultyInfoById->enrolled_students }}</td>
                    @else
                        <td   ></td>
                    @endif
            @endif

            @if(Session::get('facultyInfoMaleStudents'))
                @if($facultyInfoById->male_students!='')
                    <td   >{{ $facultyInfoById->male_students }}</td>
                @else
                    <td   ></td>
                @endif
            @endif

            @if(Session::get('facultyInfoFemaleStudents'))
                @if($facultyInfoById->female_students!='')
                    <td   >{{ $facultyInfoById->female_students }}</td>
                @else
                    <td   ></td>
                @endif
            @endif

            @if(Session::get('facultyInfoCurrentStudents'))
                @if($facultyInfoById->current_students!='')
                    <td   >{{ $facultyInfoById->current_students }}</td>
                @else
                    <td   ></td>
                @endif
            @endif

            @if(Session::get('facultyInfoYear'))
                @if($facultyInfoById->year!='')
                    <td   >{{ $facultyInfoById->year }}</td>
                @else
                    <td   ></td>
                @endif
            @endif

            @if(Session::get('facultyInfoRemarks'))
                @if($facultyInfoById->remarks!='')
                    <td   >{{ $facultyInfoById->remarks }}</td>
                @else
                    <td   ></td>
                @endif
            @endif

        </tr>

    @endforeach
    </tbody>
</table>
