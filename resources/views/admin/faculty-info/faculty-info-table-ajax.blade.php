
    @foreach($facutyInfos as $info )
        <tr>
            {{--this is hidden column by default checked all--}}
            <td style="display: none;"><input checked type="checkbox" class="individual" name="faculty_info_hide[]" value="{{ $info->id }}"></td>

            <td><input type="checkbox" class="single" name="faculty_info[]" value="{{ $info->id }}"></td>
            <td class="university">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    <?php $university = DB::table('kos_universities')->where('id',$info->university)->get()?>
                    @foreach($university as $uni)
                        {{ $uni->university }}
                    @endforeach
                </a>
            </td>

            <td class="faculty">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    <?php $faculty = \App\KosFacultyInfo::where('faculty',$info->faculty)->first()?>
                    <?php $fcl = \App\KosFaculty::find($faculty->faculty)?>
                    {{ $fcl->faculty }}
                </a>
            </td>

            <td class="faculty_date_of_approval">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    {{ $info->faculty_date_of_approval }}
                </a>
            </td>

            <td class="department">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    <?php $department = DB::table('kos_departments')->where('id',$info->department)->get()?>
                    @foreach($department as $department)
                        {{ $department->department }}
                    @endforeach
                </a>
            </td>

            <td class="department_date_of_approval">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    {{ $info->department_date_of_approval }}
                </a>
            </td>

            <td class="programs">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    <?php $program = DB::table('kos_programs')->where('id',$info->programs)->get()?>
                    @foreach($program as $program)
                        {{ $program->program }}
                    @endforeach
                </a>
            </td>

            <td class="program_date_of_approval">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    {{ $info->	program_date_of_approval }}
                </a>
            </td>

            <td class="session">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    <?php $session = DB::table('kos_sessions')->where('id',$info->session)->get()?>
                    @foreach($session as $session)
                        {{ $session->session }}
                    @endforeach
                </a>
            </td>

            <td class="no_of_seat">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    {{ $info->no_of_seat }}
                </a>
            </td>

            <td class="enrolled_students">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    {{ $info->enrolled_students }}
                </a>
            </td>

            <td class="male_students">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    {{ $info->male_students }}
                </a>
            </td>
            <td class="female_students">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    {{ $info->female_students }}
                </a>
            </td>
            <td class="current_students">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    {{ $info->current_students }}
                </a>
            </td>
            <td class="year">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    {{ $info->year }}
                </a>
            </td>
            <td class="remarks">
                <a href="{{ url('/database/faculty-info/edit/'.$info->id) }}">
                    {{ $info->remarks }}
                </a>
            </td>

        </tr>
    @endforeach


