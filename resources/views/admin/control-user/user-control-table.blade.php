@extends('admin.master')

@section('title')
    <title>User Control</title>
    @endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>User Controller</span>
                    </h4>

                    <input placeholder="Search " type="text" id="search-user">



                 </div>


             </div>
            <!-- Heading -->
            <a href="{{ url('/database/control/user-control-form') }}"><button type="button" class="mb-lg-3 btn btn-primary btn-md show-field-btn" data-toggle="modal" data-target="#exampleModalCenter">
                Add New User
            </button></a>
            <div class="clr"></div>
            @if(Session::has('message'))
                <h4 class="text text-center text-success">{{ Session::get('message') }}</h4>
            @endif
            @if(Session::has('alert'))
                <h4 class="text text-center text-danger">{{ Session::get('alert') }}</h4>
        @endif
            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Form with panel -->
                    <div class="card card-cascade narrower">

                        <div class="px-4">

                            <div class="table-wrapper">

                                <form>
                                    <table id="datatable" class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th scope="col">SL</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Designation</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $users = DB::table('dashboard_logins')->orderBy('id','desc')->where('role','user')->paginate(100)?>
                                        <?php $i=1?>
                                        @foreach($users as $user )
                                            @if($user->role != 'super-admin')
                                        <tr>
                                            <th scope="row">{{ $i }}</th>
                                            <th scope="row">{{ $user->name }}</th>
                                            <th scope="row">{{ $user->email }}</th>
                                            <th scope="row">{{ $user->designation }}</th>
                                            <td><a href="{{ url('/database/control/edit/'.$user->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                                {{--<a href="{{ url('/database/control/delete/'.$user->id) }}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure to Delete it !!!')">Delete</a>--}}
                                                <a href="{{ url('/database/control/view/'.$user->id) }}" class="btn btn-sm btn-success">View</a>
                                            </td>
                                        </tr>
                                        <?php $i++?>
                                        @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{ $users->links() }}
                                </form>


                            </div>

                        </div>

                    </div>
                    <!-- Table with panel -->
                </div>

            </div>
            <!--Grid row-->

        </div>
    </main>
    @endsection