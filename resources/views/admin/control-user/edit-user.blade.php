@extends('admin.master')

@section('title')
    <title>User Control</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href={{ url('/database/dashboard') }}>Home Page</a>
                        <span>/</span>
                        <span>User Control</span>
                    </h4>

                    <a class="btn btn-outline-info btn-rounded btn-secondary btn-lg white-text" href="{{ url('/database/control/user-control-table') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>

                </div>

            </div>
            <!-- Heading -->
            @if(Session::has('message'))
                <h5 class="text text-center text-success">{{ Session::get('message') }}</h5>
            @endif
            @if(Session::has('alert'))
                <h5 class="text text-center text-danger">{{ Session::get('alert') }}</h5>
        @endif
        <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Form with panel -->
                    <div class="card card-cascade narrower">
                        <form action="{{ url('/database/control/update') }}" method="post">
                        @csrf



                            <div class="px-4">

                                <div class="table-wrapper">

                                    <input type="text" name="id" class="form-control custom-input" id="" value="{{ $user->id }}">
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="name" class="form-control custom-input" id="" value="{{ $user->name }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">Designation</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="designation" class="form-control custom-input" id="" value="{{ $user->designation }}" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">User Email <span style="color: red"> *</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" required name="email" class="form-control custom-input" id="" value="{{ $user->email }}">
                                        </div>
                                    </div>

                                    <div class="clr"></div>
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-sm-2 col-form-label">Change Password</label>
                                        <div class="col-sm-10">
                                            <input type="password"  name="new_password" class="form-control custom-input" id="inputPassword" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-sm-2 col-form-label">Confirm Password</label>
                                        <div class="col-sm-10">
                                            <input type="password"  name="confirm_password" class="form-control custom-input" id="inputPassword" placeholder="Re-type Password">
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label for="" class="col-sm-12 col-form-label">University Permission</label>
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Select University
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <?php $universities =  DB::table('kos_universities')->orderBy('id','desc')->get()?>
                                                <?php $universityPermissions =  DB::table('kos_university_permissions')->where('role_id',$user->id)->get()?>
                                                <a class="dropdown-item" href="#"><input type="checkbox">Select All</a>
                                                @foreach($universities as $university)
                                                        @foreach($universityPermissions as $permission)
                                                            @if($university->id == $permission->university_id )
                                                            <a class="dropdown-item" href="#"><input type="checkbox" checked value="{{ $university->id }}">{{ $university->university }}</a>

                                                                @break
                                                            @else
                                                                <a class="dropdown-item" href="#"><input type="checkbox" value="{{ $university->id }}">{{ $university->university }}</a>
                                                                @break
                                                            @endif
                                                                @endforeach
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label for="" class="col-sm-12 col-form-label">Permission Module *</label>
                                        <select id="module" name="permission_module[]" required  multiple="multiple" class="custom-select col-sm-12">
                                            <?php ?>
                                                <option value="kos_centres">Centre</option>
                                                <option value="kos_centre_infos">Centre Info</option>
                                                <option value="kos_departments">Departments</option>
                                                <option value="kos_faculties">Faculty</option>
                                                <option value="kos_faculty_infos">Faculty Info</option>
                                                <option value="kos_financial_years">Financial Year</option>
                                                <option value="kos_institutes">Institutes</option>
                                                <option value="kos_institute_infos">Institute Info</option>
                                                <option value="kos_staff_infos">Manpower Info</option>
                                                <option value="kos_offices">Offices</option>
                                                <option value="kos_original_posts">Original Post</option>
                                                <option value="kos_programs">Programs</option>
                                                <option value="kos_sessions">Session</option>
                                                <option value="kos_vehicles">Vehicles</option>
                                                <option value="kos_vehicle_infos">Vehicle Info</option>
                                        </select>
                                    </div>


                                    <div class="form-group ">
                                        <label for="" class="col-sm-12 col-form-label">Permission<span style="color: red"> *</span></label>
                                        <select id="role" name="permission[]" required multiple="multiple" class="custom-select col-sm-12">
                                            <option value="add">Add</option>
                                            <option value="edit">Edit</option>
                                            <option value="delete">Delete</option>
                                            <option value="copy">Copy</option>
                                            <option value="report">PDF</option>
                                            <option value="view">View</option>
                                        </select>
                                    </div>

                                    <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg waves-effect z-depth-0" type="submit">Update</button>


                                </div>

                            </div>
                        </form>

                    </div>
                    <!-- Table with panel -->
                </div>

            </div>
            <!--Grid row-->

        </div>
    </main>
@endsection