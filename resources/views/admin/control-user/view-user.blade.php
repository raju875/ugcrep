@extends('admin.master')

@section('title')
    <title>User Control</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="containe  -fluid mt-5">

            <!-- Heading -->
            <div class="ca  d mb-4 wow fadeIn">

                <!--Ca  d content-->
                <div class="ca  d-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>User   Control  </span>
                    </h4>
                    <a class="btn btn-outline-info btn-rounded btn-secondary btn-lg white-text"
                       href="{{ url('/database/control/user-control-table') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>


                </div>

            </div>
            <!-- Heading -->

            <!--G  id   ow-->
            <div class="  ow wow fadeIn">

                <!--G  id column-->
                <div class="col-md-12 mb-4">

                    <!-- Fo  m with panel -->
                    <div class="ca  d ca  d-cascade na    owe  ">
                        <div class="px-4">

                            <div class="table-w  appe  ">

                                <form>

                                    <div class="fo  m-g  oup   ow">
                                        <label fo="name" class="col-sm-2 col-fo  m-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" readonly class="fo  m-cont  ol-plaintext" id="  "
                                                   value="{{ $user->name }}">
                                        </div>
                                    </div>

                                    <div class="fo  m-g  oup   ow">
                                        <label fo="name" class="col-sm-2 col-fo  m-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="text" readonly class="fo  m-cont  ol-plaintext" id="  "
                                                   value="{{$user->email }}">
                                        </div>
                                    </div>

                                    <div class="fo  m-g  oup   ow">
                                        <label fo="name" class="col-sm-2 col-fo  m-label">Designation</label>
                                        <div class="col-sm-10">
                                            @if($user->designation == 'null')
                                                <input type="text" readonly class="fo  m-cont  ol-plaintext" id="  "
                                                       value="{{ 'N/A' }}">
                                            @else
                                                <input type="text" readonly class="fo  m-cont  ol-plaintext" id="  "
                                                       value="{{$user->designation }}">
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group-row">
                                        <label for="varsity" class="col-sm-2 col-fo  m-label">University</label>
                                        <div class="col-sm-10">
                                            <ol>
                                                <?php $universities = \App\KosUniversityPermission::where('role_id', $user->id)->get()?>
                                                @foreach($universities as $university )
                                                    <?php $uni = \App\KosUniversity::find($university->university_id)?>
                                                    <li>
                                                        {{ $uni->university }}
                                                    </li>
                                                @endforeach
                                            </ol>
                                        </div>
                                    </div>
                                    <div class="fo  m-g  oup   ow">
                                        <label fo="va  sity" class="col-sm-2 col-form-label  m-label">Permission
                                            Module</label>
                                        <div class="col-sm-10">
                                            <ol>
                                                <?php $permissionModules = \App\KosPermissionModule::where('role_id', $user->id)->get()?>
                                                @foreach($permissionModules as $module )
                                                    @if($module->table_name == 'kos_centres')
                                                        <li>
                                                            {{ 'Centre' }}
                                                        </li>
                                                    @elseif($module->table_name == 'kos_centre_infos' )
                                                        <li>
                                                            {{ 'Centre Info' }}
                                                        </li>

                                                    @elseif($module->table_name == 'kos_departments' )
                                                        <li>
                                                            {{ 'Department' }}
                                                        </li>

                                                    @elseif($module->table_name == 'kos_faculties' )
                                                        <li>
                                                            {{ 'Faculty' }}
                                                        </li>

                                                    @elseif($module->table_name == 'kos_faculty_infos' )
                                                        <li>
                                                            {{ 'Faculty Info' }}
                                                        </li>

                                                    @elseif($module->table_name == 'kos_financial_years' )
                                                        <li>
                                                            {{ 'Financial Year' }}
                                                        </li>

                                                    @elseif($module->table_name == 'kos_institutes' )
                                                        <li>
                                                            {{ 'Institutes' }}
                                                        </li>

                                                    @elseif($module->table_name == 'kos_institute_infos' )
                                                        <li>
                                                            {{ 'Institute Info' }}
                                                        </li>

                                                    @elseif($module->table_name == 'kos_staff_infos' )
                                                        <li>
                                                            {{ 'Manpower Info' }}
                                                        </li>

                                                    @elseif($module->table_name == 'kos_offices' )
                                                        <li>
                                                            {{ 'Offices' }}
                                                        </li>

                                                    @elseif($module->table_name == 'kos_original_posts' )
                                                        <li>
                                                            {{ 'Original Post' }}
                                                        </li>

                                                    @elseif($module->table_name == 'kos_programs' )
                                                        <li>
                                                            {{ 'Program' }}
                                                        </li

                                                    @elseif($module->table_name == 'kos_sessions' )
                                                        <li>
                                                            {{ 'Session' }}
                                                        </li>

                                                    @elseif($module->table_name == 'kos_vehicles' )
                                                        <li>
                                                            {{ 'Vehicles' }}
                                                        </li>

                                                    @elseif($module->table_name == 'kos_vehicle_infos' )
                                                        <li>
                                                            {{ 'Vehicle Info' }}
                                                        </li>


                                                    @else
                                                        <li>

                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ol>
                                        </div>
                                    </div>
                                    <div class="fo  m-g  oup   ow">
                                        <label for="versity" class="col-sm-2 col-fo  m-label">Permission</label>
                                        <div class="col-sm-10">
                                            <ol>
                                                <?php $permissions = \App\KosPermission::where('role_id', $user->id)->get()?>
                                                @foreach($permissions as $permission )
                                                    @if($permission->permission == 'add')
                                                        <li>
                                                            Add
                                                        </li>
                                                    @elseif($permission->permission == 'copy')
                                                        <li>
                                                            Copy
                                                        </li>
                                                    @elseif($permission->permission == 'edit')
                                                        <li>
                                                            Edit
                                                        </li>
                                                    @elseif($permission->permission == 'delete')
                                                        <li>
                                                            Delete
                                                        </li>
                                                    @elseif($permission->permission == 'report')
                                                        <li>
                                                            PDF
                                                        </li>
                                                    @elseif($permission->permission == 'view')
                                                        <li>
                                                            View
                                                        </li>
                                                    @else
                                                    @endif
                                                @endforeach
                                            </ol>
                                        </div>
                                    </div>
                                </form>


                            </div>

                        </div>

                    </div>
                    <!-- Table with panel -->
                </div>

            </div>
            <!--G  id   ow-->

        </div>
    </main>
@endsection