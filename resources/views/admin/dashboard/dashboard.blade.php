@extends('admin.master')

@section('title')
<title>Dashboard</title>
    @endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Dashboard</span>
                    </h4>

                </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Table with panel -->
                    <div class="card card-cascade narrower">

                        <!--Card image-->
                        <div class=" text-center view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">



                            <p  class="p-5 white-text mx-3 " style="display: inline; font-size: 24px;color: #ffffff;">Public University Management Information System (PUMIS)
                                </p>



                        </div>
                        <!--/Card image-->

                        <div class="px-4">

                            <div class="table-wrapper">
                                <!--row-->
                                <div class="row">
                                        <div class="col p-5">
                                            <a href="{{ url('/database/dashboard') }}">
                                            <i class="d-100 fas fa-tachometer-alt"></i>
                                        <h3 class="d-100">Dashboard</h3>
                                            </a>
                                        </div>

                                        <div class="col p-5">
                                            <a href="{{ url('/database/activity-log/listing') }}">
                                        <i class="d-100 fas fa-user-clock"></i>
                                        <h3 class="d-100">Activity Log</h3>
                                            </a>
                                        </div>


                                    <div class="col p-5">
                                        <a href="{{ url('/database/centre/listing') }}">
                                        <i class="d-100  fab fa-centercode"></i>
                                        <h3 class="d-100">Center</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">
                                        <a href="{{ url('/database/centre-info/listing') }}">
                                        <i class="d-100 fas fa-info-circle"></i>
                                        <h3 class="d-100">Center Info</h3>
                                        </a>
                                    </div>
                                </div>
                                <!--row-->
                                <!--row-->
                                <div class="row">
                                    <div class="col p-5">
                                        <a href="{{ url('/database/university/listing') }}">
                                            <i class="d-100 fas fa-university"></i>
                                            <h3 class="d-100">University</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">
                                        <a href="{{ url('/database/district/listing') }}">
                                            <i class="d-100 fas fa-drum-steelpan"></i>
                                            <h3 class="d-100">District</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">
                                        <a href="{{ url('/database/department/listing') }}">
                                            <i class="d-100 fas fa-building"></i>
                                            <h3 class="d-100">Department</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">
                                        <a href="{{ url('/database/faculty/listing') }}">
                                            <i class="d-100 fas fa-building"></i>
                                            <h3 class="d-100">Faculty</h3>
                                        </a>
                                    </div>
                                  </div>
                                <!--row-->
                                <!--row-->
                                <div class="row">
                                    <div class="col p-5">
                                        <a href="{{ url('/database/faculty-info/listing') }}">
                                            <i class="d-100 fas fa-info-circle"></i>
                                            <h3 class="d-100">Faculty Info</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">
                                        <a href="{{ url('/database/financial-year/listing') }}">
                                            <i class="d-100 fas fa-calendar-alt"></i>
                                            <h3 class="d-100">Financial Year</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">
                                        <a href="{{ url('/database/institute/listing') }}">
                                            <i class="d-100 fas fa-building"></i>
                                            <h3 class="d-100">Institutes</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">
                                        <a href="{{ url('/database/institute-info/listing') }}">
                                            <i class="d-100 fas fa-info-circle"></i>
                                            <h3 class="d-100">Institute Info</h3>
                                        </a>
                                    </div>
                                </div>
                                <!--row-->
                                <div class="row">
                                    <div class="col p-5">
                                        <a href="{{ url('/database/manpower-info/listing') }}">
                                            <i class="d-100 fas fa-info-circle"></i>
                                            <h3 class="d-100">Manpower Info</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">
                                        <a href="{{ url('/database/office/listing') }}">
                                            <i class="d-100 fas fa-city"></i>
                                            <h3 class="d-100">Offices</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">
                                        <a href="{{ url('/database/original-post/listing') }}">
                                            <i class="d-100 fas fa-crosshairs"></i>
                                            <h3 class="d-100">Original Post</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">
                                        <a href="{{ url('/database/program/listing') }}">
                                            <i class="d-100 fas fa-hotel"></i>
                                            <h3 class="d-100">Programs</h3>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col p-5">
                                        <a href="{{ url('/database/session/listing') }}">
                                            <i class="d-100 fas fa-calendar-alt"></i>
                                            <h3 class="d-100">Session</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">
                                        <a href="{{ url('/database/vehicle/listing') }}">
                                            <i class="d-100 fas fa-subway"></i>
                                            <h3 class="d-100">Vehicles</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">
                                        <a href="{{ url('/database/vehicle-info/listing') }}">
                                            <i class="d-100 fas fa-info-circle"></i>
                                            <h3 class="d-100">Vehicle Info</h3>
                                        </a>
                                    </div>
                                    <div class="col p-5">

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <!-- Table with panel -->
                </div>

            </div>
            <!--Grid row-->

        </div>
    </main>

@endsection
