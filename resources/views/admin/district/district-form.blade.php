@extends('admin.master')

@section('title')
    <title>District</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}">Home Page</a>
                        <span>/</span>
                        <span>Add New Centre</span>
                    </h4>

                    <a href="{{ url('/database/district/listing') }}">
                        <i class="mr-1 fas fa-caret-left"></i>
                        Bact to list</a>

                </div>

            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Form with panel -->
                    <form action="{{ url('/database/district/add') }}" method="POST">
                        @csrf
                        <div class="card card-cascade narrower">
                            <div class="px-4">

                                <div class="table-wrapper">

                                    <label class="form-label mt-lg-5">District</label>
                                    <!-- Material input -->
                                    <div class="md-form">
                                        <input  name="name" required type="text" id="form1" class="form-control">
                                    </div>
                                    <button name="btn" class="mt-lg-5 mb-lg-5 btn-secondary btn btn-rounded btn-lg  waves-effect z-depth-0" type="submit">Save</button>

                                </div>

                            </div>

                        </div>
                    </form>
                    <!-- Table with panel -->
                </div>

            </div>
            <!--Grid row-->

        </div>
    </main>
@endsection
