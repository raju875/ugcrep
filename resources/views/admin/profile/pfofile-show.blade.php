@extends('admin.master')

@section('title')
    <title>Profile</title>
@endsection

@section('body')
    <main class="pt-5 mx-lg-5">
        <div class="container-fluid mt-5">

            <!-- Heading -->
            <div class="card mb-4 wow fadeIn">

                <!--Card content-->
                <div class="card-body d-sm-flex justify-content-between">

                    <h4 class="mb-2 mb-sm-0 pt-1">
                        <a href="{{ url('/database/dashboard') }}" >Home Page</a>
                        <span>/</span>
                        <span>Profile</span>
                    </h4>
            </div>
            <!-- Heading -->

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!-- Table with panel -->
                    <div class="card card-cascade narrower">
                        <!--Form Starts-->
                            <!--Card image-->
                            <div class="view custom-table view-cascade gradient-card-header blue-gradient narrower p-2 mb-3 d-flex justify-content-between align-items-center">

                                <a href="" class="white-text mx-3">Change Profile</a>

                            </div>
                            <!--/Card image-->


                            <div class="px-4">

                                <div class="table-wrapper table-responsive">
                                    @if(Session::has('message'))
                                        <h4 class="text text-center text-success">{{ Session::get('message') }}</h4>
                                    @endif
                                    @if(Session::has('alert'))
                                        <h4 class="text text-center text-danger">{{ Session::get('alert') }}</h4>
                                    @endif

                                    {{--Reset Password Starts--}}
                                    <h3 style="display: inline">Name : </h3><h4 style="display: inline">{{Session::get('adminRole') }}</h4><br>
                                    <h4 style="display: inline">Designation : </h4><h5 style="display: inline">{{ $admin -> designation }}</h5><br>
                                    <h4 style="display: inline">Department : </h4><h5 style="display: inline">{{ $admin -> department }}</h5><br>
                                    <h4 style="display: inline">Email : </h4><h5 style="display: inline">{{ $admin -> email }}</h5><br>
                                    <form action="{{ url('/database/profile/update-profile') }}" method="POST">
                                        <br>
                                        @csrf
                                        <input type="text" name="id" style="display: none" value="{{ $admin->id }}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Old Password :</label>
                                            <input required name="old_pass" type="password" class="custom-input form-control" id="" placeholder="Old Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">New Password :</label>
                                            <input required name="new_pass" type="password" class="custom-input form-control" id=""  placeholder="New Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Retype Password</label>
                                            <input required name="retype_pass" type="password" class="custom-input form-control" id="" placeholder="Confirm Password">
                                        </div>
                                        <button type="submit" name="btn" class="mb-lg-5 custom-button btn btn-primary">Save</button>
                                    </form>
                                    {{--Reset Password Ends--}}
                                    <!--Pagination -->
                                </div>

                            </div>
                        <!--Form Ends-->

                    </div>
                    <!-- Table with panel -->
                </div>

            </div>
            <!--Grid row-->

        </div>
    </main>
@endsection


