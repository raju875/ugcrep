<div style="text-align: center; padding-top: 50px;font-family: arial, sans-serif;border-collapse: collapse;">
    <h2>Public University Management Information System (PUMIS)</h2>
    {{--if selected university names are same start--}}
    @if($uni==1 )
        @foreach ($instituteInfos as $instituteInfo )
            <?php $instituteInfoById = \App\KosInstituteInfo::find($instituteInfo); ?>
            <?php $university = \App\KosUniversity::find($instituteInfoById->university); ?>
            <h3>{{ $university->university }}</h3>
            @break
        @endforeach
    @endif
    {{--if selected university names are same start--}}
</div>
<style>
    table,th,td{
        border: 1px solid #ddd;
        border-collapse: collapse;
        font-size: 13px;

    }
    th{
        padding: 12px 0;
        text-align: center;
    }
</style>



<table width="100%">
    <thead>
    <tr>
        @if(Session::get('instituteInfoUniversity'))
                @if($uni==0)
                    <th    >{{ 'University' }}</th>
                @endif
            @endif

            @if(Session::get('instituteInfoInstitute'))
                <th    >{{ 'Institute' }}</th>
            @endif

            @if(Session::get('instituteInfoInstituteDateOfApproval'))
                <th    >{{ 'Institute date of approval' }}</th>
            @endif

            @if(Session::get('instituteInfoDepartment'))
                <th    >{{ 'Department' }}</th>
            @endif

            @if(Session::get('instituteInfoDepartmentDateOfApproval'))
                <th    >{{ 'Department date of approval' }}</th>
            @endif

            @if(Session::get('instituteInfoPrograms'))
                <th    >{{ 'Programs' }}</th>
            @endif

            @if(Session::get('instituteInfoProgramDateOfApproval'))
                <th    >{{ 'Program date of approval' }}</th>
            @endif

            @if(Session::get('instituteInfoSession'))
                <th    >{{ 'Session' }}</th>
            @endif

            @if(Session::get('instituteInfoNoOfSeat'))
                <th    >{{ 'No of seat' }}</th>
            @endif

            @if(Session::get('instituteInfoEnrolledStudents'))
                <th    >{{ 'Enrolled students' }}</th>
            @endif

            @if(Session::get('instituteInfoMaleStudents'))
                <th    >{{ 'Male student' }}</th>
            @endif

            @if(Session::get('instituteInfoFemaleStudents'))
                <th    >{{ 'Female student' }}</th>
            @endif

            @if(Session::get('instituteInfoCurrentStudents'))
                <th    >{{ 'Current student' }}</th>
            @endif

            @if(Session::get('instituteInfoYear'))
                <th    >{{ 'Year' }}</th>
            @endif

            @if(Session::get('instituteInfoDateOfApproval'))
                <th    >{{ 'Date of approval' }}</th>
            @endif

            @if(Session::get('instituteInfoRemarks'))
                <th    >{{ 'Remarks' }}</th>
            @endif
    </tr>
    </thead>
    <tbody>

    @foreach($instituteInfos as $instituteInfo )
        <tr    >

                <?php $instituteInfoById = \App\KosInstituteInfo::find($instituteInfo); ?>

                    @if(Session::get('instituteInfoUniversity'))
                    @if($uni==0 )
                        {{--//if selected university name is not same--}}
                        <?php $university = \App\KosUniversity::find($instituteInfoById->university); ?>
                        @if(!empty($university))
                        <td    >{{ $university->university }}</td>
                            @else
                                <td    ></td>
                            @endif
                    @endif
                @endif


                    @if(Session::get('instituteInfoInstitute'))
                    <?php $institute = \App\KosInstitute::find($instituteInfoById->institute); ?>
                    @if (empty($institute))
                        <td    ></td>
                    @else
                        <td    >{{ $institute->institute }}</td>
                    @endif
                @endif

                    @if(Session::get('instituteInfoInstituteDateOfApproval'))
                        @if($instituteInfoById->institute_date_of_approval != '')
                        <td    >{{ $instituteInfoById->institute_date_of_approval }}</td>
                    @else
                        <td    ></td>
                    @endif
                    @endif

                    @if(Session::get('instituteInfoDepartment'))
                    <?php $institute = \App\KosDepartment::find($instituteInfoById->department); ?>
                    @if (empty($institute))
                        <td    ></td>
                    @else
                        <td    >{{ $institute->department }}</td>
                    @endif
                @endif


                    @if(Session::get('instituteInfoDepartmentDateOfApproval'))
                        @if($instituteInfoById->department_date_of_approval != '')
                    <td    >{{ $instituteInfoById->department_date_of_approval }}</td>
                    @else
                        <td    ></td>
                    @endif
                @endif

                    @if(Session::get('instituteInfoPrograms'))
                    <?php $program = \App\KosProgram::find($instituteInfoById->programs); ?>
                    @if (empty($program))
                        <td    ></td>
                    @else
                        <td    >{{ $program->program }}</td>
                    @endif
                @endif

                    @if(Session::get('instituteInfoProgramDateOfApproval'))
                        @if($instituteInfoById->program_date_of_approval != '')
                    <td    >{{ $instituteInfoById->program_date_of_approval }}</td>
                        @else
                            <td    ></td>
                        @endif
                @endif

                    @if(Session::get('instituteInfoSession'))
                        <?php $year = \App\KosSession::find($instituteInfoById->session); ?>
                        @if (empty($year))
                            <td    ></td>
                        @else
                            <td    >{{ $year->session }}</td>
                        @endif
                    @endif

                    @if(Session::get('instituteInfoNoOfSeat'))
                        @if($instituteInfoById->no_of_seat != '')
                        <td    >{{ $instituteInfoById->no_of_seat }}</td>
                    @else
                        <td    ></td>
                    @endif
                @endif

                    @if(Session::get('instituteInfoEnrolledStudents'))
                        @if($instituteInfoById->enrolled_students != '')
                    <td    >{{ $instituteInfoById->enrolled_students }}</td>
                        @else
                            <td    ></td>
                        @endif
                @endif

                    @if(Session::get('instituteInfoMaleStudents'))
                        @if($instituteInfoById->male_students != '')
                    <td    >{{ $instituteInfoById->male_students }}</td>
                        @else
                            <td    ></td>
                        @endif
                    @endif

                    @if(Session::get('instituteInfoFemaleStudents'))
                        @if($instituteInfoById->female_students != '')
                    <td    >{{ $instituteInfoById->female_students }}</td>
                        @else
                            <td    ></td>
                        @endif
                @endif

                    @if(Session::get('instituteInfoCurrentStudents'))
                        @if($instituteInfoById->current_students != '')
                    <td    >{{ $instituteInfoById->current_students }}</td>
                        @else
                            <td    ></td>
                        @endif
                    @endif

                    @if(Session::get('instituteInfoYear'))
                        @if($instituteInfoById->year != '')
                    <td    >{{ $instituteInfoById->year }}</td>
                        @else
                            <td    ></td>
                        @endif
                @endif

                    @if(Session::get('instituteInfoDateOfApproval'))
                        @if($instituteInfoById->date_of_approval != '')
                    <td    >{{ $instituteInfoById->date_of_approval }}</td>
                        @else
                            <td    ></td>
                        @endif
                @endif

                    @if(Session::get('instituteInfoRemarks'))
                        @if($instituteInfoById->remarks != '')
                    <td    >{{ $instituteInfoById->remarks }}</td>
                        @else
                            <td    ></td>
                        @endif
                    @endif
        </tr>

    @endforeach
    </tbody>
</table>
