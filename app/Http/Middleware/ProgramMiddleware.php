<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use App\KosPermissionModule;

class ProgramMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::get('adminEmail')) {
            if ( Session::get('adminRole') == 'super-admin') {
                return $next($request);
            } else {
                $userId = Session::get('adminId');
                $permissionModules = KosPermissionModule::where('role_id',$userId)->get();
                foreach ($permissionModules as $module ) {
                    if ($module->table_name == 'kos_programs') {
                        return $next($request);
                    }
                }
                return redirect('/database/dashboard');
            }

        } else {
            return redirect('/login')->with('message','Login first');
        }
    }
}
