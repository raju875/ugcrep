<?php

namespace App\Http\Middleware;

use App\DashboardLogin;
use Closure;
use Illuminate\Support\Facades\Session;

class UserControlMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::get('adminEmail')) {
            if ( Session::get('adminRole') == 'super-admin') {
                return $next($request);
            } else {
                return redirect('/database/dashboard');
            }
        }else {
            return redirect('/login')->with('message','Login first');
        }

    }
}
