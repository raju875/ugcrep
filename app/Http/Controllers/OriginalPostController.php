<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\LoginController;
use App\KosActivityLog;
use App\KosOriginalPost;
use App\KosPermission;
use App\User;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Contracts\DataTable;
use Illuminate\Support\Facades\Schema;
use PDF;
use Datatables;

class OriginalPostController extends Controller
{
    public function wrongUrl() {
        return redirect('/database/original-post/listing')->with('alert','Your action url is wrong !!');
    }

    public function originalPostListing() {
        $originalPosts = DB::table('kos_original_posts')->orderBy('id','desc')->paginate(10);
        $names = Schema::getColumnListing('kos_centres');
        return view('admin.original-post.original-post-table',[
            'originalPosts' => $originalPosts
        ]);
    }

    public function originalPostForm () {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.original-post.original-post-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.original-post.original-post-form');
                }
            }
        }
        return redirect('/database/original-post/listing');
    }

    public function originalPostAdd (Request $request) {
        $post = new KosOriginalPost();
        $post->original_post = $request->original_post;
        $post->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Original Post';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/original-post/listing')->with('message','Original Post added successfully');
    }

    public function originalPostEdit ($id ) {
        //return $id;
        $postById = KosOriginalPost::find($id);
        return view('admin.original-post.original-post-edit',[
            'postById' => $postById
        ]);
    }

    public function originalPostUpdate(Request $request ) {
        $post =  KosOriginalPost::find($request->id);
        $post->original_post = $request->original_post;
        $post->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Original Post';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/original-post/listing')->with('message','Original Post update successfully');
    }

    public function originalPostCopy ($id) {
        //return $id;
        if (Session::get('adminRole') == 'super-admin' ) {
            $post    = KosOriginalPost::find($id);
            $newPost = new KosOriginalPost();
            $newPost->original_post = $post->original_post;
            $newPost->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Original Post';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/original-post/listing')->with('message','Copy Original Post successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'copy' ) {
                    $post    = KosOriginalPost::find($id);
                    $newPost = new KosOriginalPost();
                    $newPost->original_post = $post->original_post;
                    $newPost->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Original Post';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/original-post/listing')->with('message','Copy Original Post successfully');
                }
            }
            return redirect('/database/original-post/listing')->with('alert','You do not permit to access it !!!');
        }
    }

    public function originalPostDelete($id ) {
        if (Session::get('adminRole') == 'super-admin' ) {
            KosOriginalPost::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Original Post';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/original-post/listing')->with('message','Delete Original Post successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete' ) {
                    KosOriginalPost::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Original Post';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/original-post/listing')->with('message','Delete Original Post successfully');
                }
            }
            return redirect('/database/original-post/listing')->with('alert','You do not permit to access it !!!');
        }
    }

    public function originalPost(Request $request) {
//    return $request->all();
        if ($request->has('original_post')) {
            switch ($request->input('submit')) {
                case 'copy':
                    $postId  = $request->input('original_post');
                    //$centres = $request->all();
                    $count = 0;
                    foreach ($postId as $id ) {
                        $originalPost    = KosOriginalPost::find($id);
                        $newOriginalPost = new KosOriginalPost();
                        $newOriginalPost->original_post = $originalPost->original_post;
                        $newOriginalPost->save();
                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy '.$count.' Original Post';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/original-post/listing')->with('message','Copy '.$count.' Original Post successfully');
                    break;

                case 'edit':
                    return 'edit';
                    break;

                case 'delete':
                    $deletes = $request->input('original_post');
                    $count = 0;
                    foreach ($deletes as $delete ) {
                        KosOriginalPost::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete '.$count.' Original Post';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/original-post/listing')->with('message','Delete '.$count.' Original Post successfully');
                    break;

                case 'report':
                    return 'Original Post';
                    $centreById  = $request->input('original_post');
                    return view('admin.original-post.original-post-report',[
                        'centreById' => $centreById
                    ]);
                    break;
            }
        } else {
            return redirect('/database/original-post/listing')->with('alert','Please select item(s) check box from list');
        }

    }

    public function centreReport() {
        return view('admin.centre.centre_report');
    }
}
