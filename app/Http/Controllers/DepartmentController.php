<?php

namespace App\Http\Controllers;

use App\KosDepartment;
use App\KosPermission;
use App\KosPermissionModule;
use Illuminate\Http\Request;
use App\KosActivityLog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class DepartmentController extends Controller
{
    public function wrongUrl() {
        return redirect('/database/department/listing')->with('alert','Your action url is wrong !!!');
    }

    public function departmentListing() {
        $departments = DB::table('kos_departments')->orderBy('id','decd')->get();
        return view('admin.department.department-table',[
            'departments' => $departments
        ]);
    }

    public function departmentForm() {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.department.department-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.department.department-form');
                }
            }
        }
        return redirect('/database/department/listing');
    }

    public function departmentAdd(Request $request) {
        $department = new KosDepartment();
        $department->department = $request->department;
        $department->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Department';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/department/listing')->with('message','department added successfully');
    }

    public function departmentEdit($id ) {
        $departmentById = KosDepartment::find($id);
        return view('admin.department.department-edit',[
            'departmentById' => $departmentById
        ]);
    }

    public function departmentUpdate(Request $request ) {
        $departmentById =  KosDepartment::find($request->id);
        $departmentById->department = $request->department;
        $departmentById->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Department';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();
        return redirect('/database/department/listing')->with('message','Department update successfully');
    }

    public function departmentCopy ($id) {
        //return $id;
        if (Session::get('adminRole')=='super-admin') {
            $department    = KosDepartment::find($id);
            $newDepartment = new KosDepartment();
            $newDepartment->department = $department->department;
            $newDepartment->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Department';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/department/listing')->with('message','Copy Department successfully');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'copy') {
                    $department    = KosDepartment::find($id);
                    $newDepartment = new KosDepartment();
                    $newDepartment->department = $department->department;
                    $newDepartment->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Department';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/department/listing')->with('message','Copy Department successfully');

                }
            }
        }
        return redirect('/database/department/listing');

    }

    public function departmentDelete($id ) {
        if (Session::get('adminRole')=='super-admin')  {
            KosDepartment::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Department';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/department/listing')->with('message','Delete Department successfully');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete') {
                    KosDepartment::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Department';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/department/listing')->with('message','Delete Department successfully');

                }
            }
        }
        return redirect('/database/department/listing');
    }

    public function department(Request $request) {
       //return $request->all();
        if ($request->has('department')) {
            switch ($request->input('submit')) {
                case 'copy':
                    $departmentId = $request->input('department');
                    //$centres = $request->all();
                    $count = 0;
                    foreach ($departmentId as $id ) {
                        $department  = KosDepartment::find($id);
                        $newDepartment = new KosDepartment();
                        $newDepartment->department = $department->department;
                        $newDepartment->save();
                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy '.$count.' Department';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/department/listing')->with('message','Copy '.$count.' Department successfully');
                    break;

                case 'edit':
                    return 'edit';
                    break;

                case 'delete':
                    $deletes = $request->input('department');
                    $count = 0;
                    foreach ($deletes as $delete ) {
                        KosDepartment::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete '.$count.' Department';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/department/listing')->with('message','Delete '.$count.' Department successfully');
                    break;
            }
        } else {
            return redirect('/database/department/listing')->with('alert','Please select item(s) check box from list');
        }

    }
}
