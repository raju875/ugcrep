<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KosActivityLog;
use App\KosCentre;
use App\User;
use Dompdf\Dompdf;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Contracts\DataTable;
use Illuminate\Support\Facades\Schema;
use PDF;
use Datatables;
use App\KosOffice;
use App\KosPermission;
use App\KosPermissionModule;

class OfficeController extends Controller
{

    public function wrongUrl() {
        return redirect('/database/office/listing')->with('alert','Your action url is wrong !!!');
    }

    public function officeListing() {
        $offices = DB::table('kos_offices')->orderBy('id','desc')->get();
        return view('admin.office.office-table',[
            'offices' => $offices
        ]);
    }

    public function officeForm () {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.office.office-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.office.office-form');
                }
            }
        }
        return redirect('/database/office/listing');
    }

    public function officeAdd (Request $request) {
       // return $request->all();
        $office = new KosOffice();
        $office->office = $request->office;
        $office->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Office';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/office/listing')->with('message','Office added successfully');
    }

    public function officeEdit ($id ) {
        //return $id;
        $officeById = KosOffice::find($id);
        return view('admin.office.office-edit',[
            'officeById' => $officeById
        ]);
    }

    public function officeUpdate(Request $request ) {
        $office =  KosOffice::find($request->id);
        $office->office = $request->office;
        $office->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Office';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/office/listing')->with('message','Office update successfully');
    }

    public function officeCopy ($id) {
        //return $id;
        if (Session::get('adminRole')=='super-admin') {
            $office    = KosOffice::find($id);
            $newOffice = new KosOffice();
            $newOffice->office = $office->office;
            $newOffice->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Office';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/office/listing')->with('message','Copy Office successfully');
        }else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'copy') {
                    $office    = KosOffice::find($id);
                    $newOffice = new KosOffice();
                    $newOffice->office = $office->office;
                    $newOffice->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Office';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/office/listing')->with('message','Copy Office successfully');
                }
            }
        }
        return redirect('/database/session/listing');
    }

    public function officeDelete($id ) {
        if (Session::get('adminRole')=='super-admin')  {
            KosOffice::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Office';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/office/listing')->with('message','Delete Offece successfully');
        }else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete') {
                    KosOffice::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Office';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/office/listing')->with('message','Delete Offece successfully');
                }
            }
        }
        return redirect('/database/office/listing');
    }

    public function office(Request $request)
    {
//    return $request->all();
        if ($request->has('office')) {
            switch ($request->input('submit')) {
                case 'copy':
                    $officeId = $request->input('office');
                    //$centres = $request->all();
                    $count = 0;
                    foreach ($officeId as $id) {
                        $office = KosOffice::find($id);
                        $newOffice = new KosOffice();
                        $newOffice->office = $office->office;
                        $newOffice->save();
                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy ' . $count . ' Office';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/office/listing')->with('message', 'Copy ' . $count . ' Office successfully');
                    break;

                case 'edit':
                    return 'edit';
                    break;

                case 'delete':
                    $deletes = $request->input('office');
                    $count = 0;
                    foreach ($deletes as $delete) {
                        KosOffice::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete ' . $count . ' Office';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/office/listing')->with('message', 'Delete ' . $count . ' Office successfully');
                    break;

                case 'report':
                    $centreById = $request->input('centre');
                    return view('admin.centre.centre_report', [
                        'centreById' => $centreById
                    ]);
                    break;
            }
        } else {
            return redirect('/database/office/listing')->with('alert', 'Please select item(s) check box from list');
        }

        }
}
