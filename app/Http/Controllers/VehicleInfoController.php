<?php

namespace App\Http\Controllers;

use App\KosActivityLog;
use App\KosOffice;
use App\KosVehicleInfo;
use App\KosCentreInfo;
use App\KosDepartment;
use App\KosUniversity;
use App\KosUniversityPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use PDF;
use App\KosPermission;
use App\KosPermissionModule;

class VehicleInfoController extends Controller
{
    public function wrongUrl() {
        return redirect('/database/vehicle-info/listing')->with('alert','Your action url is wrong !!!');
    }

    public function vehicleInfoListing() {
        //first forgot all vehicle info session data

        Session::forget('vehicleInfoUniversity');
        Session::forget('vehicleInfoOffice');
        Session::forget('vehicleInfoVehicle');
        Session::forget('vehicleInfoNoOfVehiclesInOrganogram');
        Session::forget('vehicleInfoYearOfOrganogramApproval');
        Session::forget('vehicleInfoFinancialYear');
        Session::forget('vehicleInfoApprovedNumberOfVehiclesByUgc');
        Session::forget('vehicleInfoNoOfVehiclesPurchasedByUniversity');
        Session::forget('vehicleInfoYearOfPurchaseVehiclesByUniversity');
        Session::forget('vehicleInfoProposedBudget');
        Session::forget('vehicleInfoApprovedBudget');
        Session::forget('vehicleInfoRemarks');

        //by default vehicle info session data show the table
        Session::put('vehicleInfoUniversity',"university");
        Session::put('vehicleInfoOffice','office');
        Session::put('vehicleInfoVehicle','vehicle');
        Session::put('vehicleInfoFinancialYear','financial_year');
        Session::put('vehicleInfoNoOfVehiclesPurchasedByUniversity','no_of_vehicles_purchased_by_university');

        return view('admin.vehicle-info.vehicle-info-table');
    }

    public function vehicleInfoForm () {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.vehicle-info.vehicle-info-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.vehicle-info.vehicle-info-form');
                }
            }
        }
        return redirect('/database/vehicle-info/listing');
    }

    public function vehicleInfoAdd (Request $request) {
        //return $request->all();

        $vehicleInfo = new KosVehicleInfo();
        $vehicleInfo->university                              = $request->university;
        $vehicleInfo->office                                  = $request->office;
        $vehicleInfo->vehicle                                 = $request->vehicle;
        $vehicleInfo->no_of_vehicles_in_organogram            = $request->no_of_vehicles_in_organogram;
        $vehicleInfo->year_of_organogram_approval             = $request->year_of_organogram_approval;
        $vehicleInfo->financial_year                          = $request->financial_year;
        $vehicleInfo->approved_number_of_vehicles_by_ugc      = $request->approved_number_of_vehicles_by_ugc;
        $vehicleInfo->no_of_vehicles_purchased_by_university  = $request->no_of_vehicles_purchased_by_university;
        $vehicleInfo->year_of_purchase_vehicles_by_university = $request->year_of_purchase_vehicles_by_university;
        $vehicleInfo->proposed_budget                         = $request->proposed_budget;
        $vehicleInfo->approved_budget                         = $request->approved_budget;
        $vehicleInfo->remarks                                 = $request->remarks;
        $vehicleInfo->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Vehicle Info';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/vehicle-info/listing')->with('message','Vehicle Info added successfully');
    }

    public function vehicleInfoEdit ($id ) {
        //return $id;;
        $vehicleInfoById = KosVehicleInfo::find($id);
        return view('admin.vehicle-info.vehicle-info-edit',[
            'vehicleInfoById' => $vehicleInfoById
        ]);
    }

    public function vehicleInfoUpdate(Request $request ) {
        $vehicleInfo =  KosVehicleInfo::find($request->id);

        $vehicleInfo->university                              = $request->university;
        $vehicleInfo->office                                  = $request->office;
        $vehicleInfo->vehicle                                 = $request->vehicle;
        $vehicleInfo->no_of_vehicles_in_organogram            = $request->no_of_vehicles_in_organogram;
        $vehicleInfo->year_of_organogram_approval             = $request->year_of_organogram_approval;
        $vehicleInfo->financial_year                          = $request->financial_year;
        $vehicleInfo->approved_number_of_vehicles_by_ugc      = $request->approved_number_of_vehicles_by_ugc;
        $vehicleInfo->no_of_vehicles_purchased_by_university  = $request->no_of_vehicles_purchased_by_university;
        $vehicleInfo->year_of_purchase_vehicles_by_university = $request->year_of_purchase_vehicles_by_university;
        $vehicleInfo->proposed_budget                         = $request->proposed_budget;
        $vehicleInfo->approved_budget                         = $request->approved_budget;
        $vehicleInfo->remarks                                 = $request->remarks;
        $vehicleInfo->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Vehicle Info';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/vehicle-info/listing')->with('message','Vehicle Info update successfully');
    }

    public function vehicleInfoCopy ($id) {
        //return $id;
        if (Session::get('adminRole') == 'super-admin' ) {
            $vehicleInfoById      = KosVehicleInfo::find($id);
            $newVehicleInfoById   = new KosVehicleInfo();
            $newVehicleInfoById->university                              = $vehicleInfoById->university;
            $newVehicleInfoById->office                                  = $vehicleInfoById->office;
            $newVehicleInfoById->vehicle                                 = $vehicleInfoById->vehicle;
            $newVehicleInfoById->no_of_vehicles_in_organogram            = $vehicleInfoById->no_of_vehicles_in_organogram;
            $newVehicleInfoById->year_of_organogram_approval             = $vehicleInfoById->year_of_organogram_approval;
            $newVehicleInfoById->financial_year                          = $vehicleInfoById->financial_year;
            $newVehicleInfoById->approved_number_of_vehicles_by_ugc      = $vehicleInfoById->approved_number_of_vehicles_by_ugc;
            $newVehicleInfoById->no_of_vehicles_purchased_by_university  = $vehicleInfoById->no_of_vehicles_purchased_by_university;
            $newVehicleInfoById->year_of_purchase_vehicles_by_university = $vehicleInfoById->year_of_purchase_vehicles_by_university;
            $newVehicleInfoById->proposed_budget                         = $vehicleInfoById->proposed_budget;
            $newVehicleInfoById->approved_budget                         = $vehicleInfoById->approved_budget;
            $newVehicleInfoById->remarks                                 = $vehicleInfoById->remarks;
            $newVehicleInfoById->save();

            $activityLog = new KosActivityLog();
            $activityLog->description = 'Copy Vehicle Info';
            $activityLog->user        =  Session::get('adminEmail');
            $activityLog->save();

            return redirect('/database/vehicle-info/listing')->with('message','Vehicle Info copy successfully');
        }else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id', $userId)->get();
            foreach ($permissions as $permission) {
                if ($permission->permission == 'copy') {
                    $vehicleInfoById      = KosVehicleInfo::find($id);
                    $newVehicleInfoById   = new KosVehicleInfo();
                    $newVehicleInfoById->university                              = $vehicleInfoById->university;
                    $newVehicleInfoById->office                                  = $vehicleInfoById->office;
                    $newVehicleInfoById->vehicle                                 = $vehicleInfoById->vehicle;
                    $newVehicleInfoById->no_of_vehicles_in_organogram            = $vehicleInfoById->no_of_vehicles_in_organogram;
                    $newVehicleInfoById->year_of_organogram_approval             = $vehicleInfoById->year_of_organogram_approval;
                    $newVehicleInfoById->financial_year                          = $vehicleInfoById->financial_year;
                    $newVehicleInfoById->approved_number_of_vehicles_by_ugc      = $vehicleInfoById->approved_number_of_vehicles_by_ugc;
                    $newVehicleInfoById->no_of_vehicles_purchased_by_university  = $vehicleInfoById->no_of_vehicles_purchased_by_university;
                    $newVehicleInfoById->year_of_purchase_vehicles_by_university = $vehicleInfoById->year_of_purchase_vehicles_by_university;
                    $newVehicleInfoById->proposed_budget                         = $vehicleInfoById->proposed_budget;
                    $newVehicleInfoById->approved_budget                         = $vehicleInfoById->approved_budget;
                    $newVehicleInfoById->remarks                                 = $vehicleInfoById->remarks;
                    $newVehicleInfoById->save();

                    $activityLog = new KosActivityLog();
                    $activityLog->description = 'Copy Vehicle Info';
                    $activityLog->user        =  Session::get('adminEmail');
                    $activityLog->save();

                    return redirect('/database/vehicle-info/listing')->with('message','Vehicle Info copy successfully');
                }
            }
        }
        return redirect('/database/vehicle-info/listing')->with('alert','You do not permit to access it !!!');
    }

    public function vehicleInfoDelete($id ) {
        if (Session::get('adminRole') == 'super-admin' )  {
            KosVehicleInfo::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Vehicle Info';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/vehicle-info/listing')->with('message','Delete Vehicle Info successfully');
        }else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete') {
                    KosVehicleInfo::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Vehicle Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/vehicle-info/listing')->with('message','Delete Vehicle Info successfully');
                }
            }
        }
        return redirect('/database/vehicle-info/listing')->with('alert','You do not permit to access it !!!');
    }

    public function vehicleInfo(Request $request)
    {
        //return $request->all();
            switch ($request->input('submit')) {
                case 'copy':
                    if ($request->has('vehicle_info')) {
                    $infoId = $request->input('vehicle_info');
                    $count = 0;
                    foreach ($infoId as $info) {
                        $vehicleInfoById      = KosVehicleInfo::find($info);
                        $newVehicleInfoById   = new KosVehicleInfo();
                        $newVehicleInfoById->university                              = $vehicleInfoById->university;
                        $newVehicleInfoById->office                                  = $vehicleInfoById->office;
                        $newVehicleInfoById->vehicle                                 = $vehicleInfoById->vehicle;
                        $newVehicleInfoById->no_of_vehicles_in_organogram            = $vehicleInfoById->no_of_vehicles_in_organogram;
                        $newVehicleInfoById->year_of_organogram_approval             = $vehicleInfoById->year_of_organogram_approval;
                        $newVehicleInfoById->financial_year                          = $vehicleInfoById->financial_year;
                        $newVehicleInfoById->approved_number_of_vehicles_by_ugc      = $vehicleInfoById->approved_number_of_vehicles_by_ugc;
                        $newVehicleInfoById->no_of_vehicles_purchased_by_university  = $vehicleInfoById->no_of_vehicles_purchased_by_university;
                        $newVehicleInfoById->year_of_purchase_vehicles_by_university = $vehicleInfoById->year_of_purchase_vehicles_by_university;
                        $newVehicleInfoById->proposed_budget                         = $vehicleInfoById->proposed_budget;
                        $newVehicleInfoById->approved_budget                         = $vehicleInfoById->approved_budget;
                        $newVehicleInfoById->remarks                                 = $vehicleInfoById->remarks;
                        $newVehicleInfoById->save();

                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy ' . $count . ' Vehicle Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/vehicle-info/listing')->with('message', 'Vehicle ' . $count . ' Copy Info successfully');
                    } else {
                        return redirect('/database/vehicle-info/listing')->with('alert', 'Please select item(s) check box from list');
                    }

                    break;

                case 'delete':
                    if ($request->has('vehicle_info')) {
                    $deletes = $request->input('vehicle_info');
                    $count = 0;
                    foreach ($deletes as $delete) {
                        KosVehicleInfo::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete ' . $count . ' Vehicle Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/vehicle-info/listing')->with('message', 'Delete ' . $count . ' Vehicle Info successfully');
                    } else {
                        return redirect('/database/vehicle-info/listing')->with('alert', 'Please select item(s) check box from list');
                    }

                    break;

                case 'report':
                    //return $request->all();
                    if (Session::get('vehicleInfoUniversity')) {

                        //if checkbox is select
                        if ($request->has('vehicle_info')) {
                            $vehicleInfos = $request->input('vehicle_info');

                            //check the column(university) is duplicate or not start here
                            $uniTracker = 0;
                            $count = 0;
                            foreach ($vehicleInfos as $vehicleInfo) {
                                $vehicleInfoById = KosVehicleInfo::find($vehicleInfo);
                                $university      = KosUniversity::find($vehicleInfoById->university);
                                break;
                            }
                            //selected compare versity  name is not nul
                            if (!empty($university)) {
                                foreach ($vehicleInfos as $vehicleInfo) {
                                    $vehicleInfoById = KosVehicleInfo::find($vehicleInfo);
                                    $universityAgain = KosUniversity::find($vehicleInfoById->university);
                                    if (empty($universityAgain)) {
                                        $uni = 0;
                                        $pdf = PDF::loadView('admin.vehicle-info.pdf-vehicle-info', [
                                            'uni' => $uni,
                                            'vehicleInfos' => $vehicleInfos
                                        ]);
                                        return $pdf->stream('vehicle-info.pdf');
                                    }

                                    if ($university->university == $universityAgain->university) {
                                        $uniTracker++;
                                        $count++;
                                    } else {
                                        $count++;
                                    }
                                }
                                //check the column(university) is duplicate or not end here


                                if ($uniTracker == $count) {
                                    //All selected university name is same

                                    $uni = 1;
                                    $pdf = PDF::loadView('admin.vehicle-info.pdf-vehicle-info', [
                                        'uni' => $uni,
                                        'vehicleInfos' => $vehicleInfos
                                    ]);
                                    return $pdf->stream('vehicle-info.pdf');

                                } else {
                                    //All selected university name is not same

                                    $uni = 0;
                                    $pdf = PDF::loadView('admin.vehicle-info.pdf-vehicle-info', [
                                        'uni' => $uni,
                                        'vehicleInfos' => $vehicleInfos
                                    ]);
                                    return $pdf->stream('vehicle-info.pdf');

                                }
                            } else {
                                //if compare versity is null
                                $uni = 0;
                                $pdf = PDF::loadView('admin.vehicle-info.pdf-vehicle-info', [
                                    'uni' => $uni,
                                    'vehicleInfos' => $vehicleInfos
                                ]);
                                return $pdf->stream('vehicle-info.pdf');
                            }



                            break;
                        } else {
                            //if checkbox is not select default pdf all in table
                            $vehicleInfos = $request->input('vehicle_info_hide');

                            //check the column(university) is duplicate or not start here
                            $uniTracker = 0;
                            $count = 0;
                            foreach ($vehicleInfos as $vehicleInfo) {
                                $vehicleInfoById = KosVehicleInfo::find($vehicleInfo);
                                $university = KosUniversity::find($vehicleInfoById->university);
                                break;
                            }
                            foreach ($vehicleInfos as $vehicleInfo) {
                                $vehicleInfoById = KosVehicleInfo::find($vehicleInfo);
                                $universityAgain = KosUniversity::find($vehicleInfoById->university);
                                if (!empty($universityAgain)) {
                                    if ($university->university == $universityAgain->university) {
                                        $uniTracker++;
                                        $count++;
                                    } else {
                                        $count++;
                                    }
                                } else {
                                    //universityAgain is null
                                    $uni = 0;
                                    $pdf = PDF::loadView('admin.vehicle-info.pdf-vehicle-info', [
                                        'uni' => $uni,
                                        'vehicleInfos' => $vehicleInfos
                                    ]);
                                    return $pdf->stream('vehicle-info.pdf');
                                }
                            }
                            //check the column(university) is duplicate or not end here


                            if ($uniTracker == $count) {
                                //All selected university name is same

                                $uni = 1;
                                $pdf = PDF::loadView('admin.vehicle-info.pdf-vehicle-info', [
                                    'uni' => $uni,
                                    'vehicleInfos' => $vehicleInfos
                                ]);
                                return $pdf->stream('vehicle-info.pdf');

                            } else {
                                //All selected university name is not same

                                $uni = 0;
                                $pdf = PDF::loadView('admin.vehicle-info.pdf-vehicle-info', [
                                    'uni' => $uni,
                                    'vehicleInfos' => $vehicleInfos
                                ]);
                                return $pdf->stream('vehicle-info.pdf');

                            }

                        }
                    } else {
                        return redirect('/database/vehicle-info/listing')->with('alert','Please select university!!!');
                    }
                    break;
            }
    }


    public function vehicleInfoModalPost(Request $request) {
        //return $request->all();

        Session::forget('vehicleInfoUniversity');
        Session::forget('vehicleInfoOffice');
        Session::forget('vehicleInfoVehicle');
        Session::forget('vehicleInfoNoOfVehiclesInOrganogram');
        Session::forget('vehicleInfoYearOfOrganogramApproval');
        Session::forget('vehicleInfoFinancialYear');
        Session::forget('vehicleInfoApprovedNumberOfVehiclesByUgc');
        Session::forget('vehicleInfoNoOfVehiclesPurchasedByUniversity');
        Session::forget('vehicleInfoYearOfPurchaseVehiclesByUniversity');
        Session::forget('vehicleInfoProposedBudget');
        Session::forget('vehicleInfoApprovedBudget');
        Session::forget('vehicleInfoRemarks');

        $columns = $request->input('column');
        foreach ($columns as $column ) {
            if ($column == "university") {
                Session::put('vehicleInfoUniversity', $column);
            }
            if ($column == "office") {
                Session::put('vehicleInfoOffice', $column);
            }
            if ($column == "vehicle") {
                Session::put('vehicleInfoVehicle', $column);
            }
            if ($column == "no_of_vehicles_in_organogram") {
                Session::put('vehicleInfoNoOfVehiclesInOrganogram', $column);
            }
            if ($column == "year_of_organogram_approval") {
                Session::put('vehicleInfoYearOfOrganogramApproval', $column);
            }
            if ($column == "financial_year") {
                Session::put('vehicleInfoFinancialYear', $column);
            }
            if ($column == "approved_number_of_vehicles_by_ugc") {
                Session::put('vehicleInfoApprovedNumberOfVehiclesByUgc', $column);
            }
            if ($column == "no_of_vehicles_purchased_by_university") {
                Session::put('vehicleInfoNoOfVehiclesPurchasedByUniversity', $column);
            }
            if ($column == "year_of_purchase_vehicles_by_university") {
                Session::put('vehicleInfoYearOfPurchaseVehiclesByUniversity', $column);
            }
            if ($column == "proposed_budget") {
                Session::put('vehicleInfoProposedBudget', $column);
            }
            if ($column == "approved_budget") {
                Session::put('vehicleInfoApprovedBudget', $column);
            }
            if ($column == "remarks") {
                Session::put('vehicleInfoRemarks', $column);
            }
        }
        return view('admin.vehicle-info.vehicle-info-modal', [
            'columns' => $columns
        ]);
    }

    public function vehicleInfoModalGet() {
        return view('admin.vehicle-info.vehicle-info-modal');
    }



}
