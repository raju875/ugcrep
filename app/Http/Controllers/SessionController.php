<?php

namespace App\Http\Controllers;

use App\KosSession;
use App\KosActivityLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\KosPermission;
use App\KosPermissionModule;

class SessionController extends Controller
{
    public function wrongUrl() {
        return redirect('/database/session/listing')->with('alert','Your action url is wrong !!!');
    }

    public function sessionListing() {
        $sessions = DB::table('kos_sessions')->orderBy('id','desc')->get();
        return view('admin.session.session-table',[
            'sessions' => $sessions
        ]);
    }

    public function sessionForm() {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.session.session-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.session.session-form');
                }
            }
        }
        return redirect('/database/session/listing');
    }

    public function sessionAdd (Request $request) {
        $session = new KosSession();
        $session->session = $request->sessions;
        $session->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Session';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/session/listing')->with('message','Session added successfully');
    }

    public function sessionEdit ($id ) {
        //return $id;
        $sessionById = KosSession::find($id);
        return view('admin.session.session-edit',[
            'sessionById' => $sessionById
        ]);
    }

    public function sessionUpdate(Request $request ) {
        $session =  KosSession::find($request->id);
        $session->session = $request->sessions;
        $session->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Session';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/session/listing')->with('message','Session update successfully');
    }

    public function sessionCopy ($id) {
        //return $id;
        if (Session::get('adminRole')=='super-admin') {
            $session    = KosSession::find($id);
            $newSession = new KosSession();
            $newSession->session = $session->session;
            $newSession->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Session';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/session/listing')->with('message','Copy Session successfully');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'copy') {
                    $session    = KosSession::find($id);
                    $newSession = new KosSession();
                    $newSession->session = $session->session;
                    $newSession->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Session';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/session/listing')->with('message','Copy Session successfully');
                }
            }
        }
        return redirect('/database/session/listing');
    }

    public function sessionDelete($id ) {
        if (Session::get('adminRole')=='super-admin')  {
            KosSession::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Session';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/session/listing')->with('message','Delete Session successfully');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete') {
                    KosSession::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Session';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/session/listing')->with('message','Delete Session successfully');
                }
            }
        }
        return redirect('/database/session/listing');
    }

    public function session(Request $request) {
        //return $request->all();
        if ($request->has('sessions')) {
            switch ($request->input('submit')) {
                case 'copy':
                    $sessionId  = $request->input('sessions');
                    //$centres = $request->all();
                    $count = 0;
                    foreach ($sessionId as $id ) {
                        $session    = KosSession::find($id);
                        $newSession = new KosSession();
                        $newSession->session = $session->session;
                        $newSession->save();
                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy '.$count.' Session';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/session/listing')->with('message','Copy '.$count.' Session successfully');
                    break;

                case 'delete':
                    $deletes = $request->input('sessions');
                    $count = 0;
                    foreach ($deletes as $delete ) {
                        KosSession::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete '.$count.' Session';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/session/listing')->with('message','Delete '.$count.' Session successfully');
                    break;
            }
        } else {
            return redirect('/database/session/listing')->with('alert','Please select item(s) check box from list');
        }

    }
}
