<?php

namespace App\Http\Controllers;

use App\KosActivityLog;
use App\KosCentre;
use App\KosCentreInfo;
use App\KosDepartment;
use App\KosSession;
use App\KosUniversity;
use App\KosUniversityPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use PDF;
use App\KosPermission;
use App\KosPermissionModule;

class CentreInfoController extends Controller
{
    public function wrongUrl() {
        return redirect('/database/centre-info/listing')->with('alert','Your action url is wrong !!!');
    }

    public function centreInfoListing() {
        //first forgot all centre info session data
        Session::forget('centreInfoUniversity');
        Session::forget('centreInfoCentre');
        Session::forget('centreInfoCentreDateOfApproval');
        Session::forget('centreInfoDepartment');
        Session::forget('centreInfoPrograms');
        Session::forget('centreInfoProgramDateOfApproval');
        Session::forget('centreInfoSession');
        Session::forget('centreInfoNoOfSeat');
        Session::forget('centreInfoEnrolledStudents');
        Session::forget('centreInfoMaleStudents');
        Session::forget('centreInfoFemaleStudents');
        Session::forget('centreInfoCurrentStudents');
        Session::forget('centreInfoYear');
        Session::forget('centreInfoDateOfApproval');
        Session::forget('centreInfoRemarks');

        //by default centre info session data
        Session::put('centreInfoUniversity',"university");
        Session::put('centreInfoCentre','centre');
        Session::put('centreInfoDepartment','department');
        Session::put('centreInfoPrograms','programs');
        Session::put('centreInfoSession','session');
        Session::put('centreInfoCurrentStudents','current_students');
        Session::put('centreInfoYear','year');

        return view('admin.centre-info.centre-info-table');
    }

    public function centreInfoForm () {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.centre-info.centre-info-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.centre-info.centre-info-form');
                }
            }
        }
        return redirect('/database/centre-info/listing');
    }

    public function centreInfoAdd (Request $request) {

        $centerInfo = new KosCentreInfo();
        $centerInfo->university               = $request->university;
        $centerInfo->centre                   = $request->centre;
        $centerInfo->centre_date_of_approval  = $request->centre_date_of_approval;
        $centerInfo->programs                 = $request->programs;
        $centerInfo->department               = $request->department;
        $centerInfo->program_date_of_approval = $request->program_date_of_approval;
        $centerInfo->session                  = $request->sessions;
        $centerInfo->no_of_seat               = $request->no_of_seat;
        $centerInfo->enrolled_students        = $request->enrolled_students;
        $centerInfo->male_students            = $request->male_students;
        $centerInfo->female_students          = $request->female_students;
        $centerInfo->current_students         = $request->current_students;
        $centerInfo->year                     = $request->year;
        $centerInfo->date_of_approval         = $request->date_of_approval;
        $centerInfo->remarks                  = $request->remarks;
        $centerInfo->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Centre Info';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/centre-info/listing')->with('message','Centre Info added successfully');
    }

    public function centreInfoEdit ($id ) {
        //return $id;;
        $centreInfoById = KosCentreInfo::find($id);
        return view('admin.centre-info.centre-info-edit',[
            'centreInfoById' => $centreInfoById
        ]);
    }

    public function centreInfoUpdate(Request $request ) {
        $centerInfo =  KosCentreInfo::find($request->id);

        $centerInfo->university               = $request->university;
        $centerInfo->centre                   = $request->centre;
        $centerInfo->centre_date_of_approval  = $request->centre_date_of_approval;
        $centerInfo->programs                 = $request->programs;
        $centerInfo->department               = $request->department;
        $centerInfo->program_date_of_approval = $request->program_date_of_approval;
        $centerInfo->session                  = $request->sessions;
        $centerInfo->no_of_seat               = $request->no_of_seat;
        $centerInfo->enrolled_students        = $request->enrolled_students;
        $centerInfo->male_students            = $request->male_students;
        $centerInfo->female_students          = $request->female_students;
        $centerInfo->current_students         = $request->current_students;
        $centerInfo->year                     = $request->year;
        $centerInfo->date_of_approval         = $request->date_of_approval;
        $centerInfo->remarks                  = $request->remarks;
        $centerInfo->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Centre Info';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/centre-info/listing')->with('message','Centre Info update successfully');
    }

    public function centreInfoCopy ($id) {
        //return $id;
        if (Session::get('adminRole') == 'super-admin' ) {
            $infoById  = KosCentreInfo::find($id);
            $newInfo   = new KosCentreInfo();
            $newInfo->university               = $infoById->university;
            $newInfo->centre                   = $infoById->centre;
            $newInfo->centre_date_of_approval  = $infoById->centre_date_of_approval;
            $newInfo->programs                 = $infoById->programs;
            $newInfo->department               = $infoById->department;
            $newInfo->program_date_of_approval = $infoById->program_date_of_approval;
            $newInfo->session                  = $infoById->sessions;
            $newInfo->no_of_seat               = $infoById->no_of_seat;
            $newInfo->enrolled_students        = $infoById->enrolled_students;
            $newInfo->male_students            = $infoById->male_students;
            $newInfo->female_students          = $infoById->female_students;
            $newInfo->current_students         = $infoById->current_students;
            $newInfo->year                     = $infoById->year;
            $newInfo->date_of_approval         = $infoById->date_of_approval;
            $newInfo->remarks                  = $infoById->remarks;
            $newInfo->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Centre Info';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/centre-info/listing')->with('message','Copy Centre Info successfully');
        }else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id', $userId)->get();
            foreach ($permissions as $permission) {
                if ($permission->permission == 'copy') {
                    $infoById  = KosCentreInfo::find($id);
                    $newInfo   = new KosCentreInfo();
                    $newInfo->university               = $infoById->university;
                    $newInfo->centre                   = $infoById->centre;
                    $newInfo->centre_date_of_approval  = $infoById->centre_date_of_approval;
                    $newInfo->programs                 = $infoById->programs;
                    $newInfo->department               = $infoById->department;
                    $newInfo->program_date_of_approval = $infoById->program_date_of_approval;
                    $newInfo->session                  = $infoById->sessions;
                    $newInfo->no_of_seat               = $infoById->no_of_seat;
                    $newInfo->enrolled_students        = $infoById->enrolled_students;
                    $newInfo->male_students            = $infoById->male_students;
                    $newInfo->female_students          = $infoById->female_students;
                    $newInfo->current_students         = $infoById->current_students;
                    $newInfo->year                     = $infoById->year;
                    $newInfo->date_of_approval         = $infoById->date_of_approval;
                    $newInfo->remarks                  = $infoById->remarks;
                    $newInfo->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Centre Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/centre-info/listing')->with('message','Copy Centre Info successfully');
                }
            }
        }
        return redirect('/database/centre-info/listing')->with('alert','You do not permit to access it !!!');
    }

    public function centreInfoDelete($id ) {
        if (Session::get('adminRole') == 'super-admin' )  {
            KosCentreInfo::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Centre Info';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/centre-info/listing')->with('message','Delete Centre Info successfully');
        }else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete') {
                    KosCentreInfo::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Centre Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/centre-info/listing')->with('message','Delete Centre Info successfully');
                }
            }
        }
        return redirect('/database/centre-info/listing')->with('alert','You do not permit to access it !!!');
    }

    public function centreInfo(Request $request)
    {
        //return $request->all();
        switch ($request->input('submit')) {
            case 'copy':
                if ($request->has('centre_info')) {
                    $infoId = $request->input('centre_info');
                    $count = 0;
                    foreach ($infoId as $info) {
                        $infos = KosCentreInfo::find($info);
                        $newInfo = new KosCentreInfo();
                        $newInfo->university = $infos->university;
                        $newInfo->centre = $infos->centre;
                        $newInfo->centre_date_of_approval = $infos->centre_date_of_approval;
                        $newInfo->department = $infos->department;
                        $newInfo->programs = $infos->programs;
                        $newInfo->program_date_of_approval = $infos->program_date_of_approval;
                        $newInfo->session = $infos->session;
                        $newInfo->no_of_seat = $infos->no_of_seat;
                        $newInfo->enrolled_students = $infos->enrolled_students;
                        $newInfo->male_students = $infos->male_students;
                        $newInfo->female_students = $infos->female_students;
                        $newInfo->current_students = $infos->current_students;
                        $newInfo->year = $infos->year;
                        $newInfo->date_of_approval = $infos->date_of_approval;
                        $newInfo->remarks = $infos->remarks;
                        $newInfo->save();

                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy ' . $count . ' Centre Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/centre-info/listing')->with('message', 'Copy ' . $count . ' Centre Info successfully');
                } else {
                    return redirect('/database/centre-info/listing')->with('alert', 'Please select item(s) check box from list');
                }
                break;

            case 'delete':
                if ($request->has('centre_info')) {
                    $deletes = $request->input('centre_info');
                    $count = 0;
                    foreach ($deletes as $delete) {
                        KosCentreInfo::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete ' . $count . ' Centre Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/centre-info/listing')->with('message', 'Delete ' . $count . ' Centre Info successfully');
                } else {
                    return redirect('/database/centre-info/listing')->with('alert', 'Please select item(s) check box from list');
                }
                break;

            case 'report':
                //return $request->all();
                if (Session::get('centreInfoUniversity')) {

                    //if checkbox is select
                    if ($request->has('centre_info')) {
                        $centreInfos = $request->input('centre_info');

                        //check the column(university) is duplicate or not start here
                        $uniTracker = 0;
                        $count = 0;
                        foreach ($centreInfos as $centreInfo) {
                            $centreInfoById = KosCentreInfo::find($centreInfo);
                            $university = KosUniversity::find($centreInfoById->university);
                            break;
                        }
                        //selected compare versity  name is not nul
                        if (!empty($university)) {
                            foreach ($centreInfos as $centreInfo) {
                                $centreInfoById = KosCentreInfo::find($centreInfo);
                                $universityAgain = KosUniversity::find($centreInfoById->university);
                                if (empty($universityAgain)) {
                                    $uni = 0;
                                    $pdf = PDF::loadView('admin.centre-info.pdf-centre-info', [
                                        'uni' => $uni,
                                        'centreInfos' => $centreInfos
                                    ]);
                                    return $pdf->stream('centre-info.pdf');
                                }

                                if ($university->university == $universityAgain->university) {
                                    $uniTracker++;
                                    $count++;
                                } else {
                                    $count++;
                                }
                            }
                            //check the column(university) is duplicate or not end here


                            if ($uniTracker == $count) {
                                //All selected university name is same

                                $uni = 1;
                                $pdf = PDF::loadView('admin.centre-info.pdf-centre-info', [
                                    'uni' => $uni,
                                    'centreInfos' => $centreInfos
                                ]);
                                return $pdf->stream('centre-info.pdf');

                            } else {
                                //All selected university name is not same

                                $uni = 0;
                                $pdf = PDF::loadView('admin.centre-info.pdf-centre-info', [
                                    'uni' => $uni,
                                    'centreInfos' => $centreInfos
                                ]);
                                return $pdf->stream('centre-info.pdf');

                            }
                        } else {
                            //if compare versity is null
                            $uni = 0;
                            $pdf = PDF::loadView('admin.centre-info.pdf-centre-info', [
                                'uni' => $uni,
                                'centreInfos' => $centreInfos
                            ]);
                            return $pdf->stream('centre-info.pdf');
                        }



                        break;
                    } else {
                        //if checkbox is not select default pdf all in table
                        $centreInfos = $request->input('centre_info_hide');

                        //check the column(university) is duplicate or not start here
                        $uniTracker = 0;
                        $count = 0;
                        foreach ($centreInfos as $centreInfo) {
                            $centreInfoById = KosCentreInfo::find($centreInfo);
                            $university = KosUniversity::find($centreInfoById->university);
                            break;
                        }
                        foreach ($centreInfos as $centreInfo) {
                            $centreInfoById = KosCentreInfo::find($centreInfo);
                            $universityAgain = KosUniversity::find($centreInfoById->university);
                            if (!empty($universityAgain)) {
                                if ($university->university == $universityAgain->university) {
                                    $uniTracker++;
                                    $count++;
                                } else {
                                    $count++;
                                }
                            }
                        }
                        //check the column(university) is duplicate or not end here


                        if ($uniTracker == $count) {
                            //All selected university name is same

                            $uni = 1;
                            $pdf = PDF::loadView('admin.centre-info.pdf-centre-info', [
                                'uni' => $uni,
                                'centreInfos' => $centreInfos
                            ]);
                            return $pdf->stream('centre-info.pdf');

                        } else {
                            //All selected university name is not same

                            $uni = 0;
                            $pdf = PDF::loadView('admin.centre-info.pdf-centre-info', [
                                'uni' => $uni,
                                'centreInfos' => $centreInfos
                            ]);
                            return $pdf->stream('centre-info.pdf');

                        }

                    }
                 } else {
                    return redirect('/database/centre-info/listing')->with('alert','Please select university!!!');
                }
                break;
        }
    }

    public function centreInfoModalPost (Request $request) {
        //return $request->all() ;
        Session::forget('centreInfoUniversity');
        Session::forget('centreInfoCentre');
        Session::forget('centreInfoCentreDateOfApproval');
        Session::forget('centreInfoDepartment');
        Session::forget('centreInfoPrograms');
        Session::forget('centreInfoProgramDateOfApproval');
        Session::forget('centreInfoSession');
        Session::forget('centreInfoNoOfSeat');
        Session::forget('centreInfoEnrolledStudents');
        Session::forget('centreInfoMaleStudents');
        Session::forget('centreInfoFemaleStudents');
        Session::forget('centreInfoCurrentStudents');
        Session::forget('centreInfoYear');
        Session::forget('centreInfoDateOfApproval');
        Session::forget('centreInfoRemarks');

        $columns = $request->input('column');
        foreach ($columns as $column ) {
            if ($column == "university") {
                Session::put('centreInfoUniversity', $column);
            }
            if ($column == "centre") {
                Session::put('centreInfoCentre', $column);
            }
            if ($column == "centre_date_of_approval") {
                Session::put('centreInfoCentreDateOfApproval', $column);
            }
            if ($column == "department") {
                Session::put('centreInfoDepartment', $column);
            }
            if ($column == "programs") {
                Session::put('centreInfoPrograms', $column);
            }
            if ($column == "program_date_of_approval") {
                Session::put('centreInfoProgramDateOfApproval', $column);
            }
            if ($column == "session") {
                Session::put('centreInfoSession', $column);
            }
            if ($column == "no_of_seat") {
                Session::put('centreInfoNoOfSeat', $column);
            }
            if ($column == "enrolled_students") {
                Session::put('centreInfoEnrolledStudents', $column);
            }
            if ($column == "male_students") {
                Session::put('centreInfoMaleStudents', $column);
            }
            if ($column == "female_students") {
                Session::put('centreInfoFemaleStudents', $column);
            }
            if ($column == "current_students") {
                Session::put('centreInfoCurrentStudents', $column);
            }
            if ($column == "year") {
                Session::put('centreInfoYear', $column);
            }
            if ($column == "date_of_approval") {
                Session::put('centreInfoDateOfApproval', $column);
            }
            if ($column == "remarks") {
                Session::put('centreInfoRemarks', $column);
            }
        }
        return view('admin.centre-info.centre-info-modal');
    }

    public function centreInfoModalGet () {
        return view('admin.centre-info.centre-info-modal');
    }












        public function centreInfoPdf (Request $request ) {
        return $request->all();
            $columns    = $request->input('column');
            $checkboxes = $request->input('checkbox');

            //check the column(university) is duplicate or not start here
            $uniTracker = 0 ;
            foreach ($checkboxes as $checkbox ) {
                $centreInfoById = KosCentreInfo::find($checkbox);
                $university     =  KosUniversity::find($centreInfoById->university);
                break;
            }
            foreach($checkboxes as $checkbox ){
                $centreInfoById  = KosCentreInfo::find($checkbox);
                $universityAgain =  KosUniversity::find($centreInfoById->university);
                if ($university->university == $universityAgain->university ) {
                    $uniTracker++;
                }else {
                    $uniTracker =0 ;
                }
            }
//            if ($uniTracker==0) {
//                return 'Unrepeated University Name';
//            } else {
//                return 'Repeated University Name';
//            }
            //check the column(university) is duplicate or not end here

            //check the column(centre) is duplicate or not start here
            $centreTracker = 0 ;
            foreach ($checkboxes as $checkbox ) {
                $centreInfoById = KosCentreInfo::find($checkbox);
                $centre         =  KosCentre::find($centreInfoById->centre);
                break;
            }
            foreach($checkboxes as $checkbox ){
                $centreInfoById = KosCentreInfo::find($checkbox);
                $centreAgain    =  KosCentre::find($centreInfoById->centre);

                if (empty($centre)) {
                    if (empty($centreAgain)) {
                        //centre and centreAgain empty
                        $centreTracker = 0 ;
                    } else {
                        //centre empty but centreAgain not empty
                        $centreTracker = 0 ;
                    }
                } else { //centre not empty
                    if (empty($centreAgain)) {
                        //centre not empty but centreAgain empty
                        $centreTracker = 0 ;
                    } else {
                        //centre & centreAgain not empty
                        if ($centre->centre == $centreAgain->centre ) {
                            $centreTracker++;
                        }else {
                            $centreTracker =0 ;
                        }
                    }
                }

            }
//            if ($centreTracker==0) {
//                return 'Unrepeated Centre Name';
//            } else {
//                return 'Repeated Centre Name';
//            }
            //check the column(centre) is duplicate or not end here

            //check the column(department) is duplicate or not start here
            $deptTracker = 0 ;
            foreach ($checkboxes as $checkbox ) {
                $centreInfoById = KosCentreInfo::find($checkbox);
                $department     =  KosDepartment::find($centreInfoById->department);
                break;
            }
            foreach($checkboxes as $checkbox ){
                $centreInfoById = KosCentreInfo::find($checkbox);
                $departmentAgain    =  KosDepartment::find($centreInfoById->department);

                if (empty($department)) {
                    if (empty($departmentAgain)) {
                        //department and deptTracker empty
                        $deptTracker = 0 ;
                    } else {
                        //department empty but departmentAgain not empty
                        $deptTracker = 0 ;
                    }
                } else { //department not empty
                    if (empty($departmentAgain)) {
                        //department not empty but departmentAgain empty
                        $deptTracker = 0 ;
                    } else {
                        //department & departmentAgain not empty
                        if ($department->department == $departmentAgain->department ) {
                            $deptTracker++;
                        }else {
                            $deptTracker =0 ;
                        }
                    }
                }

            }
//            if ($deptTracker== 0) {
//                return 'Unrepeated Department Name';
//            } else {
//                return 'Repeated Department Name';
//            }
            //check the column(department) is duplicate or not end here

            if($uniTracker==0 && $centreTracker==0 && $deptTracker==0 ) {
                //there is no unrepeated data
                $uni  = 0;
                $cen  = 0;
                $dept = 0;
                $pdf = PDF::loadView('admin.centre-info.pdf-centre-info',[
                    'columns'    => $columns,
                    'checkboxes' => $checkboxes,
                    'uni'        => $uni,
                    'cen'        => $cen,
                    'dept'       => $dept
                ]);
                return $pdf->stream('centre-info.pdf');
            }

            if($uniTracker!=0 && $centreTracker==0 && $deptTracker==0 ) {
                //University is repeated
                $uni  = 1;
                $cen  = 0;
                $dept = 0;
                $pdf = PDF::loadView('admin.centre-info.pdf-centre-info',[
                    'columns'    => $columns,
                    'checkboxes' => $checkboxes,
                    'uni'        => $uni,
                    'cen'        => $cen,
                    'dept'       => $dept
                ]);
                return $pdf->stream('centre-info.pdf');
            }

            if($uniTracker==0 && $centreTracker!=0 && $deptTracker==0 ) {
                //Centre is repeated
                $uni  = 0;
                $cen  = 1;
                $dept = 0;
                $pdf = PDF::loadView('admin.centre-info.pdf-centre-info',[
                    'columns'    => $columns,
                    'checkboxes' => $checkboxes,
                    'uni'        => $uni,
                    'cen'        => $cen,
                    'dept'       => $dept
                ]);
                return $pdf->stream('centre-info.pdf');
            }

            if($uniTracker==0 && $centreTracker==0 && $deptTracker!=0 ) {
                //Department is repeated
                $uni  = 0;
                $cen  = 0;
                $dept = 1;
                $pdf = PDF::loadView('admin.centre-info.pdf-centre-info',[
                    'columns'    => $columns,
                    'checkboxes' => $checkboxes,
                    'uni'        => $uni,
                    'cen'        => $cen,
                    'dept'       => $dept
                ]);
                return $pdf->stream('centre-info.pdf');
            }

            if($uniTracker!=0 && $centreTracker!=0 && $deptTracker==0 ) {
                //University & Centre are repeated
                $uni  = 1;
                $cen  = 1;
                $dept = 0;
                $pdf = PDF::loadView('admin.centre-info.pdf-centre-info',[
                    'columns'    => $columns,
                    'checkboxes' => $checkboxes,
                    'uni'        => $uni,
                    'cen'        => $cen,
                    'dept'       => $dept
                ]);
                return $pdf->stream('centre-info.pdf');
            }

            if($uniTracker!=0 && $centreTracker==0 && $deptTracker!=0 ) {
                //University & Department are repeated
                $uni  = 1;
                $cen  = 0;
                $dept = 1;
                $pdf = PDF::loadView('admin.centre-info.pdf-centre-info',[
                    'columns'    => $columns,
                    'checkboxes' => $checkboxes,
                    'uni'        => $uni,
                    'cen'        => $cen,
                    'dept'       => $dept
                ]);
                return $pdf->stream('centre-info.pdf');
            }

            if($uniTracker==0 && $centreTracker!=0 && $deptTracker!=0 ) {
                //Centre & Department are repeated
                $uni  = 0;
                $cen  = 1;
                $dept = 1;
                $pdf = PDF::loadView('admin.centre-info.pdf-centre-info',[
                    'columns'    => $columns,
                    'checkboxes' => $checkboxes,
                    'uni'        => $uni,
                    'cen'        => $cen,
                    'dept'       => $dept
                ]);
                return $pdf->stream('centre-info.pdf');
            }

            if($uniTracker!=0 && $centreTracker!=0 && $deptTracker!=0 ) {
                //All data are repeated
                $uni  = 1;
                $cen  = 1;
                $dept = 1;
                $pdf = PDF::loadView('admin.centre-info.pdf-centre-info',[
                    'columns'    => $columns,
                    'checkboxes' => $checkboxes,
                    'uni'        => $uni,
                    'cen'        => $cen,
                    'dept'       => $dept
                ]);
                return $pdf->stream('centre-info.pdf');
            }

        }

}
