<?php

namespace App\Http\Controllers;

use App\KosActivityLog;
use App\KosInstituteInfo;
use App\KosCentreInfo;
use App\KosDepartment;
use App\KosUniversity;
use App\KosUniversityPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use PDF;
use App\KosPermission;
use App\KosPermissionModule;

class InstituteInfoController extends Controller
{
    public function wrongUrl()
    {
        return redirect('/database/institute-info/listing')->with('alert', 'Your action url is wrong !!!');
    }

    public function instituteInfoListing()
    {
        //forgot all faculty info session data
        Session::forget('instituteInfoUniversity');
        Session::forget('instituteInfoInstitute');
        Session::forget('instituteInfoInstituteDateOfApproval');
        Session::forget('instituteInfoDepartment');
        Session::forget('instituteInfoDepartmentDateOfApproval');
        Session::forget('instituteInfoPrograms');
        Session::forget('instituteInfoProgramDateOfApproval');
        Session::forget('instituteInfoSession');
        Session::forget('instituteInfoNoOfSeat');
        Session::forget('instituteInfoEnrolledStudents');
        Session::forget('instituteInfoMaleStudents');
        Session::forget('instituteInfoFemaleStudents');
        Session::forget('instituteInfoCurrentStudents');
        Session::forget('instituteInfoYear');
        Session::forget('instituteInfoDateOfApproval');
        Session::forget('instituteInfoRemarks');

        //by default put faculty info in session

        Session::put('instituteInfoUniversity',"university");
        Session::put('instituteInfoInstitute','institute');
        Session::put('instituteInfoDepartment','department');
        Session::put('instituteInfoPrograms','programs');
        Session::put('instituteInfoSession','session');
        Session::put('instituteInfoNoOfSeat','no_of_seat');
        Session::put('instituteInfoYear','year');
        return view('admin.institute-info.institute-info-table');
    }

    public function instituteInfoForm()
    {
        if (Session::get('adminRole') == 'super-admin') {
            return view('admin.institute-info.institute-info-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission) {
                if ($permission->permission == 'add') {
                    return view('admin.institute-info.institute-info-form');
                }
            }
        }
        return redirect('/database/institute-info/listing');
    }

    public function instituteInfoAdd(Request $request)
    {

        $instituteInfo = new KosInstituteInfo();
        $instituteInfo->university = $request->university;
        $instituteInfo->institute = $request->institute;
        $instituteInfo->institute_date_of_approval = $request->institute_date_of_approval;
        $instituteInfo->department = $request->department;
        $instituteInfo->department_date_of_approval = $request->department_date_of_approval;
        $instituteInfo->programs = $request->programs;
        $instituteInfo->program_date_of_approval = $request->program_date_of_approval;
        $instituteInfo->session = $request->sessions;
        $instituteInfo->no_of_seat = $request->no_of_seat;
        $instituteInfo->enrolled_students = $request->enrolled_students;
        $instituteInfo->male_students = $request->male_students;
        $instituteInfo->female_students = $request->female_students;
        $instituteInfo->current_students = $request->current_students;
        $instituteInfo->year = $request->year;
        $instituteInfo->date_of_approval = $request->date_of_approval;
        $instituteInfo->remarks = $request->remarks;
        $instituteInfo->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Institute Info';
        $activityLog->user = Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/institute-info/listing')->with('message', 'Institute Info added successfully');
    }

    public function instituteInfoEdit($id)
    {
        //return $id;;
        $instituteInfoById = KosInstituteInfo::find($id);
        return view('admin.institute-info.institute-info-edit', [
            'instituteInfoById' => $instituteInfoById
        ]);
    }

    public function instituteInfoUpdate(Request $request)
    {
        $instituteInfo = KosInstituteInfo::find($request->id);

        $instituteInfo->university = $request->university;
        $instituteInfo->institute = $request->institute;
        $instituteInfo->institute_date_of_approval = $request->institute_date_of_approval;
        $instituteInfo->department = $request->department;
        $instituteInfo->department_date_of_approval = $request->department_date_of_approval;
        $instituteInfo->programs = $request->programs;
        $instituteInfo->program_date_of_approval = $request->program_date_of_approval;
        $instituteInfo->session = $request->sessions;
        $instituteInfo->no_of_seat = $request->no_of_seat;
        $instituteInfo->enrolled_students = $request->enrolled_students;
        $instituteInfo->male_students = $request->male_students;
        $instituteInfo->female_students = $request->female_students;
        $instituteInfo->current_students = $request->current_students;
        $instituteInfo->year = $request->year;
        $instituteInfo->date_of_approval = $request->date_of_approval;
        $instituteInfo->remarks = $request->remarks;
        $instituteInfo->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Institute Info';
        $activityLog->user = Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/institute-info/listing')->with('message', 'Institute Info update successfully');
    }

    public function instituteInfoCopy($id)
    {
        //return $id;
        if (Session::get('adminRole') == 'super-admin') {
            $infoById = KosInstituteInfo::find($id);
            $newInfo = new KosInstituteInfo();
            $newInfo->university = $infoById->university;
            $newInfo->institute = $infoById->institute;
            $newInfo->institute_date_of_approval = $infoById->institute_date_of_approval;
            $newInfo->department = $infoById->department;
            $newInfo->department_date_of_approval = $infoById->department_date_of_approval;
            $newInfo->programs = $infoById->programs;
            $newInfo->program_date_of_approval = $infoById->program_date_of_approval;
            $newInfo->session = $infoById->sessions;
            $newInfo->no_of_seat = $infoById->no_of_seat;
            $newInfo->enrolled_students = $infoById->enrolled_students;
            $newInfo->male_students = $infoById->male_students;
            $newInfo->female_students = $infoById->female_students;
            $newInfo->current_students = $infoById->current_students;
            $newInfo->year = $infoById->year;
            $newInfo->date_of_approval = $infoById->date_of_approval;
            $newInfo->remarks = $infoById->remarks;
            $newInfo->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Institute Info';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/institute-info/listing')->with('message', 'Copy Institute Info successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id', $userId)->get();
            foreach ($permissions as $permission) {
                if ($permission->permission == 'copy') {
                    $infoById = KosInstituteInfo::find($id);
                    $newInfo = new KosInstituteInfo();
                    $newInfo->university = $infoById->university;
                    $newInfo->institute = $infoById->institute;
                    $newInfo->institute_date_of_approval = $infoById->institute_date_of_approval;
                    $newInfo->department = $infoById->department;
                    $newInfo->department_date_of_approval = $infoById->department_date_of_approval;
                    $newInfo->programs = $infoById->programs;
                    $newInfo->program_date_of_approval = $infoById->program_date_of_approval;
                    $newInfo->session = $infoById->sessions;
                    $newInfo->no_of_seat = $infoById->no_of_seat;
                    $newInfo->enrolled_students = $infoById->enrolled_students;
                    $newInfo->male_students = $infoById->male_students;
                    $newInfo->female_students = $infoById->female_students;
                    $newInfo->current_students = $infoById->current_students;
                    $newInfo->year = $infoById->year;
                    $newInfo->date_of_approval = $infoById->date_of_approval;
                    $newInfo->remarks = $infoById->remarks;
                    $newInfo->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Institute Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/institute-info/listing')->with('message', 'Copy Institute Info successfully');
                }
            }
        }
        return redirect('/database/institute-info/listing')->with('alert', 'You do not permit to access it !!!');
    }

    public function instituteInfoDelete($id)
    {
        if (Session::get('adminRole') == 'super-admin') {
            KosInstituteInfo::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Institute Info';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/institute-info/listing')->with('message', 'Delete Institute Info successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id', $userId)->get();
            foreach ($permissions as $permission) {
                if ($permission->permission == 'delete') {
                    KosInstituteInfo::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Institute Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/institute-info/listing')->with('message', 'Delete Institute Info successfully');
                }
            }
        }
        return redirect('/database/institute-info/listing')->with('alert', 'You do not permit to access it !!!');
    }

    public function instituteInfo(Request $request)
    {
        //return $request->all();
        switch ($request->input('submit')) {
            case 'copy':
                if ($request->has('institute_info')) {
                    $infoId = $request->input('institute_info');
                    $count = 0;
                    foreach ($infoId as $info) {
                        $infos = KosInstituteInfo::find($info);
                        $newInfo = new KosInstituteInfo();
                        $newInfo->university = $infos->university;
                        $newInfo->institute = $infos->institute;
                        $newInfo->institute_date_of_approval = $infos->institute_date_of_approval;
                        $newInfo->department = $infos->department;
                        $newInfo->department_date_of_approval = $infos->department_date_of_approval;
                        $newInfo->programs = $infos->programs;
                        $newInfo->program_date_of_approval = $infos->program_date_of_approval;
                        $newInfo->session = $infos->sessions;
                        $newInfo->no_of_seat = $infos->no_of_seat;
                        $newInfo->enrolled_students = $infos->enrolled_students;
                        $newInfo->male_students = $infos->male_students;
                        $newInfo->female_students = $infos->female_students;
                        $newInfo->current_students = $infos->current_students;
                        $newInfo->year = $infos->year;
                        $newInfo->date_of_approval = $infos->date_of_approval;
                        $newInfo->remarks = $infos->remarks;
                        $newInfo->save();

                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Institute Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/institute-info/listing')->with('message', 'Copy ' . $count . ' Institute Info successfully');
                } else {
                    return redirect('/database/institute-info/listing')->with('alert', 'Please select item(s) check box from list');
                }

                break;

            case 'delete':
                if ($request->has('institute_info')) {
                    $deletes = $request->input('institute_info');
                    $count = 0;
                    foreach ($deletes as $delete) {
                        KosInstituteInfo::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete ' . $count . ' Institute Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/institute-info/listing')->with('message', 'Delete ' . $count . ' Institute Info successfully');
                } else {
                    return redirect('/database/institute-info/listing')->with('alert', 'Please select item(s) check box from list');
                }

            case 'report':
                //return $request->all();
                if (Session::get('instituteInfoUniversity')) {

                    //if checkbox is select
                    if ($request->has('institute_info')) {
                        $instituteInfos = $request->input('institute_info');

                        //check the column(university) is duplicate or not start here
                        $uniTracker = 0;
                        $count = 0;
                        foreach ($instituteInfos as $instituteInfo) {
                            $instituteInfoById = KosInstituteInfo::find($instituteInfo);
                            $university = KosUniversity::find($instituteInfoById->university);
                            break;
                        }
                        //selected compare versity  name is not nul
                        if (!empty($university)) {
                            foreach ($instituteInfos as $instituteInfo) {
                                $instituteInfoById = KosInstituteInfo::find($instituteInfo);
                                $universityAgain = KosUniversity::find($instituteInfoById->university);
                                if (empty($universityAgain)) {
                                    $uni = 0;
                                    $pdf = PDF::loadView('admin.centre-info.pdf-centre-info', [
                                        'uni' => $uni,
                                        'instituteInfo' => $instituteInfos
                                    ]);
                                    return $pdf->stream('institute-info.pdf');
                                }

                                if ($university->university == $universityAgain->university) {
                                    $uniTracker++;
                                    $count++;
                                } else {
                                    $count++;
                                }
                            }
                            //check the column(university) is duplicate or not end here


                            if ($uniTracker == $count) {
                                //All selected university name is same

                                $uni = 1;
                                $pdf = PDF::loadView('admin.institute-info.pdf-institute-info', [
                                    'uni' => $uni,
                                    'instituteInfos' => $instituteInfos
                                ]);
                                return $pdf->stream('institute-info.pdf');

                            } else {
                                //All selected university name is not same

                                $uni = 0;
                                $pdf = PDF::loadView('admin.institute-info.pdf-institute-info', [
                                    'uni' => $uni,
                                    'instituteInfos' => $instituteInfos
                                ]);
                                return $pdf->stream('institute-info.pdf');

                            }
                        } else {
                            //if compare versity is null
                            $uni = 0;
                            $pdf = PDF::loadView('admin.centre-info.pdf-centre-info', [
                                'uni' => $uni,
                                'instituteInfos' => $instituteInfos
                            ]);
                            return $pdf->stream('institute-info.pdf');
                        }



                        break;
                    } else {
                        //if checkbox is not select default pdf all in table
                        $instituteInfos = $request->input('institute_info_hide');

                        //check the column(university) is duplicate or not start here
                        $uniTracker = 0;
                        $count = 0;
                        foreach ($instituteInfos as $instituteInfo) {
                            $instituteInfoById = KosInstituteInfo::find($instituteInfo);
                            $university = KosUniversity::find($instituteInfoById->university);
                            break;
                        }
                        if (empty($university)){
                            $uni = 0;
                            $pdf = PDF::loadView('admin.institute-info.pdf-institute-info', [
                                'uni' => $uni,
                                'instituteInfos' => $instituteInfos
                            ]);
                            return $pdf->stream('institute-info.pdf');
                        }
                        foreach ($instituteInfos as $instituteInfo) {
                            $instituteInfoById = KosInstituteInfo::find($instituteInfo);
                            $universityAgain = KosUniversity::find($instituteInfoById->university);
                            if (!empty($universityAgain)) {
                                if ($university->university == $universityAgain->university) {
                                    $uniTracker++;
                                    $count++;
                                } else {
                                    $count++;
                                }
                            } else{
                                $uni = 0;
                                $pdf = PDF::loadView('admin.institute-info.pdf-institute-info', [
                                    'uni' => $uni,
                                    'instituteInfos' => $instituteInfos
                                ]);
                                return $pdf->stream('institute-info.pdf');
                            }
                        }
                        //check the column(university) is duplicate or not end here


                        if ($uniTracker == $count) {
                            //All selected university name is same

                            $uni = 1;
                            $pdf = PDF::loadView('admin.institute-info.pdf-institute-info', [
                                'uni' => $uni,
                                'instituteInfos' => $instituteInfos
                            ]);
                            return $pdf->stream('institute-info.pdf');

                        } else {
                            //All selected university name is not same

                            $uni = 0;
                            $pdf = PDF::loadView('admin.institute-info.pdf-institute-info', [
                                'uni' => $uni,
                                'instituteInfos' => $instituteInfos
                            ]);
                            return $pdf->stream('institute-info.pdf');

                        }

                    }
                } else {
                    return redirect('/database/institute-info/listing')->with('alert','Please select university!!!');
                }
                break;
        }
    }

        public function instituteInfoModalPost (Request $request ) {
        //return $request->all();
            Session::forget('instituteInfoUniversity');
            Session::forget('instituteInfoInstitute');
            Session::forget('instituteInfoInstituteDateOfApproval');
            Session::forget('instituteInfoDepartment');
            Session::forget('instituteInfoDepartmentDateOfApproval');
            Session::forget('instituteInfoPrograms');
            Session::forget('instituteInfoProgramDateOfApproval');
            Session::forget('instituteInfoSession');
            Session::forget('instituteInfoNoOfSeat');
            Session::forget('instituteInfoEnrolledStudents');
            Session::forget('instituteInfoMaleStudents');
            Session::forget('instituteInfoFemaleStudents');
            Session::forget('instituteInfoCurrentStudents');
            Session::forget('instituteInfoYear');
            Session::forget('instituteInfoDateOfApproval');
            Session::forget('instituteInfoRemarks');

            $columns = $request->input('column');
            foreach ($columns as $column ) {
                if ($column == "university") {
                    Session::put('instituteInfoUniversity',$column);
                }
                if ($column == "institute") {
                    Session::put('instituteInfoInstitute',$column);
                }
                if ($column == "institute_date_of_approval") {
                    Session::put('instituteInfoInstituteDateOfApproval',$column);
                }
                if ($column == "department") {
                    Session::put('instituteInfoDepartment',$column);
                }
                if ($column == "department_date_of_approval") {
                    Session::put('instituteInfoDepartmentDateOfApproval',$column);
                }
                if ($column == "programs") {
                    Session::put('instituteInfoPrograms',$column);
                }
                if ($column == "program_date_of_approval") {
                    Session::put('instituteInfoProgramDateOfApproval',$column);
                }
                if ($column == "session") {
                    Session::put('instituteInfoSession',$column);
                }
                if ($column == "no_of_seat") {
                    Session::put('instituteInfoNoOfSeat',$column);
                }
                if ($column == "enrolled_students") {
                    Session::put('instituteInfoEnrolledStudents',$column);
                }
                if ($column == "male_students") {
                    Session::put('instituteInfoMaleStudents',$column);
                }
                if ($column == "female_students") {
                    Session::put('instituteInfoFemaleStudents',$column);
                }
                if ($column == "current_students") {
                    Session::put('instituteInfoCurrentStudents',$column);
                }
                if ($column == "year") {
                    Session::put('instituteInfoYear',$column);
                }
                if ($column == "date_of_approval") {
                    Session::put('instituteInfoDateOfApproval',$column);
                }
                if ($column == "remarks") {
                    Session::put('instituteInfoRemarks',$column);
                }
            }

            return view('admin.institute-info.institute-info-modal');
        }

        public function instituteInfoModalGet () {
        return view('admin.institute-info.institute-info-modal');
        }
    }
