<?php

namespace App\Http\Controllers;

use App\KosActivityLog;
use App\KosStaffInfo;
use App\KosDepartment;
use App\KosUniversity;
use App\KosUniversityPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use PDF;
use App\KosPermission;
use App\KosPermissionModule;

class StaffInfoController extends Controller
{
    public function wrongUrl()
    {
        return redirect('/database/manpower-info/listing')->with('alert', 'Your action url is wrong !!!');
    }

    public function manpowerInfoListing()
    {

        //first forgot all centre info session data
        Session::forget('staffInfoUniversity');
        Session::forget('staffInfoOffice');
        Session::forget('staffInfoTypeOfPost');
        Session::forget('staffInfoTypeOfEmployment');
        Session::forget('staffInfoOriginalPost');
        Session::forget('staffInfoGrade');
        Session::forget('staffInfoFinancialYear');
        Session::forget('staffInfoNoOfPostInOrganogram');
        Session::forget('staffInfoNoOfPostApprovedByUGC');
        Session::forget('staffInfoDateOfApprovalByUGC');
        Session::forget('staffInfoNoOfPostRegularAppointed');
        Session::forget('staffInfoNameOfPostUpgraded1');
        Session::forget('staffInfoNameOfPostUpgraded2');
        Session::forget('staffInfoNameOfPostUpgraded3');
        Session::forget('staffInfoRemarks');

        //by default centre info session data
        Session::put('staffInfoUniversity', "university");
        Session::put('staffInfoOffice', 'office');
        Session::put('staffInfoTypeOfPost', 'type_of_post');
        Session::put('staffInfoTypeOfEmployment', 'type_of_employment');
        Session::put('staffInfoOriginalPost', 'original_post');
        Session::put('staffInfoGrade', 'grade');
        Session::put('staffInfoFinancialYear', 'financial_year');

        return view('admin.manpower-info.manpower-info-table');
    }

    public function manpowerInfoForm()
    {
        if (Session::get('adminRole') == 'super-admin') {
            return view('admin.manpower-info.manpower-info-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission) {
                if ($permission->permission == 'add') {
                    return view('admin.manpower-info.manpower-info-form');
                }
            }
        }
        return redirect('/database/manpower-info/listing');
    }

    public function manpowerInfoAdd(Request $request)
    {
        //return $request->all();

        $staffInfo = new KosStaffInfo();
        $staffInfo->university = $request->university;
        $staffInfo->office = $request->office;
        $staffInfo->type_of_post = $request->type_of_post;
        $staffInfo->type_of_employment = $request->type_of_employment;
        $staffInfo->original_post = $request->original_post;
        $staffInfo->grade = $request->grade;
        $staffInfo->financial_year = $request->financial_year;
        $staffInfo->no_of_post_in_organogram = $request->no_of_post_in_organogram;
        $staffInfo->no_of_post_approved_by_UGC = $request->no_of_post_approved_by_UGC;
        $staffInfo->date_of_approval_by_UGC = $request->date_of_approval_by_UGC;
        $staffInfo->no_of_post_regular_appointed = $request->no_of_post_regular_appointed;
        $staffInfo->no_of_post_upgraded = $request->no_of_post_upgraded;
        $staffInfo->name_of_post_upgraded_1 = $request->name_of_post_upgraded_1;
        $staffInfo->name_of_post_upgraded_2 = $request->name_of_post_upgraded_2;
        $staffInfo->name_of_post_upgraded_3 = $request->name_of_post_upgraded_3;
        $staffInfo->remarks = $request->remarks;
        $staffInfo->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Manpower Info';
        $activityLog->user = Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/manpower-info/listing')->with('message', 'Manpower Info added successfully');
    }

    public function manpowerInfoEdit($id)
    {
        //return $id;;
        $staffInfoById = KosStaffInfo::find($id);
        return view('admin.manpower-info.manpower-info-edit', [
            'staffInfoById' => $staffInfoById
        ]);
    }

    public function manpowerInfoUpdate(Request $request)
    {
        $staffInfo = KosStaffInfo::find($request->id);

        $staffInfo->university = $request->university;
        $staffInfo->office = $request->office;
        $staffInfo->type_of_post = $request->type_of_post;
        $staffInfo->type_of_employment = $request->type_of_employment;
        $staffInfo->original_post = $request->original_post;
        $staffInfo->grade = $request->grade;
        $staffInfo->financial_year = $request->financial_year;
        $staffInfo->no_of_post_in_organogram = $request->no_of_post_in_organogram;
        $staffInfo->no_of_post_approved_by_UGC = $request->no_of_post_approved_by_UGC;
        $staffInfo->date_of_approval_by_UGC = $request->date_of_approval_by_UGC;
        $staffInfo->no_of_post_regular_appointed = $request->no_of_post_regular_appointed;
        $staffInfo->no_of_post_upgraded = $request->no_of_post_upgraded;
        $staffInfo->name_of_post_upgraded_1 = $request->name_of_post_upgraded_1;
        $staffInfo->name_of_post_upgraded_2 = $request->name_of_post_upgraded_2;
        $staffInfo->name_of_post_upgraded_3 = $request->name_of_post_upgraded_3;
        $staffInfo->remarks = $request->remarks;
        $staffInfo->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Manpower Info';
        $activityLog->user = Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/manpower-info/listing')->with('message', 'Manpower Info added successfully');
    }

    public function manpowerInfoCopy($id)
    {
        if (Session::get('adminRole') == 'super-admin') {
            $staffInfo = KosStaffInfo::find($id);
            $newStaffInfo = new KosStaffInfo();

            $newStaffInfo->university = $staffInfo->university;
            $newStaffInfo->office = $staffInfo->office;
            $newStaffInfo->type_of_post = $staffInfo->type_of_post;
            $newStaffInfo->type_of_employment = $staffInfo->type_of_employment;
            $newStaffInfo->original_post = $staffInfo->original_post;
            $newStaffInfo->grade = $staffInfo->grade;
            $newStaffInfo->financial_year = $staffInfo->financial_year;
            $newStaffInfo->no_of_post_in_organogram = $staffInfo->no_of_post_in_organogram;
            $newStaffInfo->no_of_post_approved_by_UGC = $staffInfo->no_of_post_approved_by_UGC;
            $newStaffInfo->date_of_approval_by_UGC = $staffInfo->date_of_approval_by_UGC;
            $newStaffInfo->no_of_post_regular_appointed = $staffInfo->no_of_post_regular_appointed;
            $newStaffInfo->no_of_post_upgraded = $staffInfo->no_of_post_upgraded;
            $newStaffInfo->name_of_post_upgraded_1 = $staffInfo->name_of_post_upgraded_1;
            $newStaffInfo->name_of_post_upgraded_2 = $staffInfo->name_of_post_upgraded_2;
            $newStaffInfo->name_of_post_upgraded_3 = $staffInfo->name_of_post_upgraded_3;
            $newStaffInfo->remarks = $staffInfo->remarks;
            $newStaffInfo->save();

            $activityLog = new KosActivityLog();
            $activityLog->description = 'Copy Manpower Info';
            $activityLog->user = Session::get('adminEmail');
            $activityLog->save();

            return redirect('/database/manpower-info/listing')->with('message', 'Copy Manpower Info successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id', $userId)->get();
            foreach ($permissions as $permission) {
                if ($permission->permission == 'copy') {
                    $staffInfo = KosStaffInfo::find($id);
                    $newStaffInfo = new KosStaffInfo();

                    $newStaffInfo->university = $staffInfo->university;
                    $newStaffInfo->office = $staffInfo->office;
                    $newStaffInfo->type_of_post = $staffInfo->type_of_post;
                    $newStaffInfo->type_of_employment = $staffInfo->type_of_employment;
                    $newStaffInfo->original_post = $staffInfo->original_post;
                    $newStaffInfo->grade = $staffInfo->grade;
                    $newStaffInfo->financial_year = $staffInfo->financial_year;
                    $newStaffInfo->no_of_post_in_organogram = $staffInfo->no_of_post_in_organogram;
                    $newStaffInfo->no_of_post_approved_by_UGC = $staffInfo->no_of_post_approved_by_UGC;
                    $newStaffInfo->date_of_approval_by_UGC = $staffInfo->date_of_approval_by_UGC;
                    $newStaffInfo->no_of_post_regular_appointed = $staffInfo->no_of_post_regular_appointed;
                    $newStaffInfo->no_of_post_upgraded = $staffInfo->no_of_post_upgraded;
                    $newStaffInfo->name_of_post_upgraded_1 = $staffInfo->name_of_post_upgraded_1;
                    $newStaffInfo->name_of_post_upgraded_2 = $staffInfo->name_of_post_upgraded_2;
                    $newStaffInfo->name_of_post_upgraded_3 = $staffInfo->name_of_post_upgraded_3;
                    $newStaffInfo->remarks = $staffInfo->remarks;
                    $newStaffInfo->save();

                    $activityLog = new KosActivityLog();
                    $activityLog->description = 'Copy Manpower Info';
                    $activityLog->user = Session::get('adminEmail');
                    $activityLog->save();

                    return redirect('/database/manpower-info/listing')->with('message', 'Copy Manpower Info successfully');
                }
            }
        }
        return redirect('/database/manpower-info/listing')->with('alert', 'You do not permit to access it !!!');
    }

    public function manpowerInfoDelete($id)
    {
        if (Session::get('adminRole') == 'super-admin') {
            KosStaffInfo::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Manpower Info';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/manpower-info/listing')->with('message', 'Delete Manpower Info successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id', $userId)->get();
            foreach ($permissions as $permission) {
                if ($permission->permission == 'delete') {
                    KosStaffInfo::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Manpower Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/manpower-info/listing')->with('message', 'Delete Manpower Info successfully');
                }
            }
        }
        return redirect('/database/manpower-info/listing')->with('alert', 'You do not permit to access it !!!');
    }

    public function manpowerInfo(Request $request)
    {
        //return $request->all();

        switch ($request->input('submit')) {
            case 'copy':
                if ($request->has('manpower_info')) {
                    $infoId = $request->input('manpower_info');
                    $count = 0;
                    foreach ($infoId as $info) {
                        $staffInfo = KosStaffInfo::find($info);
                        $newStaffInfo = new KosStaffInfo();

                        $newStaffInfo->university = $staffInfo->university;
                        $newStaffInfo->office = $staffInfo->office;
                        $newStaffInfo->type_of_post = $staffInfo->type_of_post;
                        $newStaffInfo->type_of_employment = $staffInfo->type_of_employment;
                        $newStaffInfo->original_post = $staffInfo->original_post;
                        $newStaffInfo->grade = $staffInfo->grade;
                        $newStaffInfo->financial_year = $staffInfo->financial_year;
                        $newStaffInfo->no_of_post_in_organogram = $staffInfo->no_of_post_in_organogram;
                        $newStaffInfo->no_of_post_approved_by_UGC = $staffInfo->no_of_post_approved_by_UGC;
                        $newStaffInfo->date_of_approval_by_UGC = $staffInfo->date_of_approval_by_UGC;
                        $newStaffInfo->no_of_post_regular_appointed = $staffInfo->no_of_post_regular_appointed;
                        $newStaffInfo->no_of_post_upgraded = $staffInfo->no_of_post_upgraded;
                        $newStaffInfo->name_of_post_upgraded_1 = $staffInfo->name_of_post_upgraded_1;
                        $newStaffInfo->name_of_post_upgraded_2 = $staffInfo->name_of_post_upgraded_2;
                        $newStaffInfo->name_of_post_upgraded_3 = $staffInfo->name_of_post_upgraded_3;
                        $newStaffInfo->remarks = $staffInfo->remarks;
                        $newStaffInfo->save();

                        $activityLog = new KosActivityLog();
                        $activityLog->description = 'Copy Manpower Info';
                        $activityLog->user = Session::get('adminEmail');
                        $activityLog->save();

                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy ' . $count . ' Manpower Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/manpower-info/listing')->with('message', 'Copy ' . $count . ' Manpower Info successfully');
                } else {
                    return redirect('/database/manpower-info/listing')->with('alert', 'Please select item(s) check box from list');
                }
                break;


            case 'delete':
                if ($request->has('manpower_info')) {
                    $deletes = $request->input('manpower_info');
                    $count = 0;
                    foreach ($deletes as $delete) {
                        KosStaffInfo::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete ' . $count . ' Manpower Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/manpower-info/listing')->with('message', 'Delete ' . $count . ' Manpower Info successfully');
                } else {
                    return redirect('/database/manpower-info/listing')->with('alert', 'Please select item(s) check box from list');
                }
                break;


            case 'report':
                //return $request->all();
                if (Session::get('staffInfoUniversity')) {

                    //if checkbox is select
                    if ($request->has('manpower_info')) {
                        $staffInfos = $request->input('manpower_info');

                        //check the column(university) is duplicate or not start here
                        $uniTracker = 0;
                        $count = 0;
                        foreach ($staffInfos as $staffInfo) {
                            $staffInfoById = KosStaffInfo::find($staffInfo);
                            $university = KosUniversity::find($staffInfoById->university);
                            break;
                        }
                        //selected compare versity  name is not nul
                        if (!empty($university)) {
                            foreach ($staffInfos as $staffInfo) {
                                $staffInfoById = KosStaffInfo::find($staffInfo);
                                $universityAgain = KosUniversity::find($staffInfoById->university);
                                if (empty($universityAgain)) {
                                    $uni = 0;
                                    $pdf = PDF::loadView('admin.manpower-info.pdf-manpower-info', [
                                        'uni' => $uni,
                                        'staffInfos' => $staffInfos
                                    ]);
                                    return $pdf->stream('manpower-info.pdf');
                                }

                                if ($university->university == $universityAgain->university) {
                                    $uniTracker++;
                                    $count++;
                                } else {
                                    $count++;
                                }
                            }
                            //check the column(university) is duplicate or not end here


                            if ($uniTracker == $count) {
                                //All selected university name is same

                                $uni = 1;
                                $pdf = PDF::loadView('admin.manpower-info.pdf-manpower-info', [
                                    'uni' => $uni,
                                    'staffInfos' => $staffInfos
                                ]);
                                return $pdf->stream('manpower-info.pdf');

                            } else {
                                //All selected university name is not same

                                $uni = 0;
                                $pdf = PDF::loadView('admin.manpower-info.pdf-manpower-info', [
                                    'uni' => $uni,
                                    'staffInfos' => $staffInfos
                                ]);
                                return $pdf->stream('manpower-info.pdf');

                            }
                        } else {
                            //if compare versity is null
                            $uni = 0;
                            $pdf = PDF::loadView('admin.manpower-info.pdf-manpower-info', [
                                'uni' => $uni,
                                'staffInfos' => $staffInfos
                            ]);
                            return $pdf->stream('manpower-info.pdf');
                        }


                        break;
                    } else {
                        //if checkbox is not select default pdf all in table
                        $staffInfos = $request->input('manpower_info_hide');

                        //check the column(university) is duplicate or not start here
                        $uniTracker = 0;
                        $count = 0;
                        foreach ($staffInfos as $staffInfo) {
                            $staffInfoById = KosStaffInfo::find($staffInfo);
                            $university = KosUniversity::find($staffInfoById->university);
                            break;
                        }
                        foreach ($staffInfos as $staffInfo) {
                            $staffInfoById = KosStaffInfo::find($staffInfo);
                            $universityAgain = KosUniversity::find($staffInfoById->university);
                            if (!empty($universityAgain)) {
                                if ($university->university == $universityAgain->university) {
                                    $uniTracker++;
                                    $count++;
                                } else {
                                    $count++;
                                }
                            }
                        }
                        //check the column(university) is duplicate or not end here


                        if ($uniTracker == $count) {
                            //All selected university name is same

                            $uni = 1;
                            $pdf = PDF::loadView('admin.manpower-info.pdf-manpower-info', [
                                'uni' => $uni,
                                'staffInfos' => $staffInfos
                            ]);
                            return $pdf->stream('staff-info.pdf');

                        } else {
                            //All selected university name is not same

                            $uni = 0;
                            $pdf = PDF::loadView('admin.manpower-info.pdf-manpower-info', [
                                'uni' => $uni,
                                'staffInfos' => $staffInfos
                            ]);
                            return $pdf->stream('manpower-info.pdf');

                        }

                    }
                } else {
                    return redirect('/database/manpower-info/listing')->with('alert', 'Please select university!!!');
                }
                break;
        }
    }


    public function manpowerInfoModalpost(Request $request)
    {
        //return $request->all();
        Session::forget('staffInfoUniversity');
        Session::forget('staffInfoOffice');
        Session::forget('staffInfoTypeOfPost');
        Session::forget('staffInfoTypeOfEmployment');
        Session::forget('staffInfoOriginalPost');
        Session::forget('staffInfoGrade');
        Session::forget('staffInfoFinancialYear');
        Session::forget('staffInfoNoOfPostInOrganogram');
        Session::forget('staffInfoNoOfPostApprovedByUGC');
        Session::forget('staffInfoDateOfApprovalByUGC');
        Session::forget('staffInfoNoOfPostRegularAppointed');
        Session::forget('staffInfoNameOfPostUpgraded1');
        Session::forget('staffInfoNameOfPostUpgraded2');
        Session::forget('staffInfoNameOfPostUpgraded3');
        Session::forget('staffInfoRemarks');

        $columns = $request->input('column');
        foreach ($columns as $column) {
            if ($column == "university") {
                Session::put('staffInfoUniversity', $column);
            }
            if ($column == "office") {
                Session::put('staffInfoOffice', $column);
            }
            if ($column == "type_of_post") {
                Session::put('staffInfoTypeOfPost', $column);
            }
            if ($column == "type_of_employment") {
                Session::put('staffInfoTypeOfEmployment', $column);
            }
            if ($column == "original_post") {
                Session::put('staffInfoOriginalPost', $column);
            }
            if ($column == "grade") {
                Session::put('staffInfoGrade', $column);
            }
            if ($column == "financial_year") {
                Session::put('staffInfoFinancialYear', $column);
            }
            if ($column == "no_of_post_in_organogram") {
                Session::put('staffInfoNoOfPostInOrganogram', $column);
            }
            if ($column == "no_of_post_approved_by_UGC") {
                Session::put('staffInfoNoOfPostApprovedByUGC', $column);
            }
            if ($column == "date_of_approval_by_UGC") {
                Session::put('staffInfoDateOfApprovalByUGC', $column);
            }
            if ($column == "no_of_post_regular_appointed") {
                Session::put('staffInfoNoOfPostRegularAppointed', $column);
            }
            if ($column == "name_of_post_upgraded_1") {
                Session::put('staffInfoNameOfPostUpgraded1', $column);
            }
            if ($column == "name_of_post_upgraded_2") {
                Session::put('staffInfoNameOfPostUpgraded2', $column);
            }
            if ($column == "name_of_post_upgraded_3") {
                Session::put('staffInfoNameOfPostUpgraded3', $column);
            }
            if ($column == "remarks") {
                Session::put('staffInfoRemarks', $column);
            }
        }
        return view('admin.manpower-info.manpower-info-modal');
    }

    public function manpowerInfoModalGet()
    {
        return view('admin.manpower-info.manpower-info-modal');
    }


}
