<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DashboardLogin;
use Illuminate\Support\Facades\Session;

class AdminLogin extends Controller
{
    public function login () {
        return view('admin.login.login');
    }

    public function loginSubmit (Request $request) {
        //return $request->all();
        $res1 = $request->res1;
        $res2 = $request->res2;
        if ($res1!=$res2 ) {
            return redirect('/login')->with('alert','wrong answer!!!try again');
        } else {
            //super admin register start here
//            $countRegister = DashboardLogin::where('email',$request->email)->count();
//            if($countRegister == 0 ) {
//                $login = new DashboardLogin();
//                $login->email = $request->email;
//                $login->password = bcrypt($request->password);
//                $login->role = 'super-admin';
//                $login->save();
//            } else {
//                return redirect('/login')->with('alert','Email has already used !!! try another email');
//            }
            //super admin register end here

            $countLogin = DashboardLogin::where('email',$request->email)->count();
            if ($countLogin == 0 ) {
                return redirect('/login')->with('alert','Invalid email!!!try again');
            } else {
                $admin =  DashboardLogin::where('email',$request->email)->first();
                if ($admin ) {
                    if (password_verify($request->password,$admin->password)) {
                        Session::put('adminEmail',$admin->email);
                        Session::put('adminId',$admin->id);
                        Session::put('adminRole',$admin->role);
                        return redirect('/database/dashboard');
                    } else {
                        return redirect('/login')->with('alert','Wrong password!!!try again');
                    }
                }
            }
        }
    }

    public function logoutAdmin(Request $request) {
        Session::forget('adminEmail');
        Session::forget('adminId');

        //forgot centre info
        Session::forget('centreInfoUniversity');
        Session::forget('centreInfoCentre');
        Session::forget('centreInfoCentreDateOfApproval');
        Session::forget('centreInfoDepartment');
        Session::forget('centreInfoPrograms');
        Session::forget('centreInfoProgramDateOfApproval');
        Session::forget('centreInfoSession');
        Session::forget('centreInfoNoOfSeat');
        Session::forget('centreInfoEnrolledStudents');
        Session::forget('centreInfoMaleStudents');
        Session::forget('centreInfoFemaleStudents');
        Session::forget('centreInfoCurrentStudents');
        Session::forget('centreInfoYear');
        Session::forget('centreInfoDateOfApproval');
        Session::forget('centreInfoRemarks');

        //forgot faculty info
        Session::forget('facultyInfoUniversity');
        Session::forget('facultyInfoFaculty');
        Session::forget('facultyInfoFacultyDateOfApproval');
        Session::forget('facultyInfoDepartment');
        Session::forget('facultyInfoDepartmentDateOfApproval');
        Session::forget('facultyInfoPrograms');
        Session::forget('facultyInfoProgramDateOfApproval');
        Session::forget('facultyInfoSession');
        Session::forget('facultyInfoNoOfSeat');
        Session::forget('facultyInfoEnrolledStudents');
        Session::forget('facultyInfoMaleStudents');
        Session::forget('facultyInfoFemaleStudents');
        Session::forget('facultyInfoCurrentStudents');
        Session::forget('facultyInfoYear');
        Session::forget('facultyInfoRemarks');

        //forgot all faculty info session data
        Session::forget('instituteInfoUniversity');
        Session::forget('instituteInfoInstitute');
        Session::forget('instituteInfoInstituteDateOfApproval');
        Session::forget('instituteInfoDepartment');
        Session::forget('instituteInfoDepartmentDateOfApproval');
        Session::forget('instituteInfoPrograms');
        Session::forget('instituteInfoProgramDateOfApproval');
        Session::forget('instituteInfoSession');
        Session::forget('instituteInfoNoOfSeat');
        Session::forget('instituteInfoEnrolledStudents');
        Session::forget('instituteInfoMaleStudents');
        Session::forget('instituteInfoFemaleStudents');
        Session::forget('instituteInfoCurrentStudents');
        Session::forget('instituteInfoYear');
        Session::forget('instituteInfoDateOfApproval');
        Session::forget('instituteInfoRemarks');

        //forgot all staff info session data
        Session::forget('staffInfoUniversity');
        Session::forget('staffInfoOffice');
        Session::forget('staffInfoTypeOfPost');
        Session::forget('staffInfoTypeOfEmployment');
        Session::forget('staffInfoOriginalPost');
        Session::forget('staffInfoGrade');
        Session::forget('staffInfoFinancialYear');
        Session::forget('staffInfoNoOfPostInOrganogram');
        Session::forget('staffInfoNoOfPostApprovedByUGC');
        Session::forget('staffInfoDateOfApprovalByUGC');
        Session::forget('staffInfoNoOfPostRegularAppointed');
        Session::forget('staffInfoNameOfPostUpgraded1');
        Session::forget('staffInfoNameOfPostUpgraded2');
        Session::forget('staffInfoNameOfPostUpgraded3');
        Session::forget('staffInfoRemarks');

        //forgot all vehicle info session data
        Session::forget('vehicleInfoUniversity');
        Session::forget('vehicleInfoOffice');
        Session::forget('vehicleInfoVehicle');
        Session::forget('vehicleInfoNoOfVehiclesInOrganogram');
        Session::forget('vehicleInfoYearOfOrganogramApproval');
        Session::forget('vehicleInfoFinancialYear');
        Session::forget('vehicleInfoApprovedNumberOfVehiclesByUgc');
        Session::forget('vehicleInfoNoOfVehiclesPurchasedByUniversity');
        Session::forget('vehicleInfoYearOfPurchaseVehiclesByUniversity');
        Session::forget('vehicleInfoProposedBudget');
        Session::forget('vehicleInfoApprovedBudget');
        Session::forget('vehicleInfoRemarks');


        return redirect('/login')->with('message','Logout Sussessfully');

    }

    public function viewProfile() {
        $id = Session::get('adminId');
        $admin = DashboardLogin::find($id);
        return view('admin.profile.pfofile-show',[
            'admin' => $admin
        ]);
    }

    public function passwordChange(Request $request) {
      // return $request->all();
       $admin = DashboardLogin::find($request->id);
       if ($admin) {
           if (password_verify($request->old_pass,$admin->password)) {
               if ($request->new_pass == $request->retype_pass ) {
                  //$admin->password = $request->new_pass;
                   $admin->password = bcrypt($request->new_pass);
                   $admin->save();
                   return redirect('/database/view-profile')->with('message','Update profile successfully');
               } else {
                   return redirect('/database/view-profile')->with('alert','Retype wrong password!!!Try again.');
               }
           }else {
               return redirect('/database/view-profile')->with('alert','Wrong password!!!Try again.');
           }
       }

    }
}
