<?php

namespace App\Http\Controllers;

use App\KosActivityLog;
use App\KosFaculty;
use App\KosPermission;
use App\User;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Contracts\DataTable;
use Illuminate\Support\Facades\Schema;
use PDF;
use Datatables;
class FacultyController extends Controller
{
    public function wrongUrl() {
        return redirect('/database/faculty/listing')->with('alert','Your action url is wrong !!');
    }

    public function facultyListing() {
        $faculties = DB::table('kos_faculties')->orderBy('id','desc')->paginate(10);
        return view('admin.faculty.faculty-table',[
            'faculties' => $faculties
        ]);
    }

    public function facultyForm () {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.faculty.faculty-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.faculty.faculty-form');
                }
            }
        }
        return redirect('/database/faculty/listing');
    }

    public function facultyAdd (Request $request) {
        $faculty = new KosFaculty();
        $faculty->faculty = $request->faculty;
        $faculty->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Faculty';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/faculty/listing')->with('message','Faculty added successfully');
    }

    public function facultyEdit ($id ) {
        //return $id;
        $facultyById = KosFaculty::find($id);
        return view('admin.faculty.faculty-edit',[
            'facultyById' => $facultyById
        ]);
    }

    public function facultyUpdate(Request $request ) {
        $faculty =  KosFaculty::find($request->id);
        $faculty->faculty = $request->faculty;
        $faculty->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Faculty';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/faculty/listing')->with('message','Faculty update successfully');
    }

    public function facultyCopy ($id) {
        //return $id;
        if (Session::get('adminRole') == 'super-admin' ) {
            $faculty    = KosFaculty::find($id);
            $newFaculty = new KosFaculty();
            $newFaculty->faculty = $faculty->faculty;
            $newFaculty->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Faculty';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/faculty/listing')->with('message','Copy Faculty successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'copy' ) {
                    $faculty    = KosFaculty::find($id);
                    $newFaculty = new KosFaculty();
                    $newFaculty->faculty = $faculty->faculty;
                    $newFaculty->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Faculty';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/faculty/listing')->with('message','Copy Faculty successfully');
                }
            }
            return redirect('/database/faculty/listing')->with('alert','You do not permit to access it !!!');
        }
    }

    public function facultyDelete($id ) {
        if (Session::get('adminRole') == 'super-admin' ) {
            KosFaculty::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Faculty';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/faculty/listing')->with('message','Delete Faculty successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete' ) {
                    KosFaculty::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Faculty';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/faculty/listing')->with('message','Delete Faculty successfully');
                }
            }
            return redirect('/database/faculty/listing')->with('alert','You do not permit to access it !!!');
        }
    }

    public function faculty(Request $request) {
//    return $request->all();
        if ($request->has('faculty')) {
            switch ($request->input('submit')) {
                case 'copy':
                    $facultyId  = $request->input('faculty');
                    //$faculties = $request->all();
                    $count = 0;
                    foreach ($facultyId as $id ) {
                        $faculty    = KosFaculty::find($id);
                        $newFaculty = new KosFaculty();
                        $newFaculty->faculty = $faculty->faculty;
                        $newFaculty->save();
                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy '.$count.' Faculty';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/faculty/listing')->with('message','Copy '.$count.' Faculty successfully');
                    break;

                case 'edit':
                    return 'edit';
                    break;

                case 'delete':
                    $deletes = $request->input('faculty');
                    $count = 0;
                    foreach ($deletes as $delete ) {
                        KosFaculty::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete '.$count.' Faculty';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/faculty/listing')->with('message','Delete '.$count.' Faculty successfully');
                    break;

                case 'report':
                    $facultyById  = $request->input('faculty');
                    return view('admin.faculty.faculty_report',[
                        'facultyById' => $facultyById
                    ]);
                    break;
            }
        } else {
            return redirect('/database/faculty/listing')->with('alert','Please select item(s) check box from list');
        }
    }
}
