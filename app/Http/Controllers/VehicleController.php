<?php

namespace App\Http\Controllers;
use App\KosActivityLog;
use App\KosCentre;
use App\User;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Contracts\DataTable;
use Illuminate\Support\Facades\Schema;
use PDF;
use Datatables;
use App\KosVehicle;
use App\KosPermission;
use App\KosPermissionModule;

class VehicleController extends Controller
{

    public function wrongUrl() {
        return redirect('/database/vehicle/listing')->with('alert','Your action url is wrong !!!');
    }

    public function vehicleListing() {
        $vehicles = DB::table('kos_vehicles')->orderBy('id','desc')->get();
        return view('admin.vehicle.vehicle-table',[
            'vehicles' => $vehicles
        ]);
    }

    public function vehicleForm () {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.vehicle.vehicle-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.vehicle.vehicle-form');
                }
            }
        }
        return redirect('/database/vehicle/listing');
    }

    public function vehicleAdd (Request $request) {
        $vehicle = new KosVehicle();
        $vehicle->vehicle = $request->vehicle;
        $vehicle->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Vehicle';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/vehicle/listing')->with('message','Centre added successfully');
    }

    public function vehicleEdit ($id ) {
        //return $id;
        $vehicleById = KosVehicle::find($id);
        return view('admin.vehicle.vehicle-edit',[
            'vehicleById' => $vehicleById
        ]);
    }

    public function vehicleUpdate(Request $request ) {
        $vehicle =  KosVehicle::find($request->id);
        $vehicle->vehicle = $request->vehicle;
        $vehicle->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Vehicle';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/vehicle/listing')->with('message','Vehicle update successfully');
    }

    public function vehicleCopy ($id) {
        //return $id;
        if (Session::get('adminRole')=='super-admin') {
            $vehicle    = KosVehicle::find($id);
            $newVehicle = new KosVehicle();
            $newVehicle->vehicle = $vehicle->vehicle;
            $newVehicle->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Vehicle';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/vehicle/listing')->with('message','Copy Vehicle successfully');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'copy') {
                    $vehicle    = KosVehicle::find($id);
                    $newVehicle = new KosVehicle();
                    $newVehicle->vehicle = $vehicle->vehicle;
                    $newVehicle->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Vehicle';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/vehicle/listing')->with('message','Copy Vehicle successfully');
                }
            }
        }
        return redirect('/database/vehicle/listing');
   }

    public function vehicleDelete($id ) {
        if (Session::get('adminRole')=='super-admin')  {
            KosVehicle::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Vehicle';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/vehicle/listing')->with('message','Delete Vehicle successfully');
        }else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete') {
                    KosVehicle::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Vehicle';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/vehicle/listing')->with('message','Delete Vehicle successfully');
                }
            }
        }
        return redirect('/database/vehicle/listing');
    }

    public function vehicle(Request $request) {
//    return $request->all();
        if ($request->has('vehicle')) {
            switch ($request->input('submit')) {
                case 'copy':
                    $vehicleId  = $request->input('vehicle');
                    //$centres = $request->all();
                    $count = 0;
                    foreach ($vehicleId as $id ) {
                        $vehicle    = KosVehicle::find($id);
                        $newVehicle = new KosVehicle();
                        $newVehicle->vehicle = $vehicle->vehicle;
                        $newVehicle->save();
                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy '.$count.' Vehicle';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/vehicle/listing')->with('message','Copy '.$count.' Vehicle successfully');
                    break;

                case 'edit':
                    return 'edit';
                    break;

                case 'delete':
                    $deletes = $request->input('vehicle');
                    $count = 0;
                    foreach ($deletes as $delete ) {
                        KosVehicle::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete '.$count.' Vehicle';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/vehicle/listing')->with('message','Delete '.$count.' Vehicle successfully');
                    break;

                case 'report':
                    $centreById  = $request->input('centre');
                    return view('admin.centre.centre_report',[
                        'centreById' => $centreById
                    ]);
                    break;
            }
        } else {
            return redirect('/database/centre/listing')->with('alert','Please select item(s) check box from list');
        }

    }
}
