<?php

namespace App\Http\Controllers;

use App\KosActivityLog;
use App\KosCentre;
use App\KosPermission;
use App\User;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Contracts\DataTable;
use Illuminate\Support\Facades\Schema;
use PDF;
use Datatables;

class CentreController extends Controller
{

    public function wrongUrl() {
        return redirect('/database/centre/listing')->with('alert','Your action url is wrong !!');
    }

    public function centreListing() {
        $centres = DB::table('kos_centres')->orderBy('id','desc')->paginate(10);
        $names = Schema::getColumnListing('kos_centres');
        return view('admin.centre.centre-table',[
            'centres' => $centres,
            'names'   => $names
        ]);
    }

    public function centreForm () {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.centre.centre-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.centre.centre-form');
                }
            }
        }
        return redirect('/database/centre/listing');
    }

    public function centreAdd (Request $request) {
        $centre = new KosCentre();
        $centre->centre = $request->centre;
        $centre->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Centre';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/centre/listing')->with('message','Centre added successfully');
    }

    public function centreEdit ($id ) {
        //return $id;
       $centreById = KosCentre::find($id);
       return view('admin.centre.centre-edit',[
            'centreById' => $centreById
       ]);
    }

    public function centreUpdate(Request $request ) {
    $centre =  KosCentre::find($request->id);
    $centre->centre = $request->centre;
    $centre->save();

    $activityLog = new KosActivityLog();
    $activityLog->description = 'Update Centre';
    $activityLog->user        =  Session::get('adminEmail');
    $activityLog->save();

    return redirect('/database/centre/listing')->with('message','Centre update successfully');
}

    public function centreCopy ($id) {
        //return $id;
        if (Session::get('adminRole') == 'super-admin' ) {
            $centre    = KosCentre::find($id);
            $newCentre = new KosCentre();
            $newCentre->centre = $centre->centre;
            $newCentre->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Centre';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/centre/listing')->with('message','Copy Centre successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'copy' ) {
                    $centre    = KosCentre::find($id);
                    $newCentre = new KosCentre();
                    $newCentre->centre = $centre->centre;
                    $newCentre->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Centre';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/centre/listing')->with('message','Copy Centre successfully');
                }
            }
            return redirect('/database/centre/listing')->with('alert','You do not permit to access it !!!');
        }

    }

    public function centreDelete($id ) {
        if (Session::get('adminRole') == 'super-admin' ) {
            KosCentre::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Centre';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/centre/listing')->with('message','Delete Centre successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete' ) {
                    KosCentre::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Centre';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/centre/listing')->with('message','Delete Centre successfully');
                }
            }
            return redirect('/database/centre/listing')->with('alert','You do not permit to access it !!!');
        }

    }

    public function centre(Request $request) {
//    return $request->all();
    if ($request->has('centre')) {
        switch ($request->input('submit')) {
            case 'copy':
                $centreId  = $request->input('centre');
                //$centres = $request->all();
                $count = 0;
                foreach ($centreId as $id ) {
                    $centre    = KosCentre::find($id);
                    $newCentre = new KosCentre();
                    $newCentre->centre = $centre->centre;
                    $newCentre->save();
                    $count++;
                }
                $activity = new KosActivityLog();
                $activity->description = 'Copy '.$count.' Centre';
                $activity->user = Session::get('adminEmail');
                $activity->save();
                return redirect('/database/centre/listing')->with('message','Copy '.$count.' Centre successfully');
                break;

            case 'edit':
                return 'edit';
                break;

            case 'delete':
                $deletes = $request->input('centre');
                $count = 0;
                foreach ($deletes as $delete ) {
                    KosCentre::find($delete)->delete();
                    $count++;
                }

                $activity = new KosActivityLog();
                $activity->description = 'Delete '.$count.' Centre';
                $activity->user = Session::get('adminEmail');
                $activity->save();

                return redirect('/database/centre/listing')->with('message','Delete '.$count.' Centre successfully');
                break;

            case 'report':
                $centreById  = $request->input('centre');
                return view('admin.centre.centre_report',[
                    'centreById' => $centreById
                ]);
                break;
        }
    } else {
        return redirect('/database/centre/listing')->with('alert','Please select item(s) check box from list');
    }

}

    public function centreReport() {
        return view('admin.centre.centre_report');
    }


    public function centrePdf(Request $request){
        $columns    = $request->input('column');
        $checkboxes = $request->input('checkbox');

        //check the column is duplicate or not start here
        $tracker = 0 ;
        foreach ($checkboxes as $checkbox ) {
           $centreById = KosCentre::find($checkbox);
           $centreName = $centreById->centre;
           break;
        }
        foreach($checkboxes as $checkbox ){
            $centreQuerry = KosCentre::find($checkbox);
            if ($centreName == $centreQuerry->centre ) {
                $tracker++;
            }else {
                $tracker =0 ;
            }
        }
        //check the column is duplicate or not end here

        if ($tracker== 0 ) {
           $pdf = PDF::loadView('admin.centre.pdf-centre',[
                'columns'    => $columns,
                'checkboxes' => $checkboxes
            ]);
                return $pdf->stream('centre-info.pdf');
            ;
        } else {
            $pdf = PDF::loadView('admin.centre.duplicate-pdf-centre',[
                'columns'    => $columns,
                'checkboxes' => $checkboxes
            ]);
            return $pdf->stream('centre-info.pdf');
        }


    }




















    public function centre1DataTable() {
       //return Datatable::of(KosCentre::query())->make(true);
       return datatables()->eloquent(KosCentre::query())->toJson();
//        return view('admin.centre.centre_table1');
    }

    public function test() {
        $myObject = KosCentre::all();
//    return datatables()
//        ->eloquent(App\KosCentre::query())
//        ->toJson();
        return json_encode($myObject);
    }

    public function d() {

        $columns = DB::getSchemaBuilder()->getColumnListing('kos_centres');
        foreach ($columns as $column => $name) {
            echo  $column ."<br>" ;
        }


    }


}

