<?php

namespace App\Http\Controllers;

use App\KosActivityLog;
use App\KosFacultyInfo;
use App\KosPermission;
use App\User;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Contracts\DataTable;
use Illuminate\Support\Facades\Schema;
use PDF;
use Datatables;

use App\KosUniversity;

class FacultyInfoController extends Controller
{
    public function wrongUrl() {
        return redirect('/database/faculty-info/listing')->with('alert','Your action url is wrong !!');
    }

    public function facultyInfoListing() {

        //forgot all faculty info session data
        Session::forget('facultyInfoUniversity');
        Session::forget('facultyInfoFaculty');
        Session::forget('facultyInfoFacultyDateOfApproval');
        Session::forget('facultyInfoDepartment');
        Session::forget('facultyInfoDepartmentDateOfApproval');
        Session::forget('facultyInfoPrograms');
        Session::forget('facultyInfoProgramDateOfApproval');
        Session::forget('facultyInfoSession');
        Session::forget('facultyInfoNoOfSeat');
        Session::forget('facultyInfoEnrolledStudents');
        Session::forget('facultyInfoMaleStudents');
        Session::forget('facultyInfoFemaleStudents');
        Session::forget('facultyInfoCurrentStudents');
        Session::forget('facultyInfoYear');
        Session::forget('facultyInfoRemarks');

        //by default put faculty info in session

            Session::put('facultyInfoUniversity',"university");
            Session::put('facultyInfoFaculty','faculty');
            Session::put('facultyInfoDepartment','department');
            Session::put('facultyInfoPrograms','programs');
            Session::put('facultyInfoSession','session');
            Session::put('facultyInfoNoOfSeat','no_of_seat');
            Session::put('facultyInfoYear','year');

       // $facultyInfos = DB::table('kos_faculty_infos')->orderBy('id','desc')->paginate(10);
        return view('admin.faculty-info.faculty-info-table');
    }

    public function facultyInfoForm () {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.faculty-info.faculty-info-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.faculty-info.faculty-info-form');
                }
            }
        }
        return redirect('/database/faculty-info/listing');
    }

    public function facultyInfoAdd (Request $request) {
        //return $request->all();
        $facultyInfo = new KosFacultyInfo();
        $facultyInfo->university                  = $request->university;
        $facultyInfo->faculty                     = $request->faculty;
        $facultyInfo->faculty_date_of_approval    = $request->faculty_date_of_approval;
        $facultyInfo->department                  = $request->department;
        $facultyInfo->department_date_of_approval = $request->department_date_of_approval;
        $facultyInfo->programs                    = $request->programs;
        $facultyInfo->program_date_of_approval    = $request->program_date_of_approval;
        $facultyInfo->session                     = $request->sessions;
        $facultyInfo->no_of_seat                  = $request->no_of_seat;
        $facultyInfo->enrolled_students           = $request->enrolled_students;
        $facultyInfo->male_students               = $request->male_students;
        $facultyInfo->female_students             = $request->female_students;
        $facultyInfo->current_students            = $request->current_students;
        $facultyInfo->year                        = $request->year;
        $facultyInfo->remarks                     = $request->remarks;
        $facultyInfo->save();


        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Faculty Info';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/faculty-info/listing')->with('message','Faculty Info added successfully');
    }

    public function facultyInfoEdit ($id ) {
        //return $id;
        $facultyInfoById = KosFacultyInfo::find($id);
        return view('admin.faculty-info.faculty-info-edit',[
            'facultyInfoById' => $facultyInfoById
        ]);
    }

    public function facultyInfoUpdate(Request $request ) {
        //return $request->all();

        $facultyInfo =  KosFacultyInfo::find($request->id);
        $facultyInfo->university                  = $request->university;
        $facultyInfo->faculty                     = $request->faculty;
        $facultyInfo->faculty_date_of_approval    = $request->faculty_date_of_approval;
        $facultyInfo->department                  = $request->department;
        $facultyInfo->department_date_of_approval = $request->department_date_of_approval;
        $facultyInfo->programs                    = $request->programs;
        $facultyInfo->program_date_of_approval    = $request->program_date_of_approval;
        $facultyInfo->session                     = $request->sessions;
        $facultyInfo->no_of_seat                  = $request->no_of_seat;
        $facultyInfo->enrolled_students           = $request->enrolled_students;
        $facultyInfo->male_students               = $request->male_students;
        $facultyInfo->female_students             = $request->female_students;
        $facultyInfo->current_students            = $request->current_students;
        $facultyInfo->year                        = $request->year;
        $facultyInfo->remarks                     = $request->remarks;
        $facultyInfo->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Faculty Info';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/faculty-info/listing')->with('message','Faculty Info update successfully');
    }

    public function facultyInfoCopy ($id) {
        //return $id;
        if (Session::get('adminRole') == 'super-admin' ) {
            $facultyInfo    = KosFacultyInfo::find($id);
            $newFacultyInfo = new KosFacultyInfo();
            $newFacultyInfo->university                  = $facultyInfo->university;
            $newFacultyInfo->faculty                     = $facultyInfo->faculty;
            $newFacultyInfo->faculty_date_of_approval    = $facultyInfo->faculty_date_of_approval;
            $newFacultyInfo->department                  = $facultyInfo->department;
            $newFacultyInfo->department_date_of_approval = $facultyInfo->department_date_of_approval;
            $newFacultyInfo->programs                    = $facultyInfo->programs;
            $newFacultyInfo->program_date_of_approval    = $facultyInfo->program_date_of_approval;
            $newFacultyInfo->session                     = $facultyInfo->sessions;
            $newFacultyInfo->no_of_seat                  = $facultyInfo->no_of_seat;
            $newFacultyInfo->enrolled_students           = $facultyInfo->enrolled_students;
            $newFacultyInfo->male_students               = $facultyInfo->male_students;
            $newFacultyInfo->female_students             = $facultyInfo->female_students;
            $newFacultyInfo->current_students            = $facultyInfo->current_students;
            $newFacultyInfo->year                        = $facultyInfo->year;
            $newFacultyInfo->remarks                     = $facultyInfo->remarks;
            $newFacultyInfo->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Faculty Info';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/faculty-info/listing')->with('message','Copy Faculty Info successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'copy' ) {
                    $facultyInfo    = KosFacultyInfo::find($id);
                    $newFacultyInfo = new KosFacultyInfo();
                    $newFacultyInfo->university                  = $facultyInfo->university;
                    $newFacultyInfo->faculty                     = $facultyInfo->faculty;
                    $newFacultyInfo->faculty_date_of_approval    = $facultyInfo->faculty_date_of_approval;
                    $newFacultyInfo->department                  = $facultyInfo->department;
                    $newFacultyInfo->department_date_of_approval = $facultyInfo->department_date_of_approval;
                    $newFacultyInfo->programs                    = $facultyInfo->programs;
                    $newFacultyInfo->program_date_of_approval    = $facultyInfo->program_date_of_approval;
                    $newFacultyInfo->session                     = $facultyInfo->sessions;
                    $newFacultyInfo->no_of_seat                  = $facultyInfo->no_of_seat;
                    $newFacultyInfo->enrolled_students           = $facultyInfo->enrolled_students;
                    $newFacultyInfo->male_students               = $facultyInfo->male_students;
                    $newFacultyInfo->female_students             = $facultyInfo->female_students;
                    $newFacultyInfo->current_students            = $facultyInfo->current_students;
                    $newFacultyInfo->year                        = $facultyInfo->year;
                    $newFacultyInfo->remarks                     = $facultyInfo->remarks;
                    $newFacultyInfo->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Faculty Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/faculty-info/listing')->with('message','Copy Faculty Info successfully');
                }
            }
            return redirect('/database/faculty-info/listing')->with('alert','You do not permit to access it !!!');
        }
    }

    public function facultyInfoDelete($id ) {
        if (Session::get('adminRole') == 'super-admin' ) {
            KosFacultyInfo::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Faculty';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/faculty-info/listing')->with('message','Delete Faculty Info successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete' ) {
                    KosFacultyInfo::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Faculty Info';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/faculty-info/listing')->with('message','Delete Faculty Info successfully');
                }
            }
            return redirect('/database/faculty-info/listing')->with('alert','You do not permit to access it !!!');
        }
    }

    public function facultyInfo(Request $request) {
//    return $request->all();
            switch ($request->input('submit')) {
                case 'copy':
                    if ($request->has('faculty_info')) {
                        $facultyInfoId = $request->input('faculty_info');
                        //$faculties = $request->all();
                        $count = 0;
                        foreach ($facultyInfoId as $id) {
                            $facultyInfo = KosFacultyInfo::find($id);
                            $newFacultyInfo = new KosFacultyInfo();
                            $newFacultyInfo->university = $facultyInfo->university;
                            $newFacultyInfo->faculty = $facultyInfo->faculty;
                            $newFacultyInfo->faculty_date_of_approval = $facultyInfo->faculty_date_of_approval;
                            $newFacultyInfo->department = $facultyInfo->department;
                            $newFacultyInfo->department_date_of_approval = $facultyInfo->department_date_of_approval;
                            $newFacultyInfo->programs = $facultyInfo->programs;
                            $newFacultyInfo->program_date_of_approval = $facultyInfo->program_date_of_approval;
                            $newFacultyInfo->session = $facultyInfo->session;
                            $newFacultyInfo->no_of_seat = $facultyInfo->no_of_seat;
                            $newFacultyInfo->enrolled_students = $facultyInfo->enrolled_students;
                            $newFacultyInfo->male_students = $facultyInfo->male_students;
                            $newFacultyInfo->female_students = $facultyInfo->female_students;
                            $newFacultyInfo->current_students = $facultyInfo->current_students;
                            $newFacultyInfo->year = $facultyInfo->year;
                            $newFacultyInfo->date_of_approval = $facultyInfo->date_of_approval;
                            $newFacultyInfo->remarks = $facultyInfo->remarks;
                            $newFacultyInfo->save();
                            $count++;
                        }
                        $activity = new KosActivityLog();
                        $activity->description = 'Copy ' . $count . ' Faculty Info';
                        $activity->user = Session::get('adminEmail');
                        $activity->save();
                        return redirect('/database/faculty-info/listing')->with('message', 'Copy ' . $count . ' Faculty Info successfully');
                    } else {
                        return redirect('/database/faculty-info/listing')->with('alert', 'Please select item(s) check box from list');
                    }
                    break;

                case 'edit':
                    return 'edit';
                    break;

                case 'delete':
                    if ($request->has('faculty_info')) {
                        $deletes = $request->input('faculty_info');
                        $count = 0;
                        foreach ($deletes as $delete) {
                            KosFacultyInfo::find($delete)->delete();
                            $count++;
                        }

                        $activity = new KosActivityLog();
                        $activity->description = 'Delete ' . $count . ' Faculty Info';
                        $activity->user = Session::get('adminEmail');
                        $activity->save();

                        return redirect('/database/faculty-info/listing')->with('message', 'Delete ' . $count . ' Faculty Info successfully');
                    } else {
                        return redirect('/database/faculty-info/listing')->with('alert', 'Please select item(s) check box from list');
                    }
                    break;

                case 'report':

                    if (Session::get('facultyInfoUniversity')) {

                        //if checkbox is select
                        if ($request->has('faculty_info')) {
                            $facultyInfos = $request->input('faculty_info');

                            //check the column(university) is duplicate or not start here
                            $uniTracker = 0;
                            $count = 0;
                            foreach ($facultyInfos as $facultyInfo) {
                                $facultyInfoById = KosFacultyInfo::find($facultyInfo);
                                $university = KosUniversity::find($facultyInfoById->university);
                                break;
                            }
                            foreach ($facultyInfos as $facultyInfo) {
                                $facultyInfoById = KosFacultyInfo::find($facultyInfo);
                                $universityAgain = KosUniversity::find($facultyInfoById->university);
                                if ($university->university == $universityAgain->university) {
                                    $uniTracker++;
                                    $count++;
                                } else {
                                    $count++;
                                }
                            }
                            //check the column(university) is duplicate or not end here


                            if ($uniTracker == $count) {
                                //All selected university name is same

                                $uni = 1;
                                $pdf = PDF::loadView('admin.faculty-info.pdf-faculty-info', [
                                    'uni' => $uni,
                                    'facultyInfos' => $facultyInfos
                                ]);
                                return $pdf->stream('staff-info.pdf');

                            } else {
                                //All selected university name is not same

                                $uni = 0;
                                $pdf = PDF::loadView('admin.faculty-info.pdf-faculty-info', [
                                    'uni' => $uni,
                                    'facultyInfos' => $facultyInfos
                                ]);
                                return $pdf->stream('staff-info.pdf');

                            }
                            break;

                        } else {
                            //if checkbox is not select default pdf all in table
                            $facultyInfos = $request->input('faculty_info_hide');

                            //check the column(university) is duplicate or not start here
                            $uniTracker = 0;
                            $count = 0;
                            foreach ($facultyInfos as $facultyInfo) {
                                $facultyInfoById = KosFacultyInfo::find($facultyInfo);
                                $university = KosUniversity::find($facultyInfoById->university);
                                break;
                            }
                            if (!empty($university)) {
                                foreach ($facultyInfos as $facultyInfo) {
                                    $facultyInfoById = KosFacultyInfo::find($facultyInfo);
                                    $universityAgain = KosUniversity::find($facultyInfoById->university);
                                    if ($university->university == $universityAgain->university) {
                                        $uniTracker++;
                                        $count++;
                                    } else {
                                        $count++;
                                    }
                                }
                                //check the column(university) is duplicate or not end here

                            } else {
                                $count++;
                            }


                        }


                        if ($uniTracker == $count) {
                            //All selected university name is same

                            $uni = 1;
                            $pdf = PDF::loadView('admin.faculty-info.pdf-faculty-info', [
                                'uni' => $uni,
                                'facultyInfos' => $facultyInfos
                            ]);
                            return $pdf->stream('staff-info.pdf');

                        } else {
                            //All selected university name is not same

                            $uni = 0;
                            $pdf = PDF::loadView('admin.faculty-info.pdf-faculty-info', [
                                'uni' => $uni,
                                'facultyInfos' => $facultyInfos
                            ]);
                            return $pdf->stream('staff-info.pdf');

                        }

                    } else {
                        return redirect('/database/faculty-info/modal-listing')->with('alert','Select university!!!');
                    }
                        break;
                    }
            }



    public function facultyInfoModalPost(Request $request) {

        Session::forget('facultyInfoUniversity');
        Session::forget('facultyInfoFaculty');
        Session::forget('facultyInfoFacultyDateOfApproval');
        Session::forget('facultyInfoDepartment');
        Session::forget('facultyInfoDepartmentDateOfApproval');
        Session::forget('facultyInfoPrograms');
        Session::forget('facultyInfoProgramDateOfApproval');
        Session::forget('facultyInfoSession');
        Session::forget('facultyInfoNoOfSeat');
        Session::forget('facultyInfoEnrolledStudents');
        Session::forget('facultyInfoMaleStudents');
        Session::forget('facultyInfoFemaleStudents');
        Session::forget('facultyInfoCurrentStudents');
        Session::forget('facultyInfoYear');
        Session::forget('facultyInfoRemarks');

        $columns = $request->input('column');
        foreach ($columns as $column ) {
            if ($column == "university") {
                Session::put('facultyInfoUniversity',$column);
            }
            if ($column == "faculty") {
                Session::put('facultyInfoFaculty',$column);
            }
            if ($column == "faculty_date_of_approval") {
                Session::put('facultyInfoFacultyDateOfApproval',$column);
            }
            if ($column == "department") {
                Session::put('facultyInfoDepartment',$column);
            }
            if ($column == "department_date_of_approval") {
                Session::put('facultyInfoDepartmentDateOfApproval',$column);
            }
            if ($column == "programs") {
                Session::put('facultyInfoPrograms',$column);
            }
            if ($column == "program_date_of_approval") {
                Session::put('facultyInfoProgramDateOfApproval',$column);
            }
            if ($column == "session") {
                Session::put('facultyInfoSession',$column);
            }
            if ($column == "no_of_seat") {
                Session::put('facultyInfoNoOfSeat',$column);
            }
            if ($column == "enrolled_students") {
                Session::put('facultyInfoEnrolledStudents',$column);
            }
            if ($column == "male_students") {
                Session::put('facultyInfoMaleStudents',$column);
            }
            if ($column == "female_students") {
                Session::put('facultyInfoFemaleStudents',$column);
            }
            if ($column == "current_students") {
                Session::put('facultyInfoCurrentStudents',$column);
            }
            if ($column == "year") {
                Session::put('facultyInfoYear',$column);
            }if ($column == "remarks") {
                Session::put('facultyInfoRemarks',$column);
            }
        }

        return view('admin.faculty-info.faculty-info-modal');
    }

    public function facultyInfoModalGet() {
        return view('admin.faculty-info.faculty-info-modal');
    }
}
