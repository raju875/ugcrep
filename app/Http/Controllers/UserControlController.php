<?php

namespace App\Http\Controllers;

use App\DashboardLogin;
use App\KosPermission;
use App\KosPermissionModule;
use App\KosUniversityPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\KosActivityLog;

class UserControlController extends Controller
{
    public function controlForm(Request $request)
    {
        $tables = DB::select('SHOW TABLES');
//        foreach ($tables as $table) {
//            foreach ($table as  $value)
//                echo $value;
//           echo '<br>';
//        }
        return view('admin.control-user.user-control-form');
    }

    public function submitControl(Request $request)
    {
        //return $request->all();
        if ($request->password == $request->confirm_password) {
            $dashboardLogin = new DashboardLogin();
            $dashboardLogin->name = $request->name;
            $dashboardLogin->designation = $request->designation;
            $dashboardLogin->email = $request->email;
            $dashboardLogin->password = bcrypt($request->password);
            $dashboardLogin->role = 'user';
            $dashboardLogin->save();
            $loginId = $dashboardLogin->id;

            $universityIds = $request->input('university_id');
            foreach ($universityIds as $universityId) {
                $kosUniversity = new KosUniversityPermission();
                $kosUniversity->role_id = $loginId;
                $kosUniversity->super_admin_id = Session::get('adminId');
                $kosUniversity->university_id = $universityId;
                $kosUniversity->save();
            }

            $permissionModules = $request->input('permission_module');
            foreach ($permissionModules as $permissionModule) {
                $kosPermissionModule = new KosPermissionModule();
                $kosPermissionModule->role_id = $loginId;
                $kosPermissionModule->super_admin_id = Session::get('adminId');
                $kosPermissionModule->table_name = $permissionModule;
                $kosPermissionModule->save();

            }

            $permissions = $request->input('permission');
            foreach ($permissions as $permission) {
                $kosPermission = new KosPermission();
                $kosPermission->role_id = $loginId;
                $kosPermission->super_admin_id = Session::get('adminId');
                $kosPermission->permission = $permission;
                $kosPermission->save();
            }

            $activity = new KosActivityLog();
            $activity->description = 'Add new user';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/control/user-control-table')->with('message', 'New user added successfully');
        } else {
            return redirect('/database/control/user-control-form')->with('alert', 'Retype wrong password!!!Try again.');
        }

    }

    public function userControlTable()
    {
        return view('admin.control-user.user-control-table');
    }

    public function viewUser($id)
    {
        $user = DashboardLogin::find($id);
        return view('admin.control-user.view-user', [
            'user' => $user
        ]);
    }

    public function editUser($id)
    {
        $user = DashboardLogin::find($id);
        return view('admin.control-user.edit-user', [
            'user' => $user
        ]);
    }

    public function update(Request $request){
        //return $request->all();
        if ( !empty($request->new_password)) {
            if ($request->new_password == $request->confirm_password) {
                $dashboardLogin = DashboardLogin::find($request->id);
                $dashboardLogin->name = $request->name;
                $dashboardLogin->designation = $request->designation;
                $dashboardLogin->email = $request->email;
                $dashboardLogin->password = bcrypt($request->new_password);
                $dashboardLogin->role = 'user';
                $dashboardLogin->save();
                $loginId = $dashboardLogin->id;

                //delete previous university
               $previousUniversities = KosUniversityPermission::where('role_id',$request->id)->get();
               foreach ($previousUniversities as $university ) {
                   KosUniversityPermission::find($university->id)->delete();
               }

               //add new universities
               $universityIds = $request->input('university_id');
                foreach ($universityIds as $universityId) {
                    $kosUniversity = new KosUniversityPermission();
                    $kosUniversity->role_id = $loginId;
                    $kosUniversity->super_admin_id = Session::get('adminId');
                    $kosUniversity->university_id = $universityId;
                    $kosUniversity->save();
                }

                //delete previous permission module
                $previousModules = KosPermissionModule::where('role_id',$request->id)->get();
                foreach ($previousModules as $module ) {
                    KosPermissionModule::find($module->id)->delete();
                }

                //add new permission module
                $permissionModules = $request->input('permission_module');
                foreach ($permissionModules as $permissionModule) {
                    $kosPermissionModule = new KosPermissionModule();
                    $kosPermissionModule->role_id = $loginId;
                    $kosPermissionModule->super_admin_id = Session::get('adminId');
                    $kosPermissionModule->table_name = $permissionModule;
                    $kosPermissionModule->save();

                }

                //delete previous permissions
                $previousPermissions = KosPermission::where('role_id',$request->id)->get();
                foreach ($previousPermissions as $permission ) {
                    KosPermission::find($permission->id)->delete();
                }

                //add new permissions
                $permissions = $request->input('permission');
                foreach ($permissions as $permission) {
                    $kosPermission = new KosPermission();
                    $kosPermission->role_id = $loginId;
                    $kosPermission->super_admin_id = Session::get('adminId');
                    $kosPermission->permission = $permission;
                    $kosPermission->save();
                }

                $activity = new KosActivityLog();
                $activity->description = 'Update user';
                $activity->user = Session::get('adminEmail');
                $activity->save();
                return redirect('/database/control/user-control-table')->with('message', 'Update user successfully');

            }
            //confirm password not match
            else {
                return redirect('/database/control/edit/'.$request->id)->with('alert', 'Retype wrong password!!!Try again.');

            }
        }
        //submit edit form without change password
        else {
            $dashboardLogin = DashboardLogin::find($request->id);
            $dashboardLogin->name = $request->name;
            $dashboardLogin->designation = $request->designation;
            $dashboardLogin->department = $request->dpt;
            $dashboardLogin->email = $request->email;
            $dashboardLogin->role = $request->role;
            $dashboardLogin->save();
            $loginId = $dashboardLogin->id;

            //delete previous university
            $previousUniversities = KosUniversityPermission::where('role_id',$request->id)->get();
            foreach ($previousUniversities as $university ) {
                KosUniversityPermission::find($university->id)->delete();
            }

            //add new universities
            $universityIds = $request->input('university_id');
            foreach ($universityIds as $universityId) {
                $kosUniversity = new KosUniversityPermission();
                $kosUniversity->role_id = $loginId;
                $kosUniversity->super_admin_id = Session::get('adminId');
                $kosUniversity->university_id = $universityId;
                $kosUniversity->save();
            }

            //delete previous permission module
            $previousModules = KosPermissionModule::where('role_id',$request->id)->get();
            foreach ($previousModules as $module ) {
                KosPermissionModule::find($module->id)->delete();
            }

            //add new permission module
            $permissionModules = $request->input('permission_module');
            foreach ($permissionModules as $permissionModule) {
                $kosPermissionModule = new KosPermissionModule();
                $kosPermissionModule->role_id = $loginId;
                $kosPermissionModule->super_admin_id = Session::get('adminId');
                $kosPermissionModule->table_name = $permissionModule;
                $kosPermissionModule->save();

            }

            //delete previous permissions
            $previousPermissions = KosPermission::where('role_id',$request->id)->get();
            foreach ($previousPermissions as $permission ) {
                KosPermission::find($permission->id)->delete();
            }

            //add new permissions
            $permissions = $request->input('permission');
            foreach ($permissions as $permission) {
                $kosPermission = new KosPermission();
                $kosPermission->role_id = $loginId;
                $kosPermission->super_admin_id = Session::get('adminId');
                $kosPermission->permission = $permission;
                $kosPermission->save();
            }

            $activity = new KosActivityLog();
            $activity->description = 'Update user';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/control/user-control-table')->with('message', 'Update user successfully');

        }
    }

//    public function deleteUser($id ) {
//
//        //delete user login info
//        DashboardLogin::find($id)->delete();
//
//        //delete  university
//        $previousUniversities = KosUniversityPermission::where('role_id',$id)->get();
//        foreach ($previousUniversities as $university ) {
//            KosUniversityPermission::find($university->id)->delete();
//        }
//
//        //delete permission module
//        $previousModules = KosPermissionModule::where('role_id',$id)->get();
//        foreach ($previousModules as $module ) {
//            KosPermissionModule::find($module->id)->delete();
//        }
//
//        //delete permissions
//        $previousPermissions = KosPermission::where('role_id',$id)->get();
//        foreach ($previousPermissions as $permission ) {
//            KosPermission::find($permission->id)->delete();
//        }
//
//        $activity = new KosActivityLog();
//        $activity->description = 'Delete user info';
//        $activity->user = Session::get('adminEmail');
//        $activity->save();
//        return redirect('/database/control/user-control-table')->with('message', 'Delete user all info successfully');
//    }
}
