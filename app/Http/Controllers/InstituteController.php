<?php

namespace App\Http\Controllers;

use App\KosActivityLog;
use App\KosInstitute;
use App\KosPermission;
use App\User;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Contracts\DataTable;
use Illuminate\Support\Facades\Schema;
use PDF;
use Datatables;

class InstituteController extends Controller
{
    public function wrongUrl() {
        return redirect('/database/institute/listing')->with('alert','Your action url is wrong !!');
    }

    public function instituteListing() {
        $institutes = DB::table('kos_institutes')->orderBy('id','desc')->paginate(10);
        return view('admin.institute.institute-table',[
            'institutes' => $institutes
        ]);
    }

    public function instituteForm () {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.institute.institute-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.institute.institute-form');
                }
            }
        }
        return redirect('/database/institute/listing');
    }

    public function instituteAdd (Request $request) {
        $institute = new KosInstitute();
        $institute->institute = $request->institute;
        $institute->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Institute';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/institute/listing')->with('message','Institute added successfully');
    }

    public function instituteEdit ($id ) {
        //return $id;
        $instituteById = KosInstitute::find($id);
        return view('admin.institute.institute-edit',[
            'instituteById' => $instituteById
        ]);
    }

    public function instituteUpdate(Request $request ) {
        $institute =  KosInstitute::find($request->id);
        $institute->institute = $request->institute;
        $institute->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Institute';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/institute/listing')->with('message','Institute update successfully');
    }

    public function instituteCopy ($id) {
        //return $id;
        if (Session::get('adminRole') == 'super-admin' ) {
            $institute    = KosInstitute::find($id);
            $newInstitute = new KosInstitute();
            $newInstitute->institute = $institute->institute;
            $newInstitute->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Institute';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/institute/listing')->with('message','Copy Institute successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'copy' ) {
                    $institute    = KosInstitute::find($id);
                    $newInstitute = new KosInstitute();
                    $newInstitute->institute = $institute->institute;
                    $newInstitute->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Institute';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/institute/listing')->with('message','Copy Institute successfully');
                }
            }
            return redirect('/database/institute/listing')->with('alert','You do not permit to access it !!!');
        }
    }

    public function instituteDelete($id ) {
        if (Session::get('adminRole') == 'super-admin' ) {
            KosInstitute::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Institute';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/institute/listing')->with('message','Delete Institute successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete' ) {
                    KosInstitute::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Institute';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/institute/listing')->with('message','Delete Institute successfully');
                }
            }
            return redirect('/database/institute/listing')->with('alert','You do not permit to access it !!!');
        }
    }

    public function institute(Request $request) {
//    return $request->all();
        if ($request->has('institute')) {
            switch ($request->input('submit')) {
                case 'copy':
                    $instituteId  = $request->input('institute');
                    //$centres = $request->all();
                    $count = 0;
                    foreach ($instituteId as $id ) {
                        $institute    = KosInstitute::find($id);
                        $newInstitute = new KosInstitute();
                        $newInstitute->institute = $institute->institute;
                        $newInstitute->save();
                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy '.$count.' Institute';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/institute/listing')->with('message','Copy '.$count.' Institute successfully');
                    break;

                case 'edit':
                    return 'edit';
                    break;

                case 'delete':
                    $deletes = $request->input('institute');
                    $count = 0;
                    foreach ($deletes as $delete ) {
                        KosInstitute::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete '.$count.' Institute';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/institute/listing')->with('message','Delete '.$count.' Institute successfully');
                    break;

                case 'report':
                    return 'report';
                    $centreById  = $request->input('institute');
                    return view('admin.institute.institute-report',[
                        'centreById' => $centreById
                    ]);
                    break;
            }
        } else {
            return redirect('/database/institute/listing')->with('alert','Please select item(s) check box from list');
        }

    }

}
