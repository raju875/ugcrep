<?php

namespace App\Http\Controllers;

use App\KosDistrict;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\KosActivityLog;
use App\KosPermission;
use App\KosPermissionModule;

class DistrictController extends Controller
{

    public function wrongUrl () {
        return redirect('/database/district/listing')->with('alert','Your action url is wrong !!!');
    }

    public function districtListing() {
        $districts = DB::table('kos_districts')->orderBy('id','desc')->get();
        return view('admin.district.district-table',[
            'districts' => $districts
        ]);
    }

    public function districtForm () {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.district.district-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.district.district-form');
                }
            }
        }
        return redirect('/database/district/listing');
    }

    public function districtAdd (Request $request) {
        $district = new KosDistrict();
        $district->name = $request->name;
        $district->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new District';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/district/listing')->with('message','District added successfully');
    }

    public function districtEdit ($id ) {
        //return $id;
        $districtById = KosDistrict::find($id);
        return view('admin.district.district-edit',[
            'districtById' => $districtById
        ]);
    }

    public function districtUpdate(Request $request ) {
        $district =  KosDistrict::find($request->id);
        $district->name = $request->name;
        $district->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update District';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/district/listing')->with('message','District update successfully');
    }

    public function districtCopy ($id) {
        //return $id;
        if (Session::get('adminRole')=='super-admin') {
            $district    = KosDistrict::find($id);
            $newDistrict = new KosDistrict();
            $newDistrict->name = $district->name;
            $newDistrict->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy District';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/district/listing')->with('message','Copy District successfully');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'copy') {
                    $district    = KosDistrict::find($id);
                    $newDistrict = new KosDistrict();
                    $newDistrict->name = $district->name;
                    $newDistrict->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy District';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/district/listing')->with('message','Copy District successfully');
                }
            }
        }
        return redirect('/database/district/listing');
    }

    public function districtDelete($id ) {
        if (Session::get('adminRole')=='super-admin')  {
            KosDistrict::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete District';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/district/listing')->with('message','Delete District successfully');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete') {
                    KosDistrict::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete District';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/district/listing')->with('message','Delete District successfully');
                }
            }
        }
        return redirect('/database/district/listing');
    }

    public function district(Request $request) {
        //return $request->all();
        if ($request->has('name')) {
            switch ($request->input('submit')) {
                case 'copy':
                    $districtId  = $request->input('name');
                    $count = 0;
                    foreach ($districtId as $id ) {
                        $district    = KosDistrict::find($id);
                        $newDistrict = new KosDistrict();
                        $newDistrict->name = $district->name;
                        $newDistrict->save();
                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy '.$count.' District';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/district/listing')->with('message','Copy '.$count.' District successfully');
                    break;

                case 'delete':
                    $deletes = $request->input('name');
                    $count = 0;
                    foreach ($deletes as $delete ) {
                        KosDistrict::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete '.$count.' District';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/district/listing')->with('message','Delete '.$count.' District successfully');
                    break;

            }
        } else {
            return redirect('/database/district/listing')->with('alert','Please select item(s) check box from list');
        }

    }
}
