<?php

namespace App\Http\Controllers;

use Datatables;


use App\KosActivityLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActivityLogController extends Controller
{


//    public function activityLogListing () {
//        $activities = DB::table('kos_activity_logs')->orderBy('id','desc')->get();
//       return view('admin.activity.activity-log',[
//           'activities' => $activities
//       ]);

 //   }

    public function activityLogDelete (Request $request) {
    if ($request->has('activity')) {
        switch ($request->input('submit')) {
            case 'delete':
                $activities  = $request->input('activity');
                //$centres = $request->all();
                $count = 0;
                foreach ($activities as $activity ) {
                   KosActivityLog::find($activity)->delete();
                    $count++;
                }
                return redirect('/database/activity-log/listing')->with('message','Delete '.$count.' Activity Log successfully');
                break;

            case 'edit':
                return 'edit';
                break;
        }
    } else {
        return redirect('/database/activity-log/listing')->with('alert','Please select item(s) check box from list');
    }
    }

    public function ajaxPagination(Request $request) {

        $activities = KosActivityLog::orderBy('id','desc')->paginate(100);
        if ($request->ajax()) {
            return view('admin.activity.live_pagination', compact('activities'));
        }
        return view('admin.activity.activity-log',compact('activities'));

    }

}
