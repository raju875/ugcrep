<?php

namespace App\Http\Controllers;

use App\KosProgram;
use Illuminate\Http\Request;
use App\KosActivityLog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\KosPermission;
use App\KosPermissionModule;

class ProgramController extends Controller
{

    public function wrongUrl () {
        return redirect('/database/program/listing')->with('alert','Your action url is wrong !!!');
    }

    public function programListing() {
        $programs = DB::table('kos_programs')->orderBy('id','desc')->get();
        return view('admin.program.program-table',[
            'programs' => $programs
        ]);
    }

    public function programForm() {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.program.program-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.program.program-form');
                }
            }
        }
        return redirect('/database/program/listing');
    }

    public function programAdd (Request $request) {
        $program = new KosProgram();
        $program->program = $request->program;
        $program->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Program';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/program/listing')->with('message','Program added successfully');
    }

    public function programEdit($id ) {
        $programById = KosProgram::find($id);
        return view('admin.program.program-edit',[
            'programById' => $programById
        ]);
    }

    public function programUpdate(Request $request ) {
        $program =  KosProgram::find($request->id);
        $program->program = $request->program;
        $program->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Program';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/program/listing')->with('message','Program update successfully');
    }

    public function programCopy ($id) {
        //return $id;
        if (Session::get('adminRole')=='super-admin') {
            $program    = KosProgram::find($id);
            $newProgram = new KosProgram();
            $newProgram->program = $program->program;
            $newProgram->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Program';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/program/listing')->with('message','Copy Program successfully');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'copy') {
                    $program    = KosProgram::find($id);
                    $newProgram = new KosProgram();
                    $newProgram->program = $program->program;
                    $newProgram->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Program';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/program/listing')->with('message','Copy Program successfully');
                }
            }
        }
        return redirect('/database/program/listing');

    }


    public function programDelete($id )
    {
        if (Session::get('adminRole') == 'super-admin') {
            KosProgram::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Program';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/program/listing')->with('message', 'Delete Program successfully');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission) {
                if ($permission->permission == 'delete') {
                    KosProgram::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Program';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/program/listing')->with('message', 'Delete Program successfully');
                }
            }
        }
        return redirect('/database/program/listing')->with('message','Delete Program successfully');
    }


    public function program(Request $request) {
        //return $request->all();
        if ($request->has('program')) {
            switch ($request->input('submit')) {
                case 'copy':
                    $programId  = $request->input('program');
                    //$centres = $request->all();
                    $count = 0;
                    foreach ($programId as $id ) {
                        $program    = KosProgram::find($id);
                        $newProgram = new KosProgram();
                        $newProgram->program = $program->program;
                        $newProgram->save();
                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy '.$count.' Program';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/program/listing')->with('message','Copy '.$count.' Program successfully');
                    break;

                case 'delete':
                    $deletes = $request->input('program');
                    $count = 0;
                    foreach ($deletes as $delete ) {
                        KosProgram::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete '.$count.' Program';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/program/listing')->with('message','Delete '.$count.' Program successfully');
                    break;
            }
        } else {
            return redirect('/database/program/listing')->with('alert','Please select item(s) check box from list');
        }

    }
}
