<?php

namespace App\Http\Controllers;

use App\KosActivityLog;
use App\KosFinancialYear;
use App\KosPermission;
use App\User;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Contracts\DataTable;
use Illuminate\Support\Facades\Schema;
use PDF;
use Datatables;

class FinancialYearController extends Controller
{
    public function wrongUrl() {
        return redirect('/database/financial-year/listing')->with('alert','Your action url is wrong !!');
    }

    public function financialYearListing() {
        $financialYears = DB::table('kos_financial_years')->orderBy('id','desc')->paginate(10);
        return view('admin.financial-year.financial-year-table',[
            'financialYears' => $financialYears
        ]);
    }

    public function financialYearForm () {
        if (Session::get('adminRole')=='super-admin') {
            return view('admin.financial-year.financial-year-form');
        } else {
            $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'add') {
                    return view('admin.financial-year.financial-year-form');
                }
            }
        }
        return redirect('/database/financial-year/listing');
    }

    public function financialYearAdd (Request $request) {
        $financialYear = new KosFinancialYear();
        $financialYear->financial_year = $request->financial_year;
        $financialYear->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Add new Financial Year';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/financial-year/listing')->with('message','Financial Year added successfully');
    }

    public function financialYearEdit ($id ) {
        //return $id;
        $financialYearById = KosFinancialYear::find($id);
        return view('admin.financial-year.financial-year-edit',[
            'financialYearById' => $financialYearById
        ]);
    }

    public function financialYearUpdate(Request $request ) {
        $financialYear =  KosFinancialYear::find($request->id);
        $financialYear->financial_year = $request->financial_year;
        $financialYear->save();

        $activityLog = new KosActivityLog();
        $activityLog->description = 'Update Financial Year';
        $activityLog->user        =  Session::get('adminEmail');
        $activityLog->save();

        return redirect('/database/financial-year/listing')->with('message','Centre update successfully');
    }

    public function financialYearCopy ($id) {
        //return $id;
        if (Session::get('adminRole') == 'super-admin' ) {
            $financialYear    = KosFinancialYear::find($id);
            $newFinancialYear = new KosFinancialYear();
            $newFinancialYear->financial_year = $financialYear->financial_year;
            $newFinancialYear->save();

            $activity = new KosActivityLog();
            $activity->description = 'Copy Financial Year';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/financial-year/listing')->with('message','Copy Financial Year successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'copy' ) {
                    $financialYear    = KosFinancialYear::find($id);
                    $newFinancialYear = new KosFinancialYear();
                    $newFinancialYear->financial_year = $financialYear->financial_year;
                    $newFinancialYear->save();

                    $activity = new KosActivityLog();
                    $activity->description = 'Copy Financial Year';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/financial-year/listing')->with('message','Copy Financial Year successfully');
                }
            }
            return redirect('/database/financial-year/listing')->with('alert','You do not permit to access it !!!');
        }
    }

    public function financialYearDelete($id ) {
        if (Session::get('adminRole') == 'super-admin' ) {
            KosFinancialYear::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete Financial Year';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/financial-year/listing')->with('message','Delete Financial Year successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id',$userId)->get();
            foreach ($permissions as $permission ) {
                if ($permission->permission == 'delete' ) {
                    KosFinancialYear::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete Financial Year';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/financial-year/listing')->with('message','Delete Financial Year successfully');
                }
            }
            return redirect('/database/financial-year/listing')->with('alert','You do not permit to access it !!!');
        }
    }

    public function financialYear(Request $request) {
//    return $request->all();
        if ($request->has('financial_year')) {
            switch ($request->input('submit')) {
                case 'copy':
                    $financialYearId  = $request->input('financial_year');
                    //$centres = $request->all();
                    $count = 0;
                    foreach ($financialYearId as $id ) {
                        $financialYear    = KosFinancialYear::find($id);
                        $newFinancialYear = new KosFinancialYear();
                        $newFinancialYear->financial_year = $financialYear->financial_year;
                        $newFinancialYear->save();
                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy '.$count.' Financial Year';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/financial-year/listing')->with('message','Copy '.$count.' Financial Year successfully');
                    break;

                case 'edit':
                    return 'edit';
                    break;

                case 'delete':
                    $deletes = $request->input('financial_year');
                    $count = 0;
                    foreach ($deletes as $delete ) {
                        KosFinancialYear::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete '.$count.' Financial Year';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/financial-year/listing')->with('message','Delete '.$count.' Financial Year successfully');
                    break;

                case 'report':
                    $centreById  = $request->input('financial_year');
                    return view('admin.financial-year.financial-year-report',[
                        'centreById' => $centreById
                    ]);
                    break;
            }
        } else {
            return redirect('/database/financial-year/listing')->with('alert','Please select item(s) check box from list');
        }
    }
}
