<?php

namespace App\Http\Controllers;

use App\KosUniversity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\KosActivityLog;
use Illuminate\Support\Facades\Session;
use App\KosPermission;

class UniversityController extends Controller
{

    public function wrongUrl() {
        return redirect('/database/university/listing')->with('alert','Your action url is wrong !!');
    }

   public function universityListing() {
       $universities = DB::table('kos_universities')->orderBy('id','desc')->get();
       return view('admin.university.university-table',[
           'universities' => $universities
       ]);
   }

   public function universityForm() {
       if (Session::get('adminRole')=='super-admin') {
           return view('admin.university.university-form');
       } else {
           $permissions = KosPermission::where('role_id', Session::get('adminId'))->get();
           foreach ($permissions as $permission ) {
               if ($permission->permission == 'add') {
                   return view('admin.university.university-form');
               }
           }
       }
       return redirect('/database/university/listing');
   }

   public function universityAdd(Request $request) {
       //return $request->all();
       $this->validate($request, [
           'university' => 'required'

       ]);

       $university = new  KosUniversity();
       $university->university     = $request->university;
       $university->address        = $request->address;
       $university->district       = $request->district;
       $university->zipcode        = $request->zipcode;
       $university->establish_date = $request->establish_date;
       $university->phone          = $request->phone;
       $university->fax            = $request->fax;
       $university->email          = $request->email;
       $university->status         = $request->status;
       $university->yesno          = $request->yesno;
       $university->save();

       $activityLog = new KosActivityLog();
       $activityLog->description = 'Add University';
       $activityLog->user        =  Session::get('adminEmail');
       $activityLog->save();

       return redirect('/database/university/listing')->with('message','University added successfully');
   }

   public function universityCopy($id ) {
       if (Session::get('adminRole') == 'super-admin' ) {
           $university    = KosUniversity::find($id);
           $newUniversity = new KosUniversity();
           $newUniversity->university     = $university->university;
           $newUniversity->address        = $university->address;
           $newUniversity->district       = $university->district;
           $newUniversity->zipcode        = $university->zipcode;
           $newUniversity->establish_date = $university->establish_date;
           $newUniversity->phone          = $university->phone;
           $newUniversity->fax            = $university->fax;
           $newUniversity->email          = $university->email;
           $newUniversity->status         = $university->status;
           $newUniversity->yesno          = $university->yesno;

           $newUniversity->save();

           $activity = new KosActivityLog();
           $activity->description = 'Copy University';
           $activity->user = Session::get('adminEmail');
           $activity->save();
           return redirect('/database/university/listing')->with('message','Copy University successfully');
       } else {
           $userId = Session::get('adminId');
           $permissions = KosPermission::where('role_id', $userId)->get();
           foreach ($permissions as $permission) {
               if ($permission->permisssion == 'copy') {
                   $university    = KosUniversity::find($id);
                   $newUniversity = new KosUniversity();
                   $newUniversity->university     = $university->university;
                   $newUniversity->address        = $university->address;
                   $newUniversity->district       = $university->district;
                   $newUniversity->zipcode        = $university->zipcode;
                   $newUniversity->establish_date = $university->establish_date;
                   $newUniversity->phone          = $university->phone;
                   $newUniversity->fax            = $university->fax;
                   $newUniversity->email          = $university->email;
                   $newUniversity->status         = $university->status;
                   $newUniversity->yesno          = $university->yesno;

                   $newUniversity->save();

                   $activity = new KosActivityLog();
                   $activity->description = 'Copy University';
                   $activity->user = Session::get('adminEmail');
                   $activity->save();
                   return redirect('/database/university/listing')->with('message','Copy University successfully');
               }
               else {
                   return redirect('/database/university/listing')->with('alert','You do not permit to access it !!!');

               }
           }
       }
   }

    public function universityDelete($id ) {
        if (Session::get('adminRole') == 'super-admin' ) {
            KosUniversity::find($id)->delete();
            $activity = new KosActivityLog();
            $activity->description = 'Delete University';
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/database/university/listing')->with('message','Delete University successfully');
        } else {
            $userId = Session::get('adminId');
            $permissions = KosPermission::where('role_id', $userId)->get();
            foreach ($permissions as $permission) {
                if ($permission->permission == 'delete') {
                    KosUniversity::find($id)->delete();
                    $activity = new KosActivityLog();
                    $activity->description = 'Delete University';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/university/listing')->with('message','Delete University successfully');
                }
                else {
                    return redirect('/database/university/listing')->with('alert','You do not permit to access it !!!');
                }
            }
        }

    }

   public function universityEdit($id ){
       $universityById = KosUniversity::find($id);
       return view('admin.university.university-edit',[
           'universityById' => $universityById
       ]);
   }

   public function universityUpdate(Request $request ){
       $universityById = KosUniversity::find($request->id);
       $newUniversity  = new KosUniversity();

       $this->validate($request, [
           'university' => 'required',
           'phone'      => 'required|size:11|regex:/(01)[0-9]{9}/',
           'address'    => 'required'
       ]);

       $newUniversity->university     = $universityById->university;
       $newUniversity->address        = $universityById->address;
       $newUniversity->district       = $universityById->district;
       $newUniversity->zipcode        = $universityById->zipcode;
       $newUniversity->establish_date = $universityById->establish_date;
       $newUniversity->phone          = $universityById->phone;
       $newUniversity->fax            = $universityById->fax;
       $newUniversity->email          = $universityById->email;
       $newUniversity->status         = $universityById->status;
       $newUniversity->yesno          = $universityById->yesno;
       $universityById->save();

       $activity = new KosActivityLog();
       $activity->description = 'Update University';
       $activity->user        = Session::get('adminEmail');
       $activity->save();
       return redirect('/database/university/listing')->with('message','Update University successfully');
   }

    public function university(Request $request) {
        //return $request->all();
        if ($request->has('university')) {
            switch ($request->input('submit')) {
                case 'copy':
                    $universityId  = $request->input('university');
                    //$centres = $request->all();
                    $count = 0;
                    foreach ($universityId as $id ) {
                        $university  = KosUniversity::find($id);
                        $newUniversity = new KosUniversity();
                        $newUniversity->university     = $university->university;
                        $newUniversity->address        = $university->address;
                        $newUniversity->district       = $university->district;
                        $newUniversity->zipcode        = $university->zipcode;
                        $newUniversity->establish_date = $university->establish_date;
                        $newUniversity->phone          = $university->phone;
                        $newUniversity->fax            = $university->fax;
                        $newUniversity->email          = $university->email;
                        $newUniversity->status         = $university->status;
                        $newUniversity->yesno          = $university->yesno;
                        $newUniversity->save();
                        $count++;
                    }
                    $activity = new KosActivityLog();
                    $activity->description = 'Copy '.$count.' University';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();
                    return redirect('/database/university/listing')->with('message','Copy '.$count.' University successfully');
                    break;

                case 'delete':
                    $deletes = $request->input('university');
                    $count = 0;
                    foreach ($deletes as $delete ) {
                        KosUniversity::find($delete)->delete();
                        $count++;
                    }

                    $activity = new KosActivityLog();
                    $activity->description = 'Delete '.$count.' University';
                    $activity->user = Session::get('adminEmail');
                    $activity->save();

                    return redirect('/database/university/listing')->with('message','Delete '.$count.' University successfully');
                    break;
            }
        } else {
            return redirect('/database/university/listing')->with('alert','Please select item(s) check box from list');
        }
   }
}
