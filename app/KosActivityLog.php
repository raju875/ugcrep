<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KosActivityLog extends Model
{
    public $table = "kos_activity_logs";

    public $fillable = ['description'];
}
