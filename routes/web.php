<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login','AdminLogin@login');
Route::post('/login_check','AdminLogin@loginSubmit');
Route::post('/database/admin-logout','AdminLogin@logoutAdmin')->middleware('AuthenticateMiddleware');
Route::get('/database/view-profile','AdminLogin@viewProfile')->middleware('AuthenticateMiddleware');
Route::post('/database/profile/update-profile','AdminLogin@passwordChange')->middleware('AuthenticateMiddleware');

Route::get('/database/dashboard','WelcomeController@dashboard')->middleware('AuthenticateMiddleware');


//User Control Start Here
Route::get('/database/control/user-control-form','UserControlController@controlForm')->middleware('UserControlMiddleware');
Route::post('/database/control/control-submit','UserControlController@submitControl')->middleware('UserControlMiddleware');
Route::get('/database/control/user-control-table','UserControlController@userControlTable')->middleware('UserControlMiddleware');
Route::get('/database/control/view/{id}','UserControlController@viewUser')->middleware('UserControlMiddleware');
Route::get('/database/control/edit/{id}','UserControlController@editUser')->middleware('UserControlMiddleware');
//Route::get('/database/control/delete/{id}','UserControlController@deleteUser')->middleware('UserControlMiddleware');
Route::post('/database/control/update','UserControlController@update')->middleware('UserControlMiddleware');
//User Control End Here


//Activity Log Start Here
Route::get('/database/activity-log/listing','ActivityLogController@ajaxPagination')->middleware('UserControlMiddleware');
Route::post('/database/activity-log/delete','ActivityLogController@activityLogDelete')->middleware('UserControlMiddleware');
Route::get('ajax-pagination',array('as'=>'pagination','uses'=>'ActivityLogController@ajaxPagination'))->middleware('UserControlMiddleware');
//Activity Log End Here


//University Start Here
Route::get('/database/university/listing','UniversityController@universityListing')->middleware('UserControlMiddleware');
Route::get('/database/university/form','UniversityController@universityForm')->middleware('UserControlMiddleware');
Route::post('/database/university/add','UniversityController@universityAdd')->middleware('UserControlMiddleware');
Route::get('/database/university/copy/{id}','UniversityController@universityCopy')->middleware('UserControlMiddleware');
Route::get('/database/university/edit/{id}','UniversityController@universityEdit')->middleware('UserControlMiddleware');
Route::get('/database/university/delete/{id}','UniversityController@universityDelete')->middleware('UserControlMiddleware');
Route::post('/database/university/update','UniversityController@universityUpdate')->middleware('UserControlMiddleware');
Route::post('/database/university/edit','UniversityController@university')->middleware('UserControlMiddleware');

Route::get('/database/university/add','UniversityController@wrongUrl')->middleware('UserControlMiddleware');
Route::get('/database/university/update','UniversityController@wrongUrl')->middleware('UserControlMiddleware');
Route::get('/database/university/edit','UniversityController@wrongUrl')->middleware('UserControlMiddleware');
//University End Here


//District Start Here
Route::get('/database/district/listing','DistrictController@districtListing')->middleware('UserControlMiddleware');
Route::get('/database/district/form','DistrictController@districtForm')->middleware('UserControlMiddleware');
Route::post('/database/district/add','DistrictController@districtAdd')->middleware('UserControlMiddleware');
Route::get('/database/district/edit/{id}','DistrictController@districtEdit')->middleware('UserControlMiddleware');
Route::post('/database/district/update','DistrictController@districtUpdate')->middleware('UserControlMiddleware');
Route::get('/database/district/copy/{id}','DistrictController@districtCopy')->middleware('UserControlMiddleware');
Route::get('/database/district/delete/{id}','DistrictController@districtDelete')->middleware('UserControlMiddleware');
Route::post('/database/district/edit','DistrictController@district')->middleware('UserControlMiddleware');

Route::get('/database/district/add','DistrictController@wrongUrl')->middleware('UserControlMiddleware');
Route::get('/database/district/update','DistrictController@wrongUrl')->middleware('UserControlMiddleware');
Route::get('/database/district/edit','DistrictController@wrongUrl')->middleware('UserControlMiddleware');
//District End Here


//Center Start Here
Route::get('/database/centre/listing','CentreController@centreListing')->middleware('CentreMiddleware');
Route::get('/database/centre/form','CentreController@centreForm')->middleware('CentreMiddleware');
Route::post('/database/centre/add','CentreController@centreAdd')->middleware('CentreMiddleware');
Route::get('/database/centre/edit/{id}','CentreController@centreEdit')->middleware('CentreMiddleware');
Route::get('/database/centre/copy/{id}','CentreController@centreCopy')->middleware('CentreMiddleware');
Route::get('/database/centre/delete/{id}','CentreController@centreDelete')->middleware('CentreMiddleware');
Route::get('/database/centre/listing/{item}','CentreController@centreDependency')->middleware('CentreMiddleware');
Route::post('/database/centre/update','CentreController@centreUpdate')->middleware('CentreMiddleware');
Route::post('/database/centre/edit','CentreController@centre')->middleware('CentreMiddleware');
Route::post('/database/centre/pdf','CentreController@centrePdf')->middleware('CentreMiddleware');

Route::get('/database/centre/add','CentreController@wrongUrl')->middleware('CentreMiddleware');
Route::get('/database/centre/update','CentreController@wrongUrl')->middleware('CentreMiddleware');
Route::get('/database/centre/edit','CentreController@wrongUrl')->middleware('CentreMiddleware');
//Center End Here


//Center Info Start Here
Route::get('/database/centre-info/listing','CentreInfoController@centreInfoListing')->middleware('CentreInfoMiddleware');
Route::get('/database/centre-info/form','CentreInfoController@centreInfoForm')->middleware('CentreInfoMiddleware');
Route::post('/database/centre-info/add','CentreInfoController@centreInfoAdd')->middleware('CentreInfoMiddleware');
Route::get('/database/centre-info/edit/{id}','CentreInfoController@centreInfoEdit')->middleware('CentreInfoMiddleware');
Route::post('/database/centre-info/update','CentreInfoController@centreInfoUpdate')->middleware('CentreInfoMiddleware');
Route::get('/database/centre-info/copy/{id}','CentreInfoController@centreInfoCopy')->middleware('CentreInfoMiddleware');
Route::get('/database/centre-info/delete/{id}','CentreInfoController@centreInfoDelete')->middleware('CentreInfoMiddleware');
Route::post('/database/centre-info/edit','CentreInfoController@centreInfo')->middleware('CentreInfoMiddleware');
Route::post('/database/centre-info/modal-listing','CentreInfoController@centreInfomodalPost')->middleware('CentreInfoMiddleware');
Route::get('/database/centre-info/modal-listing','CentreInfoController@centreInfomodalGet')->middleware('CentreInfoMiddleware');

Route::get('/database/centre-info/add','CentreController@wrongUrl')->middleware('CentreInfoMiddleware');
Route::get('/database/centre-info/update','CentreController@wrongUrl')->middleware('CentreInfoMiddleware');
Route::get('/database/centre-info/edit','CentreController@wrongUrl')->middleware('CentreInfoMiddleware');

Route::post('/database/centre-info/pdf','CentreInfoController@centreInfoPdf')->middleware('CentreInfoMiddleware');
Route::get('/database/centre/listing/{item}','CentreController@centreDependency')->middleware('CentreInfoMiddleware');
//Center Info End Here


//Departments Start Here
Route::get('/database/department/listing','DepartmentController@departmentListing')->middleware('DepartmentMiddleware');
Route::get('/database/department/form','DepartmentController@departmentForm')->middleware('DepartmentMiddleware');
Route::post('/database/department/add','DepartmentController@departmentAdd')->middleware('DepartmentMiddleware');
Route::get('/database/department/copy/{id}','DepartmentController@departmentCopy')->middleware('DepartmentMiddleware');
Route::get('/database/department/delete/{id}','DepartmentController@departmentDelete')->middleware('DepartmentMiddleware');
Route::get('/database/department/edit/{id}','DepartmentController@departmentEdit')->middleware('DepartmentMiddleware');
Route::post('/database/department/update','DepartmentController@departmentUpdate')->middleware('DepartmentMiddleware');
Route::post('/database/department/edit','DepartmentController@department')->middleware('DepartmentMiddleware');

Route::get('/database/department/add','DepartmentController@wrongUrl')->middleware('DepartmentMiddleware');
Route::get('/database/department/update','DepartmentController@wrongUrl')->middleware('DepartmentMiddleware');
Route::get('/database/department/edit','DepartmentController@wrongUrl')->middleware('DepartmentMiddleware');
//Departments End Here


//Faculty Start Here
Route::get('/database/faculty/listing','FacultyController@facultyListing')->middleware('FacultyMiddleware');
Route::get('/database/faculty/form','FacultyController@facultyForm')->middleware('FacultyMiddleware');
Route::post('/database/faculty/add','FacultyController@facultyAdd')->middleware('FacultyMiddleware');
Route::get('/database/faculty/copy/{id}','FacultyController@facultyCopy')->middleware('FacultyMiddleware');
Route::get('/database/faculty/delete/{id}','FacultyController@facultyDelete')->middleware('FacultyMiddleware');
Route::get('/database/faculty/edit/{id}','FacultyController@facultyEdit')->middleware('FacultyMiddleware');
Route::post('/database/faculty/update','FacultyController@facultyUpdate')->middleware('FacultyMiddleware');
Route::post('/database/faculty/edit','FacultyController@faculty')->middleware('FacultyMiddleware');

Route::get('/database/faculty/add','FacultyController@wrongUrl')->middleware('FacultyMiddleware');
Route::get('/database/faculty/update','FacultyController@wrongUrl')->middleware('FacultyMiddleware');
Route::get('/database/faculty/edit','FacultyController@wrongUrl')->middleware('FacultyMiddleware');
//Faculty End Here


//Faculty Info Start Here
Route::get('/database/faculty-info/listing','FacultyInfoController@facultyInfoListing')->middleware('FacultyInfoMiddleware');
Route::get('/database/faculty-info/form','FacultyInfoController@facultyInfoForm')->middleware('FacultyInfoMiddleware');
Route::post('/database/faculty-info/add','FacultyInfoController@facultyInfoAdd')->middleware('FacultyInfoMiddleware');
Route::get('/database/faculty-info/copy/{id}','FacultyInfoController@facultyInfoCopy')->middleware('FacultyInfoMiddleware');
Route::get('/database/faculty-info/delete/{id}','FacultyInfoController@facultyInfoDelete')->middleware('FacultyInfoMiddleware');
Route::get('/database/faculty-info/edit/{id}','FacultyInfoController@facultyInfoEdit')->middleware('FacultyInfoMiddleware');
Route::post('/database/faculty-info/update','FacultyInfoController@facultyInfoUpdate')->middleware('FacultyInfoMiddleware');
Route::post('/database/faculty-info/edit','FacultyInfoController@facultyInfo')->middleware('FacultyInfoMiddleware');
Route::post('/database/faculty-info/modal-listing','FacultyInfoController@facultyInfoModalPost')->middleware('FacultyInfoMiddleware');
Route::get('/database/faculty-info/modal-listing','FacultyInfoController@facultyInfoModalGet')->middleware('FacultyInfoMiddleware');

Route::get('/database/faculty-info/add','FacultyInfoController@wrongUrl')->middleware('FacultyInfoMiddleware');
Route::get('/database/faculty-info/update','FacultyInfoController@wrongUrl')->middleware('FacultyInfoMiddleware');
Route::get('/database/faculty-info/edit','FacultyInfoController@wrongUrl')->middleware('FacultyInfoMiddleware');
//Faculty Info End Here


//Financial Year Start Here
Route::get('/database/financial-year/listing','FinancialYearController@financialYearListing')->middleware('FinancialYearMiddleware');
Route::get('/database/financial-year/form','FinancialYearController@financialYearForm')->middleware('FinancialYearMiddleware');
Route::post('/database/financial-year/add','FinancialYearController@financialYearAdd')->middleware('FinancialYearMiddleware');
Route::get('/database/financial-year/copy/{id}','FinancialYearController@financialYearCopy')->middleware('FinancialYearMiddleware');
Route::get('/database/financial-year/delete/{id}','FinancialYearController@financialYearDelete')->middleware('FinancialYearMiddleware');
Route::get('/database/financial-year/edit/{id}','FinancialYearController@financialYearEdit')->middleware('FinancialYearMiddleware');
Route::post('/database/financial-year/update','FinancialYearController@financialYearUpdate')->middleware('FinancialYearMiddleware');
Route::post('/database/financial-year/edit','FinancialYearController@financialYear')->middleware('FinancialYearMiddleware');

Route::get('/database/institute/add','FinancialYearController@wrongUrl')->middleware('FinancialYearMiddleware');
Route::get('/database/institute/update','FinancialYearController@wrongUrl')->middleware('FinancialYearMiddleware');
Route::get('/database/institute/edit','FinancialYearController@wrongUrl')->middleware('FinancialYearMiddleware');
//Financial Year End Here


//Institute Start Here
Route::get('/database/institute/listing','InstituteController@instituteListing')->middleware('InstituteMiddleware');
Route::get('/database/institute/form','InstituteController@instituteForm')->middleware('InstituteMiddleware');
Route::post('/database/institute/add','InstituteController@instituteAdd')->middleware('InstituteMiddleware');
Route::get('/database/institute/copy/{id}','InstituteController@instituteCopy')->middleware('InstituteMiddleware');
Route::get('/database/institute/delete/{id}','InstituteController@instituteDelete')->middleware('InstituteMiddleware');
Route::get('/database/institute/edit/{id}','InstituteController@instituteEdit')->middleware('InstituteMiddleware');
Route::post('/database/institute/update','InstituteController@instituteUpdate')->middleware('InstituteMiddleware');
Route::post('/database/institute/edit','InstituteController@institute')->middleware('InstituteMiddleware');

Route::get('/database/institute/add','InstituteController@wrongUrl')->middleware('InstituteMiddleware');
Route::get('/database/institute/update','InstituteController@wrongUrl')->middleware('InstituteMiddleware');
Route::get('/database/institute/edit','InstituteController@wrongUrl')->middleware('InstituteMiddleware');
//Institute End Here


//Institute Info Start Here
Route::get('/database/institute-info/listing','InstituteInfoController@instituteInfoListing')->middleware('InstituteInfoMiddleware');
Route::get('/database/institute-info/form','InstituteInfoController@instituteInfoForm')->middleware('InstituteInfoMiddleware');
Route::post('/database/institute-info/add','InstituteInfoController@instituteInfoAdd')->middleware('InstituteInfoMiddleware');
Route::get('/database/institute-info/copy/{id}','InstituteInfoController@instituteInfoCopy')->middleware('InstituteInfoMiddleware');
Route::get('/database/institute-info/delete/{id}','InstituteInfoController@instituteInfoDelete')->middleware('InstituteInfoMiddleware');
Route::get('/database/institute-info/edit/{id}','InstituteInfoController@instituteInfoEdit')->middleware('InstituteInfoMiddleware');
Route::post('/database/institute-info/update','InstituteInfoController@instituteInfoUpdate')->middleware('InstituteInfoMiddleware');
Route::post('/database/institute-info/edit','InstituteInfoController@instituteInfo')->middleware('InstituteInfoMiddleware');
Route::post('/database/institute-info/modal-listing','InstituteInfoController@instituteInfoModalPost')->middleware('InstituteInfoMiddleware');
Route::get('/database/institute-info/modal-listing','InstituteInfoController@instituteInfoModalGet')->middleware('InstituteInfoMiddleware');

Route::get('/database/institute-info/add','InstituteInfoController@wrongUrl')->middleware('InstituteInfoMiddleware');
Route::get('/database/institute-info/update','InstituteInfoController@wrongUrl')->middleware('InstituteInfoMiddleware');
Route::get('/database/institute-info/edit','InstituteInfoController@wrongUrl')->middleware('InstituteInfoMiddleware');
//Institute Info End Here


//Manpower Info Start Here
Route::get('/database/manpower-info/listing','StaffInfoController@manpowerInfoListing')->middleware('StaffInfoMiddleware');
Route::get('/database/manpower-info/form','StaffInfoController@manpowerInfoForm')->middleware('StaffInfoMiddleware');
Route::post('/database/manpower-info/add','StaffInfoController@manpowerInfoAdd')->middleware('StaffInfoMiddleware');
Route::get('/database/manpower-info/edit/{id}','StaffInfoController@manpowerInfoEdit')->middleware('StaffInfoMiddleware');
Route::get('/database/manpower-info/copy/{id}','StaffInfoController@manpowerInfoCopy')->middleware('StaffInfoMiddleware');
Route::post('/database/manpower-info/update','StaffInfoController@manpowerInfoUpdate')->middleware('StaffInfoMiddleware');
Route::get('/database/manpower-info/delete/{id}','StaffInfoController@manpowerInfoDelete')->middleware('StaffInfoMiddleware');
Route::post('/database/manpower-info/edit','StaffInfoController@manpowerInfo')->middleware('StaffInfoMiddleware');

Route::post('/database/manpower-info/modal-listing','StaffInfoController@manpowerInfoModalPost')->middleware('StaffInfoMiddleware');
Route::get('/database/manpower-info/modal-listing','StaffInfoController@manpowerInfoModalGet')->middleware('StaffInfoMiddleware');

Route::get('/database/manpower-info/add','StaffInfoController@wrongUrl')->middleware('StaffInfoMiddleware');
Route::get('/database/manpower-info/update','StaffInfoController@wrongUrl')->middleware('StaffInfoMiddleware');
Route::get('/database/manpower-info/edit','StaffInfoController@wrongUrl')->middleware('StaffInfoMiddleware');
//Manpower Info End Here


//Office Start Here
Route::get('/database/office/listing','OfficeController@officeListing')->middleware('OfficeMiddleware');
Route::get('/database/office/form','OfficeController@officeForm')->middleware('OfficeMiddleware');
Route::post('/database/office/add','OfficeController@officeAdd')->middleware('OfficeMiddleware');
Route::get('/database/office/edit/{id}','OfficeController@officeEdit')->middleware('OfficeMiddleware');
Route::get('/database/office/copy/{id}','OfficeController@officeCopy')->middleware('OfficeMiddleware');
Route::post('/database/office/update','OfficeController@officeUpdate')->middleware('OfficeMiddleware');
Route::get('/database/office/delete/{id}','OfficeController@officeDelete')->middleware('OfficeMiddleware');
Route::post('/database/office/edit','OfficeController@office')->middleware('OfficeMiddleware');

Route::get('/database/office/add','OfficeController@wrongUrl')->middleware('OfficeMiddleware');
Route::get('/database/office/update','OfficeController@wrongUrl')->middleware('OfficeMiddleware');
Route::get('/database/office/edit','OfficeController@wrongUrl')->middleware('OfficeMiddleware');
//Office End Here


//Original Post Start Here
Route::get('/database/original-post/listing','OriginalPostController@originalPostListing')->middleware('OriginalPostMiddleware');
Route::get('/database/original-post/form','OriginalPostController@originalPostForm')->middleware('OriginalPostMiddleware');
Route::post('/database/original-post/add','OriginalPostController@originalPostAdd')->middleware('OriginalPostMiddleware');
Route::get('/database/original-post/edit/{id}','OriginalPostController@originalPostEdit')->middleware('OriginalPostMiddleware');
Route::get('/database/original-post/copy/{id}','OriginalPostController@originalPostCopy')->middleware('OriginalPostMiddleware');
Route::post('/database/original-post/update','OriginalPostController@originalPostUpdate')->middleware('OriginalPostMiddleware');
Route::get('/database/original-post/delete/{id}','OriginalPostController@originalPostDelete')->middleware('OriginalPostMiddleware');
Route::post('/database/original-post/edit','OriginalPostController@originalPost')->middleware('OriginalPostMiddleware');

Route::get('/database/original-pos/add','OriginalPostController@wrongUrl')->middleware('OriginalPostMiddleware');
Route::get('/database/original-pos/update','OriginalPostController@wrongUrl')->middleware('OriginalPostMiddleware');
Route::get('/database/original-pos/edit','OriginalPostController@wrongUrl')->middleware('OriginalPostMiddleware');
//Original Post End Here


//Program Start Here
Route::get('/database/program/listing','ProgramController@programListing')->middleware('ProgramMiddleware');
Route::get('/database/program/form','ProgramController@programForm')->middleware('ProgramMiddleware');
Route::post('/database/program/add','ProgramController@programAdd')->middleware('ProgramMiddleware');
Route::get('/database/program/edit/{id}','ProgramController@programEdit')->middleware('ProgramMiddleware');
Route::post('/database/program/update','ProgramController@programUpdate')->middleware('ProgramMiddleware');
Route::get('/database/program/copy/{id}','ProgramController@programCopy')->middleware('ProgramMiddleware');
Route::get('/database/program/delete/{id}','ProgramController@programDelete')->middleware('ProgramMiddleware');
Route::post('/database/program/edit','ProgramController@program')->middleware('ProgramMiddleware');

Route::get('/database/program/add','ProgramController@wrongUrl')->middleware('ProgramMiddleware');
Route::get('/database/program/update','ProgramController@wrongUrl')->middleware('ProgramMiddleware');
Route::get('/database/program/edit','ProgramController@wrongUrl')->middleware('ProgramMiddleware');
//Program End Here


//Session Start Here
Route::get('/database/session/listing','SessionController@sessionListing')->middleware('SessionMiddleware');
Route::get('/database/session/form','SessionController@sessionForm')->middleware('SessionMiddleware');
Route::post('/database/session/add','SessionController@sessionAdd')->middleware('SessionMiddleware');
Route::get('/database/session/edit/{id}','SessionController@sessionEdit')->middleware('SessionMiddleware');
Route::get('/database/session/copy/{id}','SessionController@sessionCopy')->middleware('SessionMiddleware');
Route::post('/database/session/update','SessionController@sessionUpdate')->middleware('SessionMiddleware');
Route::get('/database/session/delete/{id}','SessionController@sessionDelete')->middleware('SessionMiddleware');
Route::post('/database/session/edit','SessionController@session')->middleware('SessionMiddleware');

Route::get('/database/session/add','SessionController@wrongUrl')->middleware('SessionMiddleware');
Route::get('/database/session/update','SessionController@wrongUrl')->middleware('SessionMiddleware');
Route::get('/database/session/edit','SessionController@wrongUrl')->middleware('SessionMiddleware');
//Session End Here


//Vehicle Start Here
Route::get('/database/vehicle/listing','VehicleController@vehicleListing')->middleware('VehicleMiddleware');
Route::get('/database/vehicle/form','VehicleController@vehicleForm')->middleware('VehicleMiddleware');
Route::post('/database/vehicle/add','VehicleController@vehicleAdd')->middleware('VehicleMiddleware');
Route::get('/database/vehicle/edit/{id}','VehicleController@vehicleEdit')->middleware('VehicleMiddleware');
Route::get('/database/vehicle/copy/{id}','VehicleController@vehicleCopy')->middleware('VehicleMiddleware');
Route::post('/database/vehicle/update','VehicleController@vehicleUpdate')->middleware('VehicleMiddleware');
Route::get('/database/vehicle/delete/{id}','VehicleController@vehicleDelete')->middleware('VehicleMiddleware');
Route::post('/database/vehicle/edit','VehicleController@vehicle')->middleware('VehicleMiddleware');

Route::get('/database/vehicle/add','VehicleController@wrongUrl')->middleware('VehicleMiddleware');
Route::get('/database/vehicle/update','VehicleController@wrongUrl')->middleware('VehicleMiddleware');
Route::get('/database/vehicle/edit','VehicleController@wrongUrl')->middleware('VehicleMiddleware');
//Vehicle End Here


//Vehicle Info Start Here
Route::get('/database/vehicle-info/listing','VehicleInfoController@vehicleInfoListing')->middleware('VehicleInfoMiddleware');
Route::get('/database/vehicle-info/form','VehicleInfoController@vehicleInfoForm')->middleware('VehicleInfoMiddleware');
Route::post('/database/vehicle-info/add','VehicleInfoController@vehicleInfoAdd')->middleware('VehicleInfoMiddleware');
Route::get('/database/vehicle-info/edit/{id}','VehicleInfoController@vehicleInfoEdit')->middleware('VehicleInfoMiddleware');
Route::get('/database/vehicle-info/copy/{id}','VehicleInfoController@vehicleInfoCopy')->middleware('VehicleInfoMiddleware');
Route::post('/database/vehicle-info/update','VehicleInfoController@vehicleInfoUpdate')->middleware('VehicleInfoMiddleware');
Route::get('/database/vehicle-info/delete/{id}','VehicleInfoController@vehicleInfoDelete')->middleware('VehicleInfoMiddleware');
Route::post('/database/vehicle-info/edit','VehicleInfoController@vehicleInfo')->middleware('VehicleInfoMiddleware');
Route::post('/database/vehicle-info/modal-listing','VehicleInfoController@vehicleInfoModalPost')->middleware('VehicleInfoMiddleware');
Route::get('/database/vehicle-info/modal-listing','VehicleInfoController@vehicleInfoModalGet')->middleware('VehicleInfoMiddleware');

Route::get('/database/vehicle-info/add','VehicleInfoController@wrongUrl')->middleware('VehicleInfoMiddleware');
Route::get('/database/vehicle-info/update','VehicleInfoController@wrongUrl')->middleware('VehicleInfoMiddleware');
Route::get('/database/vehicle-info/edit','VehicleInfoController@wrongUrl')->middleware('VehicleInfoMiddleware');
//Vehicle Info End Here



