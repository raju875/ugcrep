<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKosUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kos_universities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('university');
            $table->string('address')->nullable();
            $table->integer('district')->nullable();
            $table->string('zipcode')->nullable();
            $table->date('establish_date')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('yesno')->default(1);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kos_universities');
    }
}
