<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKosInstituteInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kos_institute_infos', function (Blueprint $table) {
            $table->integer('university');
            $table->integer('institute')->nullable();
            $table->string('institute_date_of_approval')->nullable();
            $table->integer('department')->nullable();
            $table->string('department_date_of_approval')->nullable();
            $table->integer('programs')->nullable();
            $table->string('program_date_of_approval')->nullable();
            $table->integer('session')->nullable();
            $table->integer('no_of_seat')->nullable();
            $table->integer('enrolled_students')->nullable();
            $table->integer('male_students')->nullable();
            $table->integer('female_students')->nullable();
            $table->integer('current_students')->nullable();
            $table->string('year	')->nullable();
            $table->string('	date_of_approval')->nullable();
            $table->text('	remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kos_institute_infos');
    }
}
